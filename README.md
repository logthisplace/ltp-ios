# Log This Place

The application supports `phones` and `tablets`. 

**IOS**: starting from `IOS 12`

**Swift Language Version**: `Swift5`

**The project was developed with**: `Xcode 13.2.1`


## Setup
You need to use your own key files from the **Firebase project**. The project uses `DEV` and `PROD` scheme for each of the area.You need to use your environment settings for this, create two projects in the Firebase console. The key file has `*.plist` extension.

Steps:

- add these files(`*.plist`) to the "Resources" folder
- check filenames with keys in `AppDelegate`(Located in the roots of the project) file
- generate keys (firebase usually generates automatically) in **[console.cloud.google.com][LinkGC]** and specify these keys in the `Config`(Located in the roots of the project) file 


Google **[console.cloud.google.com][LinkGC]** should be activated on the API key:

 - Cloud Firestore API
 - Cloud Storage for Firebase API
 - Firebase Installations API
 - Firebase Remote Config API
 - Identity Toolkit API
 - Maps SDK for iOS
 - Places API
 - Token Service API

## Prerequisites

- [Bundler](https://bundler.io)

Steps:
- run `bundle install` - this installs all dependencies from Gemfile.
- run `bundle exec pod install` - this installs all Cocoapods dependencies using CP version defined in Gemfile.

## Fastlane

[Fastlane](https://docs.fastlane.tools) is being used as an automation tool, for partially taking the burden of certificates/provisioning profile creation or app deployment from developer.
All relevant files are inside `fastlane` folder. The most important one is `Fastfile` where we have defined lanes.
You can run lane by typing:

```
bundle exec fastlane LANE_TO_BE_EXECUTED
```

### Adding new device

Other thing worth mentioning is file with devices: `devices.txt`. To add new device it is enough to add its identifier and then run 

```
bundle exec fastlane register_devices_from_file
```

followed by corresponding match lane (see below). Without running match lane, device is added but not used by any provisioning profile.

### Match

There are two (for now) main match-related lanes: `match_development`, `match_deployment`.  Each one of them is responsible for managing certificates/provisioning profiles for related environment (app variant).

- `match_development` - used only by developer. It creates/regenerates/downloads development certificate or provisioning profile. By default it is in `readonly` mode which means no changes will be made. To disable it you can pass `readonly:false` parameter. Similarily for `force` parameter - it is `false` by default which means that no new devices will be added to provisioning profile. To override you can pass `force:true` parameter. 
- `match_deployment` - It creates/regenerates/downloads distribution ad-hoc provisioning profile and distribution certificate. Flags are the same as for `match_development`.

If any change was made by wbove lane - fastlane should be able to commit those changes to separate, dedicated certificates repository. 

**If there is any need of developer changing contens of certificate repository (for example delete expired certificate) - such commit should be prepended by `[manual]` flag**

### Deployment

Testflight deployment requires us to have App Store Connect API key. It was generated and is needed to be present in `fastlane` folder under correct name (see `Fastfile` for reference).
Lane responsible for deployment is `deploy_test`. It check for the latest build number on testflight for given version number and increments it locally before archiving and uploading.

## Utils

### SwiftGen

[SwiftGen](https://github.com/SwiftGen/SwiftGen) is a tool that automatically generates swift code for project resources.

You can install SwiftGen by running:

```
brew install swiftgen
```

Generation is done automatically as part of `Run SwiftGen` run script, but you can run it manually by running `swiftgen` in terminal.


## Libraries
The project extended with the following libraries.
Instructions on how to use them in application are linked below.

| Library | Version | Link |
| ------ | ------ | ------ |
| Firebase/Firestore | 8.5.0 | [README.md][LbFir] |
| FirebaseFirestoreSwift | 8.5.0-beta| [README.md][LbFir] |
| GoogleMaps | 5.1.0 | [Documentation][LbGM] |
| SnapKit | 5.0.1 |[README.md][LbSn] |
| Firebase/Storage | 8.5.0 | [README.md][LbFir] |
| GooglePlaces | 5.0.0 | [Documentation][LbGP] |
| Swinject | 2.7.1 |[README.md][LbSwInj] |
| Firebase/Auth | 8.5.0 |[README.md][LbFir] |
| Google-Maps-iOS-Utils | 3.10.3 | [README.md][LbGMIO] |
| Firebase/Analytics | 8.5.0 | [Documentation][LbFAn] |
| Firebase/Crashlytics | 8.5.0 | [README.md][LbFir] |
| Kingfisher | 6.3.1 | [README.md][LbKF] |
| Swift_PageMenu | 1.4 | [README.md][LbSwPM] |
| DKPhotoGallery | 0.0.17 | [README.md][LbGal] |
| Firebase/RemoteConfig | 8.5.0 | [README.md][LbFir] |

[LbFir]:<https://github.com/firebase/firebase-ios-sdk/blob/master/README.md>
[LbGM]:<https://developers.google.com/maps/documentation/ios-sdk/overview>
[LbSn]:<https://github.com/SnapKit/SnapKit/blob/develop/README.md>
[LbGP]:<https://developers.google.com/maps/documentation/places/ios-sdk/overview>
[LbSwInj]:<https://github.com/Swinject/Swinject/blob/master/README.md>
[LbGMIO]:<https://github.com/googlemaps/google-maps-ios-utils/blob/main/README.md>
[LbFAn]:<https://firebase.google.com/products/analytics>
[LbKF]:<https://github.com/onevcat/Kingfisher/blob/master/README.md>
[LbSwPM]:<https://github.com/tamanyan/SwiftPageMenu/blob/master/README.md>
[LbGal]:<https://github.com/zhangao0086/DKPhotoGallery/blob/develop/README.md>
[LinkGC]:<https://console.cloud.google.com/>
