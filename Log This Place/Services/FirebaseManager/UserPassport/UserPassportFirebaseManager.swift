//
//  UserPassportFirebaseManager.swift
//  Log This Place
//

import FirebaseFirestore
import FirebaseFirestoreSwift


protocol UserPassportFirebaseManagerProtocol {
    func setAnswers(userPassport: UserPassport,
                    completionHandler: @escaping (Result<String, Error>) -> Void)
    func getUserPassport(completionHandler: @escaping (Result<UserPassport, Error>) -> Void)
}

final class UserPassportFirebaseManager: UserPassportFirebaseManagerProtocol {
    private let db: FirestoreProtocol = AppContainer.resolve()
    private let userManager: UserManagerProtocol = AppContainer.resolve()
    
    // MARK: - Update user passport
    func setAnswers(userPassport: UserPassport,
                    completionHandler: @escaping (Result<String, Error>) -> Void) {
        
        let deviceUUID: String = userManager.deviceUUID
        var firestoreDatabaseFieldKey: String = FirestoreDatabase.ConstantsUser.FIELD_DEVICE_UUID
        var firestoreDatabaseValue: String = deviceUUID
        
        if let userId = userManager.currentUser?.uid {
            firestoreDatabaseFieldKey = FirestoreDatabase.ConstantsUser.FIELD_USER_ID
            firestoreDatabaseValue = userId
        }
        
        let collection = db.collection(FirestoreDatabase.ConstantsUser.COLLECTION_NAME)
            .whereField(firestoreDatabaseFieldKey, isEqualTo: firestoreDatabaseValue)
            .limit(to: 1)
        
        collection.getDocuments(completion: {[weak self] (querySnapshot, error) in
            if let queryDocumentSnapshot = querySnapshot?.documents.first {
                self?.updateUserPassport(queryDocumentSnapshot: queryDocumentSnapshot,
                                         userPassport: userPassport,
                                         completionHandler: {(result) in
                                            completionHandler(result)
                                         })
            } else {
                self?.createUserPassport(userPassport: userPassport,
                                         completionHandler: {(result) in
                                            completionHandler(result)
                                         })
            }
        })
    }
    
    
    private func updateUserPassport(queryDocumentSnapshot: QueryDocumentSnapshot,
                                    userPassport: UserPassport,
                                    completionHandler: @escaping (Result<String, Error>) -> Void) {
        queryDocumentSnapshot
            .reference
            .updateData(userPassport.dataToUpdateDict(),
                     completion: {(error) in
                        if let error = error {
                            Logger.shared.error(String(describing: error))
                            completionHandler(.failure(error))
                            return
                        }
                        Logger.shared.info("Successfully updated")
                        completionHandler(.success(FirestoreSuccess.updatedSuccess.rawValue))
                     })
    }
    
    private func createUserPassport(userPassport: UserPassport,
                                    completionHandler: @escaping (Result<String, Error>) -> Void) {

        db.collection(FirestoreDatabase.ConstantsUser.COLLECTION_NAME)
            .document()
            .setData(userPassport.dataToCreateDict(),
                     /// replace all
                     merge: false,
                     completion: {(error) in
                        if let error = error {
                            Logger.shared.error(String(describing: error))
                            completionHandler(.failure(error))
                            return
                        }
                        Logger.shared.info("Successfully created")
                        completionHandler(.success(FirestoreSuccess.updatedSuccess.rawValue))
                     })
    }
    
    // MARK: - Get user passport
    func getUserPassport(completionHandler: @escaping (Result<UserPassport, Error>) -> Void) {
        let deviceUUID: String = userManager.deviceUUID
        var firestoreDatabaseFieldKey: String = FirestoreDatabase.ConstantsUser.FIELD_DEVICE_UUID
        var firestoreDatabaseValue: String = deviceUUID
        
        if let userId = userManager.currentUser?.uid {
            firestoreDatabaseFieldKey = FirestoreDatabase.ConstantsUser.FIELD_USER_ID
            firestoreDatabaseValue = userId
        }
        
        db.collection(FirestoreDatabase.ConstantsUser.COLLECTION_NAME)
            .whereField(firestoreDatabaseFieldKey, isEqualTo: firestoreDatabaseValue)
            .limit(to: 1)
            .getDocuments(completion: {(querySnapshot, error) in
                
                if let error = error {
                    Logger.shared.error(String(describing: error))
                    completionHandler(.failure(FirestoreError.documentDoesntExist))
                    return
                }
                
                Logger.shared.info("Successfully recived")
                if let queryDocumentSnapshot = querySnapshot?.documents.first {
                    queryDocumentSnapshot
                        .reference
                        .getDocument(completion: {(queryDocumentSnapshot, firestoreError) in
                            responseHandler(from: queryDocumentSnapshot, completionHandler: {(result) in
                                completionHandler(result)
                            })
                        })
                } else {
                    completionHandler(.failure(FirestoreError.documentDoesntExist))
                }
            })
    }
}
