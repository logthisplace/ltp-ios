//
//  UserPassport.swift
//  Log This Place
//
//

import FirebaseFirestoreSwift
import Firebase
import FirebaseAuth


struct UserPassport: Codable {
    @DocumentID var id: String? = nil
    let answers: [String: Any]?
    let device_uuid: String?
    let user_id: String?
    @ServerTimestamp var created_at: Timestamp? = nil
    @ServerTimestamp var updated_at: Timestamp? = nil
    
    enum CodingKeys: String, CodingKey {
        case id
        case answers
        case created_at
        case updated_at
        case device_uuid
        case user_id
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        _id = try container.decode(DocumentID<String>.self, forKey: .id)
        self.created_at = try container.decodeIfPresent(Timestamp.self, forKey: .created_at)
        self.updated_at = try container.decodeIfPresent(Timestamp.self, forKey: .updated_at)
        self.device_uuid = try container.decodeIfPresent(String.self, forKey: .device_uuid)
        self.answers = try container.decodeIfPresent([String: Any].self, forKey: .answers)
        self.user_id = try container.decodeIfPresent(String.self, forKey: .user_id)
    }
    
    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encodeIfPresent(self.created_at, forKey: .created_at)
        try container.encodeIfPresent(self.updated_at, forKey: .updated_at)
        try container.encodeIfPresent(self.device_uuid, forKey: .device_uuid)
        try container.encodeIfPresent(self.answers, forKey: .answers)
        try container.encodeIfPresent(self.user_id, forKey: .user_id)
    }
    
    init(answers: [String: Any]) {
        self.answers = answers
        self.device_uuid = (AppContainer.resolve() as UserManagerProtocol).deviceUUID
        self.user_id = (AppContainer.resolve() as UserManagerProtocol).currentUser?.uid
    }
    
    /// for create a new user answerSheet
    func dataToCreateDict() -> [String: Any] {
        var user = [String: Any]()
        user[FirestoreDatabase.ConstantsUser.ANSWERS] = answers
        user[FirestoreDatabase.ConstantsUser.FIELD_USER_ID] = user_id
        user[FirestoreDatabase.ConstantsUser.FIELD_DEVICE_UUID] = device_uuid
        user[FirestoreDatabase.ConstantsUser.CREATED_AT] = created_at ?? FieldValue.serverTimestamp()
        user[FirestoreDatabase.ConstantsUser.UPDATED_AT] = FieldValue.serverTimestamp()
        return user
    }
    
    /// for update user answerSheet
    func dataToUpdateDict() -> [String: Any] {
        var user = [String: Any]()
        user[FirestoreDatabase.ConstantsUser.ANSWERS] = answers
        user[FirestoreDatabase.ConstantsUser.UPDATED_AT] = FieldValue.serverTimestamp()
        return user
    }
    
}
