//
//  RemoteConfigFirebaseManager.swift
//  Log This Place
//
//

import FirebaseRemoteConfig

protocol RemoteConfigFirebaseManagerProtocol {
    func fetchAndActiveAppSettings(completionHandler: @escaping (Result<AppUpdateConfig, Error>) -> Void)
}

/// class to receive Firebase Remote Config
final class RemoteConfigFirebaseManager: RemoteConfigFirebaseManagerProtocol {
    
    private struct Constants {
        static let appSettingsKey = "app_config"
    }
    
    var remoteConfig: RemoteConfigProtocol = AppContainer.resolve()
    
    func fetchAndActiveAppSettings(completionHandler: @escaping (Result<AppUpdateConfig, Error>) -> Void) {
        remoteConfig.fetch { [weak self] (status, error) -> Void in
            guard let self = self else { return }
            if status == .success {
                self.remoteConfig.activate { changed, error in
                    if error != nil {
                        completionHandler(.failure(FirestoreError.getDataError))
                    }
                    
                    let configValue = self.remoteConfig.configValue(forKey: Constants.appSettingsKey)
                    let jsonValue = configValue.dataValue
                    
                    /// save data to defaults
                    self.remoteConfig.setDefaults([Constants.appSettingsKey: jsonValue as NSObject])
                    
                    if let appConfigData = self.decodeAppConfig(jsonValue: jsonValue) {
                        completionHandler(.success(appConfigData))
                    } else {
                        completionHandler(.failure(FirestoreError.mappingError))
                    }
                }
            } else {
                guard let error = error else {
                    completionHandler(.failure(FirestoreError.getDataError))
                    return
                }
                
                /// check internet connection
                if (error as NSError).code == RemoteConfigError.internalError.rawValue {
                    /// get saved data
                    guard let jsonValue = self.remoteConfig.defaultValue(forKey: Constants.appSettingsKey)?.dataValue else {
                        completionHandler(.failure(FirestoreError.getDataError))
                        return
                    }
                    
                    if let appConfigData = self.decodeAppConfig(jsonValue: jsonValue) {
                        completionHandler(.success(appConfigData))
                    } else {
                        completionHandler(.failure(FirestoreError.mappingError))
                    }
                    
                } else {
                    completionHandler(.failure(FirestoreError.getDataError))
                }
            }
        }
    }
    
    private func decodeAppConfig(jsonValue: Data) -> AppUpdateConfig? {
        do {
            let decoder = JSONDecoder()
            let data = try decoder.decode(AppUpdateConfig.self, from: jsonValue)
            return data
        } catch let error {
            print(error)
            return nil
        }
    }
}


struct AppUpdateConfig: Codable {
    let minimum_version_required: MinimumVersionRequired?
    let title_labels: Label?
    let description_labels: Label?
    
    struct MinimumVersionRequired: Codable {
        let ios: String?
    }
}
