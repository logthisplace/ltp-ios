//
//  Log This Place
//
//

import Foundation
import FirebaseFirestoreSwift
import Firebase

// TODO: - Incorporate this into PointOfInterestSheet and remove unknown case
enum AnswerSheetType {
    case bool(Bool)
    case int(Int)
    case string(String)
    case array([String])
    case unknown
}

struct AnswerSheet: Codable {
    @DocumentID var id: String?
    var answers: [String: Any]?
    let device_uuid: String?
    let user_id: String?
    @ServerTimestamp var created_at: Timestamp?
    @ServerTimestamp var updated_at: Timestamp?
    
    enum CodingKeys: String, CodingKey {
        case id
        case answers
        case created_at
        case updated_at
        case device_uuid
        case user_id
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        _id = try container.decode(DocumentID<String>.self, forKey: .id)
        self.created_at = try container.decodeIfPresent(Timestamp.self, forKey: .created_at)
        self.updated_at = try container.decodeIfPresent(Timestamp.self, forKey: .updated_at)
        self.answers = try container.decodeIfPresent([String: Any].self, forKey: .answers)
        self.device_uuid = try container.decodeIfPresent(String.self, forKey: .device_uuid)
        self.user_id = try container.decodeIfPresent(String.self, forKey: .user_id)
    }
    
    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encodeIfPresent(self.created_at, forKey: .created_at)
        try container.encodeIfPresent(self.updated_at, forKey: .updated_at)
        try container.encodeIfPresent(self.answers, forKey: .answers)
        try container.encodeIfPresent(self.device_uuid, forKey: .device_uuid)
        try container.encodeIfPresent(self.user_id, forKey: .user_id)
    }
    
    init(answers: [String: Any]) {
        self.answers = answers
        self.device_uuid = (AppContainer.resolve() as UserManagerProtocol).deviceUUID
        self.user_id = (AppContainer.resolve() as UserManagerProtocol).currentUser?.uid
    }
    
    // TODO: The same method in `POISynth`
    func answers(for id: String) -> AnswerSheetType {
        let answer = answers?[id]
        
        if let boolAnswer = answer as? Bool {
            return .bool(boolAnswer)
        }
        if let intAnswer = answer as? Int {
            return .int(intAnswer)
        }
        if let stringAnswer = answer as? String {
            return .string(stringAnswer)
        }
        if let arrayAnswer = answer as? [String] {
            return .array(arrayAnswer)
        }
        
        return .unknown
    }
    
    // TODO: Improve it
    func dataToDict() -> [String: Any] {
        var answerSheet = [String: Any]()
        answerSheet[FirestoreDatabase.ConstantsPointOfInterest.ANSWERS] = answers
        answerSheet[FirestoreDatabase.ConstantsPointOfInterest.FIELD_USER_ID] = user_id
        answerSheet[FirestoreDatabase.ConstantsPointOfInterest.FIELD_DEVICE_UUID] = device_uuid
        answerSheet[FirestoreDatabase.ConstantsPointOfInterest.CREATED_AT] = created_at ?? FieldValue.serverTimestamp()
        answerSheet[FirestoreDatabase.ConstantsPointOfInterest.UPDATED_AT] = FieldValue.serverTimestamp()
        return answerSheet
    }
}
