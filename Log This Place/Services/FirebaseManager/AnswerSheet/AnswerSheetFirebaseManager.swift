//
//  AnswerSheetFirebaseManager.swift
//  Log This Place
//
//

import FirebaseFirestore


protocol AnswerSheetFirebaseManagerProtocol {
    func getAnswerSheetUserByDocumentId(poiId: String,
                                        completionHandler: @escaping  (Result<AnswerSheet?, Error>) -> Void)
    
    func updateAnswerSheet(poiId: String,
                           answerSheet: AnswerSheet,
                           completionHandler: @escaping  (Result<AnswerSheet?, Error>) -> Void)
    
    
    func createAnswerSheet(poiId: String,
                           answerSheet: AnswerSheet,
                           completionHandler: @escaping  (Result<AnswerSheet?, Error>) -> Void)
    
    func answerSheetReference(poiId: String) -> DocumentReference
    
    func setDataToAnswerSheetReference(answerSheetReference: DocumentReference,
                                       answerSheet: AnswerSheet,
                                       completionHandler: @escaping  (Result<AnswerSheet?, Error>) -> Void)
}

final class AnswerSheetFirebaseManager: AnswerSheetFirebaseManagerProtocol {
    
    private let db: FirestoreProtocol = AppContainer.resolve()
    private let userManager: UserManagerProtocol = AppContainer.resolve()
    
    /// create reference to answerSheet, need for know full path for upload images
    func answerSheetReference(poiId: String) -> DocumentReference {
        return db.collection(FirestoreDatabase.ConstantsPointOfInterest.COLLECTION_NAME)
            .document(poiId)
            .collection(FirestoreDatabase.ConstantsPointOfInterest.COLLECTION_SHEET_NAME)
            .document()
    }
    
    func setDataToAnswerSheetReference(answerSheetReference: DocumentReference,
                                       answerSheet: AnswerSheet,
                                       completionHandler: @escaping  (Result<AnswerSheet?, Error>) -> Void) {
        answerSheetReference.setData(answerSheet.dataToDict(), completion: { (error) in
            if let error = error {
                Logger.shared.error(String(describing: error))
                completionHandler(.failure(error))
                return
            }
            
            Logger.shared.info("answerSheetReference \(answerSheetReference.documentID) successfully created")
            completionHandler(.success(answerSheet))
        })
    }
    
    
    func createAnswerSheet(poiId: String,
                           answerSheet: AnswerSheet,
                           completionHandler: @escaping  (Result<AnswerSheet?, Error>) -> Void) {
        db.collection(FirestoreDatabase.ConstantsPointOfInterest.COLLECTION_NAME)
            .document(poiId)
            .collection(FirestoreDatabase.ConstantsPointOfInterest.COLLECTION_SHEET_NAME)
            .addDocument(data: answerSheet.dataToDict(), completion: {(error) in
                if let error = error {
                    Logger.shared.error(String(describing: error))
                    completionHandler(.failure(error))
                }
            })
            .getDocument(completion: {(documentSnapshot, error) in
                if let error = error {
                    Logger.shared.error(String(describing: error))
                    completionHandler(.failure(error))
                    return
                }
                
                Logger.shared.info("poi: \(poiId) answerSheet successfully created")
                responseHandler(from: documentSnapshot, completionHandler: {(result) in
                    completionHandler(result)
                })
            })
    }
    
    func updateAnswerSheet(poiId: String,
                           answerSheet: AnswerSheet,
                           completionHandler: @escaping  (Result<AnswerSheet?, Error>) -> Void) {
        guard let answerSheetId = answerSheet.id else {
            Logger.shared.error("poi: \(poiId) answerSheet ID is empty")
            completionHandler(.failure(FirestoreError.answerSheetDoesntExist))
            return
        }
        
        db.collection(FirestoreDatabase.ConstantsPointOfInterest.COLLECTION_NAME)
            .document(poiId)
            .collection(FirestoreDatabase.ConstantsPointOfInterest.COLLECTION_SHEET_NAME)
            .document(answerSheetId)
            .getDocument(completion: {(documentSnapshot, error) in
                if let error = error {
                    Logger.shared.error(String(describing: error))
                    completionHandler(.failure(error))
                    return
                }
                
                documentSnapshot?.reference.setData(answerSheet.dataToDict(),
                                                    /// full update answer sheet
                                                    merge: false,
                                                    completion: {(error) in
                    if let error = error {
                        Logger.shared.error(String(describing: error))
                        completionHandler(.failure(error))
                        return
                    }
                    Logger.shared.info("poi: \(poiId) answerSheet: \(answerSheetId) successfully updated")
                    responseHandler(from: documentSnapshot, completionHandler: {(result) in
                        completionHandler(result)
                    })
                })
            })
    }
    /// Return answer sheet for user by userUUID
    func getAnswerSheetUserByDocumentId(poiId: String,
                                        completionHandler: @escaping  (Result<AnswerSheet?, Error>) -> Void) {
        /// get answers by user id
        getAnswerSheetByUserId(poiId) { result in
            switch result {
            case let .success(document):
                completionHandler(.success(document))
            case .failure:
                completionHandler(.failure(FirestoreError.documentDoesntExist))
            }
        }
    }
    
    private func getAnswerSheetByUserId(_ poiId: String,
                                        completionHandler: @escaping (Result<AnswerSheet?, Error>) -> Void) {
        if let userId = userManager.currentUser?.uid {
            db.collection(FirestoreDatabase.ConstantsPointOfInterest.COLLECTION_NAME)
                .document(poiId)
                .collection(FirestoreDatabase.ConstantsPointOfInterest.COLLECTION_SHEET_NAME)
                .whereField(FirestoreDatabase.ConstantsPointOfInterest.FIELD_USER_ID, isEqualTo: userId)
                .order(by: FirestoreDatabase.ConstantsFirebaseQuery.CREATED_AT, descending: true)
                .limit(to: 1)
                .getDocuments(completion: {(querySnapshot, error) in
                    if error == nil {
                        if let querySnapshot = querySnapshot {
                            if querySnapshot.documents.isEmpty {
                                completionHandler(.failure(FirestoreError.documentDoesntExist))
                            } else {
                                Logger.shared.info("answerSheet by UserID: \(userId) successfully received")
                                responseHandler(from: querySnapshot.documents.first, completionHandler: {(result) in
                                    completionHandler(result)
                                })
                            }
                        } else {
                            completionHandler(.failure(FirestoreError.documentDoesntExist))
                        }
                    } else {
                        completionHandler(.failure(error ?? FirestoreError.documentDoesntExist))
                    }
                })
        } else {
            completionHandler(.failure(FirestoreError.userIdDoesntExist))
        }
    }
    
}
