//
//  PinColorLogic.swift
//  Log This Place
//
//

import Foundation


struct PinColorLogic: Codable {
    let created_at: Date?
    let data: Condition?
    let type: String?
}
