//
//  PinColorLogicFirebaseManagerProtocol.swift
//  Log This Place
//
//

import Foundation


protocol PinColorLogicFirebaseManagerProtocol {
    func getPinColorLogic(completionHandler: @escaping (Result<PinColorLogic?, Error>) -> Void)
}

final class PinColorLogicFirebaseManager: PinColorLogicFirebaseManagerProtocol {
    
    private let db: FirestoreProtocol = AppContainer.resolve()
    
    func getPinColorLogic(completionHandler: @escaping (Result<PinColorLogic?, Error>) -> Void) {
        db.collection(FirestoreDatabase.ConstantsPinColor.COLLECTION_NAME)
            .order(by: FirestoreDatabase.ConstantsFirebaseQuery.CREATED_AT, descending: true)
            .limit(to: 1)
            .getDocuments(completion: {(querySnapshot, error) in
                if let error = error {
                    Logger.shared.error(String(describing: error))
                    completionHandler(.failure(error))
                    return
                }
                
                if let queryDocumentSnapshot = querySnapshot?.documents.first {
                    queryDocumentSnapshot
                        .reference
                        .getDocument(completion: {(queryDocumentSnapshot, firestoreError) in
                            if let error = firestoreError {
                                Logger.shared.error(String(describing: error))
                                completionHandler(.failure(error))
                                return
                            }
                            
                            Logger.shared.info("Successfully received")
                            responseHandler(from: queryDocumentSnapshot, completionHandler: {(result) in
                                completionHandler(result)
                            })
                        })
                } else {
                    completionHandler(.success(nil))
                }
            })
    }
    
}
