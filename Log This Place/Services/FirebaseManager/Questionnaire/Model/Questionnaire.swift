//
//  Questionnaire.swift
//  Log This Place
//
//

import Foundation
import FirebaseFirestoreSwift


enum QuestionnaireGroupUUID: String {
    case general = "9a669e27-8aa4-4afe-8e5a-336347a3a963"
    case payment = "13b9cd8b-a76a-420f-9ddd-eb1bb6110a6c"
    case experience = "7a1c1fce-3530-47de-b0f3-0a6ce9842aab"
    case inside = "f3a7464b-212f-4d96-9b95-916ee4f0de42"
    case toilet = "d58a3ea5-5266-4016-9b22-0abbf456f907"
    case entrance = "db7525aa-dfc3-4345-a88c-6fd6890e58ac"
    
    var thumbnailAsset: ImageAsset {
        switch self {
        case .entrance:
            return Asset.BuildingDetail.entrance
        case .inside:
            return Asset.BuildingDetail.inside
        case .toilet:
            return Asset.BuildingDetail.toilet
        case .general, .payment, .experience:
            // TODO: - Must be some general image
            return Asset.BuildingDetail.inside
        }
    }
}

// TODO: - Change `QuestionnaireGroup` to group with question uuid
let paymentMethodsSwishQuestionUUID = "bf1f5e68-e0f6-48ad-be7d-fa355cf26503"
let paymentMethodsCashQuestionUUID = "7a7d6622-17d5-4d2c-bd95-a665601d83ee"


struct Questionnaire: Codable {
    @DocumentID var id: String? = nil
    let created_at: Date
    let questionGroups: [QuestionGroup]
    
    private enum CodingKeys: String, CodingKey {
        case id
        case created_at
        case questionGroups = "questions"
    }
    
    init(questionGroups: [QuestionGroup]) {
        self.id = nil
        self.created_at = Date()
        self.questionGroups = questionGroups
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        _id = try container.decode(DocumentID<String>.self, forKey: .id)
        self.created_at = try container.decode(Date.self, forKey: .created_at)
        self.questionGroups = try container.decodeIfPresent(FailableCodableArray<QuestionGroup>.self, forKey: .questionGroups)?.elements ?? []
    }
}

extension Questionnaire {
    
    func categoryBySheet(sheet: [String: Any]?) -> QuestionItemValue? {
        let categoryGroup = questionGroups.first(where: { $0.questions.contains(where: { $0.uuid == FirestoreDatabase.CATEGORY_UUID })})
        let categoryQuestion = categoryGroup?.questions.first(where: { $0.uuid == FirestoreDatabase.CATEGORY_UUID })
        
        guard let answer = sheet?[FirestoreDatabase.CATEGORY_UUID] else { return nil }
        if let selectedCategoryId = answer as? String {
            return categoryQuestion?.values?.first(where: { $0.uuid == selectedCategoryId })
        } else {
            return nil
        }
    }
}

struct QuestionGroup: Codable {
    let groupName: Label
    let questions: [QuestionItem]
    let type: String
    let uuid: String?
    
    private enum CodingKeys: String, CodingKey {
        case groupName = "labels"
        case questions
        case type
        case uuid
    }
}

struct QuestionItem: Codable {
    let type: QuestionItemType
    let uuid: String
    let labels: Label
    let values: [QuestionItemValue]?
    let condition: Condition?
    let advanced: Bool?
}

struct QuestionItemValue: Codable {
    let uuid: String
    let labels: Label
}

struct Condition: Codable {
    let or: [Dependent]?
    let and: [Dependent]?
    let not: [Dependent]?
}
enum Dependent: Codable {
    case object(Condition)
    case string(String)
    
    init(from decoder: Decoder) throws {
        let container =  try decoder.singleValueContainer()
        do {
            let objectVal = try container.decode(Condition.self)
            self = .object(objectVal)
        } catch DecodingError.typeMismatch {
            let stringVal = try container.decode(String.self)
            self = .string(stringVal)
        }
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()
        switch self {
        case .object(let value):
            try container.encode(value)
        case .string(let value):
            try container.encode(value)
        }
    }
}


struct Label: Codable {
    let en: String?
    let sv: String?
    
    // TODO: - Improve logic below
    var localized: String {
        let locale = NSLocale.current.languageCode.emptyIfNil
        if locale == "en" {
            return (en ?? sv).emptyIfNil
        } else if locale == "sv" {
            return (sv ?? en).emptyIfNil
        } else {
            return (en ?? sv).emptyIfNil
        }
    }
}

enum QuestionItemType: String, Codable {
    case selectSingle = "select_single" //dropdown
    case pictures
    case boolean
    case selectMulti = "select_multi"
    case number
}
