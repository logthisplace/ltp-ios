//
//  QuestionnaireFirebaseManager.swift
//  Log This Place
//
//

import Foundation

enum QuestionnaireType {
    case poi
    case user
}

protocol QuestionnaireFirebaseManagerProtocol {
    func getQuestionnaire(questionnaireType: QuestionnaireType,
                          completionHandler: @escaping (Result<Questionnaire, Error>) -> Void)
}

final class QuestionnaireFirebaseManager: QuestionnaireFirebaseManagerProtocol {
    
    private let db: FirestoreProtocol = AppContainer.resolve()
    
    func getQuestionnaire(questionnaireType: QuestionnaireType,
                          completionHandler: @escaping (Result<Questionnaire, Error>) -> Void) {
        
        var questionnaireTypeField = String()
        
        switch questionnaireType {
        case .poi:
            questionnaireTypeField = FirestoreDatabase.ConstantsQuestionnaire.QuestionnaireType.POI.rawValue
            Logger.shared.info("Get questionnaire by POI")
        case .user:
            questionnaireTypeField = FirestoreDatabase.ConstantsQuestionnaire.QuestionnaireType.USER.rawValue
            Logger.shared.info("Get questionnaire by USER")
        }
        
        db.collection(FirestoreDatabase.ConstantsQuestionnaire.COLLECTION_NAME)
            .whereField(FirestoreDatabase.ConstantsQuestionnaire.FIELD_TYPE, isEqualTo: questionnaireTypeField)
            .order(by: FirestoreDatabase.ConstantsFirebaseQuery.CREATED_AT, descending: true)
            .limit(to: 1)
            .getDocuments(completion: {(querySnapshot, error) in
                if let error = error {
                    Logger.shared.error(String(describing: error))
                    completionHandler(.failure(error))
                    return
                }
                
                if let queryDocumentSnapshot = querySnapshot?.documents.first {
                    
                    Logger.shared.info("Questionnaire successfully recived")
                    queryDocumentSnapshot
                        .reference
                        .getDocument(completion: {(queryDocumentSnapshot, firestoreError) in
                            responseHandler(from: queryDocumentSnapshot, completionHandler: {(result) in
                                completionHandler(result)
                            })
                        })
                } else {
                    Logger.shared.info("Questionnaire is Empty")
                    completionHandler(.success(Questionnaire(questionGroups: [])))
                }
            })
    }
}
