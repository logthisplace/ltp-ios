//
//  AccessibleIconsFirebaseManager.swift
//  Log This Place
//
//

import Foundation


protocol AccessibleIconsFirebaseManagerProtocol {
    func getAccessibleIconLogic(completionHandler: @escaping (Result<AccessibleIcon?, Error>) -> Void)
}

final class AccessibleIconsFirebaseManager: AccessibleIconsFirebaseManagerProtocol {
    
    private let db: FirestoreProtocol = AppContainer.resolve()
    
    func getAccessibleIconLogic(completionHandler: @escaping (Result<AccessibleIcon?, Error>) -> Void) {
        db.collection(FirestoreDatabase.ConstantsAccessibleIcon.COLLECTION_NAME)
            .order(by: FirestoreDatabase.ConstantsFirebaseQuery.CREATED_AT, descending: true)
            .limit(to: 1)
            .getDocuments(completion: {(querySnapshot, error) in
                if let error = error {
                    Logger.shared.error(String(describing: error))
                    completionHandler(.failure(error))
                    return
                }
                
                if let queryDocumentSnapshot = querySnapshot?.documents.first {
                    Logger.shared.info("Successfully received")
                    
                    queryDocumentSnapshot
                        .reference
                        .getDocument(completion: {(queryDocumentSnapshot, firestoreError) in
                            if let error = firestoreError {
                                completionHandler(.failure(error))
                                return
                            }
                            
                            responseHandler(from: queryDocumentSnapshot, completionHandler: {(result) in
                                completionHandler(result)
                            })
                        })
                } else {
                    completionHandler(.success(nil))
                }
            })
    }
}
