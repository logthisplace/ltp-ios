//
//  AccessibleIcon.swift
//  Log This Place
//
//

import Foundation

enum AccessibleIconIDs: String, CaseIterable {
    case assistanceDogs = "0ca832e0-efca-4148-84bc-4f1c3746cd3d"
    case handicapParking = "f18be21a-db9d-4526-9ed6-1a8f251f10fb"
    case wheelchairFriendlyEntrance = "b18568f8-b96e-4d4c-80c4-19d34bad808a"
    case stepsEntrance = "3ab50457-bf46-4352-a15c-aa8e19000ba5"
    case revolvingDoor = "151c5bb4-8bec-4a33-9bc3-8610e6a36a4e"
    case accessibleToilet = "e9a87444-3b7e-404d-b4a2-f2880e72483b"
    case paymentAccessible = "7048f056-5d34-4ef9-ab54-da8bccd26b2f"
    
    var iconId: String {
        return self.rawValue
    }
    
    var thumbnailAsset: ImageAsset {
        switch self {
        case .assistanceDogs:
            return Asset.BuildingDetail.Accessibility.assistanceDog
        case .handicapParking:
            return Asset.BuildingDetail.Accessibility.handicapParking
        case .wheelchairFriendlyEntrance:
            return Asset.BuildingDetail.Accessibility.wheelchairFriendlyEntrance
        case .stepsEntrance:
            return Asset.BuildingDetail.Accessibility.stepsAtTheEntrance
        case .revolvingDoor:
            return  Asset.BuildingDetail.Accessibility.revolvingDoor
        case .accessibleToilet:
            return Asset.BuildingDetail.Accessibility.accessibleToilet
        case .paymentAccessible:
            return Asset.BuildingDetail.Accessibility.paymentAccessible
        }
    }
}


struct AccessibleIcon: Codable {
    let created_at: Date
    let data: [AccessibleIconItem]
}

struct AccessibleIconItem: Codable {
    let condition: Condition?
    let uuid: String
    let labels: Label
}
