//
//  StorageFirebaseManager.swift
//  Log This Place
//

import FirebaseStorage

protocol StorageFirebaseManagerProtocol {
    func uploadImage(image: UIImage,
                     poiId: String,
                     sheetId: String,
                     questionId: String,
                     completionHandler: @escaping (Result<String, Error>) -> Void)
    func removeImage(poiId: String,
                     sheetId: String,
                     questionId: String,
                     imageName: String,
                     completionHandler: @escaping (Result<String, Error>) -> Void)
    func getImageURLs(poiId: String,
                      sheetId: String,
                      questionId: String,
                      completionHandler: @escaping (Result<[(ImageSource, String)], Error>) -> Void)
    func getImageURLs(imageGSReferences: [String],
                      completionHandler: @escaping (Result<[ImageSource], Error>) -> Void)
}

final class StorageFirebaseManager: StorageFirebaseManagerProtocol {
    
    private struct Constants {
        static let compressionImageQuality: CGFloat = 0.5
        static let contentType: String = "image/jpeg"
        static let imageDeletedSuccess: String = L10n.ImageSelector.DeletedSuccessfully.text
        static let sheetAttachmentsFolderName: String = "sheet-attachments"
        static let ownerKey: String = "owner_uid"
    }
        
    private let storage: FirebaseStorageProtocol = AppContainer.resolve()
    private let storageRef: StorageReference
    private let metadata = with(StorageMetadata()) {
        $0.contentType = Constants.contentType
        $0.customMetadata = [Constants.ownerKey: (AppContainer.resolve() as UserManagerProtocol).currentUser?.uid ?? String()]
    }
    
    init() {
        storageRef = storage.reference()
    }
    
    public func uploadImage(image: UIImage,
                            poiId: String,
                            sheetId: String,
                            questionId: String,
                            completionHandler: @escaping (Result<String, Error>) -> Void) {
        if let imageData = image.jpegData(compressionQuality: Constants.compressionImageQuality) {
            let imageName = String(format: "%@.jpeg", "\(Date().currentTimeMillis())")
            let imagesRef = questionStorageReference(poiId: poiId,
                                                     sheetId: sheetId,
                                                     questionId: questionId).child(imageName)
            print(metadata)
            imagesRef.putData(imageData, metadata: metadata) { (metadata, error) in
                if let error = error {
                    Logger.shared.error("Save image error: \(String(describing: error))")
                    completionHandler(.failure(error))
                    return
                }
                Logger.shared.info("Image successfully created")
                
                imagesRef.downloadURL { (url, error) in
                    if let error = error {
                        Logger.shared.error("Get downloadURL error: \(String(describing: error))")
                        completionHandler(.failure(error))
                        return
                    }
                    
                    Logger.shared.info(String(describing: url?.absoluteString))
                    completionHandler(.success(url?.absoluteString ?? ""))
                }
            }
        } else {
            Logger.shared.error("Getting jpegData")
            completionHandler(.failure(LTPError(title: L10n.Common.Error.text,
                                                     description: L10n.ImageSelector.ErrorDescription.text)))
        }
    }
    
    public func removeImage(poiId: String,
                            sheetId: String,
                            questionId: String,
                            imageName: String,
                            completionHandler: @escaping (Result<String, Error>) -> Void) {
        
        let imageRef = questionStorageReference(poiId: poiId,
                                                sheetId: sheetId,
                                                questionId: questionId).child(imageName)
        
        imageRef.delete(completion: {error in
            if let error = error {
                Logger.shared.error(String(describing: error))
                completionHandler(.failure(error))
                return
            }
            
            Logger.shared.info("image successfully deleted")
            completionHandler(.success(Constants.imageDeletedSuccess))
        })
    }

    public func getImageURLs(imageGSReferences: [String],
                             completionHandler: @escaping (Result<[ImageSource], Error>) -> Void) {
        let dispatchGroup = DispatchGroup()
        let storageReference = Storage.storage()
        var images = [ImageSource]()
        
        imageGSReferences.forEach {
            dispatchGroup.enter()
            
            let gsReference = storageReference.reference(withPath: $0)
            gsReference.downloadURL{ (url, error) in
                if let error = error {
                    Logger.shared.error(String(describing: error))
                }
                
                if let urlString = url?.absoluteString {
                    images.append(.link(urlString))
                }
                
                dispatchGroup.leave()
            }
        }
        
        dispatchGroup.notify(queue: .main) {
            if images.count > 0 {
                Logger.shared.info("images: \(images.count) successfully received")
            }
           
            completionHandler(.success(images))
        }
    }
    
    public func getImageURLs(poiId: String,
                             sheetId: String,
                             questionId: String,
                             completionHandler: @escaping (Result<[(ImageSource, String)], Error>) -> Void) {
        let storageRef = questionStorageReference(poiId: poiId,
                                                  sheetId: sheetId,
                                                  questionId: questionId)
        storageRef.listAll(completion: {(result, error) in
            if let error = error {
                Logger.shared.error(String(describing: error))
                completionHandler(.failure(error))
                return
            }
            var images = [(ImageSource, String)]()
            let dispatchGroup = DispatchGroup()
            
            result.items.forEach({ storageRef in
                dispatchGroup.enter()
                
                storageRef.downloadURL{ (url, error) in
                    
                    if let error = error {
                        Logger.shared.info(String(describing: error))
                    }
                    
                    if let urlString = url?.absoluteString {
                        images.append((.link(urlString), storageRef.name))
                    }
                    
                    dispatchGroup.leave()
                }
            })
            
            dispatchGroup.notify(queue: .main) {
                Logger.shared.info("images: \(images.count) successfully received")
                completionHandler(.success(images))
            }
        })
    }
    
    
    private func questionStorageReference(poiId: String, sheetId: String, questionId: String) -> StorageReference {
        storageRef
            .child(Constants.sheetAttachmentsFolderName)
            .child(poiId)
            .child(sheetId)
            .child(questionId)
    }
}
