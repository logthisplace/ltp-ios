//
//  FirebaseHelper.swift
//  Log This Place
//
//

import UIKit
import FirebaseFirestore

func responseHandler<Model: Decodable>(from: QuerySnapshot?,
                                       completionHandler: @escaping (Result<[Model], Error>) -> Void) {
    
    let result = Result {
        try from?.documents.compactMap {
            try $0.data(as: Model.self)
        }
    }
        
    switch result {
    case .success(let result):
        completionHandler(.success(result ?? []))
    case .failure(let error):
        print(error.localizedDescription )
        completionHandler(.failure(error))
    }
}


func responseHandler<Model: Decodable>(from: DocumentSnapshot?,
                                       completionHandler: @escaping (Result<Model, Error>) -> Void) {
    let result = Result {
        try from?.data(as: Model.self)
    }
        
    switch result {
    case .success(let result):
        if let result = result {
            completionHandler(.success(result))
        } else {
            completionHandler(.failure(FirestoreError.mappingError))
        }
    case .failure(let error):
        print(error.localizedDescription )
        completionHandler(.failure(error))
    }
}
