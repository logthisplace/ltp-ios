//
//  PostalCodesFirebaseManager.swift
//  Log This Place
//
//

import Foundation
import FirebaseFirestore

protocol PostalCodesFirebaseManagerProtocol {
    func getPostalCodes(completionHandler: @escaping (Result<[String]?, Error>) -> Void)
}

final class PostalCodesFirebaseManager: PostalCodesFirebaseManagerProtocol {
    
    private let db: FirestoreProtocol = AppContainer.resolve()
    
    // TODO: - split method
    func getPostalCodes(completionHandler: @escaping (Result<[String]?, Error>) -> Void) {
        postalCodeQuery()
            .getDocuments(source: .cache) { (querySnapshot, error) in
                if let error = error {
                    completionHandler(.failure(error))
                    return
                }
                
                if let queryDocumentSnapshot = querySnapshot?.documents.first {
                    queryDocumentSnapshot
                        .reference
                        .getDocument(source: .cache, completion: {(queryDocumentSnapshot, firestoreError) in
                            if let error = firestoreError {
                                Logger.shared.error(String(describing: error))
                                completionHandler(.failure(error))
                                return
                            }
                                                        
                            do {
                                let data = try queryDocumentSnapshot?.data(as: PostalCode.self)
                                Logger.shared.info("Successfully received from Cache")
                                completionHandler(.success(data?.data))
                            } catch let error {
                                Logger.shared.error(String(describing: error))
                                completionHandler(.failure(error))
                            }
                        })
                } else {
                    /// get data from Firestore if data from Cache doesnt exist
                    self.postalCodeQuery().getDocuments { (querySnapshot, error) in
                        if let error = error {
                            completionHandler(.failure(error))
                            return
                        }
                        
                        if let queryDocumentSnapshot = querySnapshot?.documents.first {
                            queryDocumentSnapshot
                                .reference
                                .getDocument(completion: {(queryDocumentSnapshot, firestoreError) in
                                    if let error = firestoreError {
                                        Logger.shared.error(String(describing: error))
                                        completionHandler(.failure(error))
                                        return
                                    }
                                    
                                    do {
                                        let data = try queryDocumentSnapshot?.data(as: PostalCode.self)
                                        Logger.shared.info("Successfully received from Firebase")
                                        completionHandler(.success(data?.data))
                                    } catch let error {
                                        Logger.shared.error(String(describing: error))
                                        completionHandler(.failure(error))
                                    }
                                })
                        } else {
                            Logger.shared.error(FirestoreError.doesntExist.localizedDescription)
                            completionHandler(.failure(FirestoreError.doesntExist))
                        }
                    }
                }
            }
    }
    
    private func postalCodeQuery() -> Query {
        return db.collection(FirestoreDatabase.ConstantsPostalCode.COLLECTION_NAME)
            .order(by: FirestoreDatabase.ConstantsFirebaseQuery.CREATED_AT, descending: true)
            .limit(to: 1)
    }
}
