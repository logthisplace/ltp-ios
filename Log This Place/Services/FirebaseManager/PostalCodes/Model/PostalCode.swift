//
//  PostalCodes.swift
//  Log This Place
//
//

import Foundation

struct PostalCode: Codable {
    let created_at: Date?
    let data: [String]?
}
