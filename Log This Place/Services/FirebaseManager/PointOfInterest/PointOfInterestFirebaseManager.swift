//
//  PointOfInterestFirebase.swift
//  Log This Place
//

import FirebaseFirestore

enum DocumentChangedType {
    case added // added a new POI
    case modified // updated POI
    case removed // removed POI
    case error // error
}

protocol PointOfInterestFirebaseManagerProtocol {
    func createPointOfInterest(newPoi: PointOfInterest,
                               completionHandler: @escaping (Result<PointOfInterest?, Error>) -> Void)
    func getPointOfInterests(completionHandler: @escaping (Result<[PointOfInterest], Error>) -> Void)
    func getPointOfInterest(poiId: String,
                            completionHandler: @escaping (Result<PointOfInterest?, Error>) -> Void)
    func updatePoiListener(poiId: String,
                           completionHandler: @escaping (Result<PointOfInterest?, Error>) -> Void)
    func removeUpdatePoiListener()
    
    func addUpdatePointOfInterestsListener(completionHandler: @escaping (DocumentChangedType, Bool, Result<PointOfInterest?, Error>) -> Void)
    func removeUpdatePointOfInterestsListener()
}

final class PointOfInterestFirebaseManager: PointOfInterestFirebaseManagerProtocol {
    
    private let db: FirestoreProtocol = AppContainer.resolve()
    private let userManager: UserManagerProtocol = AppContainer.resolve()
    
    private static var poiUpdatesListener: ListenerRegistration?
    private var poisUpdateListener: ListenerRegistration?
    
    func updatePoiListener(poiId: String,
                           completionHandler: @escaping (Result<PointOfInterest?, Error>) -> Void) {
        removeUpdatePoiListener()
        PointOfInterestFirebaseManager.poiUpdatesListener = db.collection(FirestoreDatabase.ConstantsPointOfInterest.COLLECTION_NAME)
            .document(poiId)
            .addSnapshotListener { documentSnapshot, error in
                if error != nil {
                    return
                }
                
                responseHandler(from: documentSnapshot, completionHandler: {(result) in
                    completionHandler(result)
                })
            }
    }
    
    func removeUpdatePoiListener() {
        PointOfInterestFirebaseManager.poiUpdatesListener?.remove()
        PointOfInterestFirebaseManager.poiUpdatesListener = nil
    }
    
    func getPointOfInterest(poiId: String,
                            completionHandler: @escaping (Result<PointOfInterest?, Error>) -> Void) {
        db.collection(FirestoreDatabase.ConstantsPointOfInterest.COLLECTION_NAME)
            .document(poiId)
            .getDocument(completion: {(documentSnapshot, error) in
                if let error = error {
                    Logger.shared.error(String(describing: error))
                    completionHandler(.failure(error))
                    return
                }
                
                Logger.shared.info("POI: \(poiId) successfully received")
                responseHandler(from: documentSnapshot, completionHandler: {(result) in
                    completionHandler(result)
                })
            })
    }
    
    func createPointOfInterest(newPoi: PointOfInterest,
                               completionHandler: @escaping (Result<PointOfInterest?, Error>) -> Void) {
        db.collection(FirestoreDatabase.ConstantsPointOfInterest.COLLECTION_NAME)
            .addDocument(data: newPoi.dataToDict(), completion: {(error) in
                if let error = error {
                    Logger.shared.error(String(describing: error))
                    completionHandler(.failure(error))
                }
            })
            .getDocument(completion: {(documentSnapshot, error) in
                if let error = error {
                    Logger.shared.error(String(describing: error))
                    completionHandler(.failure(error))
                }
        
                Logger.shared.info("POI successfully created")
                responseHandler(from: documentSnapshot, completionHandler: {(result) in
                    completionHandler(result)
                })
            })
    }
    
    func getPointOfInterests(completionHandler: @escaping (Result<[PointOfInterest], Error>) -> Void) {
        let query = db.collection(FirestoreDatabase.ConstantsPointOfInterest.COLLECTION_NAME)
        
        query.getDocuments(completion: { (querySnapshot, error) in
            if let error = error {
                Logger.shared.error(String(describing: error))
                completionHandler(.failure(error))
                return
            }
            
            Logger.shared.info("POIS \(querySnapshot?.documents.count ?? 0) successfully recived")
            responseHandler(from: querySnapshot, completionHandler: {(result) in
                completionHandler(result)
            })
        })
    }
    
}

extension PointOfInterestFirebaseManager {
    
    func addUpdatePointOfInterestsListener(completionHandler: @escaping (DocumentChangedType, Bool, Result<PointOfInterest?, Error>) -> Void) {
            poisUpdateListener = db.collection(FirestoreDatabase.ConstantsPointOfInterest.COLLECTION_NAME)
                .addSnapshotListener { snapshots, error in
                    if let error = error {
                        Logger.shared.error(String(describing: error))
                        completionHandler(.error, false, .failure(FirestoreError.doesntExist))
                        return
                    }
                    
                    guard let snapshots = snapshots else {
                        completionHandler(.error, false, .failure(FirestoreError.doesntExist))
                        return
                    }
                    
                    
                    if !snapshots.isEmpty {
                        for dc in snapshots.documentChanges {
                            
                            do {
                                let poi = try dc.document.data(as: PointOfInterest.self)
                                
                                switch dc.type {
                                case .added:
                                    completionHandler(.added, snapshots.metadata.hasPendingWrites, Result.success(poi))
                                case .modified:
                                    Logger.shared.info("Modified POI: \(String(describing: poi?.id))")
                                    completionHandler(.modified, snapshots.metadata.hasPendingWrites, Result.success(poi))
                                case .removed:
                                    Logger.shared.info("Removed POI: \(String(describing: poi?.id))")
                                    completionHandler(.removed, snapshots.metadata.hasPendingWrites, Result.success(poi))
                                }
                                
                            } catch let error {
                                Logger.shared.error("Listener poi mapper error: \(String(describing: error))")
                                continue
                            }
                        }
                    }
                }
        }
    
    func removeUpdatePointOfInterestsListener() {
        poisUpdateListener?.remove()
        poisUpdateListener = nil
    }
}
