//
//  PointOfInterestType.swift
//  Log This Place
//

import Foundation

// TODO: - HARDCODED ID OF CATEGORY, We can't avoid it yet.
enum PointOfInterestType: String, Codable, CaseIterable {
    case food = "b72fe2e9-9325-4667-91e3-db3fda92910b"
    case tourism = "1ff4537a-08f3-4fcc-ac73-209e0c20f1e6"
    case authority = "8571fae7-d19c-4a3a-8990-dc058a9739d3"
    case education = "fb4cce80-a284-4e94-a11e-c7e4ce827c37"
    case toilet = "2d029ef9-2007-4adc-b01b-2be089d2b969"
    case sport = "0d0bc576-578b-4eb1-89ff-4b0bf168b40a"
    case freeTime = "a1757e54-5cd6-4d1a-bf8c-491b8d3a3062"
    case health = "35dc103e-aef7-42a1-bb0d-c5b990c2daee"
    case hotel = "fe643d75-a755-4e5c-b08e-692d9e20f88d"
    case transport = "ca244839-9500-4674-8a8d-4644ebe92efd"
    case shopping = "ddb9ab7a-c565-490c-ab10-78b4a7bbe1f9"
    case services = "bc54152f-7714-4a59-92b5-12e7967ae411"
    
    var categoryId: String {
        return self.rawValue
    }
}

extension PointOfInterestType {
    var thumbnailAsset: ImageAsset {
        switch self {
        case .food:
            return Asset.PoiCategories.drinks
        case .tourism:
            return Asset.PoiCategories.museum
        case .authority, .education:
            return Asset.PoiCategories.authorities
        case .toilet:
            return Asset.PoiCategories.publicToilet
        case .sport:
            return Asset.PoiCategories.gym
        case .freeTime:
            return Asset.PoiCategories.leisure
        case .health:
            return Asset.PoiCategories.medicalCare
        case .hotel:
            return Asset.PoiCategories.hotel
        case .transport:
            return Asset.PoiCategories.transport
        case .shopping:
            return Asset.PoiCategories.shopping
        case .services:
            return Asset.PoiCategories.services
        }
    }
}
