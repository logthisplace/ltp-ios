//
//  PointOfInterest.swift
//  Log This Place
//
//

import FirebaseFirestoreSwift
import Firebase
import GoogleMaps


struct PoiLocation: Equatable, Codable {
    let latitude: Double
    let longitude: Double
    
    var coreLocation: CLLocationCoordinate2D {
        .init(latitude: latitude, longitude: longitude)
    }
}

struct POIMeta: Codable {
    let updated_at: Date?
    let data: POIMetaData?
}

struct POIMetaData: Codable {
    let geo: POIGeometry?
}

struct POIGeometry: Codable {
    let location: PoiLocation?
}

struct POISynth: Codable {
    let computed_at: Date?
    let sheet: [String: Any]?
    
    enum CodingKeys: String, CodingKey {
        case computed_at
        case sheet
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.computed_at = try container.decodeIfPresent(Date.self, forKey: .computed_at)
        self.sheet = try container.decodeIfPresent([String: Any].self, forKey: .sheet)
    }
    
    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encodeIfPresent(self.computed_at, forKey: .computed_at)
        try container.encodeIfPresent(self.sheet, forKey: .sheet)
    }
    
    // TODO: The same method in `AnswerSheet`
    func answers(for id: String) -> AnswerSheetType {
        let answer = sheet?[id]
        
        if let boolAnswer = answer as? Bool {
            return .bool(boolAnswer)
        }
        if let intAnswer = answer as? Int {
            return .int(intAnswer)
        }
        if let stringAnswer = answer as? String {
            return .string(stringAnswer)
        }
        if let arrayAnswer = answer as? [String] {
            return .array(arrayAnswer)
        }
        
        return .unknown
    }
    
    init(sheet: [String: Any]) {
        self.sheet = sheet
        self.computed_at = Date()
    }
}

struct PointOfInterest: Codable {
    @DocumentID var id: String? = nil
    let g_places_id: String
    var _meta: POIMeta? = nil
    var _synth: POISynth? = nil
    @ServerTimestamp var created_at: Timestamp? = nil
    @ServerTimestamp var updated_at: Timestamp? = nil
    
    /// return poi location
    var location: PoiLocation? {
        return _meta?.data?.geo?.location
    }
    
    /// return category
    var category: PointOfInterestType? {
        guard let answerType = _synth?.answers(for: FirestoreDatabase.CATEGORY_UUID) else { return nil }
        if case let .string(categoryId) = (answerType) {
            return PointOfInterestType(rawValue: categoryId)
        } else {
            return nil
        }
    }
    
    /// return category id
    var categoryId: String? {
        guard let answerType = _synth?.answers(for: FirestoreDatabase.CATEGORY_UUID) else { return nil }
        if case let .string(categoryId) = (answerType) {
            return categoryId
        } else {
            return nil
        }
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        _id = try container.decode(DocumentID<String>.self, forKey: .id)
        self.g_places_id = try container.decode(String.self, forKey: .g_places_id)
        self.created_at = try container.decodeIfPresent(Timestamp.self, forKey: .created_at)
        self._meta = try container.decodeIfPresent(POIMeta.self, forKey: ._meta)
        self._synth = try container.decodeIfPresent(POISynth.self, forKey: ._synth)
    }
    
    init(googleId: String) {
        self.g_places_id = googleId
    }
    
    func dataToDict() -> [String: Any] {
        var poi = [String: Any]()
        poi[FirestoreDatabase.ConstantsPointOfInterest.GOOGLE_PLACE_ID] = g_places_id
        poi[FirestoreDatabase.ConstantsPointOfInterest.CREATED_AT] = FieldValue.serverTimestamp()
        poi[FirestoreDatabase.ConstantsPointOfInterest.UPDATED_AT] = FieldValue.serverTimestamp()
        return poi
    }
}


