//
//  UserIdentificationFirebaseManager.swift
//  Log This Place
//
//

import FirebaseAuth


protocol UserIdentificationFirebaseManagerProtocol {
    func authorization(email: String,
                       password: String,
                       completionHandler: @escaping (Result<User?, Error>) -> Void)
    
    func registration(email: String,
                      password: String,
                      completionHandler: @escaping (Result<User?, Error>) -> Void)
    
    func signOut(completionHandler: @escaping (Result<String?, Error>) -> Void)
    func checkAndsignInAnonymously(completionHandler: @escaping (Result<String?, Error>) -> Void)
    func delete(completionHandler: @escaping (Bool) -> Void)
}

final class UserIdentificationFirebaseManager: UserIdentificationFirebaseManagerProtocol {
    
    private let firebaseAuth: FirebaseAuthProtocol = AppContainer.resolve()
    
    func delete(completionHandler: @escaping (Bool) -> Void) {
        firebaseAuth.currentUser?.delete { error in
            if error != nil {
                completionHandler(false)
            } else {
                completionHandler(true)
            }
        }
    }
    
    func authorization(email: String,
                       password: String,
                       completionHandler: @escaping (Result<User?, Error>) -> Void) {
        firebaseAuth.signIn(withEmail: email, password: password) { (authResult, error) in
            if let error = error {
                Logger.shared.error(String(describing: error))
                completionHandler(.failure(error))
            } else {
                Logger.shared.info("Successfully logged in")
                completionHandler(.success(Auth.auth().currentUser))
            }
        }
    }
    
    // TODO: - Refactor method
    func registration(email: String,
                      password: String,
                      completionHandler: @escaping (Result<User?, Error>) -> Void) {
        
        if let firebaseUser = firebaseAuth.currentUser {
            let credential = EmailAuthProvider.credential(withEmail: email, password: password)
            
            if firebaseUser.isAnonymous {
                /// convert an anonymous account to a permanent account
                firebaseUser.link(with: credential) { [weak self] authResult, error in
                    if let error = error {
                        Logger.shared.error(String(describing: error))
                        completionHandler(.failure(error))
                    } else {
                        Logger.shared.info("Successfully converted to permanent account")
                        completionHandler(.success(self?.firebaseAuth.currentUser))
                    }
                }
            } else {
                /// additional check for old builds
                try? firebaseAuth.signOut()
                signInAnonymously { result in
                    switch result {
                    case .success:
                        //TODO: - duplicate code
                        firebaseUser.link(with: credential) { [weak self] authResult, error in
                            if let error = error {
                                Logger.shared.error(String(describing: error))
                                completionHandler(.failure(error))
                            } else {
                                completionHandler(.success(self?.firebaseAuth.currentUser))
                            }
                        }
                    case let .failure(error):
                        Logger.shared.error(String(describing: error))
                        completionHandler(.failure(error))
                    }
                }
            }
        } else {
            ///regular registration
            firebaseAuth.createUser(withEmail: email, password: password) { [weak self] authResult, error in
                if let error = error {
                    Logger.shared.error(String(describing: error))
                    completionHandler(.failure(error))
                } else {
                    completionHandler(.success(self?.firebaseAuth.currentUser))
                }
            }
        }
    }
    
    func signOut(completionHandler: @escaping (Result<String?, Error>) -> Void) {
        try? firebaseAuth.signOut()
        /// signIn anonymously
        signInAnonymously { result in
            completionHandler(result)
            if case let .success(userId) = result {
                print("After signOut userId is \(userId.emptyIfNil)")
            }
        }
    }
    
    func checkAndsignInAnonymously(completionHandler: @escaping (Result<String?, Error>) -> Void) {
        let firebaseUser = firebaseAuth.currentUser
        
        if firebaseUser == nil {
            firebaseAuth.signInAnonymously { [weak self] authResult, error in
                self?.signInAnonymously() { result in
                    completionHandler(result)
                }
            }
        } else {
            if let user = firebaseUser {
                completionHandler(.success(user.uid))
            } else {
                completionHandler(.failure(FirestoreError.firebaseUserError))
            }
        }
    }
    
    private func signInAnonymously(completionHandler: @escaping (Result<String?, Error>) -> Void) {
        firebaseAuth.signInAnonymously { authResult, error in
            
            if error != nil {
                Logger.shared.error(String(describing: error))
                completionHandler(.failure(error ?? FirestoreError.firebaseUserError))
                return
            }
            
            if let user = authResult?.user {
                Logger.shared.info("Successfully signIn like anonymously")
                completionHandler(.success(user.uid))
            }
        }
    }
}
