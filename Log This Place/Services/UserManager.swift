//
//  UserManager.swift
//  Log This Place
//
//

import UIKit
import FirebaseAuth

protocol UserManagerProtocol {
    var deviceUUID: String { get }
    var isUserLoggedIn: Bool { get }
    var currentUser: User? { get }
    var isAnonymous: Bool? { get }
}

final class UserManager: UserManagerProtocol {
    
    private let firebaseAuth: FirebaseAuthProtocol = AppContainer.resolve()
        
    var deviceUUID: String {
        UIDevice.current.identifierForVendor?.uuidString ?? ""
    }
    
    var isUserLoggedIn: Bool {
        return firebaseAuth.currentUser != nil
    }
    
    var currentUser: User? {
        return firebaseAuth.currentUser
    }
    
    var isAnonymous: Bool? {
        return firebaseAuth.currentUser?.isAnonymous
    }
}
