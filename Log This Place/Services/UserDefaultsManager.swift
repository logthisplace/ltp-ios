//
//  UserDefaultsManager.swift
//  Log This Place
//
//

import Foundation

enum UserDefaultsKey: String {
    case wasWelcomeShown
    case wasOnboardingShown
    case wasPinsColorInformationShown
}

protocol UserDefaultsManagerProtocol: AnyObject {
    var wasWelcomeShown: Bool { get set }
    var wasOnboardingShown: Bool { get set }
    var wasPinsColorInformationShown: Bool { get set }
}

final class UserDefaultsManager: UserDefaultsManagerProtocol {
    
    private let defaults: UserDefaults

    init(defaults: UserDefaults = .standard) {
        self.defaults = defaults
    }
    
    var wasWelcomeShown: Bool {
        set {
            defaults.setValue(newValue, forKeyPath: UserDefaultsKey.wasWelcomeShown.rawValue)
        }
        get {
            return defaults.object(forKey: UserDefaultsKey.wasWelcomeShown.rawValue) as? Bool ?? false
        }
    }
    
    var wasOnboardingShown: Bool {
        set {
            defaults.setValue(newValue, forKeyPath: UserDefaultsKey.wasOnboardingShown.rawValue)
        }
        get {
            return defaults.object(forKey: UserDefaultsKey.wasOnboardingShown.rawValue) as? Bool ?? false
        }
    }
    
    var wasPinsColorInformationShown: Bool {
        set {
            defaults.setValue(newValue, forKeyPath: UserDefaultsKey.wasPinsColorInformationShown.rawValue)
        }
        get {
            return defaults.object(forKey: UserDefaultsKey.wasPinsColorInformationShown.rawValue) as? Bool ?? false
        }
    }
}
