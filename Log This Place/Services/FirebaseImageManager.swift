//
//  HandleImageManager.swift
//  Log This Place
//
//

import UIKit

protocol FirebaseImageManagerProtocol {
    func handleImages(viewModel: FirebaseImageManager.ViewModel,
                      completionHandler: @escaping (Result<[String: [(image: ImageSource, name: String)]], Error>) -> Void)
}


final class FirebaseImageManager: FirebaseImageManagerProtocol {
    
    struct ViewModel {
        let questionnaire: Questionnaire
        let placeId: String
        let answerSheetId: String
    }
    
    private let storageDataManager: StorageDataManagerProtocol = AppContainer.resolve()
    
    func handleImages(viewModel: FirebaseImageManager.ViewModel,
                      completionHandler: @escaping (Result<[String: [(image: ImageSource, name: String)]], Error>) -> Void) {
        let dispatchGroup = DispatchGroup()
        var images = [String: [(image: ImageSource, name: String)]]()
        let questionIdsForImages = extractQuestionIdsWithPictures(questions: viewModel.questionnaire.questionGroups)
        for questionIdForImages in questionIdsForImages {
            dispatchGroup.enter()
            storageDataManager.getImageURLs(poiId: viewModel.placeId,
                                            sheetId: viewModel.answerSheetId,
                                            questionId: questionIdForImages) { result in
                switch result {
                case let .success(fetchedImages):
                    images[questionIdForImages] = fetchedImages
                case .failure:
                    //TODO: - handle failure
                    break
                }
                dispatchGroup.leave()
            }
        }
        
        dispatchGroup.notify(queue: .main) {
            completionHandler(.success(images))
        }
    }
    
    private func extractQuestionIdsWithPictures(questions: [QuestionGroup]) -> [String]  {
        var questionIdsWithPictures: [String] = []

        let pictureGroups = questions.filter({ $0.questions.contains(where: { $0.type == .pictures }) })
        for pictureGroup in pictureGroups {
            questionIdsWithPictures.append(contentsOf: pictureGroup.questions.map(({ $0.uuid })))
        }

        return questionIdsWithPictures
    }
    
}
