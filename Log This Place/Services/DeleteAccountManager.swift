import Foundation
import UIKit

protocol DeleteAccountManagerProtocol {
    var delegate: DeleteAccountManagerDelegate? { get set }
    func deleteAccountTapped()
}

protocol DeleteAccountManagerDelegate {
    func accountDeleted()
}


class DeleteAccountManager: DeleteAccountManagerProtocol {
    
    var delegate: DeleteAccountManagerDelegate?
    
    private let userIdentificationDataManager: UserIdentificationDataManagerProtocol = AppContainer.resolve()
    private let userDefaultsManager: UserDefaultsManagerProtocol = AppContainer.resolve()
    
    private let view: View?
    
    init(view: View?) {
        self.view = view
    }
    
    func deleteAccountTapped() {
        showDeleteAccountAlert { [weak self] in
            self?.view?.showLoading(fullscreen: true)
            self?.deleteAccountFromFirebase { isSuccessful in
                if(isSuccessful) {
                    self?.signOut {
                        self?.view?.hideLoading()
                        self?.showSuccessAccountDeletedAlert()
                    }
                } else {
                    self?.view?.hideLoading()
                    self?.showFailedAccountDeletionAlert()
                }
            }
        }
    }
    
    private func showDeleteAccountAlert(completionHandler: @escaping () -> Void) {
        view?.showAlert(title: "",
                        message: L10n.Profile.DeleteAccount.Alert.text,
                       actions: [
                        UIAlertAction.init(title: L10n.Common.Delete.text, style: .default, handler: { (action) in
                            completionHandler()
                        }),
                        UIAlertAction.init(title: L10n.Common.Cancel.text, style: .default, handler: nil)
                       ])
    }
    
    private func showFailedAccountDeletionAlert() {
        view?.showAlert(title: "",
                       message: L10n.Common.SomethingWentWrong.text,
                       actions: nil)
    }
    
    private func showSuccessAccountDeletedAlert() {
        view?.showAlert(title: "",
                        message: L10n.Profile.DeletedAccount.Alert.text,
                       actions: [
                        UIAlertAction.init(title: L10n.Common.Ok.text, style: .default, handler: { [weak self] (action) in
                            self?.delegate?.accountDeleted()
                        })
                       ])
    }
    
    private func deleteAccountFromFirebase(completionHandler: @escaping (Bool) -> Void) {
        userIdentificationDataManager.delete { isSuccessful in
            completionHandler(isSuccessful)
        }
    }
    
    private func signOut(completionHandler: @escaping () -> Void) {
        userIdentificationDataManager.signOut { [weak self] _ in
            /// `wasWelcomeShown` - welcome screen with login or register
            self?.userDefaultsManager.wasWelcomeShown = false
            
            completionHandler()
        }
    }
}
