//
//  PointOfInterestDataManager.swift
//  Log This Place
//
//

import Foundation

// sourcery: mockable
protocol PointOfInterestDataManagerProtocol {
    func createPointOfInterest(newPoi: PointOfInterest,
                              completionHandler: @escaping (Result<PointOfInterest?, Error>) -> Void)
    func getPointOfInterests(completionHandler: @escaping (Result<[PointOfInterest], Error>) -> Void)
    func getPointOfInterest(poiId: String,
                            completionHandler: @escaping (Result<PointOfInterest?, Error>) -> Void)
    func updatePoiListener(poiId: String,
                           completionHandler: @escaping (Result<PointOfInterest?, Error>) -> Void)
    func removeUpdatePoiListener()
    
    func addUpdatePointOfInterestsListener(completionHandler: @escaping (DocumentChangedType, Bool, Result<PointOfInterest?, Error>) -> Void)
    func removeUpdatePointOfInterestsListener()
}


final class PointOfInterestDataManager: PointOfInterestDataManagerProtocol {

    func removeUpdatePointOfInterestsListener() {
        pointOfInterestFirebaseManager.removeUpdatePointOfInterestsListener()
    }
    func addUpdatePointOfInterestsListener(completionHandler: @escaping (DocumentChangedType, Bool, Result<PointOfInterest?, Error>) -> Void) {
        pointOfInterestFirebaseManager.addUpdatePointOfInterestsListener(completionHandler: completionHandler)
    }
    
    func removeUpdatePoiListener() {
        pointOfInterestFirebaseManager.removeUpdatePoiListener()
    }
    
    func updatePoiListener(poiId: String,
                               completionHandler: @escaping (Result<PointOfInterest?, Error>) -> Void) {
        pointOfInterestFirebaseManager.updatePoiListener(poiId: poiId,
                                                             completionHandler: {(result)  in
                                                             completionHandler(result)
                                                           })
    }
    
    func getPointOfInterest(poiId: String, completionHandler: @escaping (Result<PointOfInterest?, Error>) -> Void) {
        pointOfInterestFirebaseManager.getPointOfInterest(poiId: poiId,
                                                          completionHandler: {(result)  in
                                                          completionHandler(result)
                                                        })
    }
    
    private let pointOfInterestFirebaseManager: PointOfInterestFirebaseManagerProtocol = AppContainer.resolve()
    
    func createPointOfInterest(newPoi: PointOfInterest,
                               completionHandler: @escaping (Result<PointOfInterest?, Error>) -> Void) {
        
        pointOfInterestFirebaseManager.createPointOfInterest(newPoi: newPoi,
                                                             completionHandler: {(result)  in
                                                             completionHandler(result)
                                                           })
    }
    
    func getPointOfInterests(completionHandler: @escaping (Result<[PointOfInterest], Error>) -> Void) {

        pointOfInterestFirebaseManager.getPointOfInterests(completionHandler: {(result)  in
                                                              completionHandler(result)
                                                           })
    }
    
}
