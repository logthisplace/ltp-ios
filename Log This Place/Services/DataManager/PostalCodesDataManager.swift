//
//  PostalCodesDataManager.swift
//  Log This Place
//
//

import Foundation

class CachedLocallyPostalCode {
    static var codes: [String] = []
    
    static func isCached() -> Bool {
        return !CachedLocallyPostalCode.codes.isEmpty
    }
    
    static func cacheCodes(codes: [String]) {
        CachedLocallyPostalCode.codes.removeAll()
        CachedLocallyPostalCode.codes.append(contentsOf: codes)
    }
    
    static func getCachedPostalCodes() -> [String] {
        return CachedLocallyPostalCode.codes
    }
}

protocol PostalCodesDataManagerProtocol {
    func getPostalCodes(completionHandler: @escaping (Result<[String]?, Error>) -> Void)
}

final class PostalCodesDataManager: PostalCodesDataManagerProtocol {
    
    private let postalCodesFirebaseManager: PostalCodesFirebaseManagerProtocol = AppContainer.resolve()
    
    func getPostalCodes(completionHandler: @escaping (Result<[String]?, Error>) -> Void) {
        if CachedLocallyPostalCode.isCached() {
            Logger.shared.info("Postal codes from cache")
            completionHandler(.success(CachedLocallyPostalCode.getCachedPostalCodes()))
        } else {
            postalCodesFirebaseManager.getPostalCodes(completionHandler: {(result)  in
                if case let .success(data) = result {
                    if let postalCodes = data, !postalCodes.isEmpty {
                        CachedLocallyPostalCode.cacheCodes(codes: postalCodes)
                    }
                }
                completionHandler(result)
            })
        }
        
    }
}
