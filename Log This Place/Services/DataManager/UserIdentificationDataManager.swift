//
//  UserIdentificationDataManager.swift
//  Log This Place
//
//

import Foundation
import FirebaseAuth


protocol UserIdentificationDataManagerProtocol {
    func authorization(email: String,
                       password: String,
                       completionHandler: @escaping (Result<User?, Error>) -> Void)
    
    func registration(email: String,
                      password: String,
                      completionHandler: @escaping (Result<User?, Error>) -> Void)
    
    func signOut(completionHandler: @escaping (Result<String?, Error>) -> Void)
    func checkAndsignInAnonymously(completionHandler: @escaping (Result<String?, Error>) -> Void)
    func delete(completionHandler: @escaping (Bool) -> Void)
}


final class UserIdentificationDataManager: UserIdentificationDataManagerProtocol {
    private let userIdentificationFirebaseManager: UserIdentificationFirebaseManagerProtocol = AppContainer.resolve()
    
    func delete(completionHandler: @escaping (Bool) -> Void) {
        userIdentificationFirebaseManager.delete(completionHandler: {(result) in
            completionHandler(result)
        })
    }
    
    func authorization(email: String,
                       password: String,
                       completionHandler: @escaping (Result<User?, Error>) -> Void) {
        userIdentificationFirebaseManager.authorization(email: email,
                                                        password: password,
                                                        completionHandler: {(result) in
                                                            completionHandler(result)
                                                        })
    }
    
    func registration(email: String,
                      password: String,
                      completionHandler: @escaping (Result<User?, Error>) -> Void) {
        userIdentificationFirebaseManager.registration(email: email,
                                                       password: password,
                                                       completionHandler: {(result) in
                                                        completionHandler(result)
                                                       })
    }
    
    func signOut(completionHandler: @escaping (Result<String?, Error>) -> Void) {
        userIdentificationFirebaseManager.signOut(completionHandler: {(result) in
            completionHandler(result)
        })
    }
    
    func checkAndsignInAnonymously(completionHandler: @escaping (Result<String?, Error>) -> Void) {
        userIdentificationFirebaseManager.checkAndsignInAnonymously(completionHandler: {(result) in
            completionHandler(result)
        })
    }
    
}
