//
//  QuestionnaireDataManager.swift
//  Log This Place
//

import Foundation

// sourcery: mockable
protocol QuestionnaireDataManagerProtocol {
    func getQuestionnaire(questionnaireType: QuestionnaireType,
                          completionHandler: @escaping (Result<Questionnaire, Error>) -> Void)
}

final class QuestionnaireDataManager: QuestionnaireDataManagerProtocol {
    
    // MARK: - Properties
    private let questionnaireFirebaseManager: QuestionnaireFirebaseManagerProtocol = AppContainer.resolve()
    
    func getQuestionnaire(questionnaireType: QuestionnaireType,
                          completionHandler: @escaping (Result<Questionnaire, Error>) -> Void) {
    
        questionnaireFirebaseManager.getQuestionnaire(questionnaireType: questionnaireType,
                                                         completionHandler: completionHandler)
    }
    
}
