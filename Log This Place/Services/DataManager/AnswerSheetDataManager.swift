//
//  AnswerSheetDataManager.swift
//  Log This Place
//
//

import Foundation
import FirebaseFirestore


protocol AnswerSheetDataManagerProtocol {
    func getAnswerSheetUserByDocumentId(poiId: String,
                                        completionHandler: @escaping  (Result<AnswerSheet?, Error>) -> Void)
    
    func updateAnswerSheet(poiId: String,
                           answerSheet: AnswerSheet,
                           completionHandler: @escaping  (Result<AnswerSheet?, Error>) -> Void)
    
    func answerSheetReference(poiId: String) -> DocumentReference
    
    func setDataToAnswerSheetReference(answerSheetReference: DocumentReference,
                                       answerSheet: AnswerSheet,
                                       completionHandler: @escaping  (Result<AnswerSheet?, Error>) -> Void)
    
    func createAnswerSheet(poiId: String,
                           answerSheet: AnswerSheet,
                           completionHandler: @escaping  (Result<AnswerSheet?, Error>) -> Void)
}

final class AnswerSheetDataManager: AnswerSheetDataManagerProtocol {
    
    private let answerSheetFirebaseManager: AnswerSheetFirebaseManagerProtocol = AppContainer.resolve()
    
    func getAnswerSheetUserByDocumentId(poiId: String, completionHandler: @escaping (Result<AnswerSheet?, Error>) -> Void) {
        answerSheetFirebaseManager.getAnswerSheetUserByDocumentId(poiId: poiId,
                                                                  completionHandler: {(result)  in
                                                                    completionHandler(result)
                                                                  })
    }
    
    func updateAnswerSheet(poiId: String, answerSheet: AnswerSheet, completionHandler: @escaping (Result<AnswerSheet?, Error>) -> Void) {
        answerSheetFirebaseManager.updateAnswerSheet(poiId: poiId,
                                                     answerSheet: answerSheet,
                                                     completionHandler: {(result)  in
                                                        completionHandler(result)
                                                     })
    }
    
    func createAnswerSheet(poiId: String, answerSheet: AnswerSheet, completionHandler: @escaping (Result<AnswerSheet?, Error>) -> Void) {
        answerSheetFirebaseManager.createAnswerSheet(poiId: poiId,
                                                     answerSheet: answerSheet,
                                                     completionHandler: {(result)  in
                                                        completionHandler(result)
                                                     })
    }
    
    func answerSheetReference(poiId: String) -> DocumentReference {
        return answerSheetFirebaseManager.answerSheetReference(poiId: poiId)
    }
    
    func setDataToAnswerSheetReference(answerSheetReference: DocumentReference,
                                       answerSheet: AnswerSheet,
                                       completionHandler: @escaping  (Result<AnswerSheet?, Error>) -> Void) {
        answerSheetFirebaseManager.setDataToAnswerSheetReference(answerSheetReference: answerSheetReference,
                                                                 answerSheet: answerSheet,
                                                                 completionHandler: {(result)  in
                                                                    completionHandler(result)
                                                                 })
    }
}
