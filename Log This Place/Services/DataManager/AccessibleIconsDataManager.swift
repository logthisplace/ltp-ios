//
//  AccessibleIconsDataManager.swift
//  Log This Place
//
//

import Foundation

protocol AccessibleIconsDataManagerProtocol {
    func getAccessibleIconLogic(completionHandler: @escaping (Result<AccessibleIcon?, Error>) -> Void)
}

final class AccessibleIconsDataManager: AccessibleIconsDataManagerProtocol {
    
    private let accessibleIconsFirebaseManager: AccessibleIconsFirebaseManagerProtocol = AppContainer.resolve()
    
    func getAccessibleIconLogic(completionHandler: @escaping (Result<AccessibleIcon?, Error>) -> Void) {
        accessibleIconsFirebaseManager.getAccessibleIconLogic(completionHandler: {(result)  in
            completionHandler(result)
        })
    }
}
