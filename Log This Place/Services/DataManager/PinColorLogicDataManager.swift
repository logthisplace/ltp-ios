//
//  PinColorLogicDataManager.swift
//  Log This Place
//
//

import Foundation


protocol PinColorLogicDataManagerProtocol {
    func getPinColorLogic(completionHandler: @escaping (Result<PinColorLogic?, Error>) -> Void)
}

final class PinColorLogicDataManager: PinColorLogicDataManagerProtocol {
    
    private let pinColorLogicFirebaseManager: PinColorLogicFirebaseManagerProtocol = AppContainer.resolve()
    
    func getPinColorLogic(completionHandler: @escaping (Result<PinColorLogic?, Error>) -> Void) {
        pinColorLogicFirebaseManager.getPinColorLogic(completionHandler: {(result)  in
            completionHandler(result)
        })
    }
}
