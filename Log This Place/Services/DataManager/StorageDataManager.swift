//
//  StorageDataManager.swift
//  Log This Place
//

import UIKit

// sourcery: mockable
protocol StorageDataManagerProtocol {
    func uploadImage(image: UIImage,
                     poiId: String,
                     sheetId: String,
                     questionId: String,
                     completionHandler: @escaping (Result<String, Error>) -> Void)
    
    func uploadImages(images: [UIImage],
                      poiId: String,
                      sheetId: String,
                      questionId: String,
                      completionHandler: @escaping ([Result<String, Error>]) -> Void)
    
    func removeImage(poiId: String,
                     sheetId: String,
                     questionId: String,
                     imageName: String,
                     completionHandler: @escaping (Result<String, Error>) -> Void)
    
    func removeImages(poiId: String,
                      sheetId: String,
                      questionId: String,
                      imageNames: [String],
                      completionHandler: @escaping (Result<Void, Error>) -> Void)
    
    func getImageURLs(poiId: String,
                      sheetId: String,
                      questionId: String,
                      completionHandler: @escaping (Result<[(ImageSource, String)], Error>) -> Void)
    
    func getImageURLs(imageGSReferences: [String],
                      completionHandler: @escaping (Result<[ImageSource], Error>) -> Void)
}

final class StorageDataManager: StorageDataManagerProtocol {
    
    private let storageFirebaseManager: StorageFirebaseManagerProtocol = AppContainer.resolve()
    
    func uploadImage(image: UIImage,
                     poiId: String,
                     sheetId: String,
                     questionId: String,
                     completionHandler: @escaping (Result<String, Error>) -> Void) {
        storageFirebaseManager.uploadImage(image: image,
                                           poiId: poiId,
                                           sheetId: sheetId,
                                           questionId: questionId,
                                           completionHandler: {(result) in
                                            completionHandler(result)
                                           })
    }
    
    func uploadImages(images: [UIImage],
                      poiId: String,
                      sheetId: String,
                      questionId: String,
                      completionHandler: @escaping ([Result<String, Error>]) -> Void) {
        let dispatchGroup = DispatchGroup()
        var results = [Result<String, Error>]()
        for image in images {
            dispatchGroup.enter()
            uploadImage(image: image,
                        poiId: poiId,
                        sheetId: sheetId,
                        questionId: questionId) { result in
                // TODO: - improve uploading and add error handling
                // introduce object with name so when 2/4 picture were uploaded succesfully
                // (meaning we will still be on questionnaire screen) - we can delete those that were uploaded
                results.append(result)
                dispatchGroup.leave()
            }
        }
        
        dispatchGroup.notify(queue: .main) {
            completionHandler(results)
        }
    }
    
    func removeImages(poiId: String,
                      sheetId: String,
                      questionId: String,
                      imageNames: [String],
                      completionHandler: @escaping (Result<Void, Error>) -> Void) {
        let dispatchGroup = DispatchGroup()
        for imageName in imageNames {
            dispatchGroup.enter()
            removeImage(poiId: poiId,
                        sheetId: sheetId,
                        questionId: questionId,
                        imageName: imageName) { result in
                // TODO: - improve removing and add error handling
                dispatchGroup.leave()
            }
        }
        
        dispatchGroup.notify(queue: .main) {
            completionHandler(.success(()))
        }
    }
    
    func removeImage(poiId: String,
                     sheetId: String,
                     questionId: String,
                     imageName: String,
                     completionHandler: @escaping (Result<String, Error>) -> Void) {
        storageFirebaseManager.removeImage(poiId: poiId,
                                           sheetId: sheetId,
                                           questionId: questionId,
                                           imageName: imageName,
                                           completionHandler: {(result) in
                                            completionHandler(result)
                                           })
    }
    
    func getImageURLs(imageGSReferences: [String],
                      completionHandler: @escaping (Result<[ImageSource], Error>) -> Void) {
        storageFirebaseManager.getImageURLs(imageGSReferences: imageGSReferences,
                                            completionHandler: {(result) in
                                                completionHandler(result)
                                            })
    }
    
    public func getImageURLs(poiId: String,
                             sheetId: String,
                             questionId: String,
                             completionHandler: @escaping (Result<[(ImageSource, String)], Error>) -> Void) {
        storageFirebaseManager.getImageURLs(poiId: poiId,
                                            sheetId: sheetId,
                                            questionId: questionId,
                                            completionHandler: {(result) in
                                                completionHandler(result)
                                            })
    }
}
