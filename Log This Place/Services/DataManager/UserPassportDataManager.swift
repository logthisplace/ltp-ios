//
//  UserPassportDataManager.swift
//  Log This Place
//
//

import Foundation

// sourcery: mockable
protocol UserPassportDataManagerProtocol {
    func setAnswers(userPassport: UserPassport,
                    completionHandler: @escaping (Result<String, Error>) -> Void)
    
    func getUserPassport(completionHandler: @escaping (Result<UserPassport, Error>) -> Void)
}


final class UserPassportDataManager: UserPassportDataManagerProtocol {
    private let userPassportFirebaseManager: UserPassportFirebaseManagerProtocol = AppContainer.resolve()
    
    func setAnswers(userPassport: UserPassport,
                    completionHandler: @escaping (Result<String, Error>) -> Void) {
        userPassportFirebaseManager.setAnswers(userPassport: userPassport,
                                               completionHandler: {(result) in
                                                completionHandler(result)
                                               })
    }
    
    func getUserPassport(completionHandler: @escaping (Result<UserPassport, Error>) -> Void) {
        userPassportFirebaseManager.getUserPassport(completionHandler: {(result) in
            completionHandler(result)
        })
    }
}
