//
//  ImagePickerManager.swift
//  Log This Place
//
//

import PhotosUI
import MobileCoreServices

protocol ImagePickerManagerProtocol {
    func pickImage(callback: @escaping ((UIImage, String) -> ()))
}

protocol ImagePickerManagerContext: AnyObject {
    var view: UIView! { get }
    func present(_ viewControllerToPresent: UIViewController, animated flag: Bool, completion: (() -> Void)?)
}

// TODO: - Add optimization to not load image at full size (possible memory issue)
// TODO: - Check case when user can quickly tap another image after selecting first one
final class ImagePickerManager: NSObject, ImagePickerManagerProtocol {
    private lazy var picker = UIImagePickerController()
    private var alert: UIAlertController = UIAlertController(title: nil,
                                                             message: nil,
                                                             preferredStyle: UIDevice.current.userInterfaceIdiom == .pad ? .alert : .actionSheet)
    private weak var context: ImagePickerManagerContext?
    
    // callback with image and name
    private var pickImageCallback : ((UIImage, String) -> ())?
    
    init(context: ImagePickerManagerContext) {
        super.init()
        self.context = context
        
        let galleryAction = UIAlertAction(title: L10n.ImageSelector.ChooseFromPhotoLibrary.text, style: .default) { [weak self] _ in
            self?.openGallery()
        }
        
        let cameraAction = UIAlertAction(title: L10n.ImageSelector.TakeAPhoto.text, style: .default) { [weak self] _ in
            self?.openCamera()
        }
        
        let cancelAction = UIAlertAction(title: L10n.Common.Cancel.text, style: .cancel, handler: nil)
        
        picker.delegate = self
        alert.addAction(galleryAction)
        alert.addAction(cameraAction)
        alert.addAction(cancelAction)
    }

    public func pickImage(callback: @escaping ((UIImage, String) -> ())) {
        guard let context = context else { return }
        pickImageCallback = callback;

        if UIDevice.current.userInterfaceIdiom == .pad {
            alert.popoverPresentationController?.sourceView = context.view
            alert.popoverPresentationController?.sourceRect = context.view.bounds
            alert.popoverPresentationController?.permittedArrowDirections = [.down, .up]
        }
        
        context.present(alert, animated: true, completion: nil)
    }
    
    private func openCamera() {
        alert.dismiss(animated: true, completion: nil)
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            picker.sourceType = .camera
            context?.present(picker, animated: true, completion: nil)
        } else {
            showAlert(title: L10n.Common.Error.text, body: L10n.Common.SomethingWentWrong.text)
        }
    }
    
    private func openGallery() {
        alert.dismiss(animated: true, completion: nil)
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
            picker.sourceType = .photoLibrary
            context?.present(picker, animated: true, completion: nil)
        } else {
            showAlert(title: L10n.Common.Error.text, body: L10n.Common.SomethingWentWrong.text)
        }
    }
    
    private func showAlert(title: String, body: String?) {
        let alertController = UIAlertController(title: title,
                                                message: body,
                                                preferredStyle: UIAlertController.Style.alert)

        let okAction = UIAlertAction(title: L10n.Common.Ok.text, style: UIAlertAction.Style.default)
        alertController.addAction(okAction)
        context?.present(alertController, animated: true, completion: nil)
    }
    
    private func processImageFromCamera(info: [UIImagePickerController.InfoKey : Any]) {
        DispatchQueue.global(qos: .userInitiated).async {
            guard let image = info[.originalImage] as? UIImage,
                  let imageData = image.jpegData(compressionQuality: 0.7),
                  let compressedImage = UIImage(data: imageData) else {
                let errorTxt: String = String(format:"%@ %@", "Expected a dictionary containing an image, but was provided the following", info)
                
                executeOnMainThread { [weak self] in
                    self?.showAlert(title: L10n.Common.Error.text,
                                    body: errorTxt)
                }
                return
            }
            
            executeOnMainThread { [weak self] in
                self?.pickImageCallback?(compressedImage, UUID().uuidString)
            }
        }
    }
    
    private func processImageFromLibrary(url: URL) {
        DispatchQueue.global(qos: .userInitiated).async {
            let sourceOptions = [kCGImageSourceShouldCache: false] as CFDictionary
            let downsampleOptions = [
                kCGImageSourceCreateThumbnailFromImageAlways: true,
                kCGImageSourceCreateThumbnailWithTransform: true,
                kCGImageSourceThumbnailMaxPixelSize: 2000
            ] as CFDictionary
            let data = NSMutableData()
            
            guard let source = CGImageSourceCreateWithURL(url as CFURL, sourceOptions),
                  let cgImage = CGImageSourceCreateThumbnailAtIndex(source, 0, downsampleOptions),
                  let imageDestination = CGImageDestinationCreateWithData(data, kUTTypeJPEG, 1, nil) else {
                executeOnMainThread { [weak self] in
                    self?.showAlert(title: L10n.Common.SomethingWentWrong.text, body: nil)
                }
                return
            }
            
            let destinationProperties = [
                kCGImageDestinationLossyCompressionQuality: 0.75
            ] as CFDictionary
            
            CGImageDestinationAddImage(imageDestination, cgImage, destinationProperties)
            CGImageDestinationFinalize(imageDestination)
            
            guard let imageToBeReturned = UIImage(data: data as Data) else {
                executeOnMainThread { [weak self] in
                    self?.showAlert(title: L10n.Common.SomethingWentWrong.text, body: nil)
                }
                return
            }
            
            executeOnMainThread { [weak self] in
                self?.pickImageCallback?(imageToBeReturned, UUID().uuidString)
            }
        }
    }
}

extension ImagePickerManager: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated: true, completion: nil)
        
        guard let url = info[.imageURL] as? URL else {
            processImageFromCamera(info: info)
            return
        }
        
        processImageFromLibrary(url: url)
    }
}
