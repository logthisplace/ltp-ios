//
//  LocationManager.swift
//  Rock This Place
//

import CoreLocation
import GoogleMaps

protocol LocationManagerDelegate {
    func didFetchLocation(_ location: CLLocationCoordinate2D)
    func didFailToFetchLocation(error: Error)
    func authrorizationStatusDenied()
}

protocol LocationManagerProtocol: AnyObject {
    var delegate: LocationManagerDelegate? { get set }
    
    func requestLocation()
}

final class LocationManager: NSObject, LocationManagerProtocol {

    var delegate: LocationManagerDelegate?
        
    private lazy var systemLocationManger = CLLocationManager()
    
    override init() {
        super.init()
        systemLocationManger.desiredAccuracy = kCLLocationAccuracyHundredMeters
    }
    
    func requestLocation() {
        systemLocationManger.delegate = self
        let status: CLAuthorizationStatus?
        
        if #available(iOS 14.0, *) {
            status = systemLocationManger.authorizationStatus
        } else {
            status = CLLocationManager.authorizationStatus()
        }
        
        switch status {
        case .authorizedWhenInUse, .authorizedAlways:
            requstSystemLocationManger()
            break
        case .denied, .restricted:
            delegate?.authrorizationStatusDenied()
            break
        case .notDetermined:
            requstSystemLocationManger()
            break
        default:
            break
        }
    }
    
    private func requstSystemLocationManger() {
        systemLocationManger.requestWhenInUseAuthorization()
        systemLocationManger.requestLocation()
    }
}

extension LocationManager: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location = locations.last else {
            return
        }
        
        delegate?.didFetchLocation(.init(latitude: location.coordinate.latitude,
                                         longitude: location.coordinate.longitude))
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        delegate?.didFailToFetchLocation(error: error)
    }
}
