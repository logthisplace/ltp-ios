//
//  LoggerFirebaseManager.swift
//  Log This Place
//
//

import StoreKit
import FirebaseFirestoreSwift
import Firebase

struct Log: Codable {
    @ServerTimestamp var created_at: Timestamp?
    let level: String
    let message: String
    var user_id = (AppContainer.resolve() as UserManagerProtocol).currentUser?.uid
    var client_id = "ios_app"
    var ios_device = UIDevice.current.model
    var ios_version_number = UIDevice.current.systemVersion
    var ios_app_line: UInt
    var ios_app_function: String
}


protocol LoggerFirebaseManagerProtocol {
    func logError(with log: Log)
}


final class LoggerFirebaseManager: LoggerFirebaseManagerProtocol {
    
    private let db: FirestoreProtocol = AppContainer.resolve()
    
    func logError(with log: Log) {
        
        do {
            try _ = db.collection(FirestoreDatabase.Log.COLLECTION_NAME).addDocument(from: log, completion: nil)
        } catch let error{
            print(error)
        }
    }
}
