//
//  ProfileCoordinator.swift
//  Log This Place
//
//

import UIKit


protocol ProfileCoordinatorProtocol: Coordinator {
    func showProfile(viewModel: ProfileViewModel, mapViewPresenterInformable: MapViewPresenterInformable)
    func showQuestionnaireGroup(mode: QuestionnaireViewMode, inforable: ProfilePresenterInforable)
    func showRegistration(informable: ProfilePresenterInforable)
    
    func didBackNavigationTapped(_ didUpdateProfile: Bool)
    func didLogoutTapped()
    func deletedAccount()
}

final class ProfileCoordinator: BaseCoordinator, ProfileCoordinatorProtocol  {
    var onFinish: (() -> Void)?
    
    let navigationController: UINavigationController
    
    weak var mapViewPresenterInformable: MapViewPresenterInformable?
    
    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }

    func showProfile(viewModel: ProfileViewModel,
                     mapViewPresenterInformable: MapViewPresenterInformable) {
        self.mapViewPresenterInformable = mapViewPresenterInformable
        
        let profilePresenter = ProfilePresenter(coordinator: self, viewModel: viewModel)
        let profileViewController = ProfileViewController(presenter: profilePresenter)
        profilePresenter.view = profileViewController
        navigationController.pushViewController(profileViewController, animated: true)
    }
    
    func showQuestionnaireGroup(mode: QuestionnaireViewMode,
                                inforable: ProfilePresenterInforable) {
        let coordinator = QuestionnaireCoordinator(navigationController: navigationController)
        add(coordinator: coordinator)
        coordinator.onFinish = { [weak coordinator, weak self] in
            guard let coordinator = coordinator else { return }
            self?.remove(coordinator: coordinator)
        }
        
        coordinator.showQuestionnaire(mode: mode, profilePresenterInforable: inforable)
    }
    
    func showRegistration(informable: ProfilePresenterInforable) {
        let coordinator = UserIdentificationCoordinator(navigationController: navigationController)
        coordinator.showRegistration(registrationType: .permanent)
        add(coordinator: coordinator)
        coordinator.onFinish = { [weak coordinator, weak self] in
            guard let coordinator = coordinator else { return }
            self?.remove(coordinator: coordinator)
            informable.didRegisterPermanentAccount()
        }
    }
    
    func didBackNavigationTapped(_ didUpdateProfile: Bool) {
        if didUpdateProfile {
            mapViewPresenterInformable?.didUpdateUserPassport()
        }
        onFinish?()
    }
    
    func didLogoutTapped() {
        navigationController.popViewController(animated: true)
        mapViewPresenterInformable?.didLogoutTapped()
        onFinish?()
    }
    
    func deletedAccount() {
        navigationController.viewControllers.removeAll()
        let mapCoordinator: MapCoordinatorProtocol = MapCoordinator(navigationController: navigationController)
        mapCoordinator.showMap()
    }
}
