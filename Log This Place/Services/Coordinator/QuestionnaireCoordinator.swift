//
//  QuestionnaireCoordinator.swift
//  Log This Place
//

import UIKit
import GooglePlaces
import DKPhotoGallery


protocol QuestionnaireCoordinatorProtocol {
    
    func showGallery(items: [DKPhotoGalleryItem], presentationIndex: Int)
    func showPoiSummary(viewModel: SummaryPoiViewModel)
    func showQuestionnaire(mode: QuestionnaireViewMode,
                           mapViewPresenterInformable: MapViewPresenterInformable?,
                           buildingDetailsPresenterInformable: BuildingDetailsPresenterInformable?,
                           summaryPOIPresenterInforable: SummaryPoiPresenterInformable?,
                           profilePresenterInforable: ProfilePresenterInforable?)
    
    func didUpdatePoiAnswerSheet()
    func didCreatePoiAnswerSheet()
    func didCreateProfileAnswerSheet()
    func didUpdateProfileAnswerSheet(_ userPassport: UserPassport)
    
    func failedToFetchData()

    
    func closeQuestionnaire()
}

extension QuestionnaireCoordinatorProtocol {
    func showQuestionnaire(mode: QuestionnaireViewMode,
                           mapViewPresenterInformable: MapViewPresenterInformable? = nil,
                           buildingDetailsPresenterInformable: BuildingDetailsPresenterInformable? = nil,
                           summaryPOIPresenterInforable: SummaryPoiPresenterInformable? = nil,
                           profilePresenterInforable: ProfilePresenterInforable? = nil) {
        showQuestionnaire(mode: mode,
                          mapViewPresenterInformable: mapViewPresenterInformable,
                          buildingDetailsPresenterInformable: buildingDetailsPresenterInformable,
                          summaryPOIPresenterInforable: summaryPOIPresenterInforable,
                          profilePresenterInforable: profilePresenterInforable)
    }
}

final class QuestionnaireCoordinator: BaseCoordinator, QuestionnaireCoordinatorProtocol {
    var onFinish: (() -> Void)?
    
    weak var mapViewPresenterInformable: MapViewPresenterInformable?
    weak var summaryPoiPresenterInforable: SummaryPoiPresenterInformable?
    weak var buildingDetailsPresenterInformable: BuildingDetailsPresenterInformable?
    weak var profilePresenterInforable: ProfilePresenterInforable?
    
    let navigationController: UINavigationController

    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
    
    func showGallery(items: [DKPhotoGalleryItem], presentationIndex: Int) {
        if !items.isEmpty {
            let gallery = DKPhotoGallery()
            gallery.singleTapMode = .toggleControlView
            gallery.items = items
            gallery.presentationIndex = presentationIndex
            navigationController.present(photoGallery: gallery)
        }
    }
    
    func showQuestionnaire(mode: QuestionnaireViewMode,
                           mapViewPresenterInformable: MapViewPresenterInformable? = nil,
                           buildingDetailsPresenterInformable: BuildingDetailsPresenterInformable? = nil,
                           summaryPOIPresenterInforable: SummaryPoiPresenterInformable? = nil,
                           profilePresenterInforable: ProfilePresenterInforable? = nil) {
        
        self.mapViewPresenterInformable = mapViewPresenterInformable
        self.summaryPoiPresenterInforable = summaryPOIPresenterInforable
        self.buildingDetailsPresenterInformable = buildingDetailsPresenterInformable
        self.profilePresenterInforable = profilePresenterInforable
        
        let presenter = QuestionnairePresenter(mode: mode, coordinator: self)
        let viewManager = QuestionnaireViewManager()
        let viewController = QuestionnaireViewController(presenter: presenter,
                                                         viewManager: viewManager)
        presenter.view = viewController
        navigationController.setNavigationBarHidden(true, animated: false)
        navigationController.pushViewController(viewController, animated: true)
    }
    
    func closeQuestionnaire() {
        onFinish?()
        navigationController.popViewController(animated: true)
    }

    func showPoiSummary(viewModel: SummaryPoiViewModel) {
        let coordinator = SummaryPoiCoordinator(navigationController: navigationController)
        add(coordinator: coordinator)
        coordinator.onFinish = { [weak coordinator, weak self] in
            guard let coordinator = coordinator else { return }
            self?.remove(coordinator: coordinator)
        }
        coordinator.showSummary(viewModel: viewModel,
                                mapViewPresenterInformable: mapViewPresenterInformable,
                                buildingDetailsPresenterInformable: buildingDetailsPresenterInformable)
    }
    
    func didUpdatePoiAnswerSheet() {
        summaryPoiPresenterInforable?.didUpdateAnswerSheet()
        navigationController.popViewController(animated: true)
        onFinish?()
    }
    
    func didCreatePoiAnswerSheet() {
        summaryPoiPresenterInforable?.didUpdateAnswerSheet()
        navigationController.popViewController(animated: true)
        onFinish?()
    }
    
    func didCreateProfileAnswerSheet() {
        mapViewPresenterInformable?.didUpdateUserPassport()
        navigationController.popViewController(animated: true)
        onFinish?()
    }
    
    func didUpdateProfileAnswerSheet(_ userPassport: UserPassport) {
        profilePresenterInforable?.didUpdateProfile(userPassport)
        navigationController.popViewController(animated: true)
        onFinish?()
    }
    
    func failedToFetchData() {
        navigationController.popViewController(animated: true)
        onFinish?()
    }
    
    @objc private func dismissPhotoPreview() {
        navigationController.presentedViewController?.dismiss(animated: true, completion: nil)
    }
}
