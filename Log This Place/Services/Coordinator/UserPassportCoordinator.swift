//
//  ProfileOnboardingCoordinator.swift
//  Rock This Place
//
//

import UIKit


protocol UserPassportCoordinatorProtocol {
    
    func showUserPassportQuestionScreen()
    func showCompletedScreen()
}

final class UserPassportCoordinator: BaseCoordinator, UserPassportCoordinatorProtocol {
    
    let stroryboardName = StoryboardName.userPassport
    let navigationController: UINavigationController
    let userIdetificationCoordinatorOnboarding: UserIdentificationCoordinatorProtocol

    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
        self.userIdetificationCoordinatorOnboarding = UserIdentificationCoordinator(navigationController: self.navigationController)
    }
    
    func showUserPassportQuestionScreen() {
        let viewСontroller = ProfileQuestionViewController.instantiate(storyboardName: stroryboardName)
        viewСontroller.coordinator = self
        self.navigationController.pushViewController(viewСontroller, animated: true)
    }
    
    func showCompletedScreen() {
        let viewСontroller = ProfileQuestionsCompletedViewController.instantiate(storyboardName: stroryboardName)
        viewСontroller.coordinator = self
        self.navigationController.pushViewController(viewСontroller, animated: true)
    }
    
}
