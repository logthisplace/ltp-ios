//
//  UserIdentificationCoordinator.swift
//  Log This Place
//

import UIKit


protocol UserIdentificationCoordinatorProtocol {
    func showWelcome()
    func showLogin()
    func showRegistration(registrationType: RegistrationType)
    func showUserPassport(userPassport: UserPassport?)
    func closeWelcome()
    func completePermanentAccount()
    func deletedAccount()
}

extension UserIdentificationCoordinatorProtocol {
    func showUserPassport(userPassport: UserPassport? = nil) {
        showUserPassport(userPassport: userPassport)
    }
}

final class UserIdentificationCoordinator: BaseCoordinator, UserIdentificationCoordinatorProtocol {
    
    let storyboardName = StoryboardName.userIdentification
    let navigationController: UINavigationController
    
    var onFinish: (() -> Void)?
    var onCompleteAuth: ((UserPassport?) -> Void)?

    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
    
    func showWelcome() {
        let welcomePresenter: WelcomePresenter = WelcomePresenter(coordinator: self)
        let welcomeViewСontroller = WelcomeViewController(presenter: welcomePresenter)
        welcomePresenter.view = welcomeViewСontroller
        navigationController.pushViewController(welcomeViewСontroller, animated: true)
    }
    
    func showLogin() {
        let userIdentificationPresenter = LoginPresenter(coordinator: self)
        let authorizationViewController = LoginViewController.initialize(presenter: userIdentificationPresenter,
                                                                                 storyboardName: storyboardName)
        userIdentificationPresenter.view = authorizationViewController
        navigationController.pushViewController(authorizationViewController, animated: true)
    }
    
    func showRegistration(registrationType: RegistrationType) {
        let userIdentificationPresenter = RegistrationPresenter(coordinator: self,
                                                                registrationType: registrationType)
        let registrationViewController = RegistrationViewController.initialize(presenter: userIdentificationPresenter,
                                                                               storyboardName: storyboardName)

        userIdentificationPresenter.view = registrationViewController
        
        switch registrationType {
        case .general:
            navigationController.pushViewController(registrationViewController, animated: true)
        case .permanent:
            if UIDevice.current.userInterfaceIdiom == .pad {
                registrationViewController.popoverPresentationController?.sourceView = registrationViewController.view
                registrationViewController.popoverPresentationController?.sourceRect = CGRect(x: registrationViewController.view.bounds.midX,
                                                                                              y: registrationViewController.view.bounds.midY,
                                                                                              width: 0,
                                                                                              height: 0)
            }
            navigationController.present(registrationViewController, animated: true, completion: nil)
        }
    }
    
    func showUserPassport(userPassport: UserPassport?) {
        onCompleteAuth?(userPassport)
    }
    
    func closeWelcome() {
        navigationController.popViewController(animated: true)
        onFinish?()
    }
    
    func completePermanentAccount() {
        navigationController.dismiss(animated: true, completion: {[weak self] in
            self?.onFinish?()
        })
    }
    
    func deletedAccount() {
        navigationController.viewControllers.removeAll()
        let mapCoordinator: MapCoordinatorProtocol = MapCoordinator(navigationController: navigationController)
        mapCoordinator.showMap()
    }
}
