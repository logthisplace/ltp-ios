//
//  MapCoordinator.swift
//  Log This Place
//
//

import UIKit
import GooglePlaces

protocol MapCoordinatorProtocol {
    
    func showMap()
    func showOnboarding()
    func showSelectedPoiScreen(input: SelectedPointOfInterestInput,
                               mapViewPresenterInformable: MapViewPresenterInformable?)
    
    func showAddPoi(mode: QuestionnaireViewMode)
    func showUserProfile(with model: ProfileViewModel,
                         mapViewPresenterInformable: MapViewPresenterInformable)
    
    func showBuildingDetails(buildingDetailViewModel: BuildingDetailViewModel)
    
    func dismissBottomPanel()
    func showUserIdentification(mapViewPresenterInformable: MapViewPresenterInformable)
    
    func showSearchPois(mapViewPresenterInformable: MapViewPresenterInformable?)
    func dismissSearchPois()
    func showMandatoryUpdateAlert(with model: MandatoryUpdateAlertView.ViewModel)
}

final class MapCoordinator: BaseCoordinator, MapCoordinatorProtocol {
    
    let storyboardName = StoryboardName.map
    let navigationController: UINavigationController
    
    private lazy var transitioningDelegate = BottomSlideTransitioningDelegate()
    
    private weak var selectedPoiPresenter: SelectedPointOfInterestPresenterProtocol?
    private weak var mapViewPresenterInformable: MapViewPresenterInformable?

    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
    
    func showMap() {
        let mapPresenter = MapViewPresenter(coordinator: self)
        let mapViewСontroller = MapViewController.initialize(presenter: mapPresenter, storyboardName: storyboardName)
        mapPresenter.view = mapViewСontroller

        navigationController.pushViewController(mapViewСontroller, animated: true)
    }
    
    func showSearchPois(mapViewPresenterInformable: MapViewPresenterInformable?) {
        dismissBottomPanel()
        let autocompleteSearchPresenter = AutocompleteSearchPresenter(coordinator: self,
                                                                      informable: mapViewPresenterInformable)
        let autocompleteSearchViewController = AutocompleteSearchViewController(presenter: autocompleteSearchPresenter)
        autocompleteSearchPresenter.view = autocompleteSearchViewController
        let navCon = UINavigationController(rootViewController: autocompleteSearchViewController)
        autocompleteSearchViewController.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(dismissSearchPois))
        navigationController.present(navCon, animated: true, completion: nil)
    }
    
    @objc func dismissSearchPois() {
        navigationController.presentedViewController?.dismiss(animated: true, completion: nil)
    }
    
    func showOnboarding() {
        let onboardingPresenter = OnboardingPresenter(coordinator: self)
        let onboardingController = OnboardingViewController(presenter: onboardingPresenter)
        onboardingPresenter.view = onboardingController
        navigationController.pushViewController(onboardingController, animated: false)
    }
    
    func showSelectedPoiScreen(input: SelectedPointOfInterestInput,
                                                              mapViewPresenterInformable: MapViewPresenterInformable?) {
        guard let presenter = selectedPoiPresenter else {
            let presenter = SelectedPointOfInterestPresenter(input: input, coordinator: self)
            let selectedPoiViewController = SelectedPointOfInterestViewController.initialize(presenter: presenter, storyboardName: storyboardName)
            let interactiveTransitionController = BottomSlidePercentDrivenInteractiveTransitionController()
            transitioningDelegate.percentDrivenTransitionController = interactiveTransitionController
            let bottomSlideViewController = BottomSlideContainerViewController(embededViewController: selectedPoiViewController,
                                                                               percentDrivenTransitionController: interactiveTransitionController)
            bottomSlideViewController.transitioningDelegate = transitioningDelegate
            bottomSlideViewController.modalPresentationStyle = .custom
            bottomSlideViewController.userWantsToInteracitvelyDismiss = { [weak bottomSlideViewController] in
                bottomSlideViewController?.dismiss(animated: true, completion: nil)
            }
            bottomSlideViewController.onViewDidDisappear = { [weak self] in
                self?.selectedPoiPresenter = nil
                self?.mapViewPresenterInformable?.didDismissSelectedPoi()
            }
            
            presenter.view = selectedPoiViewController
        
            navigationController.present(bottomSlideViewController, animated: true, completion: nil)
            selectedPoiPresenter = presenter
            self.mapViewPresenterInformable = mapViewPresenterInformable
            return
        }
        presenter.input = input
    }
    
    func showBuildingDetails(buildingDetailViewModel: BuildingDetailViewModel) {
        dismissBottomPanel()
        let coordinator = BuildingCoordinator(navigationController: navigationController)
        add(coordinator: coordinator)
        coordinator.onFinish = { [weak coordinator, weak self] placeId in
            guard let coordinator = coordinator else { return }
            self?.remove(coordinator: coordinator)
        }
        
        coordinator.showBuildingDetails(buildingDetailViewModel: buildingDetailViewModel,
                                        mapViewPresenterInformable: mapViewPresenterInformable)
    }
    
    func showAddPoi(mode: QuestionnaireViewMode) {
        dismissBottomPanel()
        let coordinator = QuestionnaireCoordinator(navigationController: navigationController)
        add(coordinator: coordinator)
        coordinator.onFinish = { [weak coordinator, weak self] in
            guard let coordinator = coordinator else { return }
            self?.remove(coordinator: coordinator)
        }
        
        coordinator.showQuestionnaire(mode: mode, mapViewPresenterInformable: mapViewPresenterInformable)
    }
    
    func showUserProfile(with model: ProfileViewModel,
                         mapViewPresenterInformable: MapViewPresenterInformable) {
        dismissBottomPanel()
        let coordinator = ProfileCoordinator(navigationController: navigationController)
        add(coordinator: coordinator)
        coordinator.onFinish = { [weak coordinator, weak self] in
            guard let coordinator = coordinator else { return }
            self?.remove(coordinator: coordinator)
        }
        coordinator.showProfile(viewModel: model,
                                mapViewPresenterInformable: mapViewPresenterInformable)
    }
    
    func dismissBottomPanel() {
        if let selectedPoiBottomPanel = navigationController.presentedViewController as? BottomSlideContainerViewController<SelectedPointOfInterestViewController>  {
            selectedPoiBottomPanel.dismiss(animated: true, completion: nil)
        }
    }
    
    func showUserIdentification(mapViewPresenterInformable: MapViewPresenterInformable) {
        dismissBottomPanel()
        
        self.mapViewPresenterInformable = mapViewPresenterInformable
        
        let coordinator = UserIdentificationCoordinator(navigationController: navigationController)
        add(coordinator: coordinator)
        
        coordinator.onFinish = { [weak coordinator, weak self] in
            guard let coordinator = coordinator else { return }
            self?.remove(coordinator: coordinator)
        }
        
        coordinator.onCompleteAuth = { [weak coordinator, weak self] userPassport in
            guard let coordinator = coordinator,
                  let self = self else { return }
            self.remove(coordinator: coordinator)
            
            if let userPassport = userPassport {
                self.showUserProfile(with: .init(didLogin: true, userPassport: userPassport),
                                     mapViewPresenterInformable: mapViewPresenterInformable)
            } else {
                let questionnaireCoordinator = QuestionnaireCoordinator(navigationController: self.navigationController)
                questionnaireCoordinator.showQuestionnaire(mode: .add(input: .profile),
                                                           mapViewPresenterInformable: mapViewPresenterInformable)
            }
            
            // TODO: Improve it
            for vc in self.navigationController.viewControllers {
                if vc.isKind(of: WelcomeViewController.self) ||
                    vc.isKind(of: LoginViewController.self) ||
                    vc.isKind(of: RegistrationViewController.self) {
                    vc.removeFromParent()
                }
            }
        }
        
        coordinator.showWelcome()
    }
    
    func showMandatoryUpdateAlert(with model: MandatoryUpdateAlertView.ViewModel) {
        let presenter = MandatoryUpdateAlertPresenter(alertViewModel: model)
        let alertController = MandatoryUpdateAlertViewController(presenter: presenter)
        presenter.view = alertController
        alertController.modalPresentationStyle = .overCurrentContext
        navigationController.present(alertController, animated: true, completion: nil)
    }
}
