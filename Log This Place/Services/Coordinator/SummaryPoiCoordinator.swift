//
//  Log This Place
//
//

import UIKit
import DKPhotoGallery

protocol SummaryPoiCoordinatorProtocol: Coordinator {
    func showSummary(viewModel: SummaryPoiViewModel,
                     mapViewPresenterInformable: MapViewPresenterInformable?,
                     buildingDetailsPresenterInformable: BuildingDetailsPresenterInformable?)
    func showQuestionnaireGroup(mode: QuestionnaireViewMode,
                                summaryPOIPresenterInforable: SummaryPoiPresenterInformable)
    func showGallery(items: [DKPhotoGalleryItem], presentationIndex: Int)
    func didBackNavigationTapped(answerSheetWasUpdated: Bool)
    func didBackTapped(answerSheetWasUpdated: Bool)
}

extension SummaryPoiCoordinatorProtocol {
    func showSummary(viewModel: SummaryPoiViewModel,
                     mapViewPresenterInformable: MapViewPresenterInformable? = nil,
                     buildingDetailsPresenterInformable: BuildingDetailsPresenterInformable? = nil) {
        showSummary(viewModel: viewModel,
                    mapViewPresenterInformable: mapViewPresenterInformable,
                    buildingDetailsPresenterInformable: buildingDetailsPresenterInformable)
    }
}

final class SummaryPoiCoordinator: BaseCoordinator, SummaryPoiCoordinatorProtocol  {
    var onFinish: (() -> Void)?
    
    weak var mapViewPresenterInformable: MapViewPresenterInformable?
    weak var buildingDetailsPresenterInformable: BuildingDetailsPresenterInformable?
    
    var viewModel: SummaryPoiViewModel?
    
    let navigationController: UINavigationController
    
    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
    
    func showSummary(viewModel: SummaryPoiViewModel,
                     mapViewPresenterInformable: MapViewPresenterInformable? = nil,
                     buildingDetailsPresenterInformable: BuildingDetailsPresenterInformable? = nil) {
        self.mapViewPresenterInformable = mapViewPresenterInformable
        self.buildingDetailsPresenterInformable = buildingDetailsPresenterInformable
        
        self.viewModel = viewModel
        
        let summaryPresenter = SummaryPoiPresenter(coordinator: self, viewModel: viewModel)
        let summaryViewСontroller = SummaryPoiViewController(presenter: summaryPresenter)
        summaryPresenter.view = summaryViewСontroller
        
        navigationController.pushViewController(summaryViewСontroller, animated: true)
    }
    
    func showQuestionnaireGroup(mode: QuestionnaireViewMode,
                                summaryPOIPresenterInforable: SummaryPoiPresenterInformable) {
        let coordinator = QuestionnaireCoordinator(navigationController: navigationController)
        add(coordinator: coordinator)
        coordinator.onFinish = { [weak coordinator, weak self] in
            guard let coordinator = coordinator else { return }
            self?.remove(coordinator: coordinator)
        }
        coordinator.showQuestionnaire(mode: mode,
                                      summaryPOIPresenterInforable: summaryPOIPresenterInforable)
    }
    
    func showGallery(items: [DKPhotoGalleryItem], presentationIndex: Int) {
        if !items.isEmpty {
            let gallery = DKPhotoGallery()
            gallery.singleTapMode = .toggleControlView
            gallery.items = items
            gallery.presentationIndex = presentationIndex
            navigationController.present(photoGallery: gallery)
        }
    }
    
    func didBackNavigationTapped(answerSheetWasUpdated: Bool) {
        /// refresh map screen
        if case let .add(addInput) = viewModel?.processType, case let .poi(input) = addInput {
            removeQuestionnaire()
        
        /// refresh buildgin details page
        } else if case let .add(addInput) = viewModel?.processType, case .answerSheet = addInput {
            removeQuestionnaire()
            buildingDetailsPresenterInformable?.didUpdateBuildingDetail()
            
        } else if answerSheetWasUpdated {
            buildingDetailsPresenterInformable?.didUpdateBuildingDetail()
        }
        
        onFinish?()
    }
    
    func didBackTapped(answerSheetWasUpdated: Bool) {
        didBackNavigationTapped(answerSheetWasUpdated: answerSheetWasUpdated)
        navigationController.popViewController(animated: true)
    }
    
    // TODO: Improve it
    private func removeQuestionnaire() {
        for vc in navigationController.viewControllers {
            if vc.isKind(of: QuestionnaireViewController.self) {
                vc.removeFromParent()
            }
        }
    }
}
