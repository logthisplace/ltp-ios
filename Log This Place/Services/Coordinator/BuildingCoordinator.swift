//
//  BuildingCoordinator.swift
//  Log This Place
//

import UIKit
import DKPhotoGallery

protocol BuildingCoordinatorProtocol: Coordinator {
    func showBuildingDetails(buildingDetailViewModel: BuildingDetailViewModel,
                             mapViewPresenterInformable: MapViewPresenterInformable?)
    func showQuestionnaire(mode: QuestionnaireViewMode,
                           buildingDetailsPresenterInformable: BuildingDetailsPresenterInformable?)
    func didBackTapped()
    func showGallery(items: [DKPhotoGalleryItem], presentationIndex: Int)
    func showSummary(viewModel: SummaryPoiViewModel, informable: BuildingDetailsPresenterInformable)
}

final class BuildingCoordinator: BaseCoordinator, BuildingCoordinatorProtocol  {
    
    var onFinish: ((String) -> Void)?
    
    var buildingDetailViewModel: BuildingDetailViewModel?
    
    let navigationController: UINavigationController

    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
    
    func didBackTapped() {
        if let placeId = buildingDetailViewModel?.firebasePOI?.g_places_id {
            onFinish?(placeId)
        }
    }
    
    func showSummary(viewModel: SummaryPoiViewModel,
                     informable: BuildingDetailsPresenterInformable) {
        let coordinator = SummaryPoiCoordinator(navigationController: navigationController)
        add(coordinator: coordinator)
        coordinator.onFinish = { [weak coordinator, weak self] in
            guard let coordinator = coordinator else { return }
            self?.remove(coordinator: coordinator)
        }
        coordinator.showSummary(viewModel: viewModel, buildingDetailsPresenterInformable: informable)
    }
    
    func showBuildingDetails(buildingDetailViewModel: BuildingDetailViewModel,
                             mapViewPresenterInformable: MapViewPresenterInformable?) {
        self.buildingDetailViewModel = buildingDetailViewModel
        
        let presenter = BuildingDetailsPresenter(coordinator: self,
                                                 buildingDetailViewModel: buildingDetailViewModel,
                                                 mapViewPresenterInformable: mapViewPresenterInformable)
        let buildingViewController = BuildingDetailsViewController(presenter: presenter)
        presenter.view = buildingViewController
        navigationController.pushViewController(buildingViewController, animated: true)
    }
    
    func showGallery(items: [DKPhotoGalleryItem], presentationIndex: Int) {
        if !items.isEmpty {
            let gallery = DKPhotoGallery()
            gallery.singleTapMode = .toggleControlView
            gallery.items = items
            gallery.presentationIndex = presentationIndex
            navigationController.present(photoGallery: gallery)
        }
    }
    
    func showQuestionnaire(mode: QuestionnaireViewMode,
                           buildingDetailsPresenterInformable: BuildingDetailsPresenterInformable?) {
        let coordinator = QuestionnaireCoordinator(navigationController: navigationController)
        add(coordinator: coordinator)
        coordinator.onFinish = { [weak coordinator, weak self] in
            guard let coordinator = coordinator else { return }
            self?.remove(coordinator: coordinator)
        }
        coordinator.showQuestionnaire(mode: mode,
                                      buildingDetailsPresenterInformable: buildingDetailsPresenterInformable)
    }
    
    @objc private func dismissPhotoPreview() {
        navigationController.presentedViewController?.dismiss(animated: true, completion: nil)
    }
}
