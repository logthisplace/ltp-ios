//
//  FirebaseAnalytic.swift
//  Log This Place
//
//

import StoreKit
import FirebaseAnalytics

enum AnalyticAction: String {
    case addedPoi = "added_new_poi_action"
    case updatedPoi = "updated_poi_action"
    case openAppstore = "open_appstore"
}

enum AnalyticScreen: String {
    case map = "map_screen"
    case drawer = "poi_drawer_bottom_panel"
    case profile = "profile_screen"
    case login = "login_screen"
    case welcome = "welcome_screen"
    case register = "register_screen"
    case poiDetail = "poi_detail_screen"
    case search = "search_screen"
    case addPoi = "add_new_poi_screen"
    case addAnswerSheet = "add_new_answer_sheet_screen"
    case editAnswerSheet = "edit_questionnaire_answer_sheet_screen"
    case addProfileSheet = "add_profile_answer_sheet_screen"
    case editProfileSheet = "edit_profile_answer_sheet_screen"
    case onboarding = "onboarding_screen"
    case mandatoryUpdate = "mandatory_update_alert"
    case summary = "summary_poi_screen"
}

protocol FirebaseAnalyticManagerProtocol {
    func logEvent(action: AnalyticAction)
    func logEvent(screen: AnalyticScreen)
}


final class FirebaseAnalyticManager: FirebaseAnalyticManagerProtocol {
    
    private let userManager: UserManagerProtocol = AppContainer.resolve()
    
    func logEvent(action: AnalyticAction) {
        logEvent(action.rawValue)
    }
    
    func logEvent(screen: AnalyticScreen) {
        logEvent(screen.rawValue)
    }
    
    private func logEvent(_ eventName: String) {
        Analytics.logEvent(eventName,
                           parameters: ["device_uuid": userManager.deviceUUID,
                                        "user_id": userManager.currentUser?.uid ?? "",
                                        "timestamp": Date().timeIntervalSince1970,
                                        "ios_version": UIDevice.current.systemVersion,
                                        "device_model": UIDevice.current.model,
                                        "device_name": UIDevice.current.name])
    }
}
