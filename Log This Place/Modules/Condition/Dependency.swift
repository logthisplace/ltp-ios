//
//  Dependency.swift
//  Log This Place
//
//

import Foundation

protocol Dependency {
    func hasDepends() -> Bool
}
