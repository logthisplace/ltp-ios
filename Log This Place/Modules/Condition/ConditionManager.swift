//
//  ConditionManager.swift
//  Log This Place
//
//

import Foundation

protocol ConditionManagerProtocol {
    func isConditionBasedOnAnswers(condition: Condition, answeSheet: [String: Any], generateAnswersForNotCondition: Bool) -> Bool
}

final class ConditionManager: ConditionManagerProtocol {
    
    /// For convenient work, we convect [String: Any] answers to [String: Bool]
    /// - Parameter answerSheet: can be POI answerSheet or Profile answerSheet
    private func mapAnswerSheetToBoolAnswers(answerSheet: [String: Any]) -> [String: Bool] {
        var mapedAnswerSheet = [String: Bool]()
        
        for answer in answerSheet {
            if let boolAnswer = answer.value as? Bool {
                mapedAnswerSheet[answer.key] = boolAnswer
            }
            
            if answer.value is Int {
                mapedAnswerSheet[answer.key] = true
            }
            
            if let stringAnswer = answer.value as? String {
                if !stringAnswer.isEmpty {
                    if stringAnswer == QuestionnaireCheckboxSetView.falseAnswerKey {
                        mapedAnswerSheet[answer.key] = false
                    } else if stringAnswer == QuestionnaireCheckboxSetView.trueAnswerKey {
                        mapedAnswerSheet[answer.key] = true
                    } else {
                        mapedAnswerSheet[answer.key] = true
                        mapedAnswerSheet[stringAnswer] = true
                    }
                }
            }
            
            if let arrayAnswer = answer.value as? [String] {
                if !arrayAnswer.isEmpty {
                    mapedAnswerSheet[answer.key] = true
                    arrayAnswer.forEach{
                        mapedAnswerSheet[$0] = true
                    }
                }
            }
        }
        
        return mapedAnswerSheet
    }
    
    
    /// Check if the condition passes based on the answers
    /// - Parameters:
    ///   - condition: Located only in follow-up question
    ///   - answeSheet: Answers
    ///   - generateAnswersForNotCondition: There are cases that we need to generate a negative answer for NOT `condition`
    /// - Returns: True OR False
    func isConditionBasedOnAnswers(condition: Condition,
                                   answeSheet: [String: Any],
                                   generateAnswersForNotCondition: Bool) -> Bool {
        if answeSheet.isEmpty {
            return false
        }
        
        let answers = mapAnswerSheetToBoolAnswers(answerSheet: answeSheet)
        
        if let orDependents = condition.or {
            let orOperand = Operand(type: .or, answers: answers,
                                    dependents: orDependents,
                                    generateAnswersForNotCondition: generateAnswersForNotCondition)
            return orOperand.hasDepends()
        }
        
        if let andDependents = condition.and {
            let andOperand = Operand(type: .and, answers: answers,
                                     dependents: andDependents,
                                     generateAnswersForNotCondition: generateAnswersForNotCondition)
            return andOperand.hasDepends()
        }
        
        if let notDependents = condition.not {
            let notOperand = Operand(type: .not, answers: answers,
                                     dependents: notDependents,
                                     generateAnswersForNotCondition: generateAnswersForNotCondition)
            return notOperand.hasDepends()
        }
        
        return false
    }
    
}
