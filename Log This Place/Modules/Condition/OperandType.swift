//
//  OperandType.swift
//  Log This Place
//
//

import Foundation

enum OperandType {
    case or
    case not
    case and
}
