//
//  Operand.swift
//  Log This Place
//
//

import Foundation

final class Operand: Dependency {
    
    private let type: OperandType
    private let answers: [String: Bool]
    private let dependents: [Dependent]
    private let generateAnswersForNotCondition: Bool
    
    init(type: OperandType, answers: [String: Bool], dependents: [Dependent], generateAnswersForNotCondition: Bool) {
        self.type = type
        self.answers = answers
        self.dependents = dependents
        self.generateAnswersForNotCondition = generateAnswersForNotCondition
    }
    
    func hasDepends() -> Bool {
        let ids: [String] = getIDs()
        let depends: [Condition] = getDepends()
        let addictionStatuses: [Bool] = getAddictionStatuses(depends: depends)
        
        switch type {
        case .or:
            let answerIds = answers.filter({ $0.value == true }).map({ $0.key })
            return ids.contains(where: answerIds.contains) || (addictionStatuses.filter({ $0 == true }).first != nil)
        case .and:
            let answerIds = answers.filter({ $0.value == true }).map({ $0.key })
            return ids.allSatisfy(answerIds.contains) && addictionStatuses.allSatisfy({ $0 })
        case .not:
            var answerIds = answers.filter({ $0.value == false }).map({ $0.key })
            if generateAnswersForNotCondition {
                answerIds.append(contentsOf: generateNegativeId(with: ids))
            }
            return ids.allSatisfy(answerIds.contains) && addictionStatuses.allSatisfy({ $0 == false })
        }
    }
    
    /// return list with IDs which id doesnt exist in answer list. It is necessary because if id doesnt exist in `answers` list this mean `id == false`
    private func generateNegativeId(with ids: [String]) -> [String] {
        var answerIds = [String]()
        ids.forEach({ id in
            if answers.first(where: { $0.key == id && $0.value == true }) == nil {
                answerIds.append(id)
            }
        })
        return answerIds
    }
    
    /// return list with true/false operand statuses
    private func getAddictionStatuses(depends: [Condition]) -> [Bool] {
        var addictionStatuses = [Bool]()
        for depend in depends {
            if let ORDependents = depend.or {
                addictionStatuses.append(operandHasDepends(operand: .or, dependents: ORDependents))
            }
            
            if let ANDDependents = depend.and {
                addictionStatuses.append(operandHasDepends(operand: .and, dependents: ANDDependents))
            }
            
            if let NOTDependents = depend.not {
                addictionStatuses.append(operandHasDepends(operand: .not, dependents: NOTDependents))
            }
        }
        return addictionStatuses
    }
    
    /// recurcive check
    private func operandHasDepends(operand: OperandType, dependents: [Dependent]) -> Bool {
        let dependency = Operand(type: operand, answers: answers,
                                 dependents: dependents,
                                 generateAnswersForNotCondition: generateAnswersForNotCondition)
        return dependency.hasDepends()
    }
    
    /// return all string ids from current operand object(or, not, and)
    private func getIDs() -> [String] {
        return dependents.compactMap({
            if case let .string(id) = $0 { return id } else { return "" }
        }).filter({ !$0.isEmpty })
    }
    
    /// return all objects(or, not, and) from current operand object(or, not, and)
    private func getDepends() -> [Condition] {
        return dependents.compactMap({
            if case let .object(object) = $0 { return object } else { return .init(or: [], and: [], not: []) }
        }).filter({ !($0.and?.isEmpty ?? false) && !($0.or?.isEmpty ?? false) && !($0.not?.isEmpty ?? false)})
    }
}

