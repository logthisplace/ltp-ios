//
//  FirestoreProtocol.swift
//  Log This Place
//

import FirebaseFirestore

protocol FirestoreProtocol {
    static func firestoreInstance() -> FirestoreProtocol
    
    func collection(_ collectionPath: String) -> CollectionReference
}

extension Firestore: FirestoreProtocol {
    static func firestoreInstance() -> FirestoreProtocol {
        let settings = FirestoreSettings()
        settings.isPersistenceEnabled = true
        let fir = firestore()
        fir.settings = settings
        return fir
    }
}
