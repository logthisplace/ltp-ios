//
//  FirebaseAuthProtocol.swift
//  Log This Place
//

import FirebaseAuth

protocol FirebaseAuthProtocol {
    var currentUser: User? { get }

    func createUser(withEmail email: String, password: String, completion: ((AuthDataResult?, Error?) -> Void)?)
    func signIn(withEmail email: String, password: String, completion: ((AuthDataResult?, Error?) -> Void)?)
    func signOut() throws
    func signInAnonymously(completion: ((AuthDataResult?, Error?) -> Void)?)
}

extension Auth: FirebaseAuthProtocol {}
