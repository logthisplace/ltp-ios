//
//  GooglePlacesProtocol.swift
//  Log This Place
//


import GooglePlaces

protocol GooglePlacesProtocol {
    func fetchPlace(fromPlaceID placeID: String, placeFields: GMSPlaceField, callback: @escaping GMSPlaceResultCallback)
    func fetchPlace(fromPlaceID placeID: String, placeFields: GMSPlaceField, sessionToken: GMSAutocompleteSessionToken?, callback: @escaping GMSPlaceResultCallback)
    func loadPlacePhoto(_ photoMetadata: GMSPlacePhotoMetadata, callback: @escaping GMSPlacePhotoImageResultCallback)
    func findAutocompletePredictions(fromQuery query: String,
                                     filter: GMSAutocompleteFilter?,
                                     sessionToken: GMSAutocompleteSessionToken?,
                                     callback: @escaping GMSAutocompletePredictionsCallback)
}


extension GMSPlacesClient: GooglePlacesProtocol {
    func fetchPlace(fromPlaceID placeID: String, placeFields: GMSPlaceField, callback: @escaping GMSPlaceResultCallback) {
        fetchPlace(fromPlaceID: placeID, placeFields: placeFields, sessionToken: nil, callback: callback)
    }
}
