//
//  RemoteConfigProtocol.swift
//  Log This Place
//
//

import FirebaseRemoteConfig

protocol RemoteConfigProtocol {
    static func remoteConfigInstance() -> RemoteConfigProtocol
    
    func defaultValue(forKey key: String?) -> RemoteConfigValue?
    func configValue(forKey key: String?) -> RemoteConfigValue
    func fetchAndActivate(completionHandler: ((RemoteConfigFetchAndActivateStatus, Error?) -> Void)?)
    func fetch(completionHandler: ((RemoteConfigFetchStatus, Error?) -> Void)?)
    func activate(completion: ((Bool, Error?) -> Void)?)
    func setDefaults(_ defaults: [String : NSObject]?)
    func setValue(_ value: Any?, forKey: String)
}

extension RemoteConfig: RemoteConfigProtocol {
    
    static func remoteConfigInstance() -> RemoteConfigProtocol {
        let remoteConfig = RemoteConfig.remoteConfig()
        let settings = RemoteConfigSettings()
        #if DEBUG
        settings.minimumFetchInterval = 0
        #else
        settings.minimumFetchInterval = 3600
        #endif
        remoteConfig.configSettings = settings        
        return remoteConfig
    }
}
