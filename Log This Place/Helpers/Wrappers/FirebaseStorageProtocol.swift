//
//  FirebaseStorageProtocol.swift
//  Log This Place
//

import FirebaseStorage

protocol FirebaseStorageProtocol {
    func reference() -> StorageReference
}

extension Storage: FirebaseStorageProtocol {}
