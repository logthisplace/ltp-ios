//
//  StoryboardedInitializable.swift
//  Log This Place
//

import Foundation

protocol StoryboardedInitializable: Storyboarded, AnyObject {
    associatedtype PresenterType
    
    var presenter: PresenterType? { get set }
    
    static func initialize(presenter: PresenterType, storyboardName: StoryboardName) -> Self
}

extension StoryboardedInitializable {
    static func initialize(presenter: PresenterType, storyboardName: StoryboardName) -> Self {
        let vc = Self.instantiate(storyboardName: storyboardName)
        vc.presenter = presenter
        return vc
    }
}
