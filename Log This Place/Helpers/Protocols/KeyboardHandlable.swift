//
//  KeyboardHandlable.swift
//  Log This Place
//

import UIKit

/// View that should modify view upon keyboard appearance should conform to this protocol.
/// This way keyboard event observer will be setup in `BaseViewController` and pass information about those event to interested view.
/// IMPORTANT: View that conform to this protocol should setup its constraints in a way
/// that bottom constraint of given subview is attached to top of `keyboardLayoutGuide` (See `BaseView`).
protocol KeyboardHandlable {
    var keyboardHeightConstraint: NSLayoutConstraint { get }

    func adjustKeyboardHeightConstraint(constant: CGFloat, animationDuration: Double, animationCurve: UIView.AnimationCurve)
}

extension KeyboardHandlable where Self: UIView {
    func adjustKeyboardHeightConstraint(constant: CGFloat, animationDuration: Double, animationCurve: UIView.AnimationCurve) {
        let animator = UIViewPropertyAnimator(duration: animationDuration, curve: animationCurve) { [weak self] in
            self?.keyboardHeightConstraint.constant = constant
            self?.setNeedsLayout()
            self?.layoutIfNeeded()
        }
        animator.startAnimation()
    }
}
