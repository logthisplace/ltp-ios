//
//  String+Sanitized.swift
//  Log This Place
//

import Foundation

extension Optional where Wrapped == String {
    var sanitized: String {
        emptyIfNil.sanitized
    }
}

extension String {
    var sanitized: String {
        trimmingCharacters(in: .whitespacesAndNewlines)
    }
}

extension String {
   func replace(string:String, replacement:String) -> String {
       return self.replacingOccurrences(of: string, with: replacement, options: NSString.CompareOptions.literal, range: nil)
   }

   func removeWhitespace() -> String {
       return self.replace(string: " ", replacement: "")
   }
 }
