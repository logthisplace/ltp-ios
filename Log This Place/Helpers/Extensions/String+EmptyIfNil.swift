//
//  String+EmptyIfNil.swift
//  Log This Place
//

import Foundation

extension Optional where Wrapped == String {
    var emptyIfNil: String {
        guard let text = self else { return "" }
        return text
    }
}

extension String {
    var nilIfEmpty: String? {
        guard !isEmpty else { return nil }
        return self
    }
}
