//
//  UILabel+Font.swift
//  Log This Place
//
//

import UIKit


final class LTPUILabel: UILabel {
    
    @IBInspectable var style: String? {
        didSet {
            guard let style = style,
                  let textStyle = TextStyle(rawValue: style) else { return }
            self.textStyle = textStyle
        }
    }
    
    @IBInspectable var size: CGFloat = 17.0 {
        didSet {
            self.textSize = size
        }
    }
    
    enum TextStyle: String {
        case bold
        case semibold
        case regular
    }
    
    private var textStyle: TextStyle = .regular {
        didSet {
            customize()
        }
    }
    
    private var textSize: CGFloat = 17 {
        didSet {
            customize()
        }
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        customize()
    }
    
    private func customize() {
        UILabel.customize(label: self, for: textStyle, fontSize: textSize)
    }
    
}

extension UILabel {
    static func customize(label: UILabel, for style: LTPUILabel.TextStyle, fontSize: CGFloat) {
        switch style {
        case .bold:
            label.font = FontFamily.ProximaNova.bold.font(size: fontSize)
        case .semibold:
            label.font = FontFamily.ProximaNova.semibold.font(size: fontSize)
        case .regular:
            label.font = FontFamily.ProximaNova.regular.font(size: fontSize)
        }
    }
}
