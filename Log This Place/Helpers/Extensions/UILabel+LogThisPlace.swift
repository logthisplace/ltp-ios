//
//  UILabel+LogThisPlace.swift
//  Log This Place
//

import UIKit

// TODO: Make more flexible
extension UILabel {
    static var titleLargeRegular: UILabel {
        let label = UILabel()
        label.font = FontFamily.ProximaNova.regular.font(size: 36)
        return label
    }
}
extension UILabel {
    static var titleMediumBold: UILabel {
        let label = UILabel()
        label.font = FontFamily.ProximaNova.bold.font(size: 32)
        return label
    }
}
extension UILabel {
    static var titleH1SemiBold: UILabel {
        let label = UILabel()
        label.font = FontFamily.ProximaNova.semibold.font(size: 28)
        return label
    }
}
extension UILabel {
    static var titleH2Bold: UILabel {
        let label = UILabel()
        label.font = FontFamily.ProximaNova.bold.font(size: 22)
        return label
    }
    static var titleH2SemiBold: UILabel {
        let label = UILabel()
        label.font = FontFamily.ProximaNova.semibold.font(size: 22)
        return label
    }
}
extension UILabel {
    static var titleH3SemiBold: UILabel {
        let label = UILabel()
        label.font = FontFamily.ProximaNova.semibold.font(size: 20)
        return label
    }
}
extension UILabel {
    static var titleH4Bold: UILabel {
        let label = UILabel()
        label.font = FontFamily.ProximaNova.bold.font(size: 18)
        return label
    }
    static var titleH4SemiBold: UILabel {
        let label = UILabel()
        label.font = FontFamily.ProximaNova.semibold.font(size: 18)
        return label
    }
    static var titleH4Regular: UILabel {
        let label = UILabel()
        label.font = FontFamily.ProximaNova.semibold.font(size: 18)
        return label
    }
}
extension UILabel {
    static var headlineMediumRegular: UILabel {
        let label = UILabel()
        label.font = FontFamily.ProximaNova.regular.font(size: 17)
        return label
    }
    static var headlineMediumSemiBold: UILabel {
        let label = UILabel()
        label.font = FontFamily.ProximaNova.semibold.font(size: 17)
        return label
    }
}
extension UILabel {
    static var calloutRegular: UILabel {
        let label = UILabel()
        label.font = FontFamily.ProximaNova.regular.font(size: 16)
        return label
    }
    static var calloutBold: UILabel {
        let label = UILabel()
        label.font = FontFamily.ProximaNova.bold.font(size: 16)
        return label
    }
}
extension UILabel {
    static var standartMediumRegular: UILabel {
        let label = UILabel()
        label.font = FontFamily.ProximaNova.regular.font(size: 14)
        return label
    }
    static var standartMediumBold: UILabel {
        let label = UILabel()
        label.font = FontFamily.ProximaNova.bold.font(size: 14)
        return label
    }
}
extension UILabel {
    static var footnoteRegular: UILabel {
        let label = UILabel()
        label.font = FontFamily.ProximaNova.regular.font(size: 13)
        return label
    }
}
extension UILabel {
    static var captionRegular: UILabel {
        let label = UILabel()
        label.font = FontFamily.ProximaNova.regular.font(size: 12)
        return label
    }
    static var captionSemibold: UILabel {
        let label = UILabel()
        label.font = FontFamily.ProximaNova.semibold.font(size: 12)
        return label
    }
}
