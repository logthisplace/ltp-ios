//
//  UIStackView+Color.swift
//  Log This Place
//
//


import UIKit

extension UIStackView {
    func background(color: UIColor, radius: CGFloat = 0) {
        if #available(iOS 14.0, *) {
            backgroundColor = color
            layer.cornerRadius = radius
            clipsToBounds = true
        } else {
            let subView = UIView(frame: bounds)
            subView.backgroundColor = color
            subView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            subView.radius(radius: radius)
            insertSubview(subView, at: 0)
        }
    }
}
