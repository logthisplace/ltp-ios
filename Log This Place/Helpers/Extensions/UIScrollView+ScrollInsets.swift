//
//  UIScrollView+ScrollInsets.swift
//  Log This Place
//

import UIKit

extension UIScrollView {
    /// Sets top content inset and top vertical scroll indicator inset.
    /// - Parameter value: Value to be set as inset.
    /// - Parameter adjustIndicatorInset: Flag indicating wether indicator inset should also be adjusted.
    func setTopInsets(to value: CGFloat, adjustIndicatorInset: Bool = true) {
        contentInset.top = value
        if adjustIndicatorInset {
            verticalScrollIndicatorInsets.top = value
        }
    }

    /// Sets bottom content inset and bottom vertical scroll indicator inset.
    /// - Parameter value: Value to be set as inset.
    /// - Parameter adjustIndicatorInset: Flag indicating wether indicator inset should also be adjusted.
    func setBottomInsets(to value: CGFloat, adjustIndicatorInset: Bool = true) {
        contentInset.bottom = value
        if adjustIndicatorInset {
            verticalScrollIndicatorInsets.bottom = value
        }
    }

    /// Set top and bottom content and vertical scroll indicator insets.
    /// - Parameters:
    ///   - top: Top inset value.
    ///   - bottom: Bottom inset value.
    ///   - adjustIndicatorInset: Flag indicating wether indicator inset should also be adjusted.
    func setInsets(top: CGFloat, bottom: CGFloat, adjustIndicatorInset: Bool = true) {
        setTopInsets(to: top, adjustIndicatorInset: adjustIndicatorInset)
        setBottomInsets(to: bottom, adjustIndicatorInset: adjustIndicatorInset)
    }
}
