//
//  UIStackView+Init.swift
//  Log This Place
//

import UIKit

extension UIStackView {
    static var vertical: UIStackView {
        let stackView = UIStackView()
        stackView.axis = .vertical
        return stackView
    }
    
    static var horizontal: UIStackView {
        let stackView = UIStackView()
        stackView.axis = .horizontal
        return stackView
    }
}
