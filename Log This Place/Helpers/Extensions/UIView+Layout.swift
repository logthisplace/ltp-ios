//
//  UIView+Layout.swift
//  Log This Place
//

import UIKit

extension UIView {
    static let topSpacingUnderNavigationBar: CGFloat = 16
    static let bottomSpacing: CGFloat = 16
    static let horizontalScreenMargin: CGFloat = 20
    
    func addSubviewAnimation(_ subview: UIView?, options: UIView.AnimationOptions = [.transitionCrossDissolve]) {
        if let subview = subview {
            UIView.transition(with: self, duration: 0.25, options: options, animations: {
                self.addSubview(subview)
            }, completion: nil)
        }
    }
    
    func removeFromSuperviewAnimation() {
        UIView.transition(with: self, duration: 0.25, options: [.transitionCrossDissolve], animations: {
            self.removeFromSuperview()
            }, completion: nil)
    }
    
    func addSubviews(_ subviews: [UIView]) {
        for subview in subviews {
            addSubview(subview)
        }
    }

    func removeSubviews() {
        subviews.forEach { $0.removeFromSuperview() }
    }

    func positionToTheEdges(respectingSafeArea: Bool = true) {
        positionUsingScreenMargins(topSpacing: 0,
                                   bottomSpacing: 0,
                                   leadingSpacing: 0,
                                   trailingSpacing: 0,
                                   respectingSafeArea: respectingSafeArea)
    }

    func positionUsingScreenMargins(topSpacing: CGFloat = UIView.topSpacingUnderNavigationBar,
                                    bottomSpacing: CGFloat = UIView.bottomSpacing,
                                    leadingSpacing: CGFloat = UIView.horizontalScreenMargin,
                                    trailingSpacing: CGFloat = UIView.horizontalScreenMargin,
                                    respectingSafeArea: Bool = true) {
        guard let superview = superview else { return }
        if respectingSafeArea {
            snp.makeConstraints { make in
                make.leading.equalTo(superview.safeAreaLayoutGuide.snp.leading).offset(leadingSpacing)
                make.trailing.equalTo(superview.safeAreaLayoutGuide.snp.trailing).offset(-trailingSpacing)
                make.top.equalTo(superview.safeAreaLayoutGuide).inset(topSpacing)
                make.bottom.equalTo(superview.safeAreaLayoutGuide).inset(bottomSpacing)
            }
        } else {
            snp.makeConstraints { make in
                make.leading.equalToSuperview().offset(leadingSpacing)
                make.trailing.equalToSuperview().offset(trailingSpacing)
                make.top.equalToSuperview().inset(topSpacing)
                make.bottom.equalToSuperview().inset(bottomSpacing)
            }
        }
    }
    
    func wrapped(alignment: WrappingView<UIView>.Alignment) -> WrappingView<UIView> {
        return WrappingView(contentView: self, alignment: alignment)
    }
    
    func wrappedCenteredHorizontally() -> WrappingView<UIView> {
        WrappingView(contentView: self, centerAxis: .horizontal, hardInsetsAxis: .vertical)
    }

    func wrappedCenteredVertically() -> WrappingView<UIView> {
        WrappingView(contentView: self, centerAxis: .vertical, hardInsetsAxis: .horizontal)
    }
    
    static var fillingVertical: UIView {
        with(UIView()) {
            $0.setContentHuggingPriority(.required, for: .horizontal)
        }
    }
    
    static var fillingHorizontal: UIView {
        with(UIView()) {
            $0.setContentHuggingPriority(.required, for: .vertical)
        }
    }
    
    static func horizontalSeparator(_ spacing: CGFloat = 1) -> UIView {
        with(UIView()) {
            $0.backgroundColor = UIColor.LogThisPlace.lightGray
            $0.snp.makeConstraints { make in
                make.height.equalTo(spacing)
            }
        }
    }
}
