//
//  UIImageView+Mode.swift
//  Log This Place
//

import UIKit

extension UIImageView {
    static var scaleAspectFit: UIImageView {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        return imageView
    }
    
    static func scaleAspectFit(with image: UIImage) -> UIImageView {
        let imageView = scaleAspectFit
        imageView.image = image
        imageView.contentMode = .scaleAspectFill
        return imageView
    }
    
    static var scaleAspectFill: UIImageView {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        return imageView
    }
}
