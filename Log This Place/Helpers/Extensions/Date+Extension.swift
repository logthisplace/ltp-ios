//
//  Date+Extension.swift
//  Log This Place
//
//

import UIKit


extension Date {
    func currentTimeMillis() -> Int64 {
        return Int64(self.timeIntervalSince1970 * 1000)
    }
}
