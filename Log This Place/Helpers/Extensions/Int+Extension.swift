//
//  Int+Extension.swift
//  Log This Place
//
//

import Foundation

extension Int {
    
    mutating func increment() {
        self += 1
    }
    
    mutating func decrement() {
        self -= 1
    }
}
