//
//  UIColor+LogThisPlace.swift
//  Log This Place
//

import UIKit


extension UIColor {
    enum LogThisPlace {
        /// #121619
        static let almostBlack = #colorLiteral(red: 0.07058823529, green: 0.0862745098, blue: 0.09803921569, alpha: 1)
        
        /// #3C3C43
        static let darkGray = #colorLiteral(red: 0.2352941176, green: 0.2352941176, blue: 0.262745098, alpha: 1)
        
        /// #747480
        static let gray = #colorLiteral(red: 0.4549019608, green: 0.4549019608, blue: 0.5019607843, alpha: 1)
        
        /// #C6C6C8
        static let lightGray = #colorLiteral(red: 0.7764705882, green: 0.7764705882, blue: 0.7843137255, alpha: 1)
        
        /// #E5E5E5
        static let systemLikeGray = #colorLiteral(red: 0.8980392157, green: 0.8980392157, blue: 0.8980392157, alpha: 1)
        
        /// #F2F4F8
        static let grayAlmostWhite = #colorLiteral(red: 0.9490196078, green: 0.9568627451, blue: 0.9725490196, alpha: 1)
        
        /// #9DA3A9
        static let grayAlmostDark = #colorLiteral(red: 0.6156862745, green: 0.6392156863, blue: 0.662745098, alpha: 1)
       
        /// #34C759
        static let green = #colorLiteral(red: 0.2039215686, green: 0.7803921569, blue: 0.3490196078, alpha: 1)
        
        /// #219653
        static let darkGreen = #colorLiteral(red: 0.1294117647, green: 0.5882352941, blue: 0.3254901961, alpha: 1)
        
        /// #E4F2EA
        static let lightGreen = #colorLiteral(red: 0.8941176471, green: 0.9490196078, blue: 0.9176470588, alpha: 1)

        /// #8A3FFC
        static let purple = #colorLiteral(red: 0.5411764706, green: 0.2470588235, blue: 0.9882352941, alpha: 1)
        
        /// #F3AF22
        static let yellow =  #colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1)
        
        /// #FF5F35
        static let orange = #colorLiteral(red: 1, green: 0.3725490196, blue: 0.2078431373, alpha: 1)
        
        /// #D02670
        static let pink = #colorLiteral(red: 0.8596670628, green: 0.2519749403, blue: 0.5136340261, alpha: 1)
        
        /// #FFFFFF
        static let white = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        
        /// #F6F1FF
        static let purpureLight = #colorLiteral(red: 0.9647058824, green: 0.9450980392, blue: 1, alpha: 1)
        
        /// #A165FD
        static let purpure = #colorLiteral(red: 0.631372549, green: 0.3960784314, blue: 0.9921568627, alpha: 1)
        
        /// #3498db
        static let clusterBlue = #colorLiteral(red: 0.2039215686, green: 0.5960784314, blue: 0.8588235294, alpha: 1)
        
        /// #f5f7f9
        static let backgroundGray = #colorLiteral(red: 0.9607843137, green: 0.968627451, blue: 0.9764705882, alpha: 1)
        


    }
}
