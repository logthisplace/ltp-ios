//
//  UITextField+AccessoryView.swift
//  Log This Place
//

import UIKit

extension UITextField {
    static func prepareDoneInputAccessoryView(target: Any?, doneSelector: Selector?) -> UIToolbar {
        let toolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: 44))
        toolbar.isTranslucent = false
        toolbar.barTintColor = UIColor.LogThisPlace.systemLikeGray
        let doneButton = UIBarButtonItem(title: L10n.Common.Done.text, style: .plain, target: target, action: doneSelector)
        let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        toolbar.items = [flexSpace, doneButton]
        toolbar.sizeToFit()
        return toolbar
    }

    func addDoneAccessoryInputView() {
        let toolbar = UITextField.prepareDoneInputAccessoryView(target: self, doneSelector: #selector(dismissKeyboard))
        inputAccessoryView = toolbar
    }

    @objc private func dismissKeyboard() {
        resignFirstResponder()
    }
}
