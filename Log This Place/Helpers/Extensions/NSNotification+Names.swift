//
//  NSNotification+Names.swift
//  Log This Place
//

import UIKit

extension NSNotification.Name {
    static let keyboardWillShow = UIResponder.keyboardWillShowNotification
    static let keyboardWillHide = UIResponder.keyboardWillHideNotification
}
