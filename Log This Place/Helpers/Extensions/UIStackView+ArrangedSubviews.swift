//
//  UIStackView+ArrangedSubviews.swift
//  Log This Place
//

import UIKit

extension UIStackView {
    func addArrangedSubviews(_ subviews: [UIView]) {
        subviews.forEach { addArrangedSubview($0) }
    }

    func insertArrangedSubviews(_ subviews: [UIView], at position: Int) {
        subviews.reversed().forEach { insertArrangedSubview($0, at: position) }
    }

    func removeAllArrangedSubviews() {
        removeAllArrangedSubviews(except: [])
    }

    func removeAllArrangedSubviews(except: [UIView]) {
        subviews.forEach { if !except.contains($0) { $0.removeFromSuperview() } }
    }
}
