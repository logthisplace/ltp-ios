import Foundation
import UIKit

extension UIStackView {
    
    func border(borderColor: UIColor = UIColor.LogThisPlace.lightGray,
                borderWidth: CGFloat = 1,
                radius: CGFloat = 10) {
        if #available(iOS 14.0, *) {
            layer.borderWidth = borderWidth
            layer.cornerRadius = radius
            layer.borderColor = borderColor.cgColor
        } else {
            let subView = UIView(frame: bounds)
            subView.layer.cornerRadius = radius
            subView.layer.masksToBounds = true
            subView.layer.borderWidth = borderWidth
            subView.layer.borderColor = borderColor.cgColor
            subView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            subView.radius(radius: radius)
            insertSubview(subView, at: 0)
        }
    }
    
}
