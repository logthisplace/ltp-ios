//
//  UIStackView+Padding.swift
//  Log This Place
//
//

import UIKit

extension UIStackView {
    func edgeInsets(topSpacing: CGFloat = 0,
                     bottomSpacing: CGFloat = 0,
                     leadingSpacing: CGFloat = 0,
                     trailingSpacing: CGFloat = 0) {
        isLayoutMarginsRelativeArrangement = true
        directionalLayoutMargins = NSDirectionalEdgeInsets(top: topSpacing,
                                                           leading: leadingSpacing,
                                                           bottom: bottomSpacing,
                                                           trailing: trailingSpacing)
    }
}
