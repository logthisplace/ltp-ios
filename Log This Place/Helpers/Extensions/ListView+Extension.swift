//
//  ListView+Extension.swift
//  Log This Place
//
//

import UIKit

protocol ReusableCell {
    static var reuseIdentifier: String { get }
}

extension UICollectionViewCell: ReusableCell {}
extension ReusableCell where Self: UICollectionViewCell {
    static var reuseIdentifier: String { String(describing: self) }
}

extension UITableViewCell: ReusableCell {}
extension ReusableCell where Self: UITableViewCell {
    static var reuseIdentifier: String { String(describing: self) }
}

extension UICollectionView {
    func register<T: UICollectionViewCell & NibProvideable>(item: T.Type) {
        register(T.self.nib(bundle: nil), forCellWithReuseIdentifier: T.reuseIdentifier)
    }
    
    func register<T: UICollectionViewCell>(item: T.Type) {
        register(T.self, forCellWithReuseIdentifier: T.reuseIdentifier)
    }
    
    func deque<T: UICollectionViewCell>(for indexPath: IndexPath) -> T {
        return dequeueReusableCell(withReuseIdentifier: T.reuseIdentifier, for: indexPath) as! T
    }
}

extension UITableView {
    func register<T: UITableViewCell & NibProvideable>(item: T.Type) {
        register(T.self.nib(bundle: nil), forCellReuseIdentifier: T.reuseIdentifier)
    }
    
    func register<T: UITableViewCell>(item: T.Type) {
        register(T.self, forCellReuseIdentifier: T.reuseIdentifier)
    }
    
    func deque<T: UITableViewCell>(for indexPath: IndexPath) -> T {
        dequeueReusableCell(withIdentifier: T.reuseIdentifier, for: indexPath) as! T
    }
}

protocol NibProvideable {
    static func nib(bundle: Bundle?) -> UINib
}

extension NibProvideable where Self: UICollectionViewCell {
    static func nib(bundle: Bundle?) -> UINib {
        return UINib(nibName: self.reuseIdentifier, bundle: bundle)
    }
}

extension NibProvideable where Self: UITableViewCell {
    static func nib(bundle: Bundle?) -> UINib {
        return UINib(nibName: self.reuseIdentifier, bundle: bundle)
    }
}
