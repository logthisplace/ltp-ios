//
//  View+Extension.swift
//  Log This Place
//
//

import UIKit

extension UIView {
    
    func radius(radius: CGFloat) {
        self.layer.cornerRadius = radius
        self.clipsToBounds = true
    }
    
    func hideView(duration: Double = 0) {
        fadeOut(duration: duration)
    }
    
    func showView(duration: Double = 0) {
        fadeIn(duration: duration)
    }
    
    func shadow(opacity: Float = 0.5,
                color: CGColor = UIColor.black.cgColor,
                shadowOffset: CGSize = .zero) {
        self.layer.masksToBounds = false
        self.layer.shadowOpacity = opacity
        self.layer.shadowColor = color
        self.layer.shadowOffset = shadowOffset
    }
    
    func fadeIn(duration: TimeInterval = 0.0) {
        if isHidden {
            alpha = 0
            UIView.animate(withDuration: duration) {
                self.alpha = 1
                self.isHidden = false
            }
        }
    }
    
    func fadeOut(duration: TimeInterval = 0.0) {
        if !isHidden  {
            alpha = 1
            UIView.animate(withDuration: duration, animations: {
                self.alpha = 0
                self.isHidden = true
            }) { (finished) in
                self.isHidden = finished
            }
        }
    }
}
