//
//  KeyboardObserver.swift
//  Log This Place
//

import UIKit

/// Protocol used as delegate for keyboard events.
protocol KeyboardObserverDelegate: AnyObject {

    /// Notifies when keyboard is about to show.
    func keyboardWillShow(with frame: CGRect, duration: Double, curve: UIView.AnimationCurve)

    /// Notifies when keyboard is about to hide.
    func keyboardWillHide(duration: Double, curve: UIView.AnimationCurve)
}

final class KeyboardObserver {

    weak var delegate: KeyboardObserverDelegate?

    private let notificationCenter = NotificationCenter.default

    init() {
        notificationCenter.addObserver(self, selector: #selector(keyboardWillShow), name: .keyboardWillShow, object: nil)
        notificationCenter.addObserver(self, selector: #selector(keyboardWillHide), name: .keyboardWillHide, object: nil)
    }

    @objc private func keyboardWillShow(notification: NSNotification) {
        guard let userInfo = notification.userInfo,
              let keyboardFrame = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
        else { return }
        let durationAndCurve = retrieveSpeedAndCurve(from: userInfo)
        delegate?.keyboardWillShow(with: keyboardFrame,
                                   duration: durationAndCurve.duration,
                                   curve: durationAndCurve.curve)
    }

    @objc private func keyboardWillHide(notification: NSNotification) {
        guard let userInfo = notification.userInfo else { return }
        let durationAndCurve = retrieveSpeedAndCurve(from: userInfo)
        delegate?.keyboardWillHide(duration: durationAndCurve.duration,
                                   curve: durationAndCurve.curve)
    }

    private func retrieveSpeedAndCurve(from userInfo: [AnyHashable: Any]) -> (duration: Double, curve: UIView.AnimationCurve) {
        let duration = userInfo[UIResponder.keyboardAnimationDurationUserInfoKey] as? Double ?? 0.3
        let curve = UIView.AnimationCurve(rawValue: userInfo[UIResponder.keyboardAnimationCurveUserInfoKey] as? Int ?? 0) ?? .easeOut
        return (duration, curve)
    }
}
