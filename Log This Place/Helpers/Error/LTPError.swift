//
//  LTPError.swift
//  Log This Place
//
//

import Foundation


protocol LTPErrorProtocol: LocalizedError {
    var title: String? { get }
}

struct LTPError: LTPErrorProtocol {

    var title: String?
    var errorDescription: String? { return _description }
    var failureReason: String? { return _description }

    private var _description: String

    init(title: String? = nil, description: String) {
        self.title = title ?? "Error"
        self._description = description
    }
}
