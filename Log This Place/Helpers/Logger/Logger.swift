//
//  Logger.swift
//  Log This Place
//
//
import Foundation


class Logger {

    private enum Constants {
        static let messageKey: String = "message"
        static let fileKey: String = "file"
        static let functionKey: String = "function"
        static let lineKey: String = "lineNumber"
        static let levelKey: String = "levelKey"
    }

    enum Kind: String {
        case info
        case error
    }

    static let shared = Logger()
    
    let loggerFirebaseManager: LoggerFirebaseManagerProtocol = AppContainer.resolve()

    func info(_ message: String, function: String = #function, line: UInt = #line) {
        let newMessage: String = "🔶" + message + "🔶"
        let newFunction: String = "🔶" + function + "🔶"
        sendMessage(newMessage,
                    function: newFunction,
                    line: line,
                    kind: .info)
    }

    func error(_ message: String, function: String = #function, line: UInt = #line) {
        sendMessage(message,
                    function: function,
                    line: line,
                    kind: .error)
    }

    private func sendMessage(_ message: String, function: String, line: UInt, kind: Kind) {
        switch kind {
        case .info:
            #if DEBUG
            print("function:\(function) \(message)")
            #endif
        case .error:
            logToCrashlytics(message,
                             function: function,
                             line: line,
                             level: kind.rawValue)
        }
    }

    private func logToCrashlytics(_ message: String, function: String, line: UInt, level: String) {
        var additionalUserInfo: [String: Any] = [:]
        additionalUserInfo[Constants.messageKey] = message
        additionalUserInfo[Constants.functionKey] = function
        additionalUserInfo[Constants.lineKey] = line
        additionalUserInfo[Constants.levelKey] = level
        
        print("logToCrashlytics \(additionalUserInfo)")
        loggerFirebaseManager.logError(with: .init(level: level,
                                                   message: message,
                                                   ios_app_line: line,
                                                   ios_app_function: function))
    }
}
