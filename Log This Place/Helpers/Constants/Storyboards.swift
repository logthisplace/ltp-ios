//
//  Storyboards.swift
//  Log This Place
//
//

import Foundation


enum StoryboardName: String {
    case userPassport = "UserPassport"
    case map = "Map"
    case userIdentification = "UserIdentification"
}
