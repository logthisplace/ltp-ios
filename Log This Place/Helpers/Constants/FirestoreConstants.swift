//
//  FirestoreConstants.swift
//  Log This Place
//
//

import Foundation


enum TypeOfQuestion: String {
    case poi
    case user
}

enum FirestoreSuccess: String {
    case updatedSuccess = "Data updated successfully"
}

enum FirestoreError: Error {
    case mappingError
    case addDocumentError
    case setDataError
    case getDocumentError
    case documentIdIsEmpty
    case userIdDoesntExist
    case documentDoesntExist
    case doesntExist
    case answerSheetDoesntExist
    case firebaseUserError
    case getDataError
    case listenerError
    case empty
}

struct FirestoreDatabase {
    
    static let CATEGORY_UUID = "d4c0298c-7222-46f9-b09e-b1821cdcce53"
    
    enum TypeOfQuestion: String {
        case poi
        case user
    }

    struct ConstantsFirebaseQuery {
        static let CREATED_AT = "created_at"
    }
    
    struct Log {
        static let COLLECTION_NAME = "log"
    }
    
    struct ConstantsPointOfInterest {
        static let COLLECTION_NAME = "pois"
        static let FIELD_CATEGORY = "category"
        static let GOOGLE_PLACE_ID = "g_places_id"
        static let FIELD_NAME = "name"
        static let FIELD_LATITUDE = "latitude"
        static let FIELD_LONGITUDE = "longitude"
        static let FIELD_ADDRESS = "address"
        static let COLLECTION_SHEET_NAME = "sheets"
        static let FIELD_DEVICE_UUID = "device_uuid"
        static let FIELD_USER_ID = "user_id"
        static let CREATED_AT = "created_at"
        static let UPDATED_AT = "updated_at"
        static let ANSWERS = "answers"
    }

    struct ConstantsQuestionnaire {
        static let COLLECTION_NAME = "questionnaires"
        static let FIELD_TYPE = "type"
        static let QUESTIONS_COLLECTION_NAME = "questions"
        static let FIELD_POSITION = "position"
        
        enum QuestionnaireType: String {
            case POI = "poi"
            case USER = "user"
        }
        
        enum QUESTION_KEYS: String {
            case POI_CATEGORY = "poi_category"
        }
    }
    
    struct ConstantsAccessibleIcon {
        static let COLLECTION_NAME = "poi_details_icons_logic"
    }
    
    struct ConstantsPinColor {
        static let COLLECTION_NAME = "poi_user_color_logic"
    }
    
    struct ConstantsPostalCode {
        static let COLLECTION_NAME = "postal_codes"
    }
    
    struct ConstantsUser {
        static let COLLECTION_NAME = "users"
        static let FIELD_DEVICE_UUID = "device_uuid"
        static let FIELD_USER_ID = "user_id"
        static let CREATED_AT = "created_at"
        static let UPDATED_AT = "updated_at"
        static let ANSWERS = "answers"
    }
}




