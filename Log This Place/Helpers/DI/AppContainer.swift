//
//  AppContainer.swift
//  Log This Place
//

import Swinject

/// Class used as a wrapper for dependency container.
enum AppContainer {
    private static let container = Container()
    
    static func build() {
        DependenciesContainer.build(in: container)
        UtilitiesContainer.build(in: container)
    }
    
    /// Method responsible for clearing all registered dependencies.
    /// Helpful in unit tests.
    static func clear() {
        container.removeAll()
    }
    
    /// Register single service.
    /// Helpfull in unit tests where we want to register single fake services for given test case.
    /// - Parameters:
    ///   - serviceType: Service to be registerd.
    ///   - factory: Closure returning instance of wanted service.
    static func register<T>(_ serviceType: T.Type, factory: @escaping (() -> T)) {
        container.register(serviceType) { _ in factory() }
    }

    /// Retrieves instance of registered service.
    /// - Returns: Instance of service.
    /// - Note: When given service type couldn't be found, it is programming error and so this method raises fatalError.
    static func resolve<T>() -> T {
        guard let resolved = container.resolve(T.self) else {            fatalError("Couldn't resolve dependency: \(T.self)")
        }
        return resolved
    }
}


protocol ContainerBuildable {
    static func build(in container: Container)
}


