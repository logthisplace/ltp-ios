//
//  DependenciesContainer.swift
//  Log This Place
//

import Swinject
import FirebaseFirestore
import FirebaseStorage
import FirebaseAuth
import GoogleMaps
import FirebaseRemoteConfig

enum DependenciesContainer: ContainerBuildable {
    static func build(in container: Container) {
        container.register(FirestoreProtocol.self) { _ in Firestore.firestore() }
        container.register(FirebaseStorageProtocol.self) { _ in Storage.storage() }
        container.register(FirebaseAuthProtocol.self) { _ in Auth.auth() }
        container.register(RemoteConfigProtocol.self) { _ in RemoteConfig.remoteConfigInstance() }
    }
}
