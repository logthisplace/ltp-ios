//
//  UtilitiesContainer.swift
//  Log This Place
//

import Swinject
import GooglePlaces

enum UtilitiesContainer: ContainerBuildable {
    static func build(in container: Container) {
        buildCommonUtilities(in: container)
        buildDataManagers(in: container)
        buildFirebaseManagers(in: container)
    }
    
    private static func buildCommonUtilities(in container: Container) {
        container.register(UserManagerProtocol.self) { _ in UserManager() }.inObjectScope(.container)
        container.register(UserDefaultsManagerProtocol.self) { _ in UserDefaultsManager() }.inObjectScope(.container)
        container.register(GooglePlacesProtocol.self) { _ in GMSPlacesClient.shared() }.inObjectScope(.container)
        container.register(LocationManagerProtocol.self) { _ in LocationManager() }.inObjectScope(.container)
        container.register(CityLocationManagerProtocol.self) { _ in CityLocationManager() }.inObjectScope(.container)
        container.register(FirebaseAnalyticManagerProtocol.self) { _ in FirebaseAnalyticManager() }.inObjectScope(.container)
        container.register(ConditionManagerProtocol.self) { _ in ConditionManager() }.inObjectScope(.container)
    }
    
    private static func buildFirebaseManagers(in container: Container) {
        container.register(PointOfInterestFirebaseManagerProtocol.self) { _ in PointOfInterestFirebaseManager() }.inObjectScope(.container)
        container.register(AccessibleIconsFirebaseManagerProtocol.self) { _ in AccessibleIconsFirebaseManager() }.inObjectScope(.container)
        container.register(PinColorLogicFirebaseManagerProtocol.self) { _ in PinColorLogicFirebaseManager() }.inObjectScope(.container)
        container.register(PostalCodesFirebaseManagerProtocol.self) { _ in PostalCodesFirebaseManager() }.inObjectScope(.container)
        container.register(AnswerSheetFirebaseManagerProtocol.self) { _ in AnswerSheetFirebaseManager() }.inObjectScope(.container)
        container.register(QuestionnaireFirebaseManagerProtocol.self) { _ in QuestionnaireFirebaseManager() }
        container.register(UserPassportFirebaseManagerProtocol.self) { _ in UserPassportFirebaseManager() }
        container.register(StorageFirebaseManagerProtocol.self) { _ in StorageFirebaseManager() }
        container.register(UserIdentificationFirebaseManagerProtocol.self) { _ in UserIdentificationFirebaseManager() }
        container.register(FirebaseImageManagerProtocol.self) { _ in FirebaseImageManager() }
        container.register(RemoteConfigFirebaseManagerProtocol.self) { _ in RemoteConfigFirebaseManager() }
        container.register(LoggerFirebaseManagerProtocol.self) { _ in LoggerFirebaseManager() }
    }
    
    private static func buildDataManagers(in container: Container) {
        container.register(PointOfInterestDataManagerProtocol.self) { _ in PointOfInterestDataManager()}.inObjectScope(.container)
        container.register(AccessibleIconsDataManagerProtocol.self) { _ in AccessibleIconsDataManager() }.inObjectScope(.container)
        container.register(PinColorLogicDataManagerProtocol.self) { _ in PinColorLogicDataManager() }.inObjectScope(.container)
        container.register(PostalCodesDataManagerProtocol.self) { _ in PostalCodesDataManager() }.inObjectScope(.container)
        container.register(AnswerSheetDataManagerProtocol.self )  { _ in AnswerSheetDataManager()}.inObjectScope(.container)
        container.register(QuestionnaireDataManagerProtocol.self) { _ in QuestionnaireDataManager() }.inObjectScope(.container)
        container.register(UserPassportDataManagerProtocol.self) { _ in UserPassportDataManager() }.inObjectScope(.container)
        container.register(UserIdentificationDataManagerProtocol.self) { _ in UserIdentificationDataManager() }.inObjectScope(.container)
        container.register(StorageDataManagerProtocol.self) { _ in StorageDataManager() }
    }
}
