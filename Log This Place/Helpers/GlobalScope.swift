//
//  GlobalScope.swift
//  Log This Place
//

import Foundation

/// Method that configures given object and returns it. Convenient when creating new objects with configuration.
/// - Parameters:
///   - item: Object to be configured.
///   - configBlock: Block that accepts given object and modifies it.
/// - Returns: Modified input object.
func with<T>(_ item: T, configBlock: (T) -> Void) -> T {
    configBlock(item)
    return item
}

/// Takes given closure and executes it on the main thread.
func executeOnMainThread(_ code: @escaping (() -> Void)) {
    if Thread.isMainThread {
        code()
    } else {
        DispatchQueue.main.async(execute: code)
    }
}
