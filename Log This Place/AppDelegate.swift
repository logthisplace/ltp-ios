//
//  AppDelegate.swift
//  Log This Place
//

import UIKit
import Firebase
import GoogleMaps
import GooglePlaces

// TODO: - Isolate firebase entities from upper layers
@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window : UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        window = UIWindow(frame: UIScreen.main.bounds)
        
        firebaseInit()
        googleConfig()
        
        if !isRunningUnitTests {
            AppContainer.build()
            showStartScreen()
        }
        
        UINavigationBar.appearance().tintColor = UIColor.LogThisPlace.darkGray
        
        return true
    }
    
    func firebaseInit() {
        var filePath: String?
        
        switch AppScheme.current {
        case .development:
            filePath = Bundle.main.path(forResource: "GoogleService-Info-Dev", ofType: "plist")
        case .production:
            filePath = Bundle.main.path(forResource: "GoogleService-Info-Live", ofType: "plist")
        }
        
        if let filePath = filePath {
            guard let fileopts = FirebaseOptions(contentsOfFile: filePath) else { return }
            FirebaseApp.configure(options: fileopts)
        }
    }
    
    func googleConfig() {
        GMSServices.provideAPIKey(GMS_SERVICES_API_KEY)
        GMSPlacesClient.provideAPIKey(GMS_PLACES_API_KEY)
    }
    
    func showStartScreen() {
        let navigationController: UINavigationController = UINavigationController()
        let userDefaultsManager: UserDefaultsManagerProtocol = AppContainer.resolve()
        let mapCoordinator: MapCoordinatorProtocol = MapCoordinator(navigationController: navigationController)

        if !userDefaultsManager.wasOnboardingShown {
            mapCoordinator.showOnboarding()
        } else {
            mapCoordinator.showMap()
        }
        
        window?.rootViewController = navigationController
        window?.makeKeyAndVisible()
    }
}

private extension AppDelegate {
    var isRunningUnitTests: Bool {
        NSClassFromString("XCTest") != nil
    }
}
