//
//  AppScheme.swift
//  Log This Place
//

import Foundation

enum AppScheme {
    case development
    case production
    
    static var current: AppScheme {
        #if ENV_DEVELOPMENT
            return .development
        #elseif ENV_PRODUCTION
            return .production
        #endif
    }
}
