// swiftlint:disable all
// Generated using SwiftGen — https://github.com/SwiftGen/SwiftGen

#if os(macOS)
  import AppKit
#elseif os(iOS)
  import UIKit
#elseif os(tvOS) || os(watchOS)
  import UIKit
#endif

// Deprecated typealiases
@available(*, deprecated, renamed: "ColorAsset.Color", message: "This typealias will be removed in SwiftGen 7.0")
internal typealias AssetColorTypeAlias = ColorAsset.Color
@available(*, deprecated, renamed: "ImageAsset.Image", message: "This typealias will be removed in SwiftGen 7.0")
internal typealias AssetImageTypeAlias = ImageAsset.Image

// swiftlint:disable superfluous_disable_command file_length implicit_return

// MARK: - Asset Catalogs

// swiftlint:disable identifier_name line_length nesting type_body_length type_name
internal enum Asset {
  internal static let accentColor = ColorAsset(name: "AccentColor")
  internal enum BuildingDetail {
    internal enum Accessibility {
      internal static let accessibleToilet = ImageAsset(name: "AccessibleToilet")
      internal static let assistanceDog = ImageAsset(name: "AssistanceDog")
      internal static let handicapParking = ImageAsset(name: "HandicapParking")
      internal static let paymentAccessible = ImageAsset(name: "PaymentAccessible")
      internal static let stepsAtTheEntrance = ImageAsset(name: "StepsAtTheEntrance")
      internal static let wheelchairFriendlyDor = ImageAsset(name: "WheelchairFriendlyDor")
      internal static let wheelchairFriendlyEntrance = ImageAsset(name: "WheelchairFriendlyEntrance")
      internal static let revolvingDoor = ImageAsset(name: "revolving door")
    }
    internal static let checkedBlack = ImageAsset(name: "CheckedBlack")
    internal static let direction = ImageAsset(name: "Direction")
    internal static let entrance = ImageAsset(name: "Entrance")
    internal static let inside = ImageAsset(name: "Inside")
    internal static let paymentMethods = ImageAsset(name: "PaymentMethods")
    internal static let phone = ImageAsset(name: "Phone")
    internal static let placeholder = ImageAsset(name: "Placeholder")
    internal static let toilet = ImageAsset(name: "Toilet")
    internal static let website = ImageAsset(name: "Website")
    internal static let buildingDetailItemGreen = ImageAsset(name: "buildingDetailItemGreen")
    internal static let buildingDetailItemOrange = ImageAsset(name: "buildingDetailItemOrange")
    internal static let selector = ImageAsset(name: "selector")
    internal static let testImage = ImageAsset(name: "testImage")
    internal static let testProfileAccessible = ImageAsset(name: "testProfileAccessible")
  }
  internal static let camera = ImageAsset(name: "Camera")
  internal static let logo = ImageAsset(name: "Logo")
  internal enum Map {
    internal static let mapCenter = ImageAsset(name: "MapCenter")
    internal static let mapCompass = ImageAsset(name: "MapCompass")
    internal static let mapProfile = ImageAsset(name: "MapProfile")
    internal static let mapSearch = ImageAsset(name: "MapSearch")
    internal static let googlePins = ImageAsset(name: "google_pins")
    internal static let interrogationMark = ImageAsset(name: "interrogation_mark")
  }
  internal enum Onboarding {
    internal static let next = ImageAsset(name: "next")
    internal static let onboardingFirst = ImageAsset(name: "onboarding_first")
    internal static let onboardingSecond = ImageAsset(name: "onboarding_second")
    internal static let onboardingThrees = ImageAsset(name: "onboarding_threes")
  }
  internal static let pin = ImageAsset(name: "Pin")
  internal static let pinkCircle = ImageAsset(name: "PinkCircle")
  internal enum PoiCategories {
    internal static let authorities = ImageAsset(name: "Authorities")
    internal static let drinks = ImageAsset(name: "Drinks")
    internal static let gym = ImageAsset(name: "Gym")
    internal static let hotel = ImageAsset(name: "Hotel")
    internal static let leisure = ImageAsset(name: "Leisure")
    internal static let medicalCare = ImageAsset(name: "MedicalCare")
    internal static let museum = ImageAsset(name: "Museum")
    internal static let noneCategory = ImageAsset(name: "NoneCategory")
    internal static let publicToilet = ImageAsset(name: "Public Toilet")
    internal static let services = ImageAsset(name: "Services")
    internal static let shopping = ImageAsset(name: "Shopping")
    internal static let transport = ImageAsset(name: "Transport")
  }
  internal static let standardPin = ImageAsset(name: "StandardPin")
  internal enum Summary {
    internal static let greenCrossmark = ImageAsset(name: "GreenCrossmark")
    internal static let noneCrossmark = ImageAsset(name: "NoneCrossmark")
    internal static let redCrossmark = ImageAsset(name: "RedCrossmark")
    internal static let congratulationsSmile = ImageAsset(name: "congratulationsSmile")
    internal static let summarySmile = ImageAsset(name: "summary_smile")
  }
  internal static let welcomeHeader = ImageAsset(name: "WelcomeHeader")
  internal static let cameraFill = ImageAsset(name: "camera-fill")
  internal static let checkSquareFill = ImageAsset(name: "check-square-fill")
  internal static let chevronDown = ImageAsset(name: "chevron-down")
  internal static let circleBackground = ImageAsset(name: "circle_background")
  internal static let circleChecked = ImageAsset(name: "circle_checked")
  internal static let circleEmpty = ImageAsset(name: "circle_empty")
  internal static let closeGray = ImageAsset(name: "close_gray")
  internal static let pencil = ImageAsset(name: "pencil")
  internal static let placeholderGray = ImageAsset(name: "placeholder_gray")
  internal static let poweredByGoogle = ImageAsset(name: "poweredByGoogle")
  internal static let square = ImageAsset(name: "square")
  internal static let squareChecked = ImageAsset(name: "square_checked")
  internal static let squareGray = ImageAsset(name: "square_gray")
}
// swiftlint:enable identifier_name line_length nesting type_body_length type_name

// MARK: - Implementation Details

internal final class ColorAsset {
  internal fileprivate(set) var name: String

  #if os(macOS)
  internal typealias Color = NSColor
  #elseif os(iOS) || os(tvOS) || os(watchOS)
  internal typealias Color = UIColor
  #endif

  @available(iOS 11.0, tvOS 11.0, watchOS 4.0, macOS 10.13, *)
  internal private(set) lazy var color: Color = {
    guard let color = Color(asset: self) else {
      fatalError("Unable to load color asset named \(name).")
    }
    return color
  }()

  #if os(iOS) || os(tvOS)
  @available(iOS 11.0, tvOS 11.0, *)
  internal func color(compatibleWith traitCollection: UITraitCollection) -> Color {
    let bundle = BundleToken.bundle
    guard let color = Color(named: name, in: bundle, compatibleWith: traitCollection) else {
      fatalError("Unable to load color asset named \(name).")
    }
    return color
  }
  #endif

  fileprivate init(name: String) {
    self.name = name
  }
}

internal extension ColorAsset.Color {
  @available(iOS 11.0, tvOS 11.0, watchOS 4.0, macOS 10.13, *)
  convenience init?(asset: ColorAsset) {
    let bundle = BundleToken.bundle
    #if os(iOS) || os(tvOS)
    self.init(named: asset.name, in: bundle, compatibleWith: nil)
    #elseif os(macOS)
    self.init(named: NSColor.Name(asset.name), bundle: bundle)
    #elseif os(watchOS)
    self.init(named: asset.name)
    #endif
  }
}

internal struct ImageAsset {
  internal fileprivate(set) var name: String

  #if os(macOS)
  internal typealias Image = NSImage
  #elseif os(iOS) || os(tvOS) || os(watchOS)
  internal typealias Image = UIImage
  #endif

  @available(iOS 8.0, tvOS 9.0, watchOS 2.0, macOS 10.7, *)
  internal var image: Image {
    let bundle = BundleToken.bundle
    #if os(iOS) || os(tvOS)
    let image = Image(named: name, in: bundle, compatibleWith: nil)
    #elseif os(macOS)
    let name = NSImage.Name(self.name)
    let image = (bundle == .main) ? NSImage(named: name) : bundle.image(forResource: name)
    #elseif os(watchOS)
    let image = Image(named: name)
    #endif
    guard let result = image else {
      fatalError("Unable to load image asset named \(name).")
    }
    return result
  }

  #if os(iOS) || os(tvOS)
  @available(iOS 8.0, tvOS 9.0, *)
  internal func image(compatibleWith traitCollection: UITraitCollection) -> Image {
    let bundle = BundleToken.bundle
    guard let result = Image(named: name, in: bundle, compatibleWith: traitCollection) else {
      fatalError("Unable to load image asset named \(name).")
    }
    return result
  }
  #endif
}

internal extension ImageAsset.Image {
  @available(iOS 8.0, tvOS 9.0, watchOS 2.0, *)
  @available(macOS, deprecated,
    message: "This initializer is unsafe on macOS, please use the ImageAsset.image property")
  convenience init?(asset: ImageAsset) {
    #if os(iOS) || os(tvOS)
    let bundle = BundleToken.bundle
    self.init(named: asset.name, in: bundle, compatibleWith: nil)
    #elseif os(macOS)
    self.init(named: NSImage.Name(asset.name))
    #elseif os(watchOS)
    self.init(named: asset.name)
    #endif
  }
}

// swiftlint:disable convenience_type
private final class BundleToken {
  static let bundle: Bundle = {
    #if SWIFT_PACKAGE
    return Bundle.module
    #else
    return Bundle(for: BundleToken.self)
    #endif
  }()
}
// swiftlint:enable convenience_type
