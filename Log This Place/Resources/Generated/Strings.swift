// swiftlint:disable all
// Generated using SwiftGen — https://github.com/SwiftGen/SwiftGen

import Foundation

// swiftlint:disable superfluous_disable_command file_length implicit_return

// MARK: - Strings

// swiftlint:disable explicit_type_interface function_parameter_count identifier_name line_length
// swiftlint:disable nesting type_body_length type_name vertical_whitespace_opening_braces
internal enum L10n {

  internal enum Alert {
    internal enum Close {
      /// Are you sure you want to close? Changes will not be saved.
      internal static let text = L10n.tr("Localizable", "Alert.close.text")
    }
    internal enum Location {
      internal enum Body {
        /// The app requires access to your location. Turn on location services in your device's settings.
        internal static let text = L10n.tr("Localizable", "Alert.location.body.text")
      }
      internal enum Title {
        /// Allow location access
        internal static let text = L10n.tr("Localizable", "Alert.location.title.text")
      }
    }
    internal enum Restriction {
      /// You can only view POIs, you can edit/add only when you are in Malmo
      internal static let text = L10n.tr("Localizable", "Alert.restriction.text")
    }
    internal enum UpdateApp {
      internal enum Body {
        /// Important updates have been released without which the application may not work correctly. Please update the app
        internal static let text = L10n.tr("Localizable", "Alert.updateApp.body.text")
      }
      internal enum Button {
        /// Update the app
        internal static let text = L10n.tr("Localizable", "Alert.updateApp.button.text")
      }
      internal enum Title {
        /// Important updates
        internal static let text = L10n.tr("Localizable", "Alert.updateApp.title.text")
      }
    }
  }

  internal enum AutocompleteSearch {
    internal enum ResultsNotFound {
      /// Oops! We couldn’t find what you were looking for… Try searching for something else.
      internal static let text = L10n.tr("Localizable", "AutocompleteSearch.resultsNotFound.text")
    }
  }

  internal enum BuildingDetails {
    internal enum AccessibilityInformation {
      /// Accessibility Information
      internal static let text = L10n.tr("Localizable", "BuildingDetails.AccessibilityInformation.text")
    }
    internal enum AddYourInformation {
      /// Add your information about this place
      internal static let text = L10n.tr("Localizable", "BuildingDetails.addYourInformation.text")
    }
    internal enum Call {
      /// Call
      internal static let text = L10n.tr("Localizable", "BuildingDetails.call.text")
    }
    internal enum Contribution {
      /// Contribution
      internal static let text = L10n.tr("Localizable", "BuildingDetails.contribution.text")
    }
    internal enum Date {
      /// Latest Update:
      internal static let text = L10n.tr("Localizable", "BuildingDetails.date.text")
    }
    internal enum Directions {
      /// Get Directions
      internal static let text = L10n.tr("Localizable", "BuildingDetails.directions.text")
    }
    internal enum Entrance {
      /// Entrance
      internal static let text = L10n.tr("Localizable", "BuildingDetails.entrance.text")
    }
    internal enum FullyAccessible {
      /// Fully accessible based on your needs
      internal static let text = L10n.tr("Localizable", "BuildingDetails.fullyAccessible.text")
    }
    internal enum Inside {
      /// Inside
      internal static let text = L10n.tr("Localizable", "BuildingDetails.inside.text")
    }
    internal enum Location {
      internal enum Alert {
        /// Invalid location
        internal static let text = L10n.tr("Localizable", "BuildingDetails.location.alert.text")
      }
    }
    internal enum PaymentMethods {
      /// Payment Methods:
      internal static let text = L10n.tr("Localizable", "BuildingDetails.paymentMethods.text")
      internal enum Cash {
        /// Cash
        internal static let text = L10n.tr("Localizable", "BuildingDetails.paymentMethods.cash.text")
      }
      internal enum Swish {
        /// Swish
        internal static let text = L10n.tr("Localizable", "BuildingDetails.paymentMethods.swish.text")
      }
    }
    internal enum Phone {
      internal enum Alert {
        /// This number format is not supported
        internal static let text = L10n.tr("Localizable", "BuildingDetails.phone.alert.text")
      }
    }
    internal enum Toilet {
      /// Toilet
      internal static let text = L10n.tr("Localizable", "BuildingDetails.toilet.text")
    }
    internal enum Website {
      /// Website
      internal static let text = L10n.tr("Localizable", "BuildingDetails.website.text")
      internal enum Alert {
        /// Invalid site address
        internal static let text = L10n.tr("Localizable", "BuildingDetails.website.alert.text")
      }
    }
  }

  internal enum Common {
    internal enum Google {
      internal enum Location {
        internal enum NotFound {
          /// Google location not found
          internal static let text = L10n.tr("Localizable", "Common.Google.Location.NotFound.text")
        }
      }
    }
    internal enum Profile {
      /// Profile
      internal static let text = L10n.tr("Localizable", "Common.Profile.text")
    }
    internal enum AddPicture {
      /// Add picture
      internal static let text = L10n.tr("Localizable", "Common.addPicture.text")
    }
    internal enum Back {
      /// Back
      internal static let text = L10n.tr("Localizable", "Common.back.text")
    }
    internal enum Cancel {
      /// Cancel
      internal static let text = L10n.tr("Localizable", "Common.cancel.text")
    }
    internal enum Close {
      /// Close
      internal static let text = L10n.tr("Localizable", "Common.close.text")
    }
    internal enum Continue {
      /// Continue
      internal static let text = L10n.tr("Localizable", "Common.continue.text")
    }
    internal enum Delete {
      /// Delete
      internal static let text = L10n.tr("Localizable", "Common.delete.text")
    }
    internal enum Done {
      /// Done
      internal static let text = L10n.tr("Localizable", "Common.done.text")
    }
    internal enum Edit {
      /// Edit
      internal static let text = L10n.tr("Localizable", "Common.edit.text")
    }
    internal enum Error {
      /// Error
      internal static let text = L10n.tr("Localizable", "Common.error.text")
      internal enum OpenUrl {
        /// The link cannot be opened, please update the app manually
        internal static let text = L10n.tr("Localizable", "Common.error.openUrl.text")
      }
    }
    internal enum Exit {
      /// Exit
      internal static let text = L10n.tr("Localizable", "Common.exit.text")
    }
    internal enum Finish {
      /// Finish
      internal static let text = L10n.tr("Localizable", "Common.finish.text")
    }
    internal enum LogPlace {
      /// Log Place
      internal static let text = L10n.tr("Localizable", "Common.logPlace.text")
    }
    internal enum Logout {
      /// Logout
      internal static let text = L10n.tr("Localizable", "Common.logout.text")
    }
    internal enum Next {
      /// Next
      internal static let text = L10n.tr("Localizable", "Common.next.text")
    }
    internal enum No {
      /// No
      internal static let text = L10n.tr("Localizable", "Common.no.text")
    }
    internal enum NotFound {
      /// Not found
      internal static let text = L10n.tr("Localizable", "Common.notFound.text")
    }
    internal enum Ok {
      /// OK
      internal static let text = L10n.tr("Localizable", "Common.ok.text")
    }
    internal enum PoiNotFound {
      /// Attractions not found
      internal static let text = L10n.tr("Localizable", "Common.poiNotFound.text")
    }
    internal enum Previous {
      /// Previous
      internal static let text = L10n.tr("Localizable", "Common.previous.text")
    }
    internal enum Save {
      /// Save
      internal static let text = L10n.tr("Localizable", "Common.save.text")
    }
    internal enum Settings {
      /// Settings
      internal static let text = L10n.tr("Localizable", "Common.settings.text")
    }
    internal enum SomethingWentWrong {
      /// Try again, something went wrong
      internal static let text = L10n.tr("Localizable", "Common.somethingWentWrong.text")
    }
    internal enum Start {
      /// Start
      internal static let text = L10n.tr("Localizable", "Common.start.text")
    }
    internal enum Success {
      /// Success
      internal static let text = L10n.tr("Localizable", "Common.success.text")
    }
    internal enum Warning {
      /// Warning
      internal static let text = L10n.tr("Localizable", "Common.warning.text")
    }
    internal enum Yes {
      /// Yes
      internal static let text = L10n.tr("Localizable", "Common.yes.text")
    }
  }

  internal enum ImageSelector {
    internal enum ChooseFromPhotoLibrary {
      /// Choose from photo library
      internal static let text = L10n.tr("Localizable", "ImageSelector.chooseFromPhotoLibrary.text")
    }
    internal enum DeletedSuccessfully {
      /// Image deleted successfully
      internal static let text = L10n.tr("Localizable", "ImageSelector.deletedSuccessfully.text")
    }
    internal enum ErrorDescription {
      /// Image has no CGImageRef or invalid bitmap format
      internal static let text = L10n.tr("Localizable", "ImageSelector.errorDescription.text")
    }
    internal enum Images {
      /// Images
      internal static let text = L10n.tr("Localizable", "ImageSelector.images.text")
    }
    internal enum TakeAPhoto {
      /// Take a photo
      internal static let text = L10n.tr("Localizable", "ImageSelector.takeAPhoto.text")
    }
  }

  internal enum MapView {
    internal enum All {
      /// All
      internal static let text = L10n.tr("Localizable", "MapView.all.text")
    }
    internal enum Poi {
      internal enum CantFindPostalCode {
        /// Place does not have postal code
        internal static let text = L10n.tr("Localizable", "MapView.poi.cantFindPostalCode.text")
      }
      internal enum Outside {
        /// This place is located outside the city of Malmo
        internal static let text = L10n.tr("Localizable", "MapView.poi.outside.text")
      }
    }
    internal enum SearchBar {
      /// Search for a place here
      internal static let placeholder = L10n.tr("Localizable", "MapView.searchBar.placeholder")
    }
  }

  internal enum Onboarding {
    internal enum FirstInfo {
      internal enum Body {
        /// Discover great local places and find out their exact level of accessibility.
        internal static let text = L10n.tr("Localizable", "Onboarding.firstInfo.body.text")
      }
      internal enum Title {
        /// Know before you go
        internal static let text = L10n.tr("Localizable", "Onboarding.firstInfo.title.text")
      }
    }
    internal enum SecondInfo {
      internal enum Body {
        /// Get the latest and most updated accessibility information based on your needs.
        internal static let text = L10n.tr("Localizable", "Onboarding.secondInfo.body.text")
      }
      internal enum Title {
        /// Cool accessible places for you
        internal static let text = L10n.tr("Localizable", "Onboarding.secondInfo.title.text")
      }
    }
    internal enum ThirdInfo {
      internal enum Body {
        /// You can add accessibility information yourself, directly in the app.
        internal static let text = L10n.tr("Localizable", "Onboarding.thirdInfo.body.text")
      }
      internal enum Title {
        /// Let’s make Malmö accessible together!
        internal static let text = L10n.tr("Localizable", "Onboarding.thirdInfo.title.text")
      }
    }
  }

  internal enum Picker {
    /// Select an option
    internal static let placeholder = L10n.tr("Localizable", "Picker.placeholder")
  }

  internal enum PoisInformation {
    internal enum FirstItem {
      internal enum Subtitle {
        /// Tap to add information
        internal static let text = L10n.tr("Localizable", "PoisInformation.firstItem.subtitle.text")
      }
      internal enum Title {
        /// Unlogged places
        internal static let text = L10n.tr("Localizable", "PoisInformation.firstItem.title.text")
      }
    }
    internal enum FourthItem {
      internal enum Subtitle {
        /// Based on your profile
        internal static let text = L10n.tr("Localizable", "PoisInformation.fourthItem.subtitle.text")
      }
      internal enum Title {
        /// Fully accessible
        internal static let text = L10n.tr("Localizable", "PoisInformation.fourthItem.title.text")
      }
    }
    internal enum SecondItem {
      internal enum Title {
        /// There is accessibility information
        internal static let text = L10n.tr("Localizable", "PoisInformation.secondItem.title.text")
      }
    }
    internal enum ThirdItem {
      internal enum Subtitle {
        /// Based on your profile
        internal static let text = L10n.tr("Localizable", "PoisInformation.thirdItem.subtitle.text")
      }
      internal enum Title {
        /// Partially accessible
        internal static let text = L10n.tr("Localizable", "PoisInformation.thirdItem.title.text")
      }
    }
    internal enum Title {
      /// Colour meaning
      internal static let text = L10n.tr("Localizable", "PoisInformation.title.text")
    }
  }

  internal enum Profile {
    internal enum Alert {
      internal enum RegisterPermanentAccount {
        /// Now you can use your username and password to log into your account
        internal static let text = L10n.tr("Localizable", "Profile.alert.registerPermanentAccount.text")
      }
    }
    internal enum Button {
      internal enum Delete {
        internal enum Account {
          /// Delete account
          internal static let text = L10n.tr("Localizable", "Profile.button.delete.account.text")
        }
        internal enum Contribution {
          /// Delete contributions
          internal static let text = L10n.tr("Localizable", "Profile.button.delete.contribution.text")
        }
      }
    }
    internal enum DeleteAccount {
      internal enum Alert {
        /// Are you sure you want to delete your account?
        internal static let text = L10n.tr("Localizable", "Profile.deleteAccount.alert.text")
      }
    }
    internal enum DeletedAccount {
      internal enum Alert {
        /// Your user account has been successfully deleted. The process to delete you user profile as well as your contributions has been initiated and may take some minutes.
        internal static let text = L10n.tr("Localizable", "Profile.deletedAccount.alert.text")
      }
    }
    internal enum Profile {
      /// Profile
      internal static let text = L10n.tr("Localizable", "Profile.profile.text")
    }
  }

  internal enum Questionnaire {
    internal enum Poi {
      /// Questionnaire
      internal static let title = L10n.tr("Localizable", "Questionnaire.Poi.title")
    }
    internal enum AddNewPoi {
      internal enum Empty {
        internal enum Error {
          /// This place can’t be added to the map because you didn’t answer any questions.
          internal static let text = L10n.tr("Localizable", "Questionnaire.addNewPoi.empty.error.text")
        }
      }
      internal enum Subtitle {
        /// Check if the information is correct and start answering the questions for each group.
        internal static let text = L10n.tr("Localizable", "Questionnaire.addNewPoi.subtitle.text")
      }
      internal enum Title {
        /// Add a new place
        internal static let text = L10n.tr("Localizable", "Questionnaire.addNewPoi.title.text")
      }
    }
    internal enum Address {
      /// Address
      internal static let text = L10n.tr("Localizable", "Questionnaire.address.text")
    }
    internal enum Alert {
      internal enum LoadImage {
        internal enum Error {
          internal enum ExceedLimit {
            internal enum Body {
              /// You can only upload a maximum of 20 images for each question. Please try again with less images.
              internal static let text = L10n.tr("Localizable", "Questionnaire.alert.loadImage.error.exceedLimit.body.text")
            }
            internal enum Title {
              /// Upload failed!
              internal static let text = L10n.tr("Localizable", "Questionnaire.alert.loadImage.error.exceedLimit.title.text")
            }
          }
          internal enum Other {
            internal enum Message {
              /// Something wrong when uploading images. Please try again. This error has been reported to our technical team.
              internal static let text = L10n.tr("Localizable", "Questionnaire.alert.loadImage.error.other.message.text")
            }
            internal enum Title {
              /// Upload failed!
              internal static let text = L10n.tr("Localizable", "Questionnaire.alert.loadImage.error.other.title.text")
            }
          }
          internal enum Partially {
            internal enum Message {
              /// Some error occurred and not all pictures could be uploaded to the server.
              internal static let text = L10n.tr("Localizable", "Questionnaire.alert.loadImage.error.partially.message.text")
              internal enum NumberSuccessfullyUploaded {
                /// Number of successfully uploaded pictures:
                internal static let text = L10n.tr("Localizable", "Questionnaire.alert.loadImage.error.partially.message.numberSuccessfullyUploaded.text")
              }
              internal enum NumberUnsuccessfullyUploaded {
                /// Number of unsuccessfully uploaded pictures:
                internal static let text = L10n.tr("Localizable", "Questionnaire.alert.loadImage.error.partially.message.numberUnsuccessfullyUploaded.text")
              }
            }
            internal enum Title {
              /// Could not upload all pictures
              internal static let text = L10n.tr("Localizable", "Questionnaire.alert.loadImage.error.partially.title.text")
            }
          }
        }
        internal enum Success {
          internal enum Message {
            /// Congratulations! You have successfully uploaded your images.
            internal static let text = L10n.tr("Localizable", "Questionnaire.alert.loadImage.success.message.text")
          }
          internal enum Title {
            /// Upload complete!
            internal static let text = L10n.tr("Localizable", "Questionnaire.alert.loadImage.success.title.text")
          }
        }
      }
    }
    internal enum GoToAccDetailsPage {
      /// Go to Acc Details page
      internal static let text = L10n.tr("Localizable", "Questionnaire.goToAccDetailsPage.text")
    }
    internal enum Logout {
      internal enum Body {
        /// Do you really want to exit?
        internal static let text = L10n.tr("Localizable", "Questionnaire.logout.body.text")
      }
      internal enum Title {
        /// Log-out
        internal static let text = L10n.tr("Localizable", "Questionnaire.logout.title.text")
      }
    }
    internal enum Name {
      /// Name
      internal static let text = L10n.tr("Localizable", "Questionnaire.name.text")
    }
    internal enum NotRelevant {
      /// Not relevant
      internal static let text = L10n.tr("Localizable", "Questionnaire.notRelevant.text")
    }
    internal enum PlaceName {
      /// Place name
      internal static let text = L10n.tr("Localizable", "Questionnaire.placeName.text")
    }
    internal enum QuestionNotRelevant {
      /// This question is not relevant
      internal static let text = L10n.tr("Localizable", "Questionnaire.questionNotRelevant.text")
    }
    internal enum Recap {
      /// Here’s a recap on the answers shared
      internal static let text = L10n.tr("Localizable", "Questionnaire.recap.text")
    }
    internal enum ThanksForAnswering {
      /// Thanks for answering
      internal static let text = L10n.tr("Localizable", "Questionnaire.thanksForAnswering.text")
    }
    internal enum Toast {
      internal enum LoadImage {
        internal enum Calm {
          internal enum Message {
            /// Larger files take longer to upload. 
            /// Please stay connected to the internet 
            /// until upload is complete.
            internal static let text = L10n.tr("Localizable", "Questionnaire.toast.loadImage.calm.message.text")
          }
        }
      }
    }
    internal enum UpdatePoi {
      internal enum Empty {
        internal enum Error {
          /// This place can’t be updated because you didn’t answer any questions.
          internal static let text = L10n.tr("Localizable", "Questionnaire.updatePoi.empty.error.text")
        }
      }
    }
  }

  internal enum SelectedPointOfInterest {
    internal enum AnswerQuestionsButton {
      /// Answer questions about this place
      internal static let title = L10n.tr("Localizable", "SelectedPointOfInterest.answerQuestionsButton.title")
    }
    internal enum CheckAccessibilityDetailsButton {
      /// Accessibility Details
      internal static let title = L10n.tr("Localizable", "SelectedPointOfInterest.checkAccessibilityDetailsButton.title")
    }
  }

  internal enum Summary {
    internal enum BackToMap {
      /// Back to Map
      internal static let text = L10n.tr("Localizable", "Summary.backToMap.text")
    }
    internal enum BackToPoi {
      /// Back to POI
      internal static let text = L10n.tr("Localizable", "Summary.backToPoi.text")
    }
    internal enum Completed {
      /// Completed
      internal static let text = L10n.tr("Localizable", "Summary.completed.text")
    }
    internal enum Description {
      /// Here is the accessibility information you’ve added
      internal static let text = L10n.tr("Localizable", "Summary.description.text")
    }
    internal enum EditGroup {
      /// Edit this group
      internal static let text = L10n.tr("Localizable", "Summary.editGroup.text")
    }
    internal enum NotAnswered {
      /// Not Answered
      internal static let text = L10n.tr("Localizable", "Summary.notAnswered.text")
    }
    internal enum NotCompleted {
      /// Not Completed
      internal static let text = L10n.tr("Localizable", "Summary.notCompleted.text")
    }
    internal enum Summary {
      /// Summary
      internal static let text = L10n.tr("Localizable", "Summary.summary.text")
    }
    internal enum UploadedImages {
      /// Uploaded Images
      internal static let text = L10n.tr("Localizable", "Summary.uploadedImages.text")
    }
  }

  internal enum UserIdentification {
    internal enum AccountCreated {
      internal enum Body {
        /// You will soon start completing your profile.
        internal static let text = L10n.tr("Localizable", "UserIdentification.accountCreated.body.text")
      }
      internal enum Title {
        /// Account created
        internal static let text = L10n.tr("Localizable", "UserIdentification.accountCreated.title.text")
      }
    }
    internal enum Authorization {
      /// Authorization
      internal static let text = L10n.tr("Localizable", "UserIdentification.authorization.text")
    }
    internal enum CreateAccount {
      /// Create new account
      internal static let text = L10n.tr("Localizable", "UserIdentification.createAccount.text")
    }
    internal enum Email {
      /// Email
      internal static let placeholder = L10n.tr("Localizable", "UserIdentification.email.placeholder")
    }
    internal enum EmailNotValid {
      /// Email not valid
      internal static let text = L10n.tr("Localizable", "UserIdentification.emailNotValid.text")
    }
    internal enum LogIn {
      /// Log in
      internal static let text = L10n.tr("Localizable", "UserIdentification.logIn.text")
    }
    internal enum MakePermanent {
      /// Make a permanent account
      internal static let text = L10n.tr("Localizable", "UserIdentification.makePermanent.text")
    }
    internal enum Password {
      /// Password
      internal static let placeholder = L10n.tr("Localizable", "UserIdentification.password.placeholder")
    }
    internal enum PasswordMismatch {
      /// Password mismatch
      internal static let text = L10n.tr("Localizable", "UserIdentification.passwordMismatch.text")
    }
    internal enum PasswordNotValid {
      /// Password not valid
      internal static let text = L10n.tr("Localizable", "UserIdentification.passwordNotValid.text")
    }
    internal enum Registration {
      /// Registration
      internal static let text = L10n.tr("Localizable", "UserIdentification.registration.text")
    }
    internal enum RepeatedPassword {
      /// Repeated password
      internal static let placeholder = L10n.tr("Localizable", "UserIdentification.repeatedPassword.placeholder")
    }
  }

  internal enum Welcome {
    internal enum AlreadyHaveAccount {
      /// Already have an account? Log in
      internal static let text = L10n.tr("Localizable", "Welcome.alreadyHaveAccount.text")
    }
    internal enum AreYouReady {
      /// Are you ready?
      internal static let text = L10n.tr("Localizable", "Welcome.areYouReady.text")
    }
    internal enum ContinueAsGuest {
      /// Remain as guest
      internal static let text = L10n.tr("Localizable", "Welcome.continueAsGuest.text")
    }
    internal enum CreateProfile {
      /// Create new account
      internal static let text = L10n.tr("Localizable", "Welcome.createProfile.text")
    }
    internal enum FirstDescriptionItem {
      /// Go through each one of the questions and answer the ones that are relevant for you.
      internal static let text = L10n.tr("Localizable", "Welcome.firstDescriptionItem.text")
    }
    internal enum FirstTitleItem {
      /// How does it work?
      internal static let text = L10n.tr("Localizable", "Welcome.firstTitleItem.text")
    }
    internal enum FourDescriptionItem {
      /// We will never share your personal information to anyone. Never. This is meant for you.
      internal static let text = L10n.tr("Localizable", "Welcome.fourDescriptionItem.text")
    }
    internal enum FourTitleItem {
      /// What happens with my data?
      internal static let text = L10n.tr("Localizable", "Welcome.fourTitleItem.text")
    }
    internal enum SecondDescriptionItem {
      /// We recommend you create a new account with your email address but you can also complete the profile while remaining as a guest.
      internal static let text = L10n.tr("Localizable", "Welcome.secondDescriptionItem.text")
    }
    internal enum SecondTitleItem {
      /// Do I need to create a new account?
      internal static let text = L10n.tr("Localizable", "Welcome.secondTitleItem.text")
    }
    internal enum ThirdDescriptionItem {
      /// Creating a profile help you find the right place for you based on your specific needs.
      internal static let text = L10n.tr("Localizable", "Welcome.thirdDescriptionItem.text")
    }
    internal enum ThirdTitleItem {
      /// Why is it important to create a profile?
      internal static let text = L10n.tr("Localizable", "Welcome.thirdTitleItem.text")
    }
    internal enum Title {
      /// Complete your profile
      internal static let text = L10n.tr("Localizable", "Welcome.title.text")
    }
  }
}
// swiftlint:enable explicit_type_interface function_parameter_count identifier_name line_length
// swiftlint:enable nesting type_body_length type_name vertical_whitespace_opening_braces

// MARK: - Implementation Details

extension L10n {
  private static func tr(_ table: String, _ key: String, _ args: CVarArg...) -> String {
    let format = BundleToken.bundle.localizedString(forKey: key, value: nil, table: table)
    return String(format: format, locale: Locale.current, arguments: args)
  }
}

// swiftlint:disable convenience_type
private final class BundleToken {
  static let bundle: Bundle = {
    #if SWIFT_PACKAGE
    return Bundle.module
    #else
    return Bundle(for: BundleToken.self)
    #endif
  }()
}
// swiftlint:enable convenience_type
