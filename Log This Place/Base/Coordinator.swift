//
//  Coordinator.swift
//  Log This Place
//
//

import UIKit

protocol Coordinator: AnyObject {
    var childCoordinators: [Coordinator] { get }
}
