//
//  BaseViewController.swift
//  Log This Place
//

import UIKit

class BaseViewController: UIViewController, ViewController {
    
    var onViewDidDisappear: (() -> Void)?
    
    private lazy var keyboardObserver = KeyboardObserver()
    private lazy var loadingContainer = LoadingView()
    private lazy var toastView = ToastView()
    
    /// Convenience property for subclasses.
    /// If present it will be used to add view to hierarchy.
    var contentView: BaseView? {
        nil
    }
    
    override func viewDidLoad() {
        setupUI()
        setupConstraints()
        setupBehaviour()
        
        view.backgroundColor = .white
        
        let backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        navigationItem.backBarButtonItem = backBarButtonItem
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        onViewDidDisappear?()
    }
    
    func setupUI() {
        if let contentView = contentView {
            view.addSubview(contentView)
        }
    }
    
    func setupConstraints() {
        if let contentView = contentView {
            contentView.snp.makeConstraints { make in
                if contentView.respectBottomSafeArea && contentView.respectTopSafeArea {
                    make.edges.equalTo(view.safeAreaLayoutGuide)
                } else if contentView.respectTopSafeArea {
                    make.top.leading.trailing.equalTo(view.safeAreaLayoutGuide)
                    make.bottom.equalToSuperview()
                } else if contentView.respectBottomSafeArea {
                    make.bottom.leading.trailing.equalTo(view.safeAreaLayoutGuide)
                    make.top.equalToSuperview()
                } else {
                    make.top.bottom.leading.trailing.equalToSuperview()
                }
            }
        }
    }
    
    func setupBehaviour() {
        keyboardObserver.delegate = self
    }

    func showLoading(fullscreen: Bool) {
        view.endEditing(true)
        guard loadingContainer.superview == nil else { return }

        if fullscreen {
            if let navigationController = navigationController {
                navigationController.view.addSubview(loadingContainer)
                loadingContainer.snp.makeConstraints { make in
                    make.top.leading.bottom.trailing.equalTo(navigationController.view)
                }
            } else {
                view.addSubview(loadingContainer)
                loadingContainer.snp.makeConstraints { make in
                    make.edges.equalToSuperview()
                }
            }
        } else {
            view.addSubview(loadingContainer)
            loadingContainer.snp.makeConstraints { make in
                make.top.equalTo(view.safeAreaLayoutGuide.snp.top)
                make.leading.trailing.equalTo(view.safeAreaLayoutGuide)
                make.bottom.equalTo(view)
            }
        }
    }
    
    func hideLoading() {
        loadingContainer.removeFromSuperview()
    }
    
    func showToast(_ message: String) {
        guard toastView.superview == nil else { return }
        let superView = navigationController?.view != nil ? navigationController?.view : view
                
        toastView.fill(with: .init(message: message))
        superView?.addSubviewAnimation(toastView)
        toastView.superview?.bringSubviewToFront(toastView)
        toastView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
            self.toastView.removeFromSuperview()
        }
    }
}

// MARK: - KeyboardObserverDelegate

extension BaseViewController: KeyboardObserverDelegate {
    func keyboardWillShow(with frame: CGRect, duration: Double, curve: UIView.AnimationCurve) {
        guard let keyboardHandlable = contentView as? KeyboardHandlable else { return }
        let safeAreaBottomInset = view.safeAreaInsets.bottom
        let constant = frame.height - safeAreaBottomInset
        keyboardHandlable.adjustKeyboardHeightConstraint(constant: constant, animationDuration: duration, animationCurve: curve)
    }

    func keyboardWillHide(duration: Double, curve: UIView.AnimationCurve) {
        guard let keyboardHandlable = contentView as? KeyboardHandlable else { return }
        keyboardHandlable.adjustKeyboardHeightConstraint(constant: 0, animationDuration: duration, animationCurve: curve)
    }
}
