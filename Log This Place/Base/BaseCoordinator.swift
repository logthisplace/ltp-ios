//
//  BaseCoordinator.swift
//  Log This Place
//

import Foundation


class BaseCoordinator: Coordinator {
    private(set) var childCoordinators = [Coordinator]()
    
    func add(coordinator: Coordinator) {
        guard !childCoordinators.contains(where: { $0 === coordinator }) else { return }
        childCoordinators.append(coordinator)
    }
    
    func remove(coordinator: Coordinator) {
        childCoordinators.removeAll(where: { $0 === coordinator })
    }
}
