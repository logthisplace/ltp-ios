//
//  Fillable.swift
//  Log This Place
//

import Foundation

/// Protocol that all customizable views (with view model) should conform to.
protocol Fillable {
    associatedtype ViewModel

    /// Fill given view with corresponding view model.
    /// - Parameter model: View model to be used for view population.
    func fill(with model: ViewModel)
}
