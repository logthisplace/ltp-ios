//
//  Presenter.swift
//  Log This Place
//

import Foundation

protocol Presenter: AnyObject {
    func viewDidLoad()
    func viewWillAppear()
    func viewWillDisappear() 
}
