//
//  BaseScrollView.swift
//  Log This Place
//

import UIKit

class BaseScrollView: BaseView {
    let scrollView = with(UIScrollView()) {
        $0.contentInsetAdjustmentBehavior = .always
    }

    let scrollableContentView = UIView()

    override func setupUI() {
        super.setupUI()
        addSubview(scrollView)
        scrollView.addSubview(scrollableContentView)
    }

    override func setupConstraints() {
        super.setupConstraints()

        scrollView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }

        scrollableContentView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
            make.width.equalToSuperview()
        }
    }
}
