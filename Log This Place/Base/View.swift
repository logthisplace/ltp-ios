//
//  View.swift
//  Log This Place
//

import UIKit

protocol View: AnyObject {
    var viewControllerIsVisible: Bool { get }
    func showLoading(fullscreen: Bool)
    func hideLoading()
    func showToast(_ message: String)
    func showAlert(title: String?, message: String?, actions: [UIAlertAction]?)
    func showErrorAlert(message: String?)
}

extension View where Self: UIViewController {
    var viewControllerIsVisible: Bool {
        return self.viewIfLoaded?.window != nil
    }
    
    func showAlert(title: String?, message: String?, actions: [UIAlertAction]?) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        if let actions = actions {
            actions.forEach(alertController.addAction)
        } else {
            let okAction = UIAlertAction(title: L10n.Common.Ok.text, style: .default, handler: nil)
            alertController.addAction(okAction)
        }
        present(alertController, animated: true, completion: nil)
    }
    
    func showErrorAlert(message: String?) {
        showAlert(title: L10n.Common.Error.text, message: message, actions: nil)
    }
}

