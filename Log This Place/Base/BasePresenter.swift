//
//  BasePresenter.swift
//  Log This Place
//

import Foundation

class BasePresenter: Presenter {
    
    var viewLoaded = false
    
    func viewDidLoad() {
        viewLoaded = true
    }
    
    func viewWillAppear() {}
    func viewWillDisappear() {}
}
