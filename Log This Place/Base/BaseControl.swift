//
//  BaseControl.swift
//  Log This Place
//

import UIKit

class BaseControl: UIControl {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setup()
    }
    
    func setup() {
        setupUI()
        setupConstraints()
    }
    
    func setupUI() {}
    func setupConstraints() {}
}
