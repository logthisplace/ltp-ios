//
//  WrappingView.swift
//  Log This Place
//

import UIKit

/// Class used for wrapping given view.
final class WrappingView<T: UIView>: UIView {

    /// Axis to be used during configuration. Limits the direction in which view is being setup.
    enum Axis {

        /// Sets only in horizontal direction (centerX, width).
        case horizontal

        /// Sets only in vertical direction (centerY, height).
        case vertical

        /// Sets in horizontal and vertical direction.
        case both

        /// Doesn't set in any direction.
        case none
    }

    /// Alignment to be used during configuration.
    enum Alignment {

        /// Vertical alignment.
        case vertical(VerticalAlignmentEdge)

        /// Horizontal alignment.
        case horizontal(HorizontalAlignmentEdge)
    }

    enum VerticalAlignmentEdge {
        case top
        case bottom
    }

    enum HorizontalAlignmentEdge {
        case leading
        case trailing
    }

    /// Initialize and setup
    /// - Parameters:
    ///   - contentView: View to be wrapped.
    ///   - size: Size of the view.
    ///   - insets: Space between contentView and wrapping view.
    ///   - sizeAxis: Limit size constraints only to given direction.
    ///   - centerAxis: Limit center constraints only to given direction.
    ///   - insetsAxis: Limit edge constraints only to given direction.
    ///   - hardInsetsAxis: Inidcates if edge constraints should be `equal` or more flexible `greaterThanOrEqual`.
    ///    Value that does not correspond to `insetsAxis` is ignored (e.g. `insetsAxis = .horizontal` and `hardInsetsAxis = .vertical`).
    convenience init(
        contentView: T,
        size: CGSize? = nil,
        insets: UIEdgeInsets? = .zero,
        sizeAxis: Axis = .both,
        centerAxis: Axis = .both,
        insetsAxis: Axis = .both,
        hardInsetsAxis: Axis = .both
    ) {
        self.init()

        addSubview(contentView)
        handle(size: size, sizeAxis: sizeAxis, contentView: contentView)
        handle(insets: insets, insetsAxis: insetsAxis, contentView: contentView, hardInsetsAxis: hardInsetsAxis)
        handle(centerAxis: centerAxis, contentView: contentView)
    }

    /// Use this initializer to have wrapped content that will stick to one of the edges.
    /// - Parameters:
    ///   - contentView: View to be wrapped.
    ///   - alignment: Alignment that will influence contentView position in a wrapper.
    convenience init(contentView: T, alignment: Alignment) {
        self.init()
        addSubview(contentView)
        handle(alignment: alignment, contentView: contentView)
    }

    private func handle(size: CGSize?, sizeAxis: Axis, contentView: UIView) {
        if let size = size {
            switch sizeAxis {
            case .horizontal:
                contentView.snp.makeConstraints { make in
                    make.width.equalTo(size.width)
                }
            case .vertical:
                contentView.snp.makeConstraints { make in
                    make.height.equalTo(size.height)
                }
            case .both:
                contentView.snp.makeConstraints { make in
                    make.size.equalTo(size)
                }
            case .none:
                break
            }
        }
    }

    private func handle(insets: UIEdgeInsets?, insetsAxis: Axis, contentView: UIView, hardInsetsAxis: Axis) {
        if let insets = insets {
            switch insetsAxis {
            case .horizontal:
                contentView.snp.makeConstraints { make in
                    switch hardInsetsAxis {
                    case .both, .horizontal:
                        make.leading.equalToSuperview().offset(insets.left)
                        make.trailing.equalToSuperview().offset(-insets.right)
                    case .none, .vertical:
                        make.leading.greaterThanOrEqualToSuperview().offset(insets.left)
                        make.trailing.lessThanOrEqualToSuperview().offset(-insets.right)
                    }
                }
            case .vertical:
                contentView.snp.makeConstraints { make in
                    switch hardInsetsAxis {
                    case .both, .vertical:
                        make.top.equalToSuperview().offset(insets.top)
                        make.bottom.equalToSuperview().offset(-insets.bottom)
                    case .none, .horizontal:
                        make.top.greaterThanOrEqualToSuperview().offset(insets.top)
                        make.bottom.lessThanOrEqualToSuperview().offset(-insets.bottom)
                    }
                }
            case .both:
                handle(insets: insets, insetsAxis: .horizontal, contentView: contentView, hardInsetsAxis: hardInsetsAxis)
                handle(insets: insets, insetsAxis: .vertical, contentView: contentView, hardInsetsAxis: hardInsetsAxis)
            case .none:
                break
            }
        }
    }

    private func handle(centerAxis: Axis, contentView: UIView) {
        switch centerAxis {
        case .horizontal:
            contentView.snp.makeConstraints { make in
                make.centerX.equalToSuperview()
            }
        case .vertical:
            contentView.snp.makeConstraints { make in
                make.centerY.equalToSuperview()
            }
        case .both:
            contentView.snp.makeConstraints { make in
                make.center.equalToSuperview()
            }
        case .none:
            break
        }
    }

    private func handle(alignment: Alignment, contentView: UIView) {
        switch alignment {
        case let .horizontal(horizontalAlignment):
            handle(horizontalAlignment: horizontalAlignment, contentView: contentView)
        case let .vertical(verticalAlignment):
            handle(verticalAlignment: verticalAlignment, contentView: contentView)
        }
    }

    private func handle(horizontalAlignment: HorizontalAlignmentEdge, contentView: UIView) {
        contentView.snp.makeConstraints { make in
            make.top.bottom.equalToSuperview()
            switch horizontalAlignment {
            case .leading:
                make.leading.equalToSuperview()
                make.trailing.lessThanOrEqualToSuperview()
            case .trailing:
                make.leading.greaterThanOrEqualToSuperview()
                make.trailing.equalToSuperview()
            }
        }
    }

    private func handle(verticalAlignment: VerticalAlignmentEdge, contentView: UIView) {
        contentView.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview()
            switch verticalAlignment {
            case .top:
                make.top.equalToSuperview()
                make.bottom.lessThanOrEqualToSuperview()
            case .bottom:
                make.top.greaterThanOrEqualToSuperview()
                make.bottom.equalToSuperview()
            }
        }
    }
}
