//
//  BaseView.swift
//  Log This Place
//

import UIKit

/// Base class that all custom view instances should inherit from.
class BaseView: UIView {

    private(set) lazy var keyboardHeightConstraint = keyboardUILayoutGuide.heightAnchor.constraint(equalToConstant: 0)
    
    var respectBottomSafeArea: Bool {
        true
    }
    
    var respectTopSafeArea: Bool {
        true
    }

    /// Indicates if keyboardLayoutGuide's bottom edge should consider safe area or ignore it.
    var shouldPinKeyboardLayoutGuideToSafeArea: Bool {
        true
    }

    let keyboardUILayoutGuide = UILayoutGuide()

    init() {
        super.init(frame: .zero)
        setupUI()
        setupConstraints()
        setupVisuals()
    }

    init(shouldSetupConstraints: Bool = true) {
        super.init(frame: .zero)
        setupUI()
        if shouldSetupConstraints {
            setupConstraints()
        }
        setupVisuals()
    }

    @available(*, unavailable, message: "use init()")
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    /// Method responsible for adding subviews.
    func setupUI() {}

    /// Method responsible for defining constraints between diffrent subviews.
    func setupConstraints() {
        addLayoutGuide(keyboardUILayoutGuide)
        keyboardHeightConstraint.isActive = true
        keyboardUILayoutGuide.snp.makeConstraints { make in
            if shouldPinKeyboardLayoutGuideToSafeArea {
                make.bottom.equalTo(safeAreaLayoutGuide)
            } else {
                make.bottom.equalToSuperview()
            }
        }
    }

    /// Method responsible for look setup (colors, etc.)
    func setupVisuals() {}
}
