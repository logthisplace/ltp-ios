//
//  ViewController.swift
//  Log This Place
//

import UIKit

protocol ViewController {
    var onViewDidDisappear: (() -> Void)? { get set }
}
