//
//  BuildingDetailsPresenter.swift
//  Log This Place
//

import Foundation
import GoogleMaps
import GooglePlaces
import MapKit
import DKPhotoGallery


protocol BuildingDetailsPresenterInformable: AnyObject {
    func didUpdateBuildingDetail()
}

protocol BuildingDetailsPresenterProtocol: Presenter {
    func addEditAnswerSheetTapped()
    func contantTapped(link: BuildingInformationTextLinkItem.ViewModel.Link?)
    func imageTapped(items: [ImageSource], presentationIndex: Int)
    func didBackTapped()
}

final class BuildingDetailsPresenter: BasePresenter, BuildingDetailsPresenterProtocol, BuildingDetailsPresenterInformable {
    weak var view: BuildingDetailsViewInput?
    
    private let questionnaireDataManager: QuestionnaireDataManagerProtocol = AppContainer.resolve()
    private let pointOfInterestDataManager: PointOfInterestDataManagerProtocol = AppContainer.resolve()
    private let firebaseImageManager: FirebaseImageManagerProtocol = AppContainer.resolve()
    private let placesClient: GooglePlacesProtocol = AppContainer.resolve()
    private let userDefaultsManager: UserDefaultsManagerProtocol = AppContainer.resolve()
    private let userManager: UserManagerProtocol = AppContainer.resolve()
    private let firebaseAnalyticManager: FirebaseAnalyticManagerProtocol = AppContainer.resolve()
    private let answerSheetDataManager: AnswerSheetDataManagerProtocol = AppContainer.resolve()
    private let storageDataManager: StorageDataManagerProtocol = AppContainer.resolve()
    private let accessibleIconsDataManager: AccessibleIconsDataManagerProtocol = AppContainer.resolve()
    private let conditionManager: ConditionManagerProtocol = AppContainer.resolve()

    private var buildingDetailViewModel: BuildingDetailViewModel
    private let coordinator: BuildingCoordinatorProtocol
    private var mapViewPresenterInformable: MapViewPresenterInformable?
    /// for summary screen
    private var questionnaire: Questionnaire?
    /// in the list with information we need to skip groups with id
    private let listsOfGroupsToHideInTabs = [QuestionnaireGroupUUID.general.rawValue,
                                             QuestionnaireGroupUUID.payment.rawValue,
                                             QuestionnaireGroupUUID.experience.rawValue
    ]
    
    /// when a user make some changes we need update poi on the MAP
    private var updatePoiOnMap: Bool = false
    
    init(coordinator: BuildingCoordinatorProtocol,
         buildingDetailViewModel: BuildingDetailViewModel,
         mapViewPresenterInformable: MapViewPresenterInformable?) {
        self.coordinator = coordinator
        self.buildingDetailViewModel = buildingDetailViewModel
        self.mapViewPresenterInformable = mapViewPresenterInformable
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        firebaseAnalyticManager.logEvent(screen: .poiDetail)
        
        updatePOIListener()
    }
    
    private func setPlaceInformation() {
        let placeName = buildingDetailViewModel.placeName ?? L10n.Common.NotFound.text
        let category: String = questionnaire?.categoryBySheet(sheet: buildingDetailViewModel.firebasePOI?._synth?.sheet)?.labels.localized ?? L10n.Common.NotFound.text
        view?.setTitle(placeName)
        view?.fillGeneral(with: .init(name: placeName,
                                      category: category,
                                      description: buildingDetailViewModel.address))
    }
    
    private func fetchPlaceContacts() {
        let placeName = buildingDetailViewModel.placeName ?? L10n.Common.NotFound.text
        guard let placeId = buildingDetailViewModel.firebasePOI?.g_places_id else { return }
        placesClient.fetchPlace(fromPlaceID: placeId,
                                placeFields: [.website, .phoneNumber]) {[weak self] (place: GMSPlace?, error: Error?) in
            guard let self = self else { return }
            if error != nil {
                Logger.shared.error(String(describing: error))
                return
            }
            
            var contacts = [BuildingInformationTextLinkItem.ViewModel]()

            if let websiteUrl = place?.website {
                contacts.append(.init(link: .website(link: websiteUrl)))
            }
            
            if let phoneNumber = place?.phoneNumber {
                contacts.append(.init(link: .phone(link: phoneNumber)))
            }
            
            if let location = self.buildingDetailViewModel.firebasePOI?.location {
                contacts.append(.init(link: .location(link: .init(location: location, placeName: placeName))))
            }
            
            self.view?.fillContacts(with: .init(contacts: contacts))
        }
    }

    private func handlePoiQuestionnaire(questionnaire: Questionnaire) {
        
        /// image groups for accessibility
        var imageFeatureGSReferences = [(name: String, icon: ImageAsset?, references: [String])]()
        /// text groups for accessibility
        var textFeatures = [(name: String, textFeatures: [BuildingFeatureItemView.ViewModel])]()
        
        for questionGroup in questionnaire.questionGroups {
            guard let questionGroupUUID = questionGroup.uuid else { return }
            
            /// getting payment group information for separately view
            if questionGroupUUID == QuestionnaireGroupUUID.payment.rawValue {
                gettingPaymentInformation(paymentQuestionGroup: questionGroup)
            }
            /// skip group if this group_uuid == list `skipQuestionnaireGroupIds
            if listsOfGroupsToHideInTabs.contains(questionGroupUUID) { continue }
            
            /// temporary lists for images and texts
            var textFeaturesGroup = [BuildingFeatureItemView.ViewModel]()
            var imageFeaturesGSReferenceGroup = [String]()

            for question in questionGroup.questions {
                if question.advanced ?? false { continue }

                let featureDescription: String = question.labels.localized
                switch question.type {
                case .selectSingle:
                    if case let .string(answerId) = buildingDetailViewModel.firebasePOI?._synth?.answers(for: question.uuid) {
                        guard let answer = question.values?.filter({ $0.uuid == answerId }).first else { return }
                        textFeaturesGroup.append(.init(featureType: .multy(input: .init(featureDescription: featureDescription,
                                                                                        answers: [answer.labels.localized]))))
                    }
                case .pictures:
                    if case let .array(imageGSReferences) = buildingDetailViewModel.firebasePOI?._synth?.answers(for: question.uuid) {
                        imageFeaturesGSReferenceGroup.append(contentsOf: imageGSReferences)
                    }
                case .boolean:
                    if case let .bool(bool) = buildingDetailViewModel.firebasePOI?._synth?.answers(for: question.uuid) {
                        textFeaturesGroup.append(.init(featureType: .single(input: .init(hasFeature: bool,
                                                                                         featureDescription: featureDescription))))
                    }
                case .selectMulti:
                if case let .array(answerIds) = buildingDetailViewModel.firebasePOI?._synth?.answers(for: question.uuid) {
                    let answers = question.values?.filter({answerIds.contains($0.uuid)}).map({ $0.labels.localized })
                    if let answers = answers, !answers.isEmpty {
                        textFeaturesGroup.append(.init(featureType: .multy(input: .init(featureDescription: featureDescription,
                                                                                        answers: answers))))
                    }
                }
                case .number:
                    if case let .int(int) = buildingDetailViewModel.firebasePOI?._synth?.answers(for: question.uuid) {
                        textFeaturesGroup.append(.init(featureType: .single(input: .init(hasFeature: true, featureDescription: "\(featureDescription): \(int)"))))
                    }
                }
            }
            
            if !textFeaturesGroup.isEmpty {
                textFeatures.append((name: questionGroup.groupName.localized,
                                     textFeatures: textFeaturesGroup))
            }
                        
            if !imageFeaturesGSReferenceGroup.isEmpty {
                imageFeatureGSReferences.append((name: questionGroup.groupName.localized,
                                                 icon: QuestionnaireGroupUUID.init(rawValue: questionGroupUUID)?.thumbnailAsset,
                                                 references: imageFeaturesGSReferenceGroup))
            }
        }
                
        handleBuildingTextFeature(textFeatures: textFeatures)
        handleBuildingImageFeature(imageFeatureGSReferences: imageFeatureGSReferences)
        view?.fillUpdatedAt(buildingDetailViewModel.firebasePOI?._synth?.computed_at)
    }
    
    private func gettingPaymentInformation(paymentQuestionGroup: QuestionGroup) {
        var paymentMethods = [String]()
        if doesPaymentMethodExist(paymentQuestionGroup: paymentQuestionGroup,
                                  questionId: paymentMethodsSwishQuestionUUID) {
            paymentMethods.append(L10n.BuildingDetails.PaymentMethods.Swish.text)
        }
        if doesPaymentMethodExist(paymentQuestionGroup: paymentQuestionGroup,
                                  questionId: paymentMethodsCashQuestionUUID) {
            paymentMethods.append(L10n.BuildingDetails.PaymentMethods.Cash.text)
        }
        view?.fillPayment(with: .init(paymentMethods: paymentMethods))
    }
    
    private func doesPaymentMethodExist(paymentQuestionGroup: QuestionGroup,
                                        questionId: String) -> Bool {
        if let question = paymentQuestionGroup.questions.first(where: { $0.uuid == questionId }) {
            if case let .bool(value) = buildingDetailViewModel.firebasePOI?._synth?.answers(for: question.uuid) {
                return value
            }
            return false
        }
        return false
    }
    
    // MARK: - Handle Image Groups
    private func handleBuildingImageFeature(imageFeatureGSReferences imageFeatureGSReferencesGroups: [(name: String,
                                                                                                       icon: ImageAsset?,
                                                                                                       references: [String])]) {
        
        /// if we have not images then getting image from google
        if imageFeatureGSReferencesGroups.isEmpty {
            view?.fillImageFeatures(with: [])
            guard let googlePlaceId = buildingDetailViewModel.firebasePOI?.g_places_id else { return }
            fetchGooglePlacePhoto(googlePlaceId)
            return
        }
        
        /// place photo for gallery at the top
        var placePhotos = [ImageSource]()
        /// photo for accessible groups
        var imageFeatures = [BuildingAccessibilityImageView.ViewModel]()
        
        /// getting image Download URL for each image
        let dispatchGroup = DispatchGroup()
        for imageFeatureGSReferencesGroup in imageFeatureGSReferencesGroups {
            dispatchGroup.enter()
            storageDataManager.getImageURLs(imageGSReferences: imageFeatureGSReferencesGroup.references) {result in
                if case let .success(imageSources) = result {
                    if !imageSources.isEmpty {
                        placePhotos.append(contentsOf: imageSources)
                        imageFeatures.append(.init(icon: imageFeatureGSReferencesGroup.icon,
                                                   name: imageFeatureGSReferencesGroup.name,
                                                   images: imageSources))
                    }
                }
                dispatchGroup.leave()
            }
        }
        
        dispatchGroup.notify(queue: .main) { [weak self] in
            /// sorting by name, we can change this and add logic for example for sorting by ID
            let sortedImageFeatures = imageFeatures.sorted(by: { $0.name < $1.name })
            self?.view?.fillImageFeatures(with: sortedImageFeatures)
            self?.view?.fillGallery(placePhotos)
        }
    }
    
    // MARK: - Handle Text Groups
    private func handleBuildingTextFeature(textFeatures textGeaturesGroups: [(name: String, textFeatures: [BuildingFeatureItemView.ViewModel])]) {
        
        /// generation bottom tab bar title
        var titles = [String]()
        titles.append(contentsOf: textGeaturesGroups.map({ $0.name }))
        
        /// generation bottom tab bar content
        var features = [[BuildingFeatureItemView.ViewModel]]()
        features.append(contentsOf: textGeaturesGroups.map({ $0.textFeatures }))
        
        view?.fillTextFeature(titles: titles, features: features)
    }
    
    // MARK: - Handle Building Profile Accessibility
    private func handleBuildingProfileAccessibility(_ accessibleProfileIcon: AccessibleIcon) {
        guard let answerSheet = buildingDetailViewModel.firebasePOI?._synth?.sheet else { return }
        
        var icons = [BuildingAccessibilityProfileIcon.ViewModel]()
        
        accessibleProfileIcon.data.forEach {
            guard let condition = $0.condition else { return }
            if conditionManager.isConditionBasedOnAnswers(condition: condition,
                                                          answeSheet: answerSheet,
                                                          generateAnswersForNotCondition: true) {
                guard let icon = AccessibleIconIDs(rawValue: $0.uuid) else { return }
                icons.append(.init(icon: icon.thumbnailAsset.image,
                                   info: $0.labels.localized,
                                   isChecked: icons.count == 0))
            }
        }
        
        view?.fillAccessibleProfile(fullyAccessible: false,
                                    accessibilityIcons: icons)
    }
}

// MARK: - Fetch logic
extension BuildingDetailsPresenter {
    
    private func fetchData() {
        view?.showLoading(fullscreen: true)
        
        fetchPOI() { [weak self] poi in
            /// refresh current poi data
            self?.buildingDetailViewModel.firebasePOI = poi
            
            /// fetch place contacts information
            self?.fetchPlaceContacts()
            
            /// fetch building accessible profile(icons at the top)
            self?.fetchBuldingAccessibleProfile()
            
            /// set information from answerSheet
            self?.fetchPoiQuestionnaire { [weak self] questionnaire in
                self?.questionnaire = questionnaire
                self?.view?.hideLoading()
                guard let questionnaire = questionnaire else {
                    // TODO: Handle this case
                    return
                }
                
                /// set information about current POI
                self?.setPlaceInformation()
                                
                self?.handlePoiQuestionnaire(questionnaire: questionnaire)
            }
        }
    }
    
    private func fetchBuldingAccessibleProfile() {
        accessibleIconsDataManager.getAccessibleIconLogic() { [weak self] result in
            if case let .success(accessibleIcon) = result {
                guard let accessibleIcon = accessibleIcon else { return }
                self?.handleBuildingProfileAccessibility(accessibleIcon)
            }
        }
    }
    
    private func fetchPoiQuestionnaire(completionHandler: @escaping (Questionnaire?) -> Void) {
        questionnaireDataManager.getQuestionnaire(questionnaireType: .poi) { [weak self] result in
            switch result {
            case let .failure(error):
                self?.view?.hideLoading()
                self?.view?.showErrorAlert(message: error.localizedDescription)
            case let .success(questionnaire):
                completionHandler(questionnaire)
            }
        }
    }
    
    private func fetchPOI(completionHandler: @escaping (PointOfInterest?) -> Void) {
        guard let poiId = buildingDetailViewModel.firebasePOI?.id else {
            view?.showErrorAlert(message: L10n.Common.SomethingWentWrong.text)
            return
        }
        
        pointOfInterestDataManager.getPointOfInterest(poiId: poiId) { [weak self] result in
            guard let self = self else { return }
            switch result {
            case let .success(questionnaire):
                completionHandler(questionnaire)
            case let .failure(error):
                self.view?.hideLoading()
                self.view?.showErrorAlert(message: error.localizedDescription)
            }
        }
    }
}

// MARK: - Tapped logic
extension BuildingDetailsPresenter {
    
    // MARK: - Edit OR Add new answer sheet
    func addEditAnswerSheetTapped() {
        if !viewLoaded {
            return
        }
        
        guard let poiId = buildingDetailViewModel.firebasePOI?.id else {
            view?.showErrorAlert(message: L10n.Common.SomethingWentWrong.text)
            return
        }
        let placeName = self.buildingDetailViewModel.placeName ?? L10n.Common.NotFound.text
        let category = self.buildingDetailViewModel.firebasePOI?.category
        let address = self.buildingDetailViewModel.address
        
        pointOfInterestDataManager.removeUpdatePoiListener()
        view?.showLoading(fullscreen: true)
        answerSheetDataManager.getAnswerSheetUserByDocumentId(poiId: poiId) { [weak self] result in
            guard let self = self else { return }
            self.view?.hideLoading()
            
            switch result {
            /// current user HAS own answer sheet, then he can EDIT
            case .success:
                self.coordinator.showSummary(viewModel: .init(processType: .edit(input: .answerSheet),
                                                              input: .init(placeId: poiId,
                                                                           placeName: placeName,
                                                                           placeAddress: address,
                                                                           questionnaire: self.questionnaire)), informable: self)
            case let .failure(error):
                if let error = error as? FirestoreError {
                    /// current user DOESN NOT has own answer sheet, then he can ADD own answer sheet
                    if error == FirestoreError.documentDoesntExist {
                        self.coordinator.showQuestionnaire(mode: .add(input: .answerSheet(input: .init(placeId: poiId,
                                                                                                       category: category,
                                                                                                       placeName: placeName,
                                                                                                       address: address))), buildingDetailsPresenterInformable: self)
                    }
                } else {
                    self.view?.showErrorAlert(message: L10n.Common.SomethingWentWrong.text)
                }
            }
        }
    }
    
    func didBackTapped() {
        view?.hideLoading()
        pointOfInterestDataManager.removeUpdatePoiListener()
        coordinator.didBackTapped()
    }
    
    func contantTapped(link: BuildingInformationTextLinkItem.ViewModel.Link?) {
        switch link {
        case let .phone(link):
            if let number = URL(string: "tel://\(link.removeWhitespace())") {
                UIApplication.shared.open(number)
            } else {
                view?.showAlert(title: "",
                                message: L10n.BuildingDetails.Phone.Alert.text,
                                actions: [
                                    UIAlertAction.init(title: L10n.Common.Ok.text, style: .default, handler: { (action) in }),
                                ])
            }
        case let .website(link):
            if UIApplication.shared.canOpenURL(link) {
                UIApplication.shared.open(link, options: [:])
            } else {
                view?.showAlert(title: "",
                                message: L10n.BuildingDetails.Website.Alert.text,
                                actions: [
                                    UIAlertAction.init(title: L10n.Common.Ok.text, style: .default, handler: { (action) in }),
                                ])
            }
        case let .location(link):
            let mapItem = MKMapItem(placemark: MKPlacemark(coordinate: link.location.coreLocation, addressDictionary:nil))
            mapItem.name = link.placeName
            mapItem.openInMaps(launchOptions: [MKLaunchOptionsDirectionsModeKey : MKLaunchOptionsDirectionsModeDriving])
        case .none:
            break
        }
    }
    
    func imageTapped(items: [ImageSource], presentationIndex: Int) {
        var galleryItems = [DKPhotoGalleryItem]()
        items.forEach({
            switch $0 {
            case let .link(link):
                guard let imageURL = URL(string: link) else { return }
                galleryItems.append(.init(imageURL: imageURL))
            case let .media(media):
                galleryItems.append(.init(image: media))
            }
        })
        coordinator.showGallery(items: galleryItems, presentationIndex: presentationIndex)
    }
}

extension BuildingDetailsPresenter {
    func didUpdateBuildingDetail() {
        updatePOIListener()
        updatePoiOnMap = true
    }
}

// MARK: - Update POI listener
extension BuildingDetailsPresenter {

    func updatePOIListener() {
        guard let poiId = buildingDetailViewModel.firebasePOI?.id else { return }
        
        /// update listener can call two times
        pointOfInterestDataManager.updatePoiListener(poiId: poiId) { [weak self]  result in
            guard let self = self else { return }
            switch result {
            case .success(_):
                // TODO: Optimize this. For example update only answerSheet
                self.fetchData()
            case let .failure(error):
                self.view?.hideLoading()
                self.view?.showErrorAlert(message: error.localizedDescription)
            }
        }
    }
}

// MARK: - Handle Google Place Photo Info
extension BuildingDetailsPresenter {
    private func fetchGooglePlacePhoto(_ googlePlaceId: String) {
        let placeFields: GMSPlaceField = [.photos]
        placesClient.fetchPlace(fromPlaceID: googlePlaceId, placeFields: placeFields, callback: {
            [weak self] (place: GMSPlace?, error: Error?) in
            guard let self = self else { return }
            if let error = error {
                // TODO: Handle the error.
                Logger.shared.error(String(describing: error))
                return
            }
            
            self.handleGooglePlacePhotos(place)
        })
    }
    
    private func handleGooglePlacePhotos(_ place: GMSPlace?) {
        guard let googlePlacePhotos = place?.photos?.prefix(4) else { return }

        var placePhotos = [ImageSource]()
        let dispatchGroup = DispatchGroup()

        for photo in googlePlacePhotos {
            dispatchGroup.enter()
            placesClient.loadPlacePhoto(photo, callback: { (photo, error) -> Void in
                if let error = error {
                    // TODO: Handle the error.
                    Logger.shared.error(String(describing: error))
                    dispatchGroup.leave()
                    return
                }

                if let photo = photo {
                    placePhotos.append(.media(photo))
                }

                dispatchGroup.leave()
            })
        }

        dispatchGroup.notify(queue: .main) {
            self.view?.fillGallery(placePhotos)
        }
    }
}
