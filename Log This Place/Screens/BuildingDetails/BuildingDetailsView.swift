//
//  BuildingDetailsView.swift
//  Log This Place
//

import UIKit

final class BuildingDetailsView: BaseScrollView {

    private let rootStackView = UIStackView.vertical
    
    private let buildingHeaderInfoStackView = UIStackView.vertical
    private let generalInfoStackView = with(UIStackView.vertical) {
        $0.edgeInsets(topSpacing: 0, bottomSpacing: 0, leadingSpacing: 10, trailingSpacing: 10)
        $0.spacing = 10
    }
    
    let gallery = BuildingPhotoGalleryView()
    let buildingAccessibilityProfileView = with(BuildingAccessibilityProfileView()) {
        $0.isHidden = true
    }
    let buidingGeneralInfoView = BuildingGeneralInfoView()
    let buildingPaymentInfoView = with(BuildingPaymentInfoView()) {
        $0.isHidden = true
    }
    let buildingAccessibilityView = with(BuildingAccessibilityView()) {
        $0.isHidden = true
    }
    let contributionView = BuildingContributionView()
    
    var onContactTapped: ((BuildingInformationTextLinkItem.ViewModel.Link?) -> Void)?
    var onImageTapped: (([ImageSource], Int) -> Void)?
    
    private let imagePickerManager: ImagePickerManagerProtocol
    
    init(imagePickerManager: ImagePickerManagerProtocol) {
        self.imagePickerManager = imagePickerManager
        super.init()
        if #available(iOS 13.0, *) {
            scrollView.automaticallyAdjustsScrollIndicatorInsets = false
        }
    }

    
    override func setupUI() {
        super.setupUI()
        backgroundColor = #colorLiteral(red: 0.9607843137, green: 0.968627451, blue: 0.9764705882, alpha: 1)
        
        scrollableContentView.addSubview(rootStackView)
        
        buildingHeaderInfoStackView.addArrangedSubviews([
            gallery,
            buildingAccessibilityProfileView,
            .fillingVertical
        ])
        rootStackView.addArrangedSubviews([
            buildingHeaderInfoStackView,
            buidingGeneralInfoView,
            buildingPaymentInfoView,
            buildingAccessibilityView,
            contributionView,
            .fillingVertical
        ])
    }
    
    override func setupConstraints() {
        super.setupConstraints()
        rootStackView.positionToTheEdges()
        rootStackView.spacing = 8
        
        gallery.snp.makeConstraints { make in
            make.left.right.equalToSuperview()
            make.height.equalTo(200)
        }
        
    }
}
