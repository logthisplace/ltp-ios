//
//  ContributionView.swift
//  Log This Place
//
//

import UIKit


final class BuildingContributionView: BaseView {
    
    private let rootStackView = with(UIStackView.vertical) {
        $0.spacing = 12
        $0.edgeInsets(topSpacing: 16, bottomSpacing: 16, leadingSpacing: 16, trailingSpacing: 16)
    }
    
    private let titleLabel = with(UILabel.headlineMediumSemiBold) {
        $0.text = L10n.BuildingDetails.Contribution.text
    }
    let addEditAnswerSheetButton = with(LTPButton.standard(style: .black)) {
        $0.setTitle(L10n.BuildingDetails.AddYourInformation.text, for: .normal)
    }
    
    override func setupUI() {
        super.setupUI()
        addSubview(rootStackView)
        
        rootStackView.addArrangedSubviews([
            titleLabel,
            addEditAnswerSheetButton
        ])
    }
    
    override func setupConstraints() {
        super.setupConstraints()
        backgroundColor = .white
        
        rootStackView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
    }
    
}
