//
//  BuildingGeneralInfoView.swift
//  Log This Place
//
//

import UIKit


class BuildingGeneralInfoView: BaseView {
    
    struct ViewModelGeneral {
        let name: String?
        let category: String?
        let description: String?
    }
    
    struct ViewModelContact {
        let contacts: [BuildingInformationTextLinkItem.ViewModel]
    }
    private let rootStackView = with(UIStackView.vertical) {
        $0.edgeInsets(topSpacing: 16, bottomSpacing: 16, leadingSpacing: 16, trailingSpacing: 16)
    }
    
    /// general information
    private let rootPlaceInformationStackView = with(UIStackView.vertical) {
        $0.spacing = 8
    }
    private let nameLabel = with(UILabel.titleH2Bold) {
        $0.numberOfLines = 0
    }
    private let badge = BuildingGeneralTextBadgeView()
    private let descriptioneLabel = with(UILabel.standartMediumRegular) {
        $0.numberOfLines = 0
    }
    
    /// contacts
    private let rootContactsStackView = with(UIStackView.vertical) {
        $0.edgeInsets(topSpacing: 8, bottomSpacing: 0, leadingSpacing: 0, trailingSpacing: 0)
        $0.spacing = 8
        $0.isHidden = true
    }
    private let contactsContentStackView = with(UIStackView.horizontal) {
        $0.alignment = .fill
        $0.distribution = .fillProportionally
        $0.spacing = 11
    }
    private let googleIconContainerView = UIView()
    private let googleIcon = with(UIImageView()) {
        $0.image = Asset.poweredByGoogle.image
    }
    
    var onContactTapped: ((BuildingInformationTextLinkItem.ViewModel.Link?) -> Void)?
    
    override func setupUI() {
        super.setupUI()
        googleIconContainerView.addSubview(googleIcon)
        addSubview(rootStackView)
        
        rootPlaceInformationStackView.addArrangedSubviews([
            nameLabel,
            badge.wrapped(alignment: .horizontal(.leading)),
            descriptioneLabel
        ])
        rootContactsStackView.addArrangedSubviews([
            contactsContentStackView,
            googleIconContainerView
        ])
        rootStackView.addArrangedSubviews([
            rootPlaceInformationStackView,
            rootContactsStackView
        ])
    }
    
    override func setupConstraints() {
        super.setupConstraints()
        backgroundColor = .white
        rootStackView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        
        contactsContentStackView.snp.makeConstraints { make in
            //TODO: - remove static height
            make.height.equalTo(20)
        }
        
        googleIconContainerView.snp.makeConstraints { make in
            make.height.equalTo(15)
        }
        googleIcon.snp.makeConstraints { make in
            make.leading.top.bottom.equalToSuperview()
            make.width.equalTo(115)
        }
    }
    
    @objc private func tappedContact(sender: BuildingInformationTextLinkItem) {
        onContactTapped?(sender.link)
    }
}

extension BuildingGeneralInfoView {
    
    func fillGeneral(with model: ViewModelGeneral) {
        if let name = model.name {
            nameLabel.text = name
        }
        
        if let category = model.category {
            badge.fill(with: .init(text: category))
        }
        
        if let description = model.description {
            descriptioneLabel.text = description
        }
    }
    
    func fillContacts(with model: ViewModelContact) {
        
        if !model.contacts.isEmpty {
            rootContactsStackView.isHidden = false
            contactsContentStackView.removeAllArrangedSubviews()
            
            for contact in model.contacts {
                let contactView = BuildingInformationTextLinkItem()
                contactView.fill(with: contact)
                contactView.addTarget(self, action: #selector(tappedContact), for: .valueChanged)
                contactsContentStackView.addArrangedSubview(contactView)
            }
        } else {
            rootContactsStackView.isHidden = true
        }
    }
}
