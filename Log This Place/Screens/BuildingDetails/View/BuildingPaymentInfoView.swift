//
//  BuildingPaymentInfoView.swift
//  Log This Place
//
//

import UIKit


final class BuildingPaymentInfoView: BaseView, Fillable {
    
    struct ViewModel {
        let paymentMethods: [String]
    }
    
    private let rootStackView = with(UIStackView.vertical) {
        $0.edgeInsets(topSpacing: 16, bottomSpacing: 16, leadingSpacing: 16, trailingSpacing: 16)
    }
    
    private let titleView = with(BuildingInformationTextItem()) {
        $0.fill(with: .init(title: L10n.BuildingDetails.PaymentMethods.text,
                            image: Asset.BuildingDetail.paymentMethods.image,
                            textColor: .black))
    }
    private let paymentMethodsLabel = with(UILabel.standartMediumRegular) {
        $0.textColor = UIColor.LogThisPlace.gray
        $0.numberOfLines = 0
    }
    private let titleViewContainer = UIView()
    private let paymentMethodsContainer = UIView()
    
    override func setupUI() {
        super.setupUI()
        addSubview(rootStackView)
        
        paymentMethodsContainer.addSubview(paymentMethodsLabel)
        titleViewContainer.addSubview(titleView)
        
        rootStackView.addArrangedSubviews([
            titleViewContainer,
            paymentMethodsContainer
        ])
    }
    
    override func setupConstraints() {
        super.setupConstraints()
        backgroundColor = .white
        
        rootStackView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        
        titleViewContainer.snp.makeConstraints { make in
            make.height.equalTo(23)
        }
        titleView.snp.makeConstraints { make in
            make.leading.top.bottom.trailing.equalToSuperview()
        }
        
        paymentMethodsLabel.snp.makeConstraints { make in
            make.leading.equalToSuperview().offset(23)
            make.top.bottom.trailing.equalToSuperview()
        }
    }
}

extension BuildingPaymentInfoView {
    
    func fill(with model: ViewModel) {
        paymentMethodsLabel.text = model.paymentMethods.map({ $0 }).joined(separator: ", ")
    }
    
}
