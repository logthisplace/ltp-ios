//
//  BuildingAccessibilityView.swift
//  Log This Place
//
//

import UIKit


final class BuildingAccessibilityView: BaseView {
  
    let rootStackView = with(UIStackView.vertical) {
        $0.edgeInsets(topSpacing: 16, bottomSpacing: 16, leadingSpacing: 16, trailingSpacing: 0)
    }
    
    private let headerInformStackView = UIStackView.vertical
    private let titleLabel = with(UILabel.headlineMediumSemiBold) {
        $0.text = L10n.BuildingDetails.AccessibilityInformation.text
    }
    private let lastUpdatedLabel = with(UILabel.captionRegular) {
        $0.textColor = UIColor.LogThisPlace.gray
        $0.isHidden = true
    }
    var onImageTapped: (([ImageSource], Int) -> Void)?
    
    public let buildingAccessibilityImageStackView = with(UIStackView.vertical) {
        $0.isHidden = true
        $0.edgeInsets(topSpacing: 10, bottomSpacing: 0, leadingSpacing: 0, trailingSpacing: 0)
    }
    public let buildingAccessibilityTextFeatureStackView = with(UIStackView.vertical) {
        $0.isHidden = true
        $0.edgeInsets(topSpacing: 10, bottomSpacing: 0, leadingSpacing: 0, trailingSpacing: 0)
    }

    override func setupUI() {
        super.setupUI()
        addSubview(rootStackView)
        
        headerInformStackView.addArrangedSubviews([
            titleLabel,
            lastUpdatedLabel,
        ])
        
        rootStackView.addArrangedSubviews([
            headerInformStackView,
            buildingAccessibilityImageStackView,
            buildingAccessibilityTextFeatureStackView
        ])
    }
    
    override func setupConstraints() {
        super.setupConstraints()
        backgroundColor = .white
        
        rootStackView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
    }
    
    public func clearTextFeaturesView() {
        buildingAccessibilityTextFeatureStackView.isHidden = true
        buildingAccessibilityTextFeatureStackView.removeAllArrangedSubviews()
        buildingAccessibilityTextFeatureStackView.layoutIfNeeded()
    }
    
    public func clearImageFeaturesView() {
        buildingAccessibilityImageStackView.isHidden = true
        buildingAccessibilityImageStackView.removeAllArrangedSubviews()
        buildingAccessibilityImageStackView.layoutIfNeeded()
    }
}

extension BuildingAccessibilityView {
    
    func fillImageFeatures(with imageFeatures:[BuildingAccessibilityImageView.ViewModel]) {
        buildingAccessibilityImageStackView.removeAllArrangedSubviews()
        buildingAccessibilityImageStackView.layoutIfNeeded()
        for feature in imageFeatures {
            let imageGroupView = BuildingAccessibilityImageView()
            imageGroupView.onImageTapped = onImageTapped
            imageGroupView.fill(with: feature)
            buildingAccessibilityImageStackView.isHidden = false
            buildingAccessibilityImageStackView.addArrangedSubview(imageGroupView)
        }
    }
    
    func fillTextFeature(with textFeature: BuildingAccessibilityGroupsView.ViewModel) {
        buildingAccessibilityTextFeatureStackView.removeAllArrangedSubviews()
        buildingAccessibilityTextFeatureStackView.layoutIfNeeded()
        if !textFeature.titles.isEmpty {
            let buildingAccessibilityTextFeatureGroupsView = BuildingAccessibilityGroupsView()
            buildingAccessibilityTextFeatureGroupsView.fill(with: textFeature)
            buildingAccessibilityTextFeatureStackView.isHidden = false
            buildingAccessibilityTextFeatureStackView.addArrangedSubview(buildingAccessibilityTextFeatureGroupsView)
        }
    }
    
    func fillUpdatedAt(_ date: Date?) {
        if let updatedAt = date {
            lastUpdatedLabel.isHidden = false
            lastUpdatedLabel.text = "\(L10n.BuildingDetails.Date.text) \(getDateString(date: updatedAt))"
        }
    }
    
    private func getDateString(date: Date) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy"
        let strDate = dateFormatter.string(from: date)
        return strDate
    }
    
}
