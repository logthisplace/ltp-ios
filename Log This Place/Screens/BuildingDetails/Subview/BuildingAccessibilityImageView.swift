//
//  Log This Place
//
//

import UIKit


final class BuildingAccessibilityImageView: BaseView, Fillable {
    
    var onImageTapped: (([ImageSource], Int) -> Void)?
    
    struct ViewModel {
        let icon: ImageAsset?
        let name: String
        let images: [ImageSource]
    }
    
    let infoContainerStackView = UIStackView.vertical
    let nameLabel = with(UILabel.captionSemibold) {
        $0.textAlignment = .center
        $0.numberOfLines = 0
    }
    
    let iconImageView = UIImageView()
    let containerIconView = UIView()
    
    let pickerView = ImagePickerView()

    override func setupUI() {
        super.setupUI()
        addSubview(infoContainerStackView)
        addSubview(pickerView)
        
        containerIconView.addSubview(iconImageView)
        infoContainerStackView.addArrangedSubviews([
            containerIconView,
            nameLabel
        ])
    }
    
    override func setupConstraints() {
        super.setupConstraints()
        
        containerIconView.snp.makeConstraints { make in
            make.height.equalTo(55)
        }
        
        infoContainerStackView.spacing = 6
        infoContainerStackView.snp.makeConstraints { make in
            make.width.equalTo(80)
            make.leading.equalToSuperview()
            make.centerY.equalToSuperview()
        }
        
        pickerView.snp.makeConstraints { make in
            make.leading.equalTo(infoContainerStackView.snp.trailing).offset(8)
            make.top.trailing.bottom.equalToSuperview()
        }
        
        iconImageView.snp.makeConstraints { make in
            make.center.equalToSuperview()
            make.size.equalTo(35)
        }
    }
    
    override func setupVisuals() {
        super.setupVisuals()
//        containerIconView.radius(radius: 13)
//        containerIconView.backgroundColor = UIColor.LogThisPlace.grayAlmostWhite
    }
}

extension BuildingAccessibilityImageView {
    
    func fill(with model: ViewModel) {
        nameLabel.text = model.name
        if let icon = model.icon {
            iconImageView.image = icon.image
        }
        pickerView.onImageTapped = onImageTapped
        pickerView.fill(with: .init(title: nil, images: model.images))
        pickerView.setupRootStackViewArea(top: 10, left: 0, bottom: 0, right: 0)
    }
    
}
