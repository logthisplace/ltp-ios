//
//  PageMenuExample
//
//

import UIKit

class SwiftPageMenuTableView: UITableView, UIGestureRecognizerDelegate {

    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return otherGestureRecognizer.isKind(of: UIPanGestureRecognizer.self)
    }
}

class BuildingAccessibilityFeaturesViewController: UIViewController {

    var features = [BuildingFeatureItemView.ViewModel]()
    let stackView = UIStackView.vertical

    init(features: [BuildingFeatureItemView.ViewModel]) {
        super.init(nibName: nil, bundle: nil)
        self.features = features
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.edgesForExtendedLayout = []
        self.view.backgroundColor = .white
        self.view.addSubview(stackView)
        
        stackView.snp.makeConstraints { make in
            make.leading.top.equalToSuperview()
            make.trailing.equalToSuperview().inset(20)
        }
        
        for feature in features {
            let itemView = BuildingFeatureItemView()
            itemView.fill(with: feature)
            stackView.addArrangedSubview(itemView)
        }
    }
}
