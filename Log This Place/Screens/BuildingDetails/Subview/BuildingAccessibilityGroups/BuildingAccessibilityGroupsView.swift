//
//  BuildingAccessibilityGroupingView.swift
//  Log This Place
//
//

import UIKit


final class BuildingAccessibilityGroupsView: BaseView, Fillable {
    
    struct ViewModel {
        let parentViewController: UIViewController
        let titles: [String]
        let features: [[BuildingFeatureItemView.ViewModel]]
    }
    
    override func setupUI() {
        super.setupUI()
    }
    
    override func setupConstraints() {
        super.setupConstraints()
    }
    
    func calculateMaxHeight(_ items: [[BuildingFeatureItemView.ViewModel]]) -> CGFloat {
        var maxViewHeight: CGFloat = 0
        
        items.forEach({
            var groupHeight: CGFloat = 0
            $0.forEach({
                switch $0.featureType {
                case let .single(input):
                    groupHeight += (getHeightByText(with: input.featureDescription) + 45)
                case let .multy(input):
                    groupHeight += (getHeightByText(with: input.featureDescription) + 45 +
                                    getHeightByText(with: input.answers.map({ $0 }).joined(separator: ", ")))
                }
            })
            if groupHeight > maxViewHeight {
                maxViewHeight = groupHeight
            }
        })
        
        /// additional add height of header -> ~50
        return maxViewHeight + 50
    }
    
    func getHeightByText(with text: String) -> CGFloat {
        return text.height(withConstrainedWidth: UIScreen.main.bounds.size.width - 50,
                           font: UILabel.standartMediumRegular.font)
    }
}

extension BuildingAccessibilityGroupsView {
    func fill(with model: ViewModel) {
        let vc: BuildingAccessibilityTabViewController = BuildingAccessibilityTabViewController(model:
                                                                                        .init(items: model.features,
                                                                                              titles: model.titles))
        model.parentViewController.embed(vc, inView: self)
        snp.makeConstraints { make in
            make.height.equalTo(calculateMaxHeight(model.features))
        }
    }
}
