//
//  Log This Place
//

import UIKit

final class BuildingGeneralTextBadgeView: BaseView, Fillable {
    
    struct ViewModel {
        let text: String
    }
    
    private let label = with(UILabel.captionSemibold) {
        $0.textColor = UIColor.LogThisPlace.gray
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        clipsToBounds = true
        layer.cornerRadius = bounds.height / 2
    }
    
    override func setupUI() {
        super.setupUI()
        addSubview(label)
    }
    
    override func setupConstraints() {
        super.setupConstraints()
        label.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview().inset(8)
            make.top.bottom.equalToSuperview().inset(6)
        }
    }
    
    override func setupVisuals() {
        super.setupVisuals()
        backgroundColor = UIColor.LogThisPlace.gray.withAlphaComponent(0.08)
    }
}

extension BuildingGeneralTextBadgeView {
    func fill(with model: ViewModel) {
        label.text = model.text.uppercased()
    }
}
