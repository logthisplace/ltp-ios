//
//  Log This Place
//
//

import UIKit

final class BuildingInformationTextItem: BaseView, Fillable {
    
    struct ViewModel {
        let title: String
        let image: UIImage
        let textColor: UIColor
        let underline: Bool
        let bold: Bool
        
        init(title: String, image: UIImage, textColor: UIColor = .white, underline: Bool = false, bold: Bool = false) {
            self.title = title
            self.textColor = textColor
            self.image = image
            self.underline = underline
            self.bold = bold
        }
    }

    private let label = with(UILabel.standartMediumRegular) {
        $0.textColor = .white
    }
    private let imageView = UIImageView.scaleAspectFit
    
    override func setupUI() {
        super.setupUI()
        addSubview(imageView)
        addSubview(label)
    }
    
    override func setupConstraints() {
        super.setupConstraints()
        
        imageView.snp.makeConstraints { make in
            make.size.equalTo(16)
            make.leading.equalToSuperview()
            make.centerY.equalToSuperview()
        }
        
        label.snp.makeConstraints { make in
            make.leading.equalTo(imageView.snp.trailing).offset(7)
            make.trailing.equalToSuperview()
            make.centerY.equalToSuperview()
        }
    }
}

extension BuildingInformationTextItem {
    func fill(with model: ViewModel) {
        imageView.image = model.image
        label.textColor = model.textColor
        
        if model.underline {
            label.attributedText = NSAttributedString(string: model.title, attributes: [.underlineStyle: NSUnderlineStyle.single.rawValue])
        } else {
            label.text = model.title
        }
        
        if model.bold {
            label.font = FontFamily.ProximaNova.bold.font(size: 14)
        }
    }
}
