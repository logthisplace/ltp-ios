//
//  Log This Place
//
//

import UIKit

final class BuildingInformationTextLinkItem: BaseControl, Fillable {
    
    struct ViewModel {
        let link: Link
        
        enum Link {
            case phone(link: String)
            case website(link: URL)
            case location(link: Poi)
            
            struct Poi {
                let location: PoiLocation
                let placeName: String?
            }
            
            var icon: UIImage {
                switch self {
                case .phone:
                    return Asset.BuildingDetail.phone.image
                case .website:
                    return Asset.BuildingDetail.website.image
                case .location:
                    return Asset.BuildingDetail.direction.image
                }
            }
            
            var description: String {
                switch self {
                case .phone:
                    return L10n.BuildingDetails.Call.text
                case .website:
                    return L10n.BuildingDetails.Website.text
                case .location:
                    return L10n.BuildingDetails.Directions.text
                }
            }
        }
    }

    
    private let tapGR = UITapGestureRecognizer()
    private let informationTextItem = BuildingInformationTextItem()
    var link: ViewModel.Link?
    
    init() {
        super.init(frame: .zero)
    }
    
    required init?(coder: NSCoder) {
        fatalError("Use init(id:)")
    }
    
    override func setup() {
        super.setup()
        addGestureRecognizer(tapGR)
        tapGR.addTarget(self, action: #selector(controlTapped))
    }
    
    override func setupUI() {
        super.setupUI()
        addSubview(informationTextItem)
        
    }
    
    override func setupConstraints() {
        super.setupConstraints()
        
        informationTextItem.snp.makeConstraints { make in
            make.leading.trailing.top.bottom.equalToSuperview()
        }
    }
    
    @objc private func controlTapped() {
        sendActions(for: .valueChanged)
    }
}

extension BuildingInformationTextLinkItem {
    func fill(with model: ViewModel) {
        link = model.link
        informationTextItem.fill(with: .init(title: model.link.description,
                                             image: model.link.icon,
                                             textColor: .black,
                                             underline: true))
    }
}
