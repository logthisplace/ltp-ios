//
//  Log This Place
//

import UIKit

final class BuildingFeatureItemView: BaseView, Fillable {
    
    struct ViewModel {
        let featureType: FeatureType
        
        enum FeatureType {
            case single(input: SingleFeatureType)
            case multy(input: MultyFeatureType)
        }
        
        struct MultyFeatureType {
            let featureDescription: String
            let answers: [String]
        }
        
        struct SingleFeatureType {
            let hasFeature: Bool
            let featureDescription: String
        }
    }
    
    private let textLabel = with(UILabel.standartMediumRegular) {
    $0.numberOfLines = 0
}
    private let separator = UIView.horizontalSeparator()
    
    private let statusLabelContainer = UIView()
    private let statusLabel = UILabel.captionSemibold
    private let userAnswersLabel = with(UILabel.standartMediumRegular) {
        $0.textColor = UIColor.LogThisPlace.gray
        $0.numberOfLines = 0
    }
    
    private let labelStackView = UIStackView.vertical
    
    override func setupUI() {
        super.setupUI()
        labelStackView.addArrangedSubviews([
            textLabel,
            userAnswersLabel
        ])
        statusLabelContainer.addSubview(statusLabel)
        addSubviews([
            statusLabelContainer,
            labelStackView,
            separator
        ])
    }
    
    override func setupConstraints() {
        super.setupConstraints()

        statusLabelContainer.snp.makeConstraints { make in
            make.trailing.equalToSuperview()
            make.width.equalTo(45)
            make.height.equalTo(30)
            make.centerY.equalToSuperview()
        }
        statusLabel.snp.makeConstraints { make in
            make.center.equalToSuperview()
        }
        labelStackView.snp.makeConstraints { make in
            make.top.bottom.equalToSuperview().inset(20)
            make.leading.equalToSuperview()
            make.trailing.equalTo(statusLabelContainer.snp.leading).offset(0)
            make.centerY.equalToSuperview()
        }
        separator.snp.makeConstraints { make in
            make.leading.trailing.bottom.equalToSuperview()
        }
    }
    
    override func setupVisuals() {
        super.setupVisuals()
        statusLabelContainer.layer.cornerRadius = 7
        statusLabelContainer.layer.masksToBounds = true
    }
    
}

extension BuildingFeatureItemView {
    func fill(with model: ViewModel) {
        switch model.featureType {
        case let .single(input):
            textLabel.text = input.featureDescription
            statusLabelContainer.isHidden = false
            userAnswersLabel.isHidden = true
            if input.hasFeature {
                statusLabelContainer.backgroundColor = UIColor.LogThisPlace.lightGreen
                statusLabel.textColor = UIColor.LogThisPlace.darkGreen
                statusLabel.text = L10n.Common.Yes.text
            } else {
                statusLabelContainer.backgroundColor = UIColor.LogThisPlace.grayAlmostWhite
                statusLabel.textColor = UIColor.LogThisPlace.gray
                statusLabel.text = L10n.Common.No.text
            }
        case let .multy(input):
            textLabel.text = input.featureDescription
            statusLabelContainer.isHidden = true
            userAnswersLabel.isHidden = false
            userAnswersLabel.text = input.answers.map({ $0 }).joined(separator: ", ")
        }
    }
}
