//
//  Log This Place
//
//

import UIKit


final class BuildingAccessibilityProfileIcon: BaseControl, Fillable {
    
    struct ViewModel {
        let icon: UIImage
        let info: String
        let isChecked: Bool
    }
    
    private let tapGR = UITapGestureRecognizer()
    private let accessibilityIcon = with(UIImageView()) {
        $0.contentMode = .scaleAspectFit
    }
    
    private let selectorIcon = with(UIImageView()) {
        $0.image = Asset.BuildingDetail.selector.image
        $0.isHidden = true
    }
    
    var isChecked: Bool = false {
        didSet {
           updateCheckmark()
        }
    }
    
    var info: String = .init()
    
    init() {
        super.init(frame: .zero)
    }
    
    required init?(coder: NSCoder) {
        fatalError("Use init(id:)")
    }
    
    override func setup() {
        super.setup()
        layer.cornerRadius = 12
        addGestureRecognizer(tapGR)
        tapGR.addTarget(self, action: #selector(controlTapped))
    }
    
    override func setupUI() {
        super.setupUI()
        addSubview(accessibilityIcon)
        addSubview(selectorIcon)
        updateCheckmark()
    }
    
    override func setupConstraints() {
        super.setupConstraints()

        accessibilityIcon.snp.makeConstraints { make in
            make.top.equalToSuperview().inset(6)
            make.centerX.equalToSuperview()
            make.size.equalTo(32)
        }
        
        selectorIcon.snp.makeConstraints { make in
            make.bottom.equalToSuperview()
            make.height.equalTo(11)
            make.width.equalTo(22)
            make.centerX.equalToSuperview()
        }
    }
    
    private func updateCheckmark() {
        selectorIcon.isHidden = !isChecked
    }
    
    @objc private func controlTapped() {
        isChecked = true
        sendActions(for: .valueChanged)
    }
}

extension BuildingAccessibilityProfileIcon {
    func fill(with model: ViewModel) {
        isChecked = model.isChecked
        accessibilityIcon.image = model.icon
        info = model.info
    }
}
