//
//  Log This Place
//
//

import UIKit


final class BuildingAccessibilityProfileView: BaseView, Fillable {
    
    struct ViewModel {
        let fullyAccessible: Bool
        let accessibleIcons: [BuildingAccessibilityProfileIcon.ViewModel]
    }
    
    private let rootStackView = UIStackView.vertical
    private let accessibleInfoView = with(BuildingInformationTextItem()) {
        $0.fill(with: .init(title: L10n.BuildingDetails.FullyAccessible.text,
                            image: Asset.cameraFill.image,
                            bold: true))
    }
    private let containerInfoTextitem = with(UIView()) {
        $0.isHidden = false
        $0.backgroundColor = UIColor.LogThisPlace.darkGreen
    }
    
    private let accessibilityIconsView = BuildingAccessibilityProfileIconsView()

    override func setupUI() {
        super.setupUI()
        backgroundColor = .white
        addSubview(rootStackView)
        
        containerInfoTextitem.addSubview(accessibleInfoView)
        
        rootStackView.addArrangedSubviews([
            containerInfoTextitem,
            accessibilityIconsView
        ])
    }
    
    override func setupConstraints() {
        super.setupConstraints()
        rootStackView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        
        containerInfoTextitem.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview()
            make.height.equalTo(36)
        }
        
        accessibleInfoView.snp.makeConstraints { make in
            make.centerY.equalToSuperview()
            make.leading.trailing.equalToSuperview().offset(16)
        }
    }
}

extension BuildingAccessibilityProfileView {
    func fill(with model: ViewModel) {
        containerInfoTextitem.isHidden = !model.fullyAccessible
        accessibilityIconsView.fill(with: .init(accessibilityIcons: model.accessibleIcons))
    }
}
