//
//  Log This Place
//
//

import UIKit

final class BuildingAccessibilityProfileIconsView: BaseView, Fillable {
    
    struct ViewModel {
        let accessibilityIcons: [BuildingAccessibilityProfileIcon.ViewModel]
    }
    
    private let containerInfoTextitem = with(UIView()) {
        $0.isHidden = true
        $0.backgroundColor = UIColor.LogThisPlace.systemLikeGray
    }
    private let infoTextitem = BuildingInformationTextItem()
    private let rootStackView = UIStackView.vertical
    private let accessibilityIconsStackView = with(UIStackView.horizontal) {
        $0.alignment = .fill
        $0.distribution = .fillEqually
        $0.edgeInsets(topSpacing: 5, bottomSpacing: 0, leadingSpacing: 12, trailingSpacing: 12)
    }
    
    override func setupUI() {
        super.setupUI()
        addSubview(rootStackView)
        
        containerInfoTextitem.addSubview(infoTextitem)
        
        rootStackView.addArrangedSubviews([
            accessibilityIconsStackView,
            containerInfoTextitem
        ])
    }
    
    override func setupConstraints() {
        super.setupConstraints()
        rootStackView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        
        accessibilityIconsStackView.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview()
            make.height.equalTo(64)
        }
        
        containerInfoTextitem.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview()
            make.height.equalTo(36)
        }
        
        infoTextitem.snp.makeConstraints { make in
            make.centerY.equalToSuperview()
            make.leading.trailing.equalToSuperview().offset(16)
        }
    }
    
    @objc private func tappedAccessibilityIcon(sender: BuildingAccessibilityProfileIcon) {
        let allExceptTapped = accessibilityIconsStackView.arrangedSubviews.filter {
            $0 != sender
        }
                
        allExceptTapped.forEach {
            ($0 as? BuildingAccessibilityProfileIcon)?.isChecked = false
        }
        
        if sender.isChecked {
            showContainerInfo(information: sender.info)
        } else {
            containerInfoTextitem.isHidden = true
        }
    }
    
    private func showContainerInfo(information: String) {
        containerInfoTextitem.isHidden = false
        infoTextitem.fill(with: .init(title: information ,
                                      image: Asset.BuildingDetail.checkedBlack.image,
                                      textColor: .black,
                                      bold: true))
    }
}

extension BuildingAccessibilityProfileIconsView {
    func fill(with model: ViewModel) {
        /// clear old data
        accessibilityIconsStackView.removeAllArrangedSubviews()
        containerInfoTextitem.isHidden = true
        
        for icon in model.accessibilityIcons {
            if icon.isChecked {
                showContainerInfo(information: icon.info)
            }
            let accessibilityIconView = BuildingAccessibilityProfileIcon()
            accessibilityIconView.fill(with: icon)
            accessibilityIconView.addTarget(self, action: #selector(tappedAccessibilityIcon), for: .valueChanged)
            accessibilityIconsStackView.addArrangedSubview(accessibilityIconView)
        }
    }
}
