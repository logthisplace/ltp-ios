//
//  Log This Place
//

import UIKit
import Kingfisher


final class BuildingPhotoGalleryView: BaseView, Fillable, UIScrollViewDelegate{
    
    struct ViewModel {
        let images: [ImageSource]
    }
    
    var images = [ImageSource]()
    var onImageTapped: (([ImageSource], Int) -> Void)?
    var presentationIndex: Int = 0
    
    private let scrollView = with(UIScrollView()) {
        $0.isPagingEnabled = true
        $0.showsHorizontalScrollIndicator = false
    }
    
    private let pageIndicator = with(UIPageControl()) {
        $0.currentPageIndicatorTintColor = .white
        $0.pageIndicatorTintColor = UIColor.black.withAlphaComponent(0.4)
        $0.isUserInteractionEnabled = false
    }
    
    private let numberOfPhotosView = with(UIView()) {
        $0.backgroundColor = UIColor.black.withAlphaComponent(0.4)
    }
    
    private let cameraImageView = with(UIImageView.scaleAspectFit) {
        $0.image = Asset.camera.image
    }
    
    private let numberOfPhotosLabel = with(UILabel()) {
        $0.font = .systemFont(ofSize: 14)
        $0.textColor = .white
    }
    
    private let placeholderImageView = with(UIImageView()) {
        $0.image = Asset.placeholderGray.image
        $0.contentMode = .scaleAspectFill
    }
    
    private lazy var tapGR = UITapGestureRecognizer(target: self, action: #selector(didTapGallery))
    
    override init() {
        super.init()
        scrollView.delegate = self
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        numberOfPhotosView.layer.cornerRadius = numberOfPhotosView.bounds.height / 2
    }
    
    override func setupUI() {
        super.setupUI()
        addSubviews([
            placeholderImageView,
            scrollView,
            numberOfPhotosView,
            pageIndicator
        ])
        numberOfPhotosView.addSubviews([
            cameraImageView,
            numberOfPhotosLabel
        ])
        
        addGestureRecognizer(tapGR)
    }
    
    override func setupConstraints() {
        super.setupConstraints()
        scrollView.positionToTheEdges()
        
        placeholderImageView.snp.makeConstraints{ make in
            make.margins.equalToSuperview()
        }
        cameraImageView.snp.makeConstraints { make in
            make.height.equalTo(14)
            make.width.equalTo(16)
            make.leading.equalTo(10)
            make.centerY.equalToSuperview()
        }
        numberOfPhotosLabel.snp.makeConstraints { make in
            make.leading.equalTo(cameraImageView.snp.trailing).offset(6)
            make.trailing.equalToSuperview().inset(10)
            make.centerY.equalToSuperview()
        }
        numberOfPhotosView.snp.makeConstraints { make in
            make.leading.equalToSuperview().inset(20)
            make.bottom.equalToSuperview().inset(30)
            make.height.equalTo(28)
        }
        pageIndicator.snp.makeConstraints { make in
            make.leading.equalTo(numberOfPhotosView)
            make.top.equalTo(numberOfPhotosView.snp.bottom).offset(6)
            guard #available(iOS 14.0, *) else {
                make.height.equalTo(12)
                return
            }
        }
    }
    
    override func setupVisuals() {
        super.setupVisuals()
        if #available(iOS 14.0, *) {
            pageIndicator.backgroundStyle = .minimal
            pageIndicator.allowsContinuousInteraction = false
        }
        
        clipsToBounds = true
    }
    
    @objc private func didTapGallery() {
        onImageTapped?(images, presentationIndex)
    }
}

// MARK: - UIScrollView delegate

extension BuildingPhotoGalleryView {
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        let pageNumber = targetContentOffset.pointee.x / scrollView.bounds.width
        pageIndicator.currentPage = Int(pageNumber)
        presentationIndex = Int(pageNumber)
    }
}

// MARK: - Fillable

extension BuildingPhotoGalleryView {
    func fill(with model: ViewModel) {
        images = model.images
        numberOfPhotosLabel.text = "\(model.images.count)"
        pageIndicator.numberOfPages = model.images.count
        scrollView.layoutIfNeeded()
     
        var frame = CGRect.zero
        for element in model.images.enumerated() {
            frame.size = scrollView.bounds.size
            frame.origin.x = scrollView.bounds.width * CGFloat(element.offset)
            let imageView = UIImageView.scaleAspectFill
            imageView.frame = frame
            switch element.element {
            case let .link(link):
                let url = URL(string: link)
                imageView.kf.setImage(with: url,
                                      placeholder: Asset.placeholderGray.image)
            case let .media(media):
                imageView.image = media
            }
            
            scrollView.addSubview(imageView)
        }
        scrollView.contentSize = CGSize(width: scrollView.bounds.width * CGFloat(model.images.count),
                                        height: scrollView.bounds.height)
        pageIndicator.currentPage = 0
    }
}
