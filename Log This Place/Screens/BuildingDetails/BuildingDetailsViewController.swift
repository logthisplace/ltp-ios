//
//  BuildingDetailsViewController.swift
//  Log This Place
//
//

import Foundation
import UIKit

struct BuildingDetailViewModel {
    let placeName: String?
    let address: String?
    var firebasePOI: PointOfInterest?
}

protocol BuildingDetailsViewInput: View {
    func fillGallery(_ images: [ImageSource])
    func fillGeneral(with model: BuildingGeneralInfoView.ViewModelGeneral)
    func fillContacts(with model: BuildingGeneralInfoView.ViewModelContact)
    func fillPayment(with model: BuildingPaymentInfoView.ViewModel)
    func fillTextFeature(titles: [String], features: [[BuildingFeatureItemView.ViewModel]])
    func fillImageFeatures(with imageFeatures:[BuildingAccessibilityImageView.ViewModel])
    func fillAccessibleProfile(fullyAccessible: Bool, accessibilityIcons: [BuildingAccessibilityProfileIcon.ViewModel])
    func fillUpdatedAt(_ date: Date?)
    func setTitle(_ buildingDetailTitle: String)
}

final class BuildingDetailsViewController: BaseViewController, ImagePickerManagerContext, UIGestureRecognizerDelegate {
    
    override var contentView: BaseView {
        buildingDetailsView
    }

    private let presenter: BuildingDetailsPresenterProtocol
    private lazy var buildingDetailsView = BuildingDetailsView(imagePickerManager: ImagePickerManager(context: self))
    
    init(presenter: BuildingDetailsPresenterProtocol) {
        self.presenter = presenter
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("use init(presenter:)")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.viewDidLoad()
        
        buildingDetailsView.contributionView.addEditAnswerSheetButton.addTarget(self, action: #selector(addEditAnswerSheetTapped), for: .touchUpInside)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    override func willMove(toParent parent: UIViewController?) {
        super.willMove(toParent: parent)
        if parent == nil {
            presenter.didBackTapped()
        }
    }
    
    override func setupBehaviour() {
        super.setupBehaviour()
        buildingDetailsView.onContactTapped = { [weak self] in
            self?.presenter.contantTapped(link: $0)
        }
        buildingDetailsView.onImageTapped = { [weak self] images, presentationIndex in
            self?.presenter.imageTapped(items: images, presentationIndex: presentationIndex)
        }
    }
    
    @objc private func addEditAnswerSheetTapped() {
        presenter.addEditAnswerSheetTapped()
    }
}

extension BuildingDetailsViewController: BuildingDetailsViewInput {
    func fillAccessibleProfile(fullyAccessible: Bool,
                               accessibilityIcons: [BuildingAccessibilityProfileIcon.ViewModel]) {
        if !accessibilityIcons.isEmpty {
            buildingDetailsView.buildingAccessibilityProfileView.isHidden = false
            buildingDetailsView.buildingAccessibilityProfileView.fill(with: .init(fullyAccessible: fullyAccessible,
                                                                                  accessibleIcons: accessibilityIcons))
        } else {
            buildingDetailsView.buildingAccessibilityProfileView.isHidden = true
        }
    }
    
    func fillTextFeature(titles: [String], features: [[BuildingFeatureItemView.ViewModel]]) {
        if !titles.isEmpty && !features.isEmpty {
            buildingDetailsView.buildingAccessibilityView.isHidden = false
            buildingDetailsView.buildingAccessibilityView.buildingAccessibilityImageStackView.isHidden = false
            buildingDetailsView.buildingAccessibilityView.fillTextFeature(with: .init(parentViewController: self,
                                                                                      titles: titles,
                                                                                      features: features))
        } else {
            buildingDetailsView.buildingAccessibilityView.clearTextFeaturesView()
        }
    }
    
    func fillImageFeatures(with imageFeatures: [BuildingAccessibilityImageView.ViewModel]) {
        if !imageFeatures.isEmpty {
            buildingDetailsView.buildingAccessibilityView.isHidden = false
            buildingDetailsView.buildingAccessibilityView.buildingAccessibilityTextFeatureStackView.isHidden = false
            buildingDetailsView.buildingAccessibilityView.onImageTapped = buildingDetailsView.onImageTapped
            buildingDetailsView.buildingAccessibilityView.fillImageFeatures(with: imageFeatures)
        } else {
            buildingDetailsView.buildingAccessibilityView.clearImageFeaturesView()
        }
    }
    
    func fillUpdatedAt(_ date: Date?) {
        guard let date = date else { return }
        buildingDetailsView.buildingAccessibilityView.isHidden = false
        buildingDetailsView.buildingAccessibilityView.fillUpdatedAt(date)
    }
    
    func fillGeneral(with model: BuildingGeneralInfoView.ViewModelGeneral) {
        buildingDetailsView.buidingGeneralInfoView.fillGeneral(with: model)
    }
    
    func fillContacts(with model: BuildingGeneralInfoView.ViewModelContact) {
        buildingDetailsView.buidingGeneralInfoView.onContactTapped = buildingDetailsView.onContactTapped
        buildingDetailsView.buidingGeneralInfoView.fillContacts(with: model)
    }
    
    func fillPayment(with model: BuildingPaymentInfoView.ViewModel) {
        if !model.paymentMethods.isEmpty {
            buildingDetailsView.buildingPaymentInfoView.isHidden = false
            buildingDetailsView.buildingPaymentInfoView.fill(with: model)
        } else {
            buildingDetailsView.buildingPaymentInfoView.isHidden = true
        }
    }
    
    func setTitle(_ buildingDetailTitle: String) {
        navigationController?.navigationBar.topItem?.title = buildingDetailTitle
    }
    
    func fillGallery(_ images: [ImageSource]) {
        buildingDetailsView.gallery.onImageTapped = buildingDetailsView.onImageTapped
        buildingDetailsView.gallery.fill(with: .init(images: images))
    }
    
}


