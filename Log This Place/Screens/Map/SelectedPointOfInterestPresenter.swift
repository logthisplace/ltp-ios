//
//  SelectedPointOfInterestPresenter.swift
//  Log This Place
//

import UIKit

struct SelectedPointOfInterestInput {
    let name: String
    let googleId: String
    let address: String?
    let location: PoiLocation?
    let typeOfScreen: SelectedPointOfInterestViewType
    let category: QuestionItemValue?
    let color: UIColor
}

protocol SelectedPointOfInterestPresenterProtocol: Presenter {
    var input: SelectedPointOfInterestInput { get set }
    
    func questionnaireButtonTapped()
    func buildingDetailsButtonTapped()
}

final class SelectedPointOfInterestPresenter: BasePresenter, SelectedPointOfInterestPresenterProtocol {
    
    weak var view: SelectedPointOfInterestViewInput?
    
    var input: SelectedPointOfInterestInput {
        didSet {
            updateData()
        }
    }
    
    private var viewModel: SelectedPointOfInterestViewModel {
        var icon = Asset.PoiCategories.noneCategory.image
        if let categoryId = input.category?.uuid {
            icon = PointOfInterestType(rawValue: categoryId)?.thumbnailAsset.image ?? Asset.PoiCategories.noneCategory.image
        }
        return SelectedPointOfInterestViewModel(title: input.name,
                                                icon: icon,
                                                color: input.color,
                                                address: input.address,
                                                typeOfScreen: input.typeOfScreen)
    }
    
    private let firebaseAnalyticManager: FirebaseAnalyticManagerProtocol = AppContainer.resolve()
    
    private let coordinator: MapCoordinatorProtocol
    
    init(input: SelectedPointOfInterestInput,
         coordinator: MapCoordinatorProtocol) {
        self.input = input
        self.coordinator = coordinator
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        firebaseAnalyticManager.logEvent(screen: .drawer)
        updateData()
    }
    
    private func updateData() {
        if viewLoaded {
            view?.fill(with: viewModel)
        }
    }
    
    /// Answer on questions
    func questionnaireButtonTapped() {
        coordinator.showAddPoi(mode: .add(input: .poi(input: .init(address: input.address,
                                                                   placeName: input.name,
                                                                   googleId: input.googleId,
                                                                   location: input.location))))
    }
    
    /// View building detail page
    func buildingDetailsButtonTapped() {
        switch input.typeOfScreen {
        case let .firebasePOI(firebasePOI):
            coordinator.showBuildingDetails(buildingDetailViewModel: .init(placeName: input.name,
                                                                           address: input.address,
                                                                           firebasePOI: firebasePOI))
        default:
            // TODO: - handle error
            break
        }
    }
}
