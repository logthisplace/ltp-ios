//
//  MapViewInput.swift
//  Log This Place
//
//

import Foundation
import GoogleMaps


protocol MapViewInput: View {
    
    func show(pointOfInterests: [MapPoiViewModel])
    func updateMapPoiMarker(with poi: MapPoiViewModel)
    func addMapPoiMarker(with poi: MapPoiViewModel,
                         marker: GMSMarker)
    func resetMapRotation()
    func cluster()
    func centerTo(location: CLLocationCoordinate2D)
    func zoomTo(zoom: Float)
    func centerToWithPin(location: CLLocationCoordinate2D)
    func drawCityPolygon(_ polygon: GMSPolygon)
    func removeLastlyTappedGooglePoiMarker()
    func setCenterButton(enabled: Bool)
    func setCurrentlyTappedMarkerAsSelected()
    func setLastlySelectedMarkerNil()
    func updateLastlyTappedMarker(withTintColor color: UIColor, location: CLLocationCoordinate2D)
    func updateLastlySelectedMarkerToDefaultColor()
    func showPoiCategory(categories: [PoiCategoryViewModel])
    func showPinsColorInformationView(items: [ColorInformationItemView.ViewModel])
    func removePinByLocation(location: CLLocationCoordinate2D)
}

extension MapViewInput {
    func addMapPoiMarker(with poi: MapPoiViewModel,
                         marker: GMSMarker = GMSMarker()) {
        return addMapPoiMarker(with: poi, marker: marker)
    }
}
