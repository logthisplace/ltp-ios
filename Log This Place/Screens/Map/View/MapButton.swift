//
//  MapButton.swift
//  Log This Place
//

import UIKit
import SnapKit

final class MapButton: UIControl {
    
    private struct Constants {
        static let radius: CGFloat = 50
    }
    
    @IBInspectable var image: UIImage? {
        didSet {
            imageButton.setImage(image, for: .normal)
        }
    }
    
    override var isEnabled: Bool {
        didSet {
            imageButton.isEnabled = isEnabled
        }
    }
    
    private var imageButton = with(UIButton(type: .system)) {
        $0.tintColor = .black
    }
        
    required init?(coder: NSCoder) {
        super.init(coder: coder)

        prepareUI()
        prepareBehavior()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        layer.cornerRadius = bounds.width / 2
        layer.shadowOpacity = 0.5
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOffset = .zero
    }
    
    private func prepareUI() {
        backgroundColor = .white

        addSubview(imageButton)
        
        snp.makeConstraints { make in
            make.size.equalTo(self.frame.size.height)
        }

        imageButton.snp.makeConstraints { make in
            make.leading.top.equalTo(10)
            make.center.equalToSuperview()
        }
    }
    
    private func prepareBehavior() {
        imageButton.addTarget(self, action: #selector(imageButtonTapped), for: .touchUpInside)
    }
    
    @objc private func imageButtonTapped() {
        sendActions(for: .touchUpInside)
    }
}
