//
//  ColorInformationView.swift
//  Log This Place
//
//

import Foundation
import UIKit

final class ColorInformationView: BaseView, Fillable {
    struct ViewModel {
        let items: [ColorInformationItemView.ViewModel]
    }
    private let rootView = UIView()
    private let label = with(UILabel.titleH1SemiBold) {
        $0.text = L10n.PoisInformation.Title.text
        $0.textAlignment = .center
    }
    private let contentStackView = with(UIStackView.vertical) {
        $0.edgeInsets(topSpacing: 0, bottomSpacing: 32, leadingSpacing: 28, trailingSpacing: 28)
        $0.spacing = 32
    }
    
    let backgroudView = with(UIView()) {
        $0.backgroundColor = UIColor.black.withAlphaComponent(0.3)
    }
    let buttonContainer = UIView()
    private let closeImageView = with(UIImageView()) {
        $0.image = Asset.closeGray.image
    }
    
    override init() {
        super.init()
    }
    
    override func setupUI() {
        super.setupUI()
        
        addSubview(backgroudView)
        addSubview(rootView)
        
        buttonContainer.addSubview(closeImageView)
        
        rootView.addSubview(buttonContainer)
        rootView.addSubview(contentStackView)
    }
    
    override func setupConstraints() {
        super.setupConstraints()
        backgroudView.positionToTheEdges(respectingSafeArea: false)
        
        rootView.snp.makeConstraints { make in
            make.center.equalToSuperview()
            make.leading.trailing.equalToSuperview().inset(20)
        }
        buttonContainer.snp.makeConstraints { make in
            make.height.equalTo(30)
            make.leading.trailing.equalToSuperview().inset(16)
            make.top.equalToSuperview().inset(16)
        }
        closeImageView.snp.makeConstraints { make in
            make.trailing.equalToSuperview()
            make.centerY.equalToSuperview()
            make.size.equalTo(30)
        }
        contentStackView.snp.makeConstraints { make in
            make.top.equalTo(buttonContainer.snp.bottom).inset(8)
            make.leading.trailing.bottom.equalToSuperview()
        }
    }
    
    override func setupVisuals() {
        super.setupVisuals()
        rootView.backgroundColor = .white
        rootView.radius(radius: 8)
    }
}

extension ColorInformationView {
    func fill(with model: ViewModel) {
        contentStackView.removeSubviews()
        
        contentStackView.addArrangedSubview(label)
        for item in model.items {
            let itemView = ColorInformationItemView()
            itemView.fill(with: item)
            contentStackView.addArrangedSubview(itemView)
        }
    }
}

final class ColorInformationItemView: BaseView, Fillable {
    struct ViewModel {
        let iconType: IconType
        let title: String
        let subtitle: String?
        
        public enum IconType {
            case color(input: ColorInput)
            case icon(input: IconInput)
            
            struct ColorInput {
                let color: UIColor
            }
            
            struct IconInput {
                let icon: UIImage
            }
        }
    }
    let iconContainer = UIView()
    let titleLabel = with(UILabel.calloutBold) {
        $0.numberOfLines = 0
    }
    let subtitleLabel = with(UILabel.standartMediumRegular) {
        $0.numberOfLines = 0
        $0.textColor = UIColor.LogThisPlace.gray
    }
    let textStackView = with(UIStackView.vertical) {
        $0.spacing = 4
    }
    
    override func setupUI() {
        super.setupUI()
        addSubviews([iconContainer, textStackView])
        textStackView.addArrangedSubviews([
            titleLabel,
            subtitleLabel
        ])
    }
    
    override func setupConstraints() {
        super.setupConstraints()
        
        iconContainer.snp.makeConstraints { make in
            make.height.equalTo(38)
            make.width.equalTo(35)
            make.leading.equalToSuperview()
            make.centerY.equalToSuperview()
        }
        textStackView.snp.makeConstraints { make in
            make.leading.equalTo(iconContainer.snp.trailing).offset(24)
            make.trailing.equalToSuperview()
            make.top.bottom.equalToSuperview()
            make.height.greaterThanOrEqualTo(35)
        }
    }
    
    func fill(with model: ViewModel) {
        switch model.iconType {
        case let .color(input):
            let colorView = MapPinView()
            colorView.fill(with: .init(tintColor: input.color, image: nil))
            iconContainer.addSubview(colorView)
            colorView.positionToTheEdges()
        case let .icon(input):
            let imageView = UIImageView()
            imageView.image = input.icon
            iconContainer.addSubview(imageView)
            imageView.positionToTheEdges()
        }
        
        titleLabel.text = model.title
        if let subtitle = model.subtitle {
            subtitleLabel.text = subtitle
        } else {
            textStackView.removeArrangedSubview(subtitleLabel)
        }
    }
}
