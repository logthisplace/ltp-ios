//
//  PoiCategoryCell.swift
//  Log This Place
//

import UIKit

struct PoiCategoryViewModel {
    var selected: Bool = false
    let categoryItem: QuestionItemValue
}

final class PoiCategoryCell: UICollectionViewCell, NibProvideable {

    private struct Constants {
        static let cellRadius: CGFloat = 22
    }
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var titleLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.layoutIfNeeded()
        
        setupUI()
    }
    
    func setupUI() {
        containerView.layer.cornerRadius = Constants.cellRadius
        containerView.layer.masksToBounds = true
        titleLbl.font = LTPUILabel.calloutRegular.font
    }
    
    func setData(category: PoiCategoryViewModel) {
        containerView.backgroundColor = category.selected ? UIColor.LogThisPlace.gray : .clear
        titleLbl.text = category.categoryItem.labels.localized
    }
}
