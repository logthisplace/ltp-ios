//
//  SearchBarView.swift
//  Log This Place
//
//

import UIKit


final class SearchBarView: BaseView {
    
    private let searchBarContainer = UIView()
    private let searchBarContentView = with(UIView()) {
        $0.layer.masksToBounds = false
        $0.layer.shadowOpacity = 0.5
        $0.layer.shadowColor = UIColor.black.cgColor
        $0.layer.shadowOffset = .zero
        $0.backgroundColor = UIColor.white
        $0.layer.cornerRadius = 26
    }
    let searchBar = UISearchBar()
    
    override func setupUI() {
        super.setupUI()
        addSubview(searchBarContainer)

        searchBarContainer.addSubview(searchBarContentView)
        searchBarContentView.addSubview(searchBar)
    }
    
    override func setupConstraints() {
        super.setupConstraints()
        searchBarContainer.positionToTheEdges()
        
        searchBarContentView.snp.makeConstraints{ make in
            make.margins.equalToSuperview()
        }
        
        searchBar.snp.makeConstraints{ make in
            make.margins.equalToSuperview()
        }
    }
    
    override func setupVisuals() {
        super.setupVisuals()
        searchBar.placeholder = L10n.MapView.SearchBar.placeholder
        searchBar.setImage(Asset.Map.mapSearch.image, for: .search, state: .normal)
        searchBar.textField?.font = LTPUILabel.titleH4Regular.font
        
        if #available(iOS 13.0, *) {
            searchBar.searchTextField.backgroundColor = .white
        } else if let searchField = searchBar.value(forKey: "searchField") as? UITextField {
            searchBar.layer.borderWidth = 1
            searchBar.layer.borderColor = UIColor.white.cgColor
            searchBar.barTintColor = .white
            searchField.leftView?.frame = .init(x: 0, y: 0, width: 40, height: 40)
            searchField.backgroundColor = .white
            searchField.subviews.first?.backgroundColor = .white
        }
        
        /// additional check for remove background subviews
        if let searchBarSubviews = searchBar.subviews.last?.subviews {
            for view in searchBarSubviews {
                if type(of: view) == NSClassFromString("UISearchBarBackground"){
                    view.alpha = 0.0
                }
            }
        }
        
        searchBar.setPositionAdjustment(UIOffset(horizontal: -5, vertical: 0), for: .search)
    }
}
