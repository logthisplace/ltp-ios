//
//  MapPinView.swift
//  Log This Place
//

import UIKit

final class MapPinView: UIView, Fillable {
    
    struct ViewModel {        
        static let selectedPinColor = UIColor.LogThisPlace.purple
        static let accessiblePinColor = UIColor.LogThisPlace.green
        static let notAccessiblePinColor = UIColor.LogThisPlace.yellow

        let tintColor: UIColor
        let image: UIImage?
    }
    
    var defaultTintColor: UIColor = ViewModel.notAccessiblePinColor
    
    private var pinView = with(UIImageView()) {
        $0.image = #imageLiteral(resourceName: "Pin")
    }
    
    private var pinImage = with(UIImageView()) {
        $0.contentMode = .scaleAspectFit
    }
    
    init() {
        super.init(frame: .init(x: 0, y: 0, width: 35, height: 39))
        setupViewHierarchy()
        setupConstraints()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func set(tintColor: UIColor) {
        pinView.tintColor = tintColor
    }
    
    private func setupViewHierarchy() {
        addSubview(pinView)
        pinView.addSubview(pinImage)
    }
    
    private func setupConstraints() {
        pinView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        
        pinImage.snp.makeConstraints { make in
            make.leading.equalTo(10)
            make.top.equalTo(9)
            make.size.equalTo(16)
        }
    }
}

extension MapPinView {
    func fill(with model: ViewModel) {
        defaultTintColor = model.tintColor
        pinView.tintColor = model.tintColor
        pinImage.image = model.image
    }
}
