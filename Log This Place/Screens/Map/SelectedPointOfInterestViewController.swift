//
//  SelectedPointOfInterestViewController.swift
//  Log This Place
//

import UIKit

protocol SelectedPointOfInterestViewInput: View {
    func fill(with model: SelectedPointOfInterestViewModel)
}

enum SelectedPointOfInterestViewType {
    /// Google POI
    case googlePOI

    /// Saved POI to Firebase
    case firebasePOI(input: PointOfInterest)
}

struct SelectedPointOfInterestViewModel {
    let title: String
    let icon: UIImage?
    let color: UIColor
    let address: String?
    let typeOfScreen: SelectedPointOfInterestViewType
}

final class SelectedPointOfInterestViewController: BaseViewController, StoryboardedInitializable {
    
    @IBOutlet weak var nameLabel: LTPUILabel!
    @IBOutlet weak var addressLabel: LTPUILabel!
    @IBOutlet weak var categoryImageContainer: UIView!
    @IBOutlet weak var categoryImageView: UIImageView!
    @IBOutlet weak var addressContainer: UIView!
    @IBOutlet weak var checkAccessibilityDetailsBtn: UIButton!
    @IBOutlet weak var anwserQuestionsBtn: UIButton!
    
    var presenter: SelectedPointOfInterestPresenterProtocol?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter?.viewDidLoad()
    }
    
    override func setupUI() {
        checkAccessibilityDetailsBtn.setTitle(L10n.SelectedPointOfInterest.CheckAccessibilityDetailsButton.title, for: .normal)
        anwserQuestionsBtn.setTitle(L10n.SelectedPointOfInterest.AnswerQuestionsButton.title, for: .normal)
    }
    
    override func setupBehaviour() {
        super.setupBehaviour()
        anwserQuestionsBtn.addTarget(self, action: #selector(questionnaireButtonTapped), for: .touchUpInside)
        checkAccessibilityDetailsBtn.addTarget(self, action: #selector(buildingDetailsButtonTapped), for: .touchUpInside)
    }
    
    @objc private func questionnaireButtonTapped() {
        presenter?.questionnaireButtonTapped()
    }
    
    @objc private func buildingDetailsButtonTapped() {
        presenter?.buildingDetailsButtonTapped()
    }
}

// MARK: - SelectedPointOfInterestViewInput

extension SelectedPointOfInterestViewController: SelectedPointOfInterestViewInput {
    func fill(with model: SelectedPointOfInterestViewModel) {
        nameLabel.text = model.title
        categoryImageView.image = model.icon
        categoryImageContainer.backgroundColor = model.color
        
        if let address = model.address {
            addressContainer.isHidden = false
            addressLabel.text = address
        } else {
            addressContainer.isHidden = true
        }
        
        updateTypeOfScreen(typeOfScreen: model.typeOfScreen)
    }
    
    func updateTypeOfScreen(typeOfScreen: SelectedPointOfInterestViewType) {
        switch typeOfScreen {
        case .googlePOI:
            checkAccessibilityDetailsBtn.isHidden = true
            anwserQuestionsBtn.isHidden = false
        case .firebasePOI:
            checkAccessibilityDetailsBtn.isHidden = false
            anwserQuestionsBtn.isHidden = true
        }
    }
}
