//
//  Log This Place
//
//

import UIKit
import GoogleMaps
import FirebaseStorage
import GoogleMapsUtils

struct MapPoiViewModel {
    let location: CLLocationCoordinate2D
    let pinViewModel: MapPinView.ViewModel
}

final class MapViewController: BaseViewController, StoryboardedInitializable {

    // MARK: - Constants
    private struct Constants {
        static let malmoLantitude: Double = 55.604981
        static let malmoLongitude: Double = 13.003822
        static let cityZoom: Float = 14.81
        static let minMapZoom: Float = 12
        static let maxMapZoom: Float = 19.5
        static let mapPinZIndex: Int32 = 100
        static let defaultClasterZoom: Float = 16
        static let googleMapCustomStyleName: String = "google_map_style"
        static let googleMapCustomStyleExtension: String = "json"
    }
    
    // MARK: - Init UI
    @IBOutlet weak var mapCompassBtn: MapButton!
    @IBOutlet weak var mapColorInformBtn: MapButton!
    @IBOutlet weak var mapCenterBtn: MapButton!
    @IBOutlet weak var mapSearchBarContainer: UIView!
    @IBOutlet weak var mapProfileBtn: MapButton!
    @IBOutlet weak var categoryCollectionView: UICollectionView!
    
    // MARK: - Properties
    private let searchBarView = SearchBarView()
    var presenter: MapViewPresenterProtocol?
    
    var currentClasterZoom: Float = Constants.defaultClasterZoom
    var toClasterZoom: Float {
        currentClasterZoom += 1
        return  mapView.camera.zoom <= currentClasterZoom ? currentClasterZoom :
            (mapView.camera.zoom >= Constants.maxMapZoom ? Constants.maxMapZoom : mapView.camera.zoom)
    }

    private var pois: [MapPoiViewModel] = []
    private var poiCategories: [PoiCategoryViewModel] = []
    private let camera = GMSCameraPosition.camera(withLatitude: Constants.malmoLantitude,
                                                  longitude: Constants.malmoLongitude,
                                                  zoom: Constants.cityZoom)
    private lazy var mapView: GMSMapView = GMSMapView.map(withFrame: self.view.frame,
                                                          camera: camera)
    private var lastlyTappedMarker: GMSMarker?
    private var lastlySelectedMarker: GMSMarker?
    private var mapViewMarkers = [GMSMarker]()
    private var clusterManager: GMUClusterManager!
    private var lastlyTappedGooglePoiMarker: GMSMarker?
    
    private var currentDeviceOrientation = UIDevice.current.orientation
    
    /// view for display information about colors
    private let colorInformationView = ColorInformationView()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        categoryCollectionView.delegate = self
        categoryCollectionView.dataSource = self
        registerCell()
        setupSearchBar()
        setupPoisColorInformationView()
        
        addMapView()
        setMapStyle()
        
        mapView.delegate = self
        mapView.isMyLocationEnabled = true
        mapView.setMinZoom(Constants.minMapZoom,
                           maxZoom: Constants.maxMapZoom)
        setupClustering()
        
        presenter?.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: true)
        presenter?.viewWillAppear()
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        if currentDeviceOrientation != UIDevice.current.orientation {
            presenter?.didChangeOrientation()
            coordinator.animate(alongsideTransition: {_ in }, completion: { [weak self] _ in
                self?.updateLastlySelectedMarkerToDefaultColor()
            })
            currentDeviceOrientation = UIDevice.current.orientation
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        presenter?.viewWillDisappear()
    }
    
    private func addMapView() {
        mapView.alpha = 0
        view.addSubview(mapView)
        view.sendSubviewToBack(mapView)
        UIView.animate(withDuration: 0.3) { [weak self] in
            self?.mapView.alpha = 1
        }
        mapView.positionToTheEdges(respectingSafeArea: false)
    }
    
    private func setupPoisColorInformationView() {
        let tapHideButtonRecognizer = UITapGestureRecognizer(target: self, action:  #selector(hidePinsColorInformationView))
        let tapBackgroundRecognizer = UITapGestureRecognizer(target: self, action:  #selector(hidePinsColorInformationView))

        colorInformationView.buttonContainer.isUserInteractionEnabled = true
        colorInformationView.buttonContainer.addGestureRecognizer(tapHideButtonRecognizer)
        
        colorInformationView.backgroudView.isUserInteractionEnabled = true
        colorInformationView.backgroudView.addGestureRecognizer(tapBackgroundRecognizer)
    }
    
    private func setupClustering() {
        let iconGenerator = GMUDefaultClusterIconGenerator.init(buckets: [10, 20, 30, 40, 50],
                                                                backgroundColors: [UIColor.LogThisPlace.clusterBlue,
                                                                                   UIColor.LogThisPlace.clusterBlue,
                                                                                   UIColor.LogThisPlace.clusterBlue,
                                                                                   UIColor.LogThisPlace.clusterBlue,
                                                                                   UIColor.LogThisPlace.clusterBlue])
        let algorithm = GMUNonHierarchicalDistanceBasedAlgorithm()
        let renderer = GMUDefaultClusterRenderer(mapView: mapView,
                                       clusterIconGenerator: iconGenerator)
        renderer.minimumClusterSize = 13
        renderer.maximumClusterZoom = 17
        
        clusterManager = GMUClusterManager(map: mapView,
                                           algorithm: algorithm,
                                           renderer: renderer)
        clusterManager.setMapDelegate(self)
    }
    
    @objc private func searchBarTapped() {
        presenter?.searchBarTapped()
    }
    
    private func drawPins() {
        clusterManager.clearItems()
        mapViewMarkers.removeAll()
        for poi in pois {
            addMapPoiMarker(with: poi)
        }
        cluster()
    }
    
    // MARK: - Search bar
    
    func setupSearchBar() {
        mapSearchBarContainer.addSubview(searchBarView)
        searchBarView.positionToTheEdges()

        searchBarView.searchBar.isUserInteractionEnabled = false

        let mapSearchBarGesture = UITapGestureRecognizer(target: self, action:  #selector(searchBarTapped))
        mapSearchBarContainer.isUserInteractionEnabled = true
        mapSearchBarContainer.addGestureRecognizer(mapSearchBarGesture)
    }
    
    // MARK: - Map Settings
    func setMapStyle() {
        mapView.padding = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 65)

        if let styleURL = Bundle.main.url(forResource: Constants.googleMapCustomStyleName,
                                          withExtension: Constants.googleMapCustomStyleExtension) {
            mapView.mapStyle = try? GMSMapStyle(contentsOfFileURL: styleURL)
        }
    }
    
    // MARK: - Collection View Settings
    func registerCell() {
        categoryCollectionView.register(item: PoiCategoryCell.self)
    }
    
    // MARK: - Actions
    @IBAction func showProfileTapped(_ sender: Any) {
        presenter?.showProfileTapped()
    }
    
    @IBAction func centerMapTapped() {
        presenter?.centerMapTapped()
    }
    
    @IBAction func resetMapRotationTapped() {
        presenter?.resetMapRotationTapped()
    }
    
    @IBAction func showPinsColorInformationTapped(_ sender: Any) {
        presenter?.showPinsColorInformationTapped()
    }
    
    @objc func hidePinsColorInformationView() {
        colorInformationView.removeFromSuperviewAnimation()
    }
    
    private func update(marker: GMSMarker, with model: MapPinView.ViewModel) {
        let mapPin = MapPinView()
        mapPin.fill(with: model)
        marker.iconView?.removeFromSuperview()
        marker.iconView = mapPin
    }
}

extension MapViewController: GMSMapViewDelegate {
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        mapView.animate(toLocation: marker.position)
        
        // cluster tapped
        if marker.userData is GMUCluster {
            mapView.animate(toZoom: toClasterZoom)
            return true
        }

        // normal marker tapped
        let location = PoiLocation(latitude: marker.position.latitude, longitude: marker.position.longitude)
        presenter?.markerTapped(at: location)
        
        return false
    }
    
    func mapView(_ mapView: GMSMapView, didTapPOIWithPlaceID placeID: String, name: String, location: CLLocationCoordinate2D) {
        presenter?.googlePlaceTapped(placeID: placeID,
                                    location: location)
    }
    
    func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition) {
        if mapView.camera.zoom < currentClasterZoom { currentClasterZoom = Constants.defaultClasterZoom }
        presenter?.mapRestriction(didChange: position)
    }
}

// MARK: - Presenter delegate
extension MapViewController: MapViewInput {
    
    func drawCityPolygon(_ polygon: GMSPolygon) {
        polygon.map = mapView
    }
    
    func cluster() {
        clusterManager.cluster()
    }
    
    func updateMapPoiMarker(with poi: MapPoiViewModel) {
        if let row = mapViewMarkers.firstIndex(where: { $0.position.isEqual(poi.location) }) {
            mapViewMarkers[row].tracksViewChanges = true
            update(marker: mapViewMarkers[row], with: poi.pinViewModel)
            mapViewMarkers[row].tracksViewChanges = false
        }
    }
    
    func addMapPoiMarker(with poi: MapPoiViewModel,
                         marker: GMSMarker) {
        marker.position = poi.location
        marker.tracksViewChanges = false
        marker.zIndex = Constants.mapPinZIndex
        marker.collisionBehavior = GMSCollisionBehavior.requiredAndHidesOptional
        mapViewMarkers.append(marker)
        update(marker: marker, with: poi.pinViewModel)
        clusterManager.add(marker)
    }
    
    func show(pointOfInterests: [MapPoiViewModel]) {
        if pointOfInterests.isEmpty {
            showAlert(title: L10n.Common.NotFound.text,
                            message: L10n.Common.PoiNotFound.text,
                            actions: nil)
        }
        pois.removeAll()
        pois.append(contentsOf: pointOfInterests)
        drawPins()
    }
    
    func resetMapRotation() {
        mapView.animate(toBearing: .zero)
    }
    
    func zoomTo(zoom: Float) {
        mapView.animate(toZoom: zoom)
    }
    
    func centerTo(location: CLLocationCoordinate2D) {
        mapView.animate(toLocation: location)
    }
    
    func removeLastlyTappedGooglePoiMarker() {
        guard let location = lastlyTappedGooglePoiMarker?.position else { return }
        if let row = mapViewMarkers.firstIndex(where: { $0.position.isEqual(location) }) {
            mapViewMarkers[row].map = nil
            lastlyTappedGooglePoiMarker = nil
            clusterManager.remove(mapViewMarkers[row])
            mapViewMarkers.remove(at: row)
        }
    }
    
    func centerToWithPin(location: CLLocationCoordinate2D) {
        removeLastlyTappedGooglePoiMarker()
        lastlyTappedGooglePoiMarker = GMSMarker()
        guard let tappedGooglePoiMarker = lastlyTappedGooglePoiMarker else { return }
        addMapPoiMarker(with: .init(location: location,
                                    pinViewModel: .init(tintColor: UIColor.LogThisPlace.grayAlmostDark,
                                                        image: nil)),
                        marker: tappedGooglePoiMarker)
        mapView.animate(toLocation: location)
        cluster()
    }
    
    func removePinByLocation(location: CLLocationCoordinate2D) {
        if let row = mapViewMarkers.firstIndex(where: { $0.position.isEqual(location) }) {
            mapViewMarkers[row].map = nil
            lastlyTappedGooglePoiMarker = nil
            clusterManager.remove(mapViewMarkers[row])
            mapViewMarkers.remove(at: row)
        }
    }
    
    func setCenterButton(enabled: Bool) {
        mapCenterBtn.isEnabled = enabled
    }
    
    func setCurrentlyTappedMarkerAsSelected() {
        lastlySelectedMarker = lastlyTappedMarker
    }
    
    /// need clear when a user has choosen Firestore POI then he will click to Google POI
    func setLastlySelectedMarkerNil() {
        lastlySelectedMarker = nil
    }
    
    func updateLastlySelectedMarkerToDefaultColor() {
        guard let lastlySelectedPinView = (lastlySelectedMarker?.iconView as? MapPinView) else {
            removeLastlyTappedGooglePoiMarker()
            return
        }
        lastlySelectedMarker?.tracksViewChanges = true
        lastlySelectedPinView.set(tintColor: lastlySelectedPinView.defaultTintColor)
        lastlySelectedMarker?.tracksViewChanges = false
    }
    
    func updateLastlyTappedMarker(withTintColor color: UIColor, location: CLLocationCoordinate2D) {
        lastlyTappedMarker = mapViewMarkers.first(where: { $0.position.isEqual(location) })
        lastlyTappedMarker?.tracksViewChanges = true
        (lastlyTappedMarker?.iconView as? MapPinView)?.set(tintColor: color)
        lastlyTappedMarker?.tracksViewChanges = false
    }
    
    func showPoiCategory(categories: [PoiCategoryViewModel]) {
        poiCategories.removeAll()
        poiCategories.append(contentsOf: categories)
        categoryCollectionView.reloadData()
    }
    
    func showPinsColorInformationView(items: [ColorInformationItemView.ViewModel]) {
        colorInformationView.fill(with: .init(items: items))
        view.addSubviewAnimation(colorInformationView)
        colorInformationView.positionToTheEdges(respectingSafeArea: false)
    }
}

// MARK: - Collection View delegate
extension MapViewController: UICollectionViewDataSource, UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return poiCategories.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: PoiCategoryCell = collectionView.deque(for: indexPath)
        
        cell.setData(category: poiCategories[indexPath.row])
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, shouldSelectItemAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        presenter?.categoryFilterTapped(category: poiCategories[indexPath.row])
        
        if let row = poiCategories.firstIndex(where: {$0.selected == true}) {
            poiCategories[row].selected = false
        }
        poiCategories[indexPath.row].selected = true
        
        collectionView.reloadData()
        collectionView.selectItem(at: indexPath, animated: true, scrollPosition: .centeredHorizontally)
    }
}

// TODO: - dynamic calculation width of cell. AutomaticSize doesnt work after reloadData in didSelectItemAt
extension MapViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        // TODO: - need find better way
        let label = UILabel(frame: CGRect.zero)
        label.text = poiCategories[indexPath.item].categoryItem.labels.localized
        label.sizeToFit()
        return CGSize(width: label.frame.width + 50, height: 40)
    }
}

