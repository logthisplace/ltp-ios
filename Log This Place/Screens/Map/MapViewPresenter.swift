//
//  MapViewPresenter.swift
//  Log This Place
//
//

import GoogleMaps
import GooglePlaces
import FirebaseFirestore


protocol MapViewPresenterInformable: AnyObject {
    func didDismissSelectedPoi()
//    func didAddNewPOI(poiId: String?)
//    func didUpdatePOI(with poiId: String)
    func didUpdateUserPassport()
    func didSelectedFoundPoi(placeID: String,
                             location: CLLocationCoordinate2D)
    func didLogoutTapped()
}

protocol MapViewPresenterProtocol: Presenter {
    func showProfileTapped()
    func centerMapTapped()
    func resetMapRotationTapped()
    func markerTapped(at location: PoiLocation)
    func categoryFilterTapped(category: PoiCategoryViewModel)
    func googlePlaceTapped(placeID: String,
                           location: CLLocationCoordinate2D)
    func searchBarTapped()
    func showPinsColorInformationTapped()
    func mapRestriction(didChange position: GMSCameraPosition)
    func didChangeOrientation()
}

final class MapViewPresenter: BasePresenter, MapViewPresenterProtocol, MapViewPresenterInformable {
    
    private struct Constants {
        static let centerCityLatitude: Double = 55.59886444316501
        static let centerCityLongitude: Double = 12.994156647100327
        /// when a user click by category
        static let poisByCategoryZoom: Float = 12
        /// general zoom
        static let poisZoom: Float = 14
        /// when a user click to POI from "Search POI Screen"
        static let foundPoiZoom: Float = 17
    }
    
    weak var view: MapViewInput?
    
    private let poiDataManager: PointOfInterestDataManagerProtocol = AppContainer.resolve()
    private let questionnareDataManager: QuestionnaireDataManagerProtocol = AppContainer.resolve()
    private let userPassportDataManager: UserPassportDataManagerProtocol = AppContainer.resolve()
    private let userDefaultsManager: UserDefaultsManagerProtocol = AppContainer.resolve()
    private let cityLocationManager: CityLocationManagerProtocol = AppContainer.resolve()
    private let placesClient: GooglePlacesProtocol = AppContainer.resolve()
    private let locationManager: LocationManagerProtocol = AppContainer.resolve()
    private let firebaseAnalyticManager: FirebaseAnalyticManagerProtocol = AppContainer.resolve()
    private let userIdentificationDataManager: UserIdentificationDataManagerProtocol = AppContainer.resolve()
    private let pinColorLogicDataManager: PinColorLogicDataManagerProtocol = AppContainer.resolve()
    private var conditionManager: ConditionManagerProtocol = AppContainer.resolve()
    private var userManager: UserManagerProtocol = AppContainer.resolve()
    private var remoteConfigManager: RemoteConfigFirebaseManagerProtocol = AppContainer.resolve()
    
    private let coordinator: MapCoordinatorProtocol
        
    private var userPassport: UserPassport?
    
    /// information - what color on the map what does it mean
    /// displayed in custom alert
    private let pinsColorInformation: [ColorInformationItemView.ViewModel] = [
        .init(iconType: .icon(input: .init(icon: Asset.Map.googlePins.image)),
              title: L10n.PoisInformation.FirstItem.Title.text,
              subtitle: L10n.PoisInformation.FirstItem.Subtitle.text),
        .init(iconType: .color(input: .init(color: UIColor.LogThisPlace.purple)),
              title: L10n.PoisInformation.SecondItem.Title.text,
              subtitle: nil),
        .init(iconType: .color(input: .init(color: UIColor.LogThisPlace.yellow)),
              title: L10n.PoisInformation.ThirdItem.Title.text,
              subtitle: L10n.PoisInformation.ThirdItem.Subtitle.text),
        .init(iconType: .color(input: .init(color: UIColor.LogThisPlace.green)),
              title: L10n.PoisInformation.FourthItem.Title.text,
              subtitle: L10n.PoisInformation.FourthItem.Subtitle.text),
    ]
    
    /// collect all pois
    private var allPointOfInterests = [PointOfInterest]()
    /// collect only dispayed pois, need for example for filtering 
    private var currentlyDisplayedPointOfInterests = [PointOfInterest]()
    private var lastlyPoiLocation: CLLocationCoordinate2D?
    
    /// collect all categories
    private var categories = [PoiCategoryViewModel]()
    
    /// logic for calculation color for each POI
    private var poiColorConditionLogic: Condition?
    /// when a user click to Firestore POI we will save clicked POI, for updating on the map
    private var poiIdForPossibleUpdate: String?
    private var dataWasFetched = false
    
    init(coordinator: MapCoordinatorProtocol) {
        self.coordinator = coordinator
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        checkMandatoryUpdates()
        
        firebaseAnalyticManager.logEvent(screen: .map)
        locationManager.delegate = self
        fillCityPolygon()
        fetchData()
    }
    
    override func viewWillAppear() {
        super.viewWillAppear()
        if dataWasFetched {
            addPoisChangeListener()
        }
    }
        
    override func viewWillDisappear() {
        super.viewWillDisappear()
        removePoisChangeListener()
    }
    
    private func fetchData() {
        
        view?.showLoading(fullscreen: true)
        let dispatchGroup = DispatchGroup()
        
        /// get user profile data
        dispatchGroup.enter()
        fetchUserData { [weak self]  result in
            guard let self = self else { return }
            if case let .success(userPassport) = result {
                if self.userDefaultsManager.wasWelcomeShown {
                    self.userPassport = userPassport
                }
            }
            dispatchGroup.leave()
        }
        
        /// get condition for generating poi color
        dispatchGroup.enter()
        getPoiColorLogic { [weak self] result in
            if case let .success(poiColorLogic) = result {
                self?.poiColorConditionLogic = poiColorLogic?.data
            }
            dispatchGroup.leave()
        }
        
        /// get categories
        dispatchGroup.enter()
        getPoiTypes { [weak self] categories in
            dispatchGroup.leave()
            self?.view?.showPoiCategory(categories: categories)
        }
        
        dispatchGroup.notify(queue: .main) {
            self.getPointOfInterests { [weak self] in
                self?.dataWasFetched = true
                self?.addPoisChangeListener()
            }
        }
    }
    
    private func fetchUserData(completionHandler: @escaping (Result<UserPassport, Error>) -> Void) {
        userIdentificationDataManager.checkAndsignInAnonymously() { [weak self] result in
            guard let self = self else { return }
            switch result {
            case .success(_):
                self.getUserPassport { result in
                    completionHandler(result)
                }
            case let .failure(error):
                // TODO: - Handle error
                completionHandler(.failure(error))
            }
        }
    }
    
    /// check for alert display where to be shown what color on the map what does it mean
    /// works with `pinsColorInformation`
    private func checkWasPinsColorInformationShown() {
        if !userDefaultsManager.wasPinsColorInformationShown {
            userDefaultsManager.wasPinsColorInformationShown = true
            view?.showPinsColorInformationView(items: pinsColorInformation)
        }
    }
    
    private func getUserPassport(completionHandler: @escaping (Result<UserPassport, Error>) -> Void) {
        userPassportDataManager.getUserPassport(completionHandler: {(result) in
            completionHandler(result)
        })
    }
    
    func showProfileTapped() {
        if userDefaultsManager.wasWelcomeShown {
            coordinator.showUserProfile(with: .init(didLogin: false, userPassport: userPassport),
                                        mapViewPresenterInformable: self)
        } else {
            coordinator.showUserIdentification(mapViewPresenterInformable: self)
        }
    }
    
    func searchBarTapped() {
        coordinator.showSearchPois(mapViewPresenterInformable: self)
    }
    
    func centerMapTapped() {
        view?.setCenterButton(enabled: false)
        locationManager.requestLocation()
    }
    
    func resetMapRotationTapped() {
        view?.resetMapRotation()
    }
    
    func markerTapped(at location: PoiLocation) {
        guard let tappedPoi = currentlyDisplayedPointOfInterests.first(where: { $0.location == location }) else {
            return
        }
        
        processPointOfInterestTap(tappedPoi: tappedPoi)
    }
    
    /// Method to show information about the POI that was clicked on the map. Verification of POI by Postal Code
    /// - Parameters:
    ///   - placeID: google place id
    ///   - location: google place location
    func googlePlaceTapped(placeID: String,
                           location: CLLocationCoordinate2D) {
        
        if let poi = allPointOfInterests.first(where: { $0.g_places_id == placeID }) {
            /// This Poi is already present, so we want to process it as edit
            processPointOfInterestTap(tappedPoi: poi)
            return
        }
        view?.updateLastlySelectedMarkerToDefaultColor()
        view?.setLastlySelectedMarkerNil()
        
        view?.showLoading(fullscreen: true)
        
        let placeFields: GMSPlaceField = [.name, .formattedAddress, .types, .addressComponents]
        placesClient.fetchPlace(fromPlaceID: placeID,
                                placeFields: placeFields) { [weak self] (place: GMSPlace?, error: Error?) in
            guard let self = self else { return }
            self.view?.hideLoading()
            
            if let error = error {
                self.coordinator.dismissBottomPanel()
                Logger.shared.error(error.localizedDescription)
                self.view?.showErrorAlert(message: error.localizedDescription)
                return
            }
            guard let place = place else {
                self.coordinator.dismissBottomPanel()
                Logger.shared.error(L10n.Common.Google.Location.NotFound.text)
                self.view?.showErrorAlert(message: L10n.Common.Google.Location.NotFound.text)
                return
            }
            guard let addressComponents = place.addressComponents,
                  let postalCodeComponent = addressComponents.first(where: { $0.types.contains(where: { $0 == "postal_code" })}),
                  let postalCode = Int(postalCodeComponent.name.replacingOccurrences(of: " ", with: "")) else {
                      self.coordinator.dismissBottomPanel()
                      self.view?.showErrorAlert(message: L10n.MapView.Poi.CantFindPostalCode.text)
                      Logger.shared.error("Postal Code is nill, place \(placeID)")
                      return
                  }
            
            /// Verification of POI by Postal Code
            self.cityLocationManager.isLocationInCity(postalCode: postalCode) { (result) in
                switch result {
                case let .success(val):
                    if val {
                        self.coordinator.showSelectedPoiScreen(input: .init(name: place.name ?? L10n.Common.NotFound.text,
                                                                            googleId: placeID,
                                                                            address: place.formattedAddress ?? L10n.Common.NotFound.text,
                                                                            location: .init(latitude: location.latitude,
                                                                                            longitude: location.longitude),
                                                                            typeOfScreen: .googlePOI,
                                                                            category: nil,
                                                                            color: UIColor.LogThisPlace.grayAlmostDark),
                                                                                              mapViewPresenterInformable: self)
                        self.view?.centerToWithPin(location: location)
                    } else {
                        Logger.shared.error("Postal Code \(postalCode) does not belong to the city")
                        self.view?.removeLastlyTappedGooglePoiMarker()
                        self.coordinator.dismissBottomPanel()
                        self.view?.showErrorAlert(message: L10n.MapView.Poi.Outside.text)
                    }
                case let .failure(error):
                    Logger.shared.error(error.localizedDescription)
                    self.view?.removeLastlyTappedGooglePoiMarker()
                    self.coordinator.dismissBottomPanel()
                    self.view?.showErrorAlert(message: error.localizedDescription)
                }
            }
        }
    }
    
    func didChangeOrientation() {
        coordinator.dismissBottomPanel()
    }
    
    func showPinsColorInformationTapped() {
        view?.showPinsColorInformationView(items: pinsColorInformation)
    }
    
    private func getPoiColorLogic(completionHandler: @escaping (Result<PinColorLogic?, Error>) -> Void) {
        pinColorLogicDataManager.getPinColorLogic { result in
            completionHandler(result)
        }
    }
    
    /// Categories do not exist as a separate entity, but as part of questions. Handle categories for `categories` list
    private func getPoiTypes(completionHandler: @escaping ([PoiCategoryViewModel]) -> Void) {
        questionnareDataManager.getQuestionnaire(questionnaireType: .poi,
                                                 completionHandler: { [weak self] result in
                                                    if case .success(let data) = result {
                                                        
                                                        guard let poiCategoryGroup = data.questionGroups.first(where: { $0.questions.contains(where: { $0.uuid == FirestoreDatabase.CATEGORY_UUID })}) else {
                                                            completionHandler([])
                                                            return
                                                        }
                                                        
                                                        let allCategoriesItem = PoiCategoryViewModel(selected: true,
                                                                                                     categoryItem: .init(uuid: "All",
                                                                                                                         labels: .init(en: L10n.MapView.All.text,
                                                                                                                                       sv: L10n.MapView.All.text)))
                                                        self?.categories.append(allCategoriesItem)
                                                        for category in poiCategoryGroup.questions.first?.values ?? [] {
                                                            self?.categories.append(PoiCategoryViewModel(categoryItem: category))
                                                        }
                                                        completionHandler(self?.categories ?? [])
                                                    } else {
                                                        completionHandler([])
                                                    }
                                                    
                                                 })
    }
    
    /// Filter all POIs by category uuid
    func categoryFilterTapped(category: PoiCategoryViewModel) {
        view?.removeLastlyTappedGooglePoiMarker()
        coordinator.dismissBottomPanel()
        if category.categoryItem.uuid == "All" {
            currentlyDisplayedPointOfInterests = allPointOfInterests
        } else {
            currentlyDisplayedPointOfInterests = allPointOfInterests.filter({ $0.categoryId == category.categoryItem.uuid })
        }
        let mappedResult = mapPoisToMapPoiViewModel(with: currentlyDisplayedPointOfInterests)
        view?.show(pointOfInterests: mappedResult)
        view?.zoomTo(zoom: Constants.poisByCategoryZoom)
        let randomPoiLocation = currentlyDisplayedPointOfInterests.randomElement()?.location
        if let randomPoiLocation = randomPoiLocation {
            view?.centerTo(location: randomPoiLocation.coreLocation)
        }
    }
    
    /// restriction of map display by city
    func mapRestriction(didChange position: GMSCameraPosition) {
        var latitude  = position.target.latitude;
        var longitude = position.target.longitude;
        
        latitude = min(position.target.latitude, cityLocationManager.cityBounds.northEast.latitude)
        latitude = max(latitude, cityLocationManager.cityBounds.southWest.latitude)
        longitude = min(position.target.longitude, cityLocationManager.cityBounds.northEast.longitude)
        longitude = max(longitude, cityLocationManager.cityBounds.southWest.longitude)
        
        if (latitude != position.target.latitude || longitude != position.target.longitude) {
            var newLocation = CLLocationCoordinate2D();
            newLocation.latitude  = latitude;
            newLocation.longitude = longitude;
            view?.centerTo(location: newLocation)
        }
    }
    
    /// Method to fetch information about saved POI. Information about the place is taken from `g_places_id`
    /// `g_places_id` - saved in Firestore
    /// - Parameter tappedPoi: object received from Firestore
    private func processPointOfInterestTap(tappedPoi: PointOfInterest) {
        guard tappedPoi.id != nil else { return }
        
        poiIdForPossibleUpdate = tappedPoi.id
        view?.removeLastlyTappedGooglePoiMarker()
        
        view?.showLoading(fullscreen: true)
        placesClient.fetchPlace(fromPlaceID: tappedPoi.g_places_id,
                                placeFields: [.name, .formattedAddress]) { [weak self] place, error in
            guard let self = self else { return }
            self.view?.hideLoading()
            
            if error != nil {
                Logger.shared.error(String(describing: error))
                self.view?.showErrorAlert(message: error?.localizedDescription)
                return
            }
            
            guard let placeName = place?.name,
                  let placeAddress = place?.formattedAddress else {
                self.coordinator.dismissBottomPanel()
                self.view?.showErrorAlert(message: L10n.Common.SomethingWentWrong.text)
                return
            }
            Logger.shared.info("googleId: \(tappedPoi.g_places_id)")
            self.showPointOfInterestTap(placeName: placeName, placeAddress: placeAddress, tappedPoi: tappedPoi)
        }
    }
    
    /// Method to show place in bottom drawable view
    private func showPointOfInterestTap(placeName: String, placeAddress: String, tappedPoi: PointOfInterest) {
        view?.updateLastlySelectedMarkerToDefaultColor()
        view?.updateLastlyTappedMarker(withTintColor: MapPinView.ViewModel.selectedPinColor,
                                       location: CLLocationCoordinate2D(latitude: tappedPoi.location?.latitude ?? 0.0,
                                                                        longitude: tappedPoi.location?.longitude ?? 0.0))
        view?.setCurrentlyTappedMarkerAsSelected()
        
        let tappedPoiCategory = categories.first(where: { $0.categoryItem.uuid == tappedPoi.categoryId })?.categoryItem
        coordinator.showSelectedPoiScreen(input: .init(name: placeName,
                                                       googleId: tappedPoi.g_places_id,
                                                       address: placeAddress,
                                                       location: tappedPoi.location,
                                                       typeOfScreen: .firebasePOI(input: tappedPoi),
                                                       category: tappedPoiCategory,
                                                       color: getPinViewColorByPoi(with: tappedPoi)),
                                                                         mapViewPresenterInformable: self)
        if let tappedPoiLocation = tappedPoi.location?.coreLocation {
            view?.centerTo(location: tappedPoiLocation)
        }
    }
    
    private func getPointOfInterests(completionHandler: @escaping () -> Void) {
        view?.showLoading(fullscreen: true)
        poiDataManager.getPointOfInterests() { [weak self] result in
            guard let self = self else { return }
            
            switch result {
            case let .success(data):
                let mappedResult = self.mapPoisToMapPoiViewModel(with: data)
                
                if self.allPointOfInterests.isEmpty {
                    self.allPointOfInterests = data
                }
                self.currentlyDisplayedPointOfInterests = data
                self.view?.show(pointOfInterests: mappedResult)
                
                if let lastlyPoiLocation = self.lastlyPoiLocation {
                    self.view?.centerTo(location: lastlyPoiLocation)
                } else {
                    /// set camera to the center
                    self.view?.centerTo(location: .init(latitude: Constants.centerCityLatitude,
                                                        longitude: Constants.centerCityLongitude))
                    self.view?.zoomTo(zoom: Constants.poisZoom)
                }
                self.view?.hideLoading()
                
                /// check and show alert with pins color information
                self.checkWasPinsColorInformationShown()
                
                completionHandler()
            case let .failure(error):
                self.view?.hideLoading()
                self.view?.showErrorAlert(message: error.localizedDescription)
            }
        }
    }
    
    /// Method that converts the list of POIs into objects that will be displayed on the map
    /// - Parameter pois: list of POIs from Firestore
    /// - Returns: objects that will be displayed on the map
    private func mapPoisToMapPoiViewModel(with pois: [PointOfInterest]) -> [MapPoiViewModel]  {
        return pois.compactMap { poi -> MapPoiViewModel? in
            guard let location = poi.location?.coreLocation else {
                return nil
            }
            
            return MapPoiViewModel(location: location,
                                   pinViewModel: .init(tintColor: getPinViewColorByPoi(with: poi),
                                                       image: poi.category?.thumbnailAsset.image ?? Asset.PoiCategories.noneCategory.image))
        }
    }
    
    private func refreshPinColors() {
        let mappedResult = mapPoisToMapPoiViewModel(with: currentlyDisplayedPointOfInterests)
        self.view?.show(pointOfInterests: mappedResult)
    }
    
    /// Returns a color for each POI based on the user's profile
    /// - Parameter poi: certain POI
    /// - Returns: returns the color for the given POI
    private func getPinViewColorByPoi(with poi: PointOfInterest) -> UIColor {
        guard userPassport != nil,
              let poiColorConditionLogic = poiColorConditionLogic else { return UIColor.LogThisPlace.purpure }
        
        var pinColor: UIColor = MapPinView.ViewModel.notAccessiblePinColor
        
        /// merge user profile answer sheet and poi answer sheet
        let poiAnswerSheet = poi._synth?.sheet ?? [:]
        let userPassportAnswerSheet = userPassport?.answers ?? [:]
        let answerSheet = poiAnswerSheet.merged(with: userPassportAnswerSheet)
        
        if conditionManager.isConditionBasedOnAnswers(condition: poiColorConditionLogic,
                                                      answeSheet: answerSheet,
                                                      generateAnswersForNotCondition: true) {
            pinColor = MapPinView.ViewModel.accessiblePinColor
        }
        return pinColor
    }
}

// MARK: - Presenter Informable
extension MapViewPresenter {
    func didDismissSelectedPoi() {
        view?.removeLastlyTappedGooglePoiMarker()
        view?.updateLastlySelectedMarkerToDefaultColor()
    }
    
    func didUpdateUserPassport() {
        view?.showLoading(fullscreen: true)
        getUserPassport { [weak self] result in
            self?.view?.hideLoading()
            switch result {
            case .success(let data):
                self?.userPassport = data
                self?.refreshPinColors()
            case let .failure(error):
                guard let error = error as? FirestoreError else { return }
                /// user doents exist
                if error == FirestoreError.documentDoesntExist {
                    self?.userPassport = nil
                    self?.refreshPinColors()
                }
            }
        }
    }
    
    func didSelectedFoundPoi(placeID: String,
                             location: CLLocationCoordinate2D) {
        view?.zoomTo(zoom: 17)
        view?.centerTo(location: location)
        googlePlaceTapped(placeID: placeID,
                          location: location)
    }
    
    private func listenerUpdatePoiById(with poiId: String) {
        poiDataManager.updatePoiListener(poiId: poiId) { [weak self]  result in
            guard let self = self else { return }
            switch result {
            case let .success(poi):
                Logger.shared.info("POI \(poiId) was changed on Firebase. Map Screen" )
                guard let poi = poi else { return }
                self.updatePoi(with: poi)
            case .failure:
                self.poiDataManager.removeUpdatePoiListener()
            }
        }
    }
    
    func didLogoutTapped() {
        userPassport = nil
        refreshPinColors()
    }
}

// MARK: - Location manager delegate
extension MapViewPresenter: LocationManagerDelegate {
    func didFetchLocation(_ location: CLLocationCoordinate2D) {
        view?.setCenterButton(enabled: true)
        view?.centerTo(location: location)
    }
    
    func didFailToFetchLocation(error: Error) {
        view?.setCenterButton(enabled: true)
    }
    
    func authrorizationStatusDenied() {
        view?.showAlert(title: L10n.Alert.Location.Title.text,
                        message: L10n.Alert.Location.Body.text,
                        actions: [UIAlertAction(title: L10n.Common.Settings.text, style: .default, handler: {_ in
                            guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
                                return
                            }
                            
                            if UIApplication.shared.canOpenURL(settingsUrl) {
                                UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                                })
                            }
                        }), UIAlertAction(title: L10n.Common.Cancel.text, style: .default, handler:nil )])
    }
}

// MARK: - Mandatory Update Alert Check Logic
extension MapViewPresenter {
    private func checkMandatoryUpdates() {
        let currentAppVersion: String = Bundle.main.shortVersion
        
        remoteConfigManager.fetchAndActiveAppSettings { [weak self] result in
            switch result {
            case let .success(appSettings):
                guard let minimumRequiredVersion = appSettings.minimum_version_required?.ios,
                      let title = appSettings.title_labels?.localized,
                      let description = appSettings.description_labels?.localized else { return }
                let compareResult = currentAppVersion.compare(minimumRequiredVersion, options: .numeric)
                switch compareResult {
                    case .orderedSame, .orderedDescending: break
                    case .orderedAscending:
                        DispatchQueue.main.async {
                            self?.coordinator.showMandatoryUpdateAlert(with: .init(title: title,
                                                                                   description: description))
                        }
                }
            case let .failure(error):
                Logger.shared.info(String(describing: error))
            }
        }
    }
}

// MARK: - Listener
extension MapViewPresenter {
    
    /// Listener for `pois` table in Firestore
    private func addPoisChangeListener() {
        poiDataManager.addUpdatePointOfInterestsListener() { [weak self] changeType, hasPendingWrites, result  in
                
        switch result {
            case let .success(poi):
                switch changeType {
                case .added:
                    /// sometimes UPDATED POI has `DocumentChangedType == ADDED`, this is additional check
                    if !hasPendingWrites && poi?.id == self?.poiIdForPossibleUpdate {
                        self?.addOrUpdatePoi(poi)
                    } else if hasPendingWrites {
                        self?.addOrUpdatePoi(poi)
                    }
                case .modified:
                    self?.addOrUpdatePoi(poi)
                case .removed:
                    self?.removePoi(with: poi)
                case .error:
                    break // skip
                }
            case let .failure(error):
                Logger.shared.error("Refresh POI listener error: \(String(describing: error))")
            }
        }
    }
    
    private func removePoisChangeListener() {
        poiDataManager.removeUpdatePointOfInterestsListener()
    }
    
    private func addOrUpdatePoi(_ poi: PointOfInterest?) {
        guard let poi = poi else { return }
        
        /// check if this is a new POI
        if allPointOfInterests.first(where: { $0.id == poi.id }) == nil {
            addNewPoi(with: poi)
        } else {
            /// update exist poi in the lists
            updatePoi(with: poi)
        }
    }
    
    /// Updating a specific POI on the map
    /// - Parameter poi: new POI data
    private func updatePoi(with poi: PointOfInterest) {
        guard let location = poi.location?.coreLocation else { return }
        if let row = currentlyDisplayedPointOfInterests.firstIndex(where: {
            if let poiLocation = $0.location?.coreLocation {
                return poiLocation.isEqual(location)
            } else {
                return false
            }
        }) {
            currentlyDisplayedPointOfInterests[row] = poi
            view?.updateMapPoiMarker(with: .init(location: location,
                                                 pinViewModel: .init(tintColor: getPinViewColorByPoi(with: poi),
                                                                     image: poi.category?.thumbnailAsset.image ?? Asset.PoiCategories.noneCategory.image)))
        }
    }
    
    /// Add new POI to map
    /// - Parameter poi: new POI data
    private func addNewPoi(with poi: PointOfInterest) {
        guard let location = poi.location?.coreLocation,
              allPointOfInterests.first(where: { $0.id == poi.id }) == nil,
              let poiId = poi.id else {
            return
        }
        currentlyDisplayedPointOfInterests.append(poi)
        allPointOfInterests.append(poi)
        view?.addMapPoiMarker(with: .init(location: location,
                                      pinViewModel: .init(tintColor: getPinViewColorByPoi(with: poi),
                                                          image: poi.category?.thumbnailAsset.image ?? Asset.PoiCategories.noneCategory.image)))
        view?.cluster()
        Logger.shared.info("Poi \(poiId) successfully added on the Map")
    }
    
    /// remove POI
    private func removePoi(with poi: PointOfInterest?) {
        guard let location = poi?.location?.coreLocation else { return }

        if let row = currentlyDisplayedPointOfInterests.firstIndex(where: {
            if let poiLocation = $0.location?.coreLocation {
                return poiLocation.isEqual(location)
            } else {
                return false
            }
        }) {
            currentlyDisplayedPointOfInterests.remove(at: row)
        }
        
        if let row = allPointOfInterests.firstIndex(where: {
            if let poiLocation = $0.location?.coreLocation {
                return poiLocation.isEqual(location)
            } else {
                return false
            }
        }) {
            allPointOfInterests.remove(at: row)
        }
        
        view?.removePinByLocation(location: location)
    }
}


// MARK: - City Polygon
extension MapViewPresenter {
    private func fillCityPolygon() {
        if let url = Bundle.main.url(forResource: "city_polygon", withExtension: "json") {
            do {
                let data = try Data(contentsOf: url)
                let decoder = JSONDecoder()
                let jsonData = try decoder.decode(Polygon.self, from: data)
                
                let path = GMSMutablePath()
                for te in jsonData.coordinates {
                    if te.indices.contains(0) && te.indices.contains(1) {
                        let longitude = te[0]
                        let latitude = te[1]
                        path.add(CLLocationCoordinate2D(latitude: latitude,
                                                        longitude: longitude))
                    }
                }
                
                let polygon = GMSPolygon(path: path)
                polygon.strokeColor = UIColor.LogThisPlace.purple
                polygon.strokeWidth = 3.0
                polygon.fillColor = .clear
                
                view?.drawCityPolygon(polygon)
            }
            catch {
                //TODO: - Handle error
                Logger.shared.error(String(describing: error))
            }
        } else {
            Logger.shared.error("File for draw polygon didnt find")
        }
    }
}

struct Polygon: Decodable {
    var coordinates: [[Double]]
}
