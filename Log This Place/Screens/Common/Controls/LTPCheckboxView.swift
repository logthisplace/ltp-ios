//
//  LTPCheckboxView.swift
//  Log This Place
//
//

import UIKit


class LTPCheckboxView: BaseView, Fillable {
    
    struct ViewModel {
        let question: String
        var answerSheet: LTPAnswerSheet.ViewModel
        // TODO: - think about pass question id
        let questionId: String
        let condition: Condition?
        
        var isAnswered: Bool {
            answerSheet.items.first(where: { $0.isChecked }) != nil
        }
    }
    
    var output: LTPAnswerSheet.Output {
        answersView.answerSheet.output!
    }
    
    var onAnswerTap: ((String?, String) -> Void)?
    
    private let titleLabel = with(UILabel.headlineMediumSemiBold) {
        $0.numberOfLines = 0
    }
    private let questionContainer = UIView()
    private let answersView = AnswersView()
    private var questionId: String?
    
    override init() {
        super.init()
        answersView.answerSheet.addTarget(self, action: #selector(answerTapped), for: .valueChanged)
    }
    
    override func setupUI() {
        super.setupUI()
        backgroundColor = .white
        addSubview(questionContainer)
        questionContainer.addSubviews([
            titleLabel,
            answersView
        ])
    }
    
    override func setupConstraints() {
        super.setupConstraints()
        questionContainer.positionToTheEdges()

        titleLabel.snp.makeConstraints { make in
            make.top.equalToSuperview().offset(16)
            make.leading.trailing.equalToSuperview().inset(16)
        }
        answersView.snp.makeConstraints { make in
            make.top.equalTo(titleLabel.snp.bottom).offset(8)
            make.leading.trailing.equalToSuperview().inset(16)
            make.bottom.equalToSuperview().inset(16)
        }
    }
    
    func selectSingleItem(withId id: String) {
        answersView.answerSheet.selectSingleItem(withId: id)
    }
    
    func deselectItem(withId id: String) {
        answersView.answerSheet.deselectItem(withId: id)
    }
    
    func deselectAll() {
        answersView.answerSheet.deselectAll()
    }
    
    @objc private func answerTapped(sender: LTPAnswerSheet) {
        guard let lastlyTappedAnswer = sender.lastlyTappedAnswer else { return }
        onAnswerTap?(questionId, lastlyTappedAnswer.output.id)
    }
}

extension LTPCheckboxView {
    func fill(with model: ViewModel) {
        questionId = model.questionId
        titleLabel.text = model.question
        answersView.answerSheet.fill(with: model.answerSheet)
    }
}

private final class AnswersView: BaseView {
    
    let answerSheet = LTPAnswerSheet()
    
    override func setupUI() {
        super.setupUI()
        addSubview(answerSheet)
    }
    
    override func setupConstraints() {
        super.setupConstraints()
        answerSheet.positionToTheEdges()
    }
}
