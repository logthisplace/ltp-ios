//
//  Log This Place
//

import Foundation

final class QuestionnaireRTPDropdownField: LTPDropdownField, AnswerProvideable {
    
    struct ViewModel {
        let questionId: String
        let dropdownFieldViewModel: LTPDropdownField.ViewModel
    }
    
    let questionId: String
    
    init(questionId: String) {
        self.questionId = questionId
        super.init(frame: .zero)
    }
    
    required init?(coder: NSCoder) {
        fatalError("Use init(questionId:)")
    }
    
    func answer() -> TypeOfAnswer {
        .dropdown(.init(questionId: questionId, answer: selectedItem))
    }
}
