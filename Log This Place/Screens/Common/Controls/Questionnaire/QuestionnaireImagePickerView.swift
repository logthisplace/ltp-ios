//
//  QuestionnaireImagePickerView.swift
//  Log This Place
//

import Foundation
import UIKit

final class QuestionnaireImagePickerView: ImagePickerView, AnswerProvideable, QuestionnaireViewProvideable {
    
    struct ViewModel {
        let questionId: String
        let imagePickerViewModel: ImagePickerView.ViewModel
        let condition: Condition?
    }
    
    let questionId: String

    init(questionId: String) {
        self.questionId = questionId
        super.init(frame: .zero)
    }
    
    required init?(coder: NSCoder) {
        fatalError("Use init(questionId:)")
    }
    
    func answer() -> TypeOfAnswer {
        .image(.init(questionId: questionId, answer: imagesWithIdentifiers))
    }
    
    func showQuestion(duration: Double) {
        showView(duration: duration)
    }
    
    func hideQuestion(duration: Double) {
        hideView(duration: duration)
        clearImages()
    }
}
