//
//  QuestionnaireViewProvideable.swift
//  Log This Place
//
//

import Foundation
import UIKit

protocol QuestionnaireViewProvideable {
    var questionId: String { get }
    func hideQuestion(duration: Double)
    func showQuestion(duration: Double)
}

extension QuestionnaireViewProvideable {
    func hideQuestion(duration: Double = 0) {
        hideQuestion(duration: duration)
    }
    
    func showQuestion(duration: Double = 0) {
        showQuestion(duration: duration)
    }
}
