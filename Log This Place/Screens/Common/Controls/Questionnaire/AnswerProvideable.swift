//
//  AnswerProvideable.swift
//  Log This Place
//

import UIKit

protocol AnswerProvideable {
    var questionId: String { get }
    
    init(questionId: String)
    
    func answer() -> TypeOfAnswer
}

/// Struct that holds questionnaire answer
struct Answer<AnswerType> {
    let questionId: String
    let answer: AnswerType
}

enum TypeOfAnswer {
    case int(Answer<Int?>)
    case string(Answer<String?>)
    case boolean(Answer<LTPAnswer.Output?>)
    case checkboxSet(Answer<[LTPAnswer.Output]>)
    case dropdown(Answer<LTPDropdownField.ItemViewModel?>)
    case image(Answer<[(image: ImageSource, id: String)]>)
}
