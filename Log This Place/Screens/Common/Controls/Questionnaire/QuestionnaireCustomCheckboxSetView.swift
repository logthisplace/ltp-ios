//
//  QuestionnaireCustomCheckboxSetView.swift
//  Log This Place
//
//

import Foundation

final class QuestionnaireCustomCheckboxSetView: RTPCheckboxView, AnswerProvideable, QuestionnaireViewProvideable {
    static let trueAnswerKey = "true"
    static let falseAnswerKey = "false"
    static let notRelevantAnswerKey = "Not Relevant"
    
    struct ViewModel {
        let questionId: String
        let checkboxSetViewViewModel: RTPCheckboxView.ViewModel
    }

    let questionId: String

    init(questionId: String) {
        self.questionId = questionId
        super.init()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func answer() -> TypeOfAnswer {
        switch output {
        case let .single(checkboxOutput):
            return .string(.init(questionId: questionId, answer: checkboxOutput))
        case let .multiple(checkboxesOutput):
            return .checkboxSet(.init(questionId: questionId, answer: checkboxesOutput))
        case let .boolean(checkboxOutput):
            if checkboxOutput == QuestionnaireCustomCheckboxSetView.notRelevantAnswerKey {
                return .boolean(.init(questionId: questionId,
                                      answer: .init(id: QuestionnaireCustomCheckboxSetView.notRelevantAnswerKey,
                                                    isChecked: true)))
            }
            
            if let value = (checkboxOutput as NSString?)?.boolValue {
                return .boolean(.init(questionId: questionId, answer: .init(id: questionId, isChecked: value)))
            }
            
            return .boolean(.init(questionId: questionId, answer: nil))
        }
    }
    
    func showQuestion(duration: Double) {
        showView(duration: duration)
    }
    
    func hideQuestion(duration: Double) {
        hideView(duration: duration)
        deselectAll()
    }
}
