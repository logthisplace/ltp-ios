//
//  Log This Place
//

import Foundation

/// Subclass used in questionnaire for retrieving answers.
final class QuestionnaireLTPFormTextField: LTPFormTextField, AnswerProvideable, QuestionnaireViewProvideable {
    
    struct ViewModel {
        let questionId: String
        let formTextFieldViewModel: LTPFormTextField.ViewModel
        let condition: Condition?
    }
    
    let questionId: String
    
    init(questionId: String) {
        self.questionId = questionId
        super.init(frame: .zero)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func showQuestion(duration: Double) {
        showView(duration: duration)
    }
    
    func hideQuestion(duration: Double) {
        hideView(duration: duration)
        clearInputField()
    }
}

extension QuestionnaireLTPFormTextField {
    func answer() -> TypeOfAnswer {
        switch type {
        case .number:
            let converted = Int(text ?? "")
            return .int(.init(questionId: questionId, answer: converted))
        case .normal:
            return .string(.init(questionId: questionId, answer: text))
        }
    }
}
