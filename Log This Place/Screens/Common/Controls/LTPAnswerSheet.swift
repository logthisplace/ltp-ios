//
//  LTPAnswerSheet.swift
//  Log This Place
//

import UIKit

final class LTPAnswerSheet: BaseControl, Fillable {
    
    struct ViewModel {
        let mode: Mode
        var items: [LTPAnswer.ViewModel]
    }
    
    enum Output {
        case single(String?)
        case boolean(String?)
        case multiple([LTPAnswer.Output])
    }
    
    enum Mode {
        case singleChoice
        case multiChoice
        case boolean
    }
    
    var lastlyTappedAnswer: LTPAnswer?
    
    private let answersStackView = UIStackView.vertical
    private var mode: Mode?
    
    private var answers = [LTPAnswer]()
    
    override func setupUI() {
        super.setupUI()
        addSubview(answersStackView)
    }
    
    override func setupConstraints() {
        super.setupConstraints()
        answersStackView.positionToTheEdges()
        answersStackView.spacing = 8
    }
    
    var output: Output? {
        if mode == .boolean {
            return .boolean(answers.first(where: { $0.isChecked })?.output.id)
        }
        if mode == .singleChoice {
            return .single(answers.first(where: { $0.isChecked })?.output.id)
        }
        if mode == .multiChoice {
            return .multiple(answers.map { $0.output })
        }
        return nil
    }
    
    func selectSingleItem(withId id: String) {
        answers.forEach({ $0.isChecked = $0.id == id })
    }
    
    func deselectItem(withId id: String) {
        answers.first(where: { $0.id == id })?.isChecked = false
    }
    
    func deselectAll() {
        answers.forEach{
            $0.isChecked = false
        }
    }

    @objc private func rowTapped(sender: LTPAnswer) {
        switch mode {
        case .singleChoice, .boolean:
            let allExceptTapped = answersStackView.arrangedSubviews.filter {
                $0 != sender
            }
            
            allExceptTapped.forEach {
                ($0 as? LTPAnswer)?.isChecked = false
            }
        case .multiChoice:
            break
        case .none:
            break
        }
        lastlyTappedAnswer = sender
        sendActions(for: .valueChanged)
    }
}

extension LTPAnswerSheet {
    func fill(with model: ViewModel) {
        mode = model.mode
        for item in model.items {
            let answer = LTPAnswer()
            answer.fill(with: .init(id: item.id,
                                    text: item.text,
                                    isChecked: item.isChecked,
                                    checkboxType: mode == .multiChoice ? .square : .circle))
            answersStackView.addArrangedSubview(answer)
            answers.append(answer)
            answer.addTarget(self, action: #selector(rowTapped), for: .valueChanged)
            //if !model.isSingleChoice {
                answer.allowDeselection = true
            //}
        }
    }
}
