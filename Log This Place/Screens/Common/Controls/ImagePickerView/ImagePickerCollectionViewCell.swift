//
//  ImagePickerCollectionViewCell.swift
//  Log This Place
//

import UIKit

final class ImagePickerCollectionViewCell: UICollectionViewCell, NibProvideable {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var removeButton: UIButton!
    
    var removeButtonHandler: (() -> Void)?
    var isRemovingPossible: Bool = true {
        didSet {
            removeButton.isHidden = !isRemovingPossible
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        removeButton.tintColor = .red
        removeButton.addTarget(self, action: #selector(removeButtonTapped), for: .touchUpInside)
    }
    
    @objc private func removeButtonTapped() {
        removeButtonHandler?()
    }
}
