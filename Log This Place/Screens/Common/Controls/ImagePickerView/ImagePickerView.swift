//
//  ImagePickerView.swift
//  Log This Place
//

import UIKit
import Kingfisher

enum ImageSource {
    case link(String)
    case media(UIImage)
}

class ImagePickerView: BaseControl, Fillable {
    
    var onImageTapped: (([ImageSource], Int) -> Void)?
    
    struct ViewModel {
        enum Mode {
            case interactive
            case viewOnly
            
            var hasPossibilityOfRemovingImages: Bool {
                switch self {
                case .interactive:
                    return true
                case .viewOnly:
                    return false
                }
            }
        }
        let title: String?
        let buttonTitle: String?
        let mode: Mode
        let images: [(image: ImageSource, id: String)]?
        
        init(title: String?, buttonTitle: String? = nil, images: [(image: ImageSource, id: String)]? = nil) {
            self.title = title
            self.buttonTitle = buttonTitle
            self.images = images
            self.mode = .interactive
        }
        
        init(title: String?, images: [ImageSource]) {
            self.title = title
            self.buttonTitle = nil
            self.images = images.map { ($0, UUID().uuidString) }
            self.mode = .viewOnly
        }
    }
    
    var title: String? {
        didSet {
            updateTitle()
        }
    }
    
    var buttonTitle: String = L10n.Common.AddPicture.text {
        didSet {
            button.setTitle(buttonTitle, for: .normal)
        }
    }
    
    var imagePickerManager: ImagePickerManagerProtocol?
    
    private(set) var imagesWithIdentifiers = [(image: ImageSource, id: String)]() {
        didSet {
            sendActions(for: .valueChanged)
        }
    }
    
    private var model: ViewModel?
        
    private let titleLabel = with(UILabel.headlineMediumSemiBold) {
        $0.numberOfLines = 0
    }
    
    private let stackView = with(UIStackView.vertical) {
        $0.edgeInsets(topSpacing: 16, bottomSpacing: 16, leadingSpacing: 16, trailingSpacing: 16)
    }
    private lazy var button = with(LTPButton.standard(style: .black)) {
        $0.setTitle(buttonTitle, for: .normal)
        $0.setImage(Asset.cameraFill.image, for: .normal)
        $0.imageView?.contentMode = .scaleAspectFit
        $0.contentVerticalAlignment = .center
        $0.tintColor = UIColor.white
        $0.contentHorizontalAlignment = .center
        $0.contentEdgeInsets = .init(top: 0, left: 3, bottom: 0, right: 3)
        $0.titleEdgeInsets = . init(top: 0, left: 0, bottom: 0, right: -3)
        $0.imageEdgeInsets = .init(top: 0, left: -3, bottom: 0, right: 3)
    }
    
    private let singlePhotoSize: CGFloat = 140
    
    private var flowLayout: UICollectionViewFlowLayout {
        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.itemSize = CGSize(width: singlePhotoSize, height: singlePhotoSize)
        flowLayout.sectionInset = .zero
        flowLayout.scrollDirection = .horizontal
        return flowLayout
    }
    
    private lazy var imagesCollectionView = with(UICollectionView(frame: .zero,
                                                                  collectionViewLayout: flowLayout)) {
        $0.collectionViewLayout = flowLayout
        $0.showsHorizontalScrollIndicator = false
        $0.backgroundColor = .white
    }
    
    private var separatorUnderImagesAdded = false

    override func setup() {
        super.setup()
        backgroundColor = .white

        button.addTarget(self, action: #selector(pickImageTapped), for: .touchUpInside)
        imagesCollectionView.dataSource = self
        imagesCollectionView.delegate = self
        imagesCollectionView.register(item: ImagePickerCollectionViewCell.self)
        updateTitle()
    }
    
    override func setupUI() {
        super.setupUI()
        addSubview(stackView)
        stackView.addArrangedSubviews([
            titleLabel,
            button
        ])
    }
    
    override func setupConstraints() {
        super.setupConstraints()
        stackView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        stackView.spacing = 8

        imagesCollectionView.snp.makeConstraints { make in
            make.height.equalTo(singlePhotoSize)
        }
        
        button.snp.makeConstraints { make in
            make.height.equalTo(50)
        }
        
    }
    
    public func setupRootStackViewArea(top: CGFloat = 16,
                                       left: CGFloat = 16,
                                       bottom: CGFloat = 16,
                                       right: CGFloat = 16) {
        stackView.layoutMargins = UIEdgeInsets(top: top, left: left, bottom: bottom, right: right)
        stackView.isLayoutMarginsRelativeArrangement = true
    }
    
    private func updateTitle() {
        titleLabel.text = title
        titleLabel.isHidden = title == nil
    }
    
    @objc private func pickImageTapped() {
        imagePickerManager?.pickImage { [weak self] image, imageName in
            guard let `self` = self else { return }
            self.add(image: .media(image), imageName: imageName)
        }
    }
    
    func clearImages() {
        imagesWithIdentifiers.removeAll()
        imagesCollectionView.reloadData()
        if imagesWithIdentifiers.isEmpty {
            stackView.removeArrangedSubview(imagesCollectionView)
        }
        separatorUnderImagesAdded = false
    }
    
    
    private func add(image: ImageSource, imageName: String) {
        if !imagesWithIdentifiers.contains(where: { $0.id == imageName }) {
            imagesWithIdentifiers.append((image, imageName))
            imagesCollectionView.reloadData()
            if !separatorUnderImagesAdded {
                separatorUnderImagesAdded = true
                stackView.addArrangedSubviews([
                    imagesCollectionView
                ])
            }
        }
    }
    
    private func remove(at index: Int) {
        imagesWithIdentifiers.remove(at: index)
        imagesCollectionView.reloadData()
        if imagesWithIdentifiers.isEmpty {
            stackView.removeArrangedSubview(imagesCollectionView)
        }
        separatorUnderImagesAdded = false
    }
    
    func fill(with model: ViewModel) {
        self.model = model
        title = model.title
        
        switch model.mode {
        case .interactive:
            button.isHidden = false
        case .viewOnly:
            button.isHidden = true
            // titleLabel.font = UIFont.systemFont(ofSize: 17)
        }
        if let buttonTitle = model.buttonTitle {
            self.buttonTitle = buttonTitle
        }
        
        if let images = model.images {
            for imageObject in images {
                add(image: imageObject.image, imageName: imageObject.id)
            }
        }
    }
}

extension ImagePickerView: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: ImagePickerCollectionViewCell = collectionView.deque(for: indexPath)
        cell.isRemovingPossible = model?.mode.hasPossibilityOfRemovingImages ?? false
        
        switch imagesWithIdentifiers[indexPath.row].image {
        case let .link(link):
            let url = URL(string: link)
            cell.imageView.kf.setImage(with: url,
                                       placeholder: Asset.placeholderGray.image)
        case let .media(media):
            cell.imageView.image = media
        }
        
        cell.removeButtonHandler = { [weak self] in self?.remove(at: indexPath.row) }
        return cell
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return imagesWithIdentifiers.count
    }
}

extension ImagePickerView: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        onImageTapped?(imagesWithIdentifiers.compactMap({ $0.0 }), indexPath.row)
    }
}
