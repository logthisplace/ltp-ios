//
//  LTPTextField.swift
//  Log This Place
//

import UIKit

final class LTPTextField: UITextField {
    private struct Constants {
        static let maxNumberLength: Int = 6
        static let maxStringLength: Int = 64
    }
    
    var sanitizedText: String { text.sanitized }
    
    var nilableSanitizedText: String? {
        if let text = text, !text.isEmpty {
            return sanitizedText
        } else {
            return nil
        }
    }
    
    enum FieldType {
        case normal
        case number
    }
    
    var type: FieldType = .normal {
        didSet {
            updateType()
        }
    }
    
    var isPasteActionEnabled = true
    
    override var isEnabled: Bool {
        didSet {
            updateIsEnabled()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setup()
    }
    
    deinit {
        delegate = nil
    }
    
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        bounds.insetBy(dx: 10, dy: 10)
    }
    
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        bounds.insetBy(dx: 10, dy: 10)
    }
    
    override func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
        if !isPasteActionEnabled, action == #selector(UIResponderStandardEditActions.paste(_:)) {
            return false
        }
        return super.canPerformAction(action, withSender: sender)
    }
    
    private func updateType() {
        switch type {
        case .normal:
            keyboardType = .default
        case .number:
            keyboardType = .numberPad
        }
    }
    
    private func updateIsEnabled() {
        textColor = isEnabled ? .black : UIColor.LogThisPlace.gray
    }
    
    private func setup() {
        delegate = self
        font = FontFamily.ProximaNova.regular.font(size: 16)
        
        layer.cornerRadius = 10
        layer.borderWidth = 1
        layer.masksToBounds = true
        layer.borderColor = UIColor.LogThisPlace.darkGray.withAlphaComponent(0.18).cgColor
    }
}

extension LTPTextField: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let currentString: NSString = textField.text! as NSString
        let newString: NSString = currentString.replacingCharacters(in: range, with: string) as NSString

        switch type {
        case .normal:
            return newString.length <= Constants.maxStringLength
        case .number:
            if newString.length >= Constants.maxNumberLength {
                return false
            }
            
            let invalidCharacters = CharacterSet(charactersIn: "0123456789").inverted
            return string.rangeOfCharacter(from: invalidCharacters) == nil
        }
    }
}

