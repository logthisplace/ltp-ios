//
//  LTPDropdownField.swift
//  Log This Place
//

import UIKit

class LTPDropdownField: BaseControl, Fillable {
    
    struct ViewModel {
        let title: String?
        let subtitle: String?
        let items: [ItemViewModel]
        let selectedItemId: String?
    }
    
    struct ItemViewModel {
        let id: String
        let value: String
    }
    
    var title: String? {
        didSet {
            updateTitle()
        }
    }
    
    var subtitle: String? {
        didSet {
            updateSubtitle()
        }
    }
    
    var items = [ItemViewModel]() {
        didSet {
            underlyingItems = [.init(id: "", value: "-")] + items
        }
    }
    
    var selectedItem: ItemViewModel? {
        get {
            let isSelectedAndDifferentThanEmpty = pickerView.selectedRow(inComponent: 0) > indexOfEmptyEntry
            if isSelectedAndDifferentThanEmpty {
                return underlyingItems[pickerView.selectedRow(inComponent: 0)]
            }
            return nil
        }
    }
    
    
    private var underlyingItems = [ItemViewModel]() {
        didSet {
            pickerView.reloadAllComponents()
        }
    }
    
    private let indexOfEmptyEntry = 0
    
    private let titleLabel = UILabel.footnoteRegular
    private let subtitleLabel = with(UILabel.footnoteRegular) {
        $0.textColor = UIColor.LogThisPlace.gray.withAlphaComponent(0.6)
    }
    private let fakeTextField = with(LTPTextField()) {
        $0.isUserInteractionEnabled = false
    }
    private let invisibleTextField = LTPTextField()
    
    private let chevronImageView = with(UIImageView()) {
        $0.contentMode = .scaleAspectFit
        $0.image = Asset.chevronDown.image
        $0.tintColor = UIColor.LogThisPlace.darkGray.withAlphaComponent(0.6)
    }
    
    private let pickerView = UIPickerView()
    private let stackView = UIStackView.vertical
    private let activateButton = UIButton()
    
    override func setup() {
        super.setup()
        
        updateTitle()
        updateSubtitle()
        
        fakeTextField.placeholder = L10n.Picker.placeholder
        
        invisibleTextField.inputView = pickerView
        invisibleTextField.addDoneAccessoryInputView()
        
        pickerView.dataSource = self
        pickerView.delegate = self
        
        activateButton.addTarget(self, action: #selector(activateButtonTapped), for: .touchUpInside)
        pickerView.backgroundColor = UIColor.LogThisPlace.systemLikeGray
    }
    
    override func setupUI() {
        super.setupUI()
        
        addSubview(stackView)
        addSubview(activateButton)
        addSubview(invisibleTextField)
        stackView.addArrangedSubviews([
            titleLabel,
            subtitleLabel,
            fakeTextField
        ])
        fakeTextField.addSubview(chevronImageView)
    }
    
    override func setupConstraints() {
        super.setupConstraints()
        stackView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        
        activateButton.snp.makeConstraints { make in
            make.edges.equalTo(fakeTextField)
        }
        
        chevronImageView.snp.makeConstraints { make in
            make.size.equalTo(20)
            make.centerY.equalToSuperview()
            make.trailing.equalToSuperview().inset(12)
        }
        
        stackView.spacing = 10
        stackView.setCustomSpacing(6, after: subtitleLabel)
    }
    
    private func updateTitle() {
        titleLabel.text = title?.uppercased()
        titleLabel.isHidden = title == nil
    }
    
    private func updateSubtitle() {
        subtitleLabel.text = subtitle
        subtitleLabel.isHidden = subtitle == nil
    }
    
    @objc private func activateButtonTapped() {
        invisibleTextField.becomeFirstResponder()
    }
    
    func select(itemWithId: String) {
        if let index = underlyingItems.firstIndex(where: { $0.id == itemWithId }) {
            pickerView.selectRow(index, inComponent: 0, animated: true)
            fakeTextField.text = underlyingItems[index].value
        }
    }
    
    func fill(with model: ViewModel) {
        title = model.title
        subtitle = model.subtitle
        items = model.items
        if let selectedItemId = model.selectedItemId {
            select(itemWithId: selectedItemId)
        }
    }
}

extension LTPDropdownField: UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        underlyingItems.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        underlyingItems[row].value
    }
}

extension LTPDropdownField: UIPickerViewDelegate {
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if row == indexOfEmptyEntry {
            fakeTextField.text = nil
        } else {
            fakeTextField.text = underlyingItems[row].value
        }
        sendActions(for: .valueChanged)
    }
}
