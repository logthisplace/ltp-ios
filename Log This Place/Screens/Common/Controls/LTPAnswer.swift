//
//  LTPAnswer.swift
//  Log This Place
//

import UIKit

final class LTPAnswer: BaseControl, Fillable {
    
    struct ViewModel {
        let id: String
        let text: String
        let isChecked: Bool
        let checkboxType: CheckboxType
        
        init(id: String, text: String, isChecked: Bool, checkboxType: CheckboxType = .square) {
            self.id = id
            self.text = text
            self.isChecked = isChecked
            self.checkboxType = checkboxType
        }
    }
    
    struct Output {
        let id: String
        let isChecked: Bool
    }
    
    enum CheckboxType {
        case square
        case circle
    }
    
    var checkedImage: UIImage = Asset.circleChecked.image
    var uncheckedImage: UIImage = Asset.circleEmpty.image
    var checkboxType: CheckboxType = .circle {
        didSet {
            switch checkboxType {
            case .circle:
                checkedImage = Asset.circleChecked.image
                uncheckedImage = Asset.circleEmpty.image
            case .square:
                checkedImage = Asset.squareChecked.image
                uncheckedImage = Asset.squareGray.image
            }
        }
    }
    
    var isChecked: Bool = false {
        didSet {
            checkbox.image = isChecked ? checkedImage : uncheckedImage
            backgroundColor = isChecked ? UIColor.LogThisPlace.systemLikeGray : .white
        }
    }
    
    var output: Output {
        guard let id = id else { fatalError("No id found. Call fill before extracting output") }
        return .init(id: id, isChecked: isChecked)
    }
    
    var allowDeselection = false
    
    private let textLabel = with(UILabel.calloutRegular) {
        $0.numberOfLines = 0
    }
    private let checkbox = UIImageView.scaleAspectFit(with: Asset.circleEmpty.image)
    private lazy var tapGR = UITapGestureRecognizer(target: self, action: #selector(didTapAnswer))
    
    var id: String?
    
    init() {
        super.init(frame: .zero)
        
        layer.cornerRadius = 10
        layer.borderWidth = 2
        layer.borderColor = UIColor.LogThisPlace.systemLikeGray.cgColor
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func setupUI() {
        super.setupUI()
        addSubviews([
            textLabel,
            checkbox
        ])
        
        addGestureRecognizer(tapGR)
    }
    
    override func setupConstraints() {
        super.setupConstraints()
        textLabel.snp.makeConstraints { make in
            make.leading.equalToSuperview().inset(12)
            make.top.bottom.equalToSuperview().inset(12)
        }
        
        checkbox.snp.makeConstraints { make in
            make.size.equalTo(20)
            make.leading.equalTo(textLabel.snp.trailing).offset(12)
            make.trailing.equalToSuperview().inset(12)
            make.centerY.equalToSuperview()
        }
    }
    
    @objc private func didTapAnswer() {
        if !isChecked || allowDeselection {
            isChecked.toggle()
            sendActions(for: .valueChanged)
        }
    }
}
 
extension LTPAnswer {
    func fill(with model: ViewModel) {
        id = model.id
        textLabel.text = model.text
        checkboxType = model.checkboxType
        isChecked = model.isChecked
    }
}
