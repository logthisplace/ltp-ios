//
//  Log This Place
//

import UIKit

/// Class responsible for displaying multiple checkboxes.
class CheckboxSetView: BaseControl, Fillable {
    
    /// Mode for checkboxes set view.
    enum Mode {
        /// May contains multiple checkboxes groupped together.
        case multiple([Checkbox.ViewModel])
        
        /// Contains only one checkbox.
        case single(Checkbox.ViewModel)
    }
    
    /// Similar to mode - it differentiates between set view with multiple and single checkboxes.
    enum Output {
        case multiple([Checkbox.Output])
        case single(Checkbox.Output)
    }
    
    struct ViewModel {
        let title: String?
        let isSingleChoice: Bool
        let mode: Mode
        let selectedItemIds: [String]
    }
    
    var title: String? {
        didSet {
            titleLabel.isHidden = title == nil
            titleLabel.text = title?.uppercased()
        }
    }
    
    var output: Output {
        switch mode {
        case .multiple:
            return .multiple(checkboxes.map{ $0.output })
        case .single:
            return .single(checkboxes[0].output)
        case .none:
            fatalError("Should call fill(model:) before calling output")
        }
    }
    
    /// Flag indicating wether there can be only one selected option or multiple.
    var isSingleChoice = false
    
    private(set) var mode: Mode!
    
    private let titleLabel = UILabel.footnoteRegular
    private let rootStackView = UIStackView.vertical
    private let checkboxStackView = UIStackView.vertical
    
    private var checkboxes = [Checkbox]()
    
    override func setupUI() {
        super.setupUI()
        addSubview(rootStackView)
        
        rootStackView.addArrangedSubviews([
            titleLabel,
            checkboxStackView
        ])
    }
    
    override func setupConstraints() {
        super.setupConstraints()
        rootStackView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        
        rootStackView.spacing = 14
        checkboxStackView.spacing = 12
    }
    
    /// Selects given items. When mode is .single, or `isSingleChoice` is true, only first id is taken.
    /// If id is not present - nothing is going to be selected.
    /// - Parameter items: Item ids to be selected
    func select(items ids: [String]) {
        switch mode {
        case .single:
            if ids.first == checkboxes.first?.id {
                checkboxes.first?.isChecked = true
            }
        case .multiple:
            if isSingleChoice {
                if ids.first == checkboxes.first?.id {
                    checkboxes.first?.isChecked = true
                }
            } else {
                for id in ids {
                    let checkbox = checkboxes.first(where: { $0.id == id })
                    checkbox?.isChecked = true
                }
            }
        case .none:
            break
        }
    }
    
    func fill(with model: ViewModel) {
        title = model.title
        isSingleChoice = model.isSingleChoice
        mode = model.mode
        
        var checkboxes: [Checkbox.ViewModel]
        switch mode! {
        case let .single(model):
            checkboxes = [model]
        case let .multiple(model):
            checkboxes = model
        }
        checkboxes.forEach {
            let checkbox = Checkbox(id: $0.id)
            checkbox.fill(with: $0)
            checkboxStackView.addArrangedSubview(checkbox)
            self.checkboxes.append(checkbox)
            checkbox.addTarget(self, action: #selector(tappedCheckbox), for: .valueChanged)
        }
        
        select(items: model.selectedItemIds)
    }
    
    @objc private func tappedCheckbox(sender: Checkbox) {
        if isSingleChoice {
            let allExceptTapped = checkboxStackView.arrangedSubviews.filter {
                $0 != sender
            }
            
            allExceptTapped.forEach {
                ($0 as? Checkbox)?.isChecked = false
            }
        }
        sendActions(for: .valueChanged)
    }
}
