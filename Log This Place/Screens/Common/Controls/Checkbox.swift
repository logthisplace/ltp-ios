//
//  Log This Place
//

import UIKit

final class Checkbox: BaseControl, Fillable {
    
    struct ViewModel {
        let id: String
        let title: String
        let subtitle: String?
        let isChecked: Bool
        
        init(id: String, title: String, subtitle: String? = nil, isChecked: Bool = false) {
            self.id = id
            self.title = title
            self.subtitle = subtitle
            self.isChecked = isChecked
        }
    }
    
    struct Output {
        let id: String
        let isChecked: Bool
    }
    
    var title: String? {
        didSet {
            titleLabel.text = title
        }
    }
    
    var subtitle: String? {
        didSet {
            subtitleLabel.text = subtitle
        }
    }
    
    var isChecked: Bool = false {
        didSet {
           updateCheckmark()
        }
    }
    
    var output: Output {
        get {
            .init(id: id, isChecked: isChecked)
        }
    }
    
    var id: String
    
    private let checkboxImageView = with(UIImageView()) {
        $0.contentMode = .scaleAspectFit
        $0.tintColor = UIColor.LogThisPlace.darkGray.withAlphaComponent(0.3)
    }
    
    private let titleLabel = with(UILabel.calloutRegular) {
        $0.numberOfLines = 0
        $0.setContentHuggingPriority(.required, for: .vertical)
    }
    
    private let subtitleLabel = with(UILabel.footnoteRegular) {
        $0.numberOfLines = 0
        $0.textColor = UIColor.LogThisPlace.gray.withAlphaComponent(0.6)
    }
    
    private let labelStackView = UIStackView.vertical
    private let tapGR = UITapGestureRecognizer()
    
    init(id: String) {
        self.id = id
        super.init(frame: .zero)
    }
    
    required init?(coder: NSCoder) {
        fatalError("Use init(id:)")
    }
    
    override func setup() {
        super.setup()
        
        addGestureRecognizer(tapGR)
        tapGR.addTarget(self, action: #selector(controlTapped))
    }
    
    override func setupUI() {
        super.setupUI()
        addSubview(checkboxImageView)
        addSubview(labelStackView)
        
        labelStackView.addArrangedSubviews([
            titleLabel,
            subtitleLabel
        ])
        
        updateCheckmark()
    }
    
    override func setupConstraints() {
        super.setupConstraints()
        checkboxImageView.snp.makeConstraints { make in
            make.width.equalTo(16)
            make.height.equalTo(22)
            make.leading.top.equalToSuperview()
            make.bottom.lessThanOrEqualToSuperview()
        }
        
        labelStackView.snp.makeConstraints { make in
            make.top.equalToSuperview()
            make.leading.equalTo(checkboxImageView.snp.trailing).offset(8)
            make.trailing.bottom.equalToSuperview()
        }
    }
    
    private func updateCheckmark() {
        checkboxImageView.image = isChecked
            ? Asset.checkSquareFill.image
            : Asset.square.image
    }
    
    @objc private func controlTapped() {
        isChecked.toggle()
        sendActions(for: .valueChanged)
    }
    
    func fill(with model: ViewModel) {
        id = model.id
        title = model.title
        subtitle = model.subtitle
        isChecked = model.isChecked
    }
}
