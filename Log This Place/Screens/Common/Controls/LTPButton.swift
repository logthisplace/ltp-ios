//
//  LTPButton.swift
//  Rock This Place
//

import UIKit

final class LTPButton: UIButton {
    
    @IBInspectable var style: String? {
        didSet {
            guard let style = style,
                  let buttonStyle = ButtonStyle(rawValue: style) else { return }
            self.buttonStyle = buttonStyle
        }
    }
    
    enum ButtonStyle: String {
        case black
        case gray
        case clear
    }
    
    private var buttonStyle: ButtonStyle = .black {
        didSet {
            customize()
        }
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        customize()
    }
    
    private func customize() {
        UIButton.customize(button: self, for: buttonStyle)
    }
}

extension UIButton {
    static func standard(style: LTPButton.ButtonStyle) -> UIButton {
        let button = UIButton(type: .system)
        customize(button: button, for: style)
        return button
    }
    
    static func customize(button: UIButton, for style: LTPButton.ButtonStyle) {

        switch style {
        case.black:
            button.contentEdgeInsets = . init(top: 14, left: 24, bottom: 14, right: 24)
            button.layer.cornerRadius = 14
            button.titleLabel?.font = FontFamily.ProximaNova.semibold.font(size: 17)
            button.backgroundColor = .black
            button.setTitleColor(.white, for: .normal)
        case .gray:
            button.contentEdgeInsets = . init(top: 14, left: 24, bottom: 14, right: 24)
            button.layer.cornerRadius = 14
            button.titleLabel?.font = FontFamily.ProximaNova.semibold.font(size: 17)
            button.backgroundColor = UIColor.LogThisPlace.gray.withAlphaComponent(0.08)
            button.setTitleColor(UIColor.LogThisPlace.almostBlack, for: .normal)
        case .clear:
            button.titleLabel?.font = FontFamily.ProximaNova.regular.font(size: 16)
            button.backgroundColor = .clear
            button.setTitleColor(UIColor.LogThisPlace.darkGray, for: .normal)
        }
    }
}

extension UIButton {
    // workaround for setting background color.
    // we can't do it in RTPButton subclass, because it uses init(coder) and that means
    // we can't use system-provided init with type (and we want to use it for button to behave as expected e.g. when pressed)
    func set(enabled: Bool) {
        isEnabled = enabled
        alpha = enabled ? 1.0 : 0.5
    }
}
