//
//  LTPFormTextField.swift
//  Log This Place
//

import SnapKit

/// Wrapper around RTPTextField with title and possibility to change width of the field depending on the type.
class LTPFormTextField: BaseControl, Fillable {
    
    struct ViewModel {
        let title: String?
        let placeholder: String?
        let type: LTPTextField.FieldType
        let value: String?
        
        init(title: String?,
             placeholder: String? = nil,
             type: LTPTextField.FieldType = .normal,
             value: String? = nil) {
            self.title = title
            self.type = type
            self.value = value
            if placeholder == nil {
                switch type {
                case .normal:
                    self.placeholder = title
                case .number:
                    self.placeholder = "0"
                }
            } else {
                self.placeholder = placeholder
            }
        }
    }
    
    var title: String? {
        didSet {
            updateTitle()
        }
    }
    
    var placeholder: String? {
        didSet {
            textField.placeholder = placeholder
        }
    }
    
    var text: String? {
        get { textField.nilableSanitizedText }
        set { textField.text = newValue }
    }
    
    var type: LTPTextField.FieldType {
        get { textField.type }
        set { textField.type = newValue }
    }
    
    override var isEnabled: Bool {
        willSet {
            textField.isEnabled = newValue
        }
    }
        
    private let textField = LTPTextField()
    private lazy var wrappedTextField = WrappingView(contentView: textField, alignment: .horizontal(.leading))
    private let titleLabel = with(UILabel.headlineMediumSemiBold) {
        $0.numberOfLines = 0
    }
    private let stackView = with(UIStackView.vertical) {
        $0.edgeInsets(topSpacing: 16, bottomSpacing: 16, leadingSpacing: 16, trailingSpacing: 16)
        $0.spacing = 8
    }
    
    private var trailingTextFieldConstraint: Constraint?
    private var widthTextFieldConstraint: Constraint?
    private let separatorView = UIView.horizontalSeparator()

    override func addTarget(_ target: Any?, action: Selector, for controlEvents: UIControl.Event) {
        textField.addTarget(target, action: action, for: controlEvents)
    }
    
    override func setup() {
        super.setup()
        updateTitle()
        update(type: .normal)
        
        textField.isPasteActionEnabled = false
        textField.addDoneAccessoryInputView()
    }
    
    override func setupUI() {
        super.setupUI()
        backgroundColor = .white

        addSubview(stackView)
        stackView.addArrangedSubviews([
            titleLabel,
            wrappedTextField
        ])
    }
    
    override func setupConstraints() {
        super.setupConstraints()
        stackView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
    }
    
    private func updateTitle() {
        titleLabel.text = title
        titleLabel.isHidden = title == nil
    }
    
    private func update(type: LTPTextField.FieldType) {
        switch type {
        case .normal:
            widthTextFieldConstraint?.isActive = false
            textField.snp.makeConstraints { make in
                trailingTextFieldConstraint = make.trailing.equalToSuperview().constraint
            }
        case .number:
            trailingTextFieldConstraint?.isActive = false
            textField.snp.makeConstraints { make in
                widthTextFieldConstraint = make.width.equalTo(118).constraint
            }
        }
    }
    
    func fill(with model: ViewModel) {
        textField.type = model.type
        update(type: model.type)
        title = model.title
        placeholder = model.placeholder
        text = model.value
    }
    
    func clearInputField() { text = "" }
}
