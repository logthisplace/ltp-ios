//
//  ToastView.swift
//  Log This Place
//
//

import UIKit

/// Grayed-out view with activity indicator put inside of white
final class ToastView: BaseView, Fillable {
    
    struct ViewModel {
        let message: String
    }
    
    private let substrateView = with(UIView()) {
        $0.backgroundColor = .white
        $0.layer.cornerRadius = 15
    }
    private let textLabel = with(UILabel.calloutRegular) {
        $0.numberOfLines = 0
        $0.textAlignment = .center
    }

    override init() {
        super.init()
    }

    override func setupUI() {
        super.setupUI()
        substrateView.addSubview(textLabel)
        addSubview(substrateView)
    }

    override func setupConstraints() {
        super.setupConstraints()
        substrateView.snp.makeConstraints { make in
            make.bottom.equalToSuperview().inset(100)
            make.centerX.equalToSuperview()
        }
        textLabel.snp.makeConstraints { make in
            make.edges.equalToSuperview().inset(10)
            make.center.equalToSuperview()
        }
    }
}

extension ToastView {
    
    func fill(with model: ViewModel) {
        textLabel.text = model.message
    }
}
