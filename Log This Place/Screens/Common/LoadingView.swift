//
//  LoadingView.swift
//  Log This Place
//

import UIKit

/// Grayed-out view with activity indicator put inside of white, centered rectangle.
final class LoadingView: BaseView {

    private let activityIndicatorBackground = with(UIView()) {
        $0.backgroundColor = .white
        $0.layer.cornerRadius = 20
    }
    
    // TODO: - move to constants, etc
    private var gifIndicator = UIImage.gifImageWithName("IndicatorLogThisPlace")
    private lazy var containerGifIndicator = UIImageView(image: gifIndicator)

    override init() {
        super.init()
        backgroundColor = UIColor.black.withAlphaComponent(0.3)
    }

    override func setupUI() {
        super.setupUI()
        addSubview(activityIndicatorBackground)
        activityIndicatorBackground.addSubview(containerGifIndicator)
    }

    override func setupConstraints() {
        super.setupConstraints()
        activityIndicatorBackground.snp.makeConstraints { make in
            make.size.equalTo(90)
            make.center.equalToSuperview()
        }
        
        containerGifIndicator.snp.makeConstraints { make in
            make.size.equalTo(70)
            make.center.equalToSuperview()
        }
        
    }
}
