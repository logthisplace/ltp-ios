//
//  PhotoPreviewViewController.swift
//  Log This Place
//

import UIKit

final class PhotoPreviewViewController: BaseViewController {
    
    let imageView = with(UIImageView()) {
        $0.contentMode = .scaleAspectFit
    }
    
    override func setupUI() {
        super.setupUI()
        view.addSubview(imageView)
    }
    
    override func setupConstraints() {
        super.setupConstraints()
        imageView.positionToTheEdges()
    }
}
