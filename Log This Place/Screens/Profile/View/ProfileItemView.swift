//
//  Log This Place
//

import UIKit

final class ProfileItemView: BaseView, Fillable {

    struct ViewModel {
        let groupId: Int
        let title: String
        let subtitle: String
        let isChecked: Bool
    }
    
    var onTap: ((Int) -> Void)?
    
    private let titleLabel = with(UILabel.calloutRegular) {
        $0.numberOfLines = 0
    }
    private let subtitleLabel = with(UILabel.calloutRegular) {
        $0.textColor = UIColor.LogThisPlace.grayAlmostDark
        $0.numberOfLines = 0
    }
    private let pencilImageView = with(UIImageView()) {
        $0.image = Asset.pencil.image
    }
    
    private let textStackView = UIStackView.vertical
    private let rootStackView = with(UIStackView()) {
        $0.background(color: .white)
        $0.border()
        $0.distribution = .fillProportionally
        $0.spacing = 2
        $0.edgeInsets(topSpacing: 20, bottomSpacing: 20, leadingSpacing: 16, trailingSpacing: 16)
    }
    
    private var groupId: Int!
    
    override func setupUI() {
        super.setupUI()
        addSubview(rootStackView)
        
        rootStackView.addArrangedSubviews([
            textStackView,
            pencilImageView.wrappedCenteredVertically()
        ])
        
        textStackView.addArrangedSubviews([
            titleLabel
        ])
        
        let tapGR = UITapGestureRecognizer(target: self, action: #selector(rowTapped))
        addGestureRecognizer(tapGR)
    }
    
    override func setupConstraints() {
        super.setupConstraints()
        rootStackView.positionToTheEdges()
        
        pencilImageView.snp.makeConstraints { make in
            make.size.equalTo(27)
        }
    }
    
    @objc func rowTapped() {
        onTap?(groupId)
    }
}

extension ProfileItemView {
    func fill(with model: ViewModel) {
        self.groupId = model.groupId
        titleLabel.text = model.title
        if !model.subtitle.isEmpty {
            subtitleLabel.text = model.subtitle
            textStackView.addArrangedSubview(subtitleLabel)
        }
    }
}
