//
//  ProfileView.swift
//  Log This Place
//
//

import UIKit


final class ProfileView: BaseScrollView, Fillable {
    
    struct ViewModel {
        let groups: [Group]
        
        struct Group {
            let name: String
            let items: [ProfileItemView.ViewModel]
        }
    }
    
    var onGroupTapped: ((Int) -> Void)?
    
    let logoutButton = with(LTPButton.standard(style: .gray)) {
        $0.setTitle(L10n.Common.Logout.text, for: .normal)
        $0.isHidden = true
    }
    let registerButton = with(LTPButton.standard(style: .black)) {
        $0.setTitle(L10n.UserIdentification.Registration.text, for: .normal)
        $0.isHidden = true
    }
    let deleteAccountButton = with(LTPButton.standard(style: .gray)) {
        $0.setTitle(L10n.Profile.Button.Delete.Account.text, for: .normal)
    }
    
    private let logoImageContainerView = UIView()
    private let logoImage = with(UIImageView()) {
        $0.image = Asset.Summary.summarySmile.image
    }
    
    private let titleLabel = with(UILabel.titleH2Bold) {
        $0.text = L10n.Questionnaire.ThanksForAnswering.text
        $0.textAlignment = .center
        $0.numberOfLines = 0
    }
    
    private let subtitleLabel = with(UILabel.calloutRegular) {
        $0.text = L10n.Questionnaire.Recap.text
        $0.textAlignment = .center
        $0.numberOfLines = 0
        $0.textColor = UIColor.LogThisPlace.grayAlmostDark
    }
    
    private let groupNameLabel = with(UILabel.calloutRegular) {
        $0.numberOfLines = 0
    }
    
    private let answersStackView = with(UIStackView.vertical) {
        $0.spacing = 8
        $0.edgeInsets(topSpacing: 16, bottomSpacing: 16, leadingSpacing: 0, trailingSpacing: 0)
    }
    private let rootStackView = with(UIStackView.vertical) {
        $0.spacing = 12
        $0.edgeInsets(topSpacing: 20, bottomSpacing: 20, leadingSpacing: 20, trailingSpacing: 20)
    }
    
    override func setupUI() {
        super.setupUI()
        scrollableContentView.addSubview(rootStackView)
        
        logoImageContainerView.addSubview(logoImage)
        rootStackView.addArrangedSubviews([
            logoImageContainerView,
            titleLabel,
            subtitleLabel,
            answersStackView,
            registerButton,
            logoutButton,
            deleteAccountButton
        ])
    }
    
    override func setupConstraints() {
        super.setupConstraints()
        rootStackView.positionToTheEdges()
        
        logoImageContainerView.snp.makeConstraints { make in
            make.height.equalTo(50)
        }
        
        logoImage.snp.makeConstraints { make in
            make.center.equalToSuperview()
            make.size.equalTo(45)
        }
    }
}

extension ProfileView {
    func fill(with model: ViewModel) {
        answersStackView.removeAllArrangedSubviews()
        
        for group in model.groups {
            answersStackView.addArrangedSubview(groupView(group.name))
            
            for item in group.items {
                let itemView = ProfileItemView()
                itemView.fill(with: item)
                itemView.onTap = { [weak self] groupId in self?.onGroupTapped?(groupId) }
                answersStackView.addArrangedSubview(itemView)
            }
        }
    }
    
    //wrapped
    func groupView(_ groupName: String) -> UIView {
        let groupContainerView = UIView()
        
        let groupNameContainerView = with(UIView()) {
            $0.backgroundColor = UIColor.LogThisPlace.purpureLight
            $0.layer.cornerRadius = 11
            $0.layer.masksToBounds = true
        }
        
        let groupNameLabel = with(UILabel()) {
            $0.font = .systemFont(ofSize: 11)
            $0.text = groupName.uppercased()
            $0.textColor = UIColor.LogThisPlace.purpure
        }
        
        groupNameContainerView.addSubview(groupNameLabel)
        groupContainerView.addSubview(groupNameContainerView)
        
        groupNameLabel.snp.makeConstraints { make in
            make.top.bottom.equalToSuperview()
            make.centerY.centerX.equalToSuperview()
        }
        
        groupNameContainerView.snp.makeConstraints{ make in
            make.height.equalToSuperview()
            make.width.equalTo(groupNameLabel.snp.width).offset(20)
        }
        
        groupContainerView.snp.makeConstraints{ make in
            make.height.equalTo(22)
        }
        
        return groupContainerView
    }
}
