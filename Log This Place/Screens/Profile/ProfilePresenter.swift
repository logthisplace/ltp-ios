//
//  ProfilePresenter.swift
//  Log This Place
//
//

import UIKit

protocol ProfilePresenterInforable: AnyObject {
    func didRegisterPermanentAccount()
    func didUpdateProfile(_ userPassport: UserPassport)
}

protocol ProfilePresenterProtocol: Presenter {
    func groupTapped(groupId: Int)
    func registerTapped()
    func logoutTapped()
    func didBackNavigationTapped()
    func deleteAccountTapped()
}

final class ProfilePresenter: BasePresenter, ProfilePresenterProtocol, ProfilePresenterInforable, DeleteAccountManagerDelegate {
    
    weak var view: ProfileViewInput?
    let coordinator: ProfileCoordinatorProtocol
    private let questionnaireDataManager: QuestionnaireDataManagerProtocol = AppContainer.resolve()
    private let questionnaireMapperManager: QuestionnaireMapperManagerProtocol = QuestionnaireMapperManager()
    private let userManager: UserManagerProtocol = AppContainer.resolve()
    private let userDefaultsManager: UserDefaultsManagerProtocol = AppContainer.resolve()
    private let userIdentificationDataManager: UserIdentificationDataManagerProtocol = AppContainer.resolve()
    private let firebaseAnalyticManager: FirebaseAnalyticManagerProtocol = AppContainer.resolve()
    private var isAnonymous: Bool {
        userManager.isAnonymous ?? true
    }
    private var viewModel: ProfileViewModel
    
    private var questionnaireGroups = [QuestionnaireGroupViewManagerInput]()
    private var didUpdateProfile = false
    
    private var deleteAccountManager: DeleteAccountManagerProtocol?
    
    init(coordinator: ProfileCoordinatorProtocol,
         viewModel: ProfileViewModel) {
        self.coordinator = coordinator
        self.viewModel = viewModel
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        firebaseAnalyticManager.logEvent(screen: .profile)
        
        deleteAccountManager = DeleteAccountManager(view: view)
        deleteAccountManager?.delegate = self
        
        handleButtonSettings()
        fetchData()
    }
    
    private func handleButtonSettings() {
        view?.buttonSettings(isAnonymous: userManager.isAnonymous ?? true)
    }
    
    private func fetchData() {
        view?.showLoading(fullscreen: true)
        questionnaireDataManager.getQuestionnaire(questionnaireType: .user) { [weak self] result in
            guard let self = self else { return }
            switch result {
            case let .success(questionnaire):
                self.handle(questionnaire)
            case .failure:
                break
            }
            self.view?.hideLoading()
        }
    }
    
    private func handle(_ questionnaire: Questionnaire) {
        questionnaireGroups = questionnaireMapperManager.convertToQuestionnaireViewModel(questionnaire: questionnaire,
                                                                                             answerSheet: viewModel.userPassport?.answers ?? [:])
        view?.fill(with: .init(groups: convertToProfileViewModel(groups: questionnaireGroups)))
    }
    
    private func convertToProfileViewModel(groups: [QuestionnaireGroupViewManagerInput]) -> [ProfileView.ViewModel.Group] {
        var profileGroups = [ProfileView.ViewModel.Group]()
        for (index, group) in groups.enumerated() {
            var items = [ProfileItemView.ViewModel]()
            for question in group.questions.enumerated() {
                switch question.element {
                case let .checkboxSet(checkboxSet):
                    let checkedAnswers = checkboxSet.answerSheet.items.filter { $0.isChecked }
                    let subtitle = checkedAnswers.map { $0.text }.joined(separator: ", ")
                    items.append(.init(groupId: index,
                                       title: checkboxSet.question,
                                       subtitle: subtitle,
                                       isChecked: true))
                case .input: break
                case .picture: break
                }
            }
            profileGroups.append(.init(name: group.groupName, items: items))
        }
        return profileGroups
    }
}

extension ProfilePresenter {
    func groupTapped(groupId: Int) {
        guard questionnaireGroups.indices.contains(groupId) else { return }
        
        coordinator.showQuestionnaireGroup(mode: .edit(input: .profile(input: .init(userPassport: viewModel.userPassport,
                                                                                    groups: questionnaireGroups,
                                                                                    tappedGroupId: groupId))), inforable: self)
    }
    
    func registerTapped() {
        coordinator.showRegistration(informable: self)
    }
    
    func logoutTapped() {
        view?.showAlert(title: L10n.Questionnaire.Logout.Title.text, message: L10n.Questionnaire.Logout.Body.text, actions: [
            UIAlertAction.init(title: L10n.Common.Yes.text, style: .default, handler: { [weak self] (action) in
                guard let self = self else { return }
                self.view?.showLoading(fullscreen: true)
                /// `wasWelcomeShown` - welcome screen with login or register
                self.userDefaultsManager.wasWelcomeShown = false
                self.userIdentificationDataManager.signOut { _ in
                    self.view?.hideLoading()
                    self.coordinator.didLogoutTapped()
                }
            }),
            UIAlertAction.init(title: L10n.Common.No.text, style: .default, handler: { (action) in }),
        ])
    }
    
    func didBackNavigationTapped() {
        didUpdateProfile = didUpdateProfile ? true : viewModel.didLogin
        coordinator.didBackNavigationTapped(didUpdateProfile)
    }
    
    func deleteAccountTapped() {
        deleteAccountManager?.deleteAccountTapped()
    }
}

extension ProfilePresenter {
    func didRegisterPermanentAccount() {
        handleButtonSettings()
        view?.showAlert(title: L10n.Common.Success.text,
                        message: L10n.Profile.Alert.RegisterPermanentAccount.text, actions: [
                    UIAlertAction.init(title: L10n.Common.Ok.text, style: .default, handler: { (action) in }),
                ])
    }
    
    func didUpdateProfile(_ userPassport: UserPassport) {
        didUpdateProfile = true
        viewModel.userPassport = userPassport
        fetchData()
    }
}

extension ProfilePresenter {
    func accountDeleted() {
        coordinator.deletedAccount()
    }
}
