//
//  ProfileViewController.swift
//  Log This Place
//
//

import UIKit

struct ProfileViewModel {
    var didLogin: Bool = false
    var userPassport: UserPassport?
}

protocol ProfileViewInput: View {
    func fill(with model: ProfileView.ViewModel)
    func buttonSettings(isAnonymous: Bool)
}

final class ProfileViewController: BaseViewController {
    
    override var contentView: BaseView {
        profileView
    }
    
    private let presenter: ProfilePresenterProtocol
    private lazy var profileView = ProfileView()
    
    init(presenter: ProfilePresenterProtocol) {
        self.presenter = presenter
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("use init(presenter:)")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.viewDidLoad()
        
        profileView.logoutButton.addTarget(self, action: #selector(logoutTapped), for: .touchUpInside)
        profileView.registerButton.addTarget(self, action: #selector(registerTapped), for: .touchUpInside)
        profileView.deleteAccountButton.addTarget(self, action: #selector(deleteAccountTapped), for: .touchUpInside)
    }
    
    override func setupBehaviour() {
        super.setupBehaviour()
        profileView.onGroupTapped = { [weak self] group in
            self?.presenter.groupTapped(groupId: group)
        }
    }
    
    override func setupUI() {
        super.setupUI()
        title = L10n.Profile.Profile.text
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    override func willMove(toParent parent: UIViewController?) {
        super.willMove(toParent: parent)
        if parent == nil {
            presenter.didBackNavigationTapped()
        }
    }
    
    @objc private func deleteAccountTapped() {
        presenter.deleteAccountTapped()
    }
    
    @objc private func logoutTapped() {
        presenter.logoutTapped()
    }
    
    @objc private func registerTapped() {
        presenter.registerTapped()
    }
}

extension ProfileViewController: ProfileViewInput {
    func fill(with model: ProfileView.ViewModel) {
        profileView.fill(with: model)
    }
    
    func buttonSettings(isAnonymous: Bool) {
        profileView.registerButton.isHidden = !isAnonymous
        profileView.logoutButton.isHidden = isAnonymous
    }
}
