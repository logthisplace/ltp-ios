//
//  Log This Place
//
//

import UIKit
import GooglePlaces

struct SummaryPoiViewModel {
    let processType: ProcessType
    
    var input: Input
    
    struct Input {
        let placeId: String
        let placeName: String?
        let placeAddress: String?
        let questionnaire: Questionnaire?
    }
    
    struct AddPoiInput {
        let newPoi: PointOfInterest?
        let location: CLLocationCoordinate2D?
    }
    
    enum ProcessType {
        enum Add {
            case poi(input: AddPoiInput)
            case answerSheet
        }
        enum Edit {
            case answerSheet
        }
        
        case add(input: Add)
        case edit(input: Edit)
    }
}

protocol SummaryPoiViewInput: View {
    func fillPlaceDetail(name: String?, address: String?, category: String?)
    func fillGroupAnswers(groups: [SummaryPoiGroupItemView.ViewModel])
    func fillImages(images: [ImageSource])
    func fillButtonTitle(_ title: String)
}

final class SummaryPoiViewController: BaseViewController {
    
    override var contentView: BaseView {
        summaryView
    }
        
    private let presenter: SummaryPoiPresenterProtocol
    private lazy var summaryView = SummaryPoiView()
    
    init(presenter: SummaryPoiPresenterProtocol) {
        self.presenter = presenter
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("use init(presenter:)")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.viewDidLoad()
        
        summaryView.bottomButtonView.button.addTarget(self, action: #selector(backTapped), for: .touchUpInside)
    }
    
    override func setupBehaviour() {
        super.setupBehaviour()
        summaryView.onGroupTapped = { [weak self] groupId in
            self?.presenter.groupTapped(groupId: groupId)
        }
        summaryView.onImageTapped = { [weak self] images, presentationIndex in
            self?.presenter.imageTapped(items: images, presentationIndex: presentationIndex)
        }
    }
    
    override func setupUI() {
        super.setupUI()
        title = L10n.Summary.Summary.text
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    override func willMove(toParent parent: UIViewController?) {
        super.willMove(toParent: parent)
        if parent == nil {
            presenter.didBackNavigationTapped()
        }
    }
    

    @objc func backTapped() {
        presenter.didBackTapped()
    }
}

extension SummaryPoiViewController: SummaryPoiViewInput {
    func fillPlaceDetail(name: String?, address: String?, category: String?) {
        summaryView.fillPlaceDetail(name: name,
                                    address: address,
                                    category: category)
    }
    
    func fillGroupAnswers(groups: [SummaryPoiGroupItemView.ViewModel]) {
        summaryView.fillAnswerGroups(groups: groups)
    }
    
    func fillImages(images: [ImageSource]) {
        summaryView.imagesView.onImageTapped = summaryView.onImageTapped
        summaryView.fillImages(images: images)
    }
    
    func fillButtonTitle(_ title: String) {
        summaryView.bottomButtonView.button.setTitle(title, for: .normal)
    }
}
