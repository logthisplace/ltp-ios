//
//  Log This Place
//
//

import UIKit

final class SummaryPoiDetailView: BaseView, Fillable {
    struct ViewModel {
        let name: String
        let address: String
        let category: String
    }
    
    private let rootStackView = with(UIStackView.vertical) {
        $0.spacing = 18
        $0.edgeInsets(topSpacing: 20, bottomSpacing: 20, leadingSpacing: 16, trailingSpacing: 16)
        $0.layer.borderWidth = 1
        $0.layer.borderColor = UIColor.LogThisPlace.grayAlmostWhite.cgColor
        $0.layer.cornerRadius = 8
    }
    private let titleLabel = with(UILabel.titleH3SemiBold) {
        $0.numberOfLines = 0
    }
    private let addressContainer = UIView()
    private let addressLabel = with(UILabel.standartMediumRegular) {
        $0.numberOfLines = 0
    }
    private let addressImageView = with(UIImageView.scaleAspectFit) {
        $0.image = Asset.standardPin.image
    }
    private let categoryContainer = with(UIView()) {
        $0.clipsToBounds = true
        $0.layer.cornerRadius = 12
        $0.backgroundColor = UIColor.LogThisPlace.gray.withAlphaComponent(0.08)
    }
    private let categoryLabel = with(UILabel.captionSemibold) {
        $0.textColor = UIColor.LogThisPlace.gray
    }
    
    override func setupUI() {
        super.setupUI()
        addSubview(rootStackView)
        
        addressContainer.addSubviews([
            addressLabel,
            addressImageView
        ])
        categoryContainer.addSubview(categoryLabel)
        rootStackView.addArrangedSubviews([
            titleLabel,
            addressContainer,
            categoryContainer.wrapped(alignment: .horizontal(.leading))
        ])
    }
    
    override func setupConstraints() {
        super.setupConstraints()
        rootStackView.positionUsingScreenMargins(topSpacing: 16,
                                                 bottomSpacing: 16,
                                                 leadingSpacing: 16,
                                                 trailingSpacing: 16)

        /// addresss
        addressImageView.snp.makeConstraints { make in
            make.size.equalTo(16)
            make.leading.equalTo(addressContainer)
            make.centerY.equalTo(addressContainer)
        }
        addressLabel.snp.makeConstraints { make in
            make.leading.equalTo(addressImageView.snp.trailing).offset(7)
            make.trailing.equalTo(addressContainer)
            make.centerY.equalTo(addressContainer)
        }
        
        /// category
        categoryLabel.snp.makeConstraints { make in
            make.leading.trailing.equalTo(categoryContainer).inset(8)
            make.top.bottom.equalTo(categoryContainer).inset(6)
        }
    }
    
    override func setupVisuals() {
        super.setupVisuals()
        backgroundColor = .white
    }
}

extension SummaryPoiDetailView {
    func fill(with model: ViewModel) {
        titleLabel.text = model.name
        addressLabel.text = model.address
        categoryLabel.text = model.category.uppercased()
    }
}
