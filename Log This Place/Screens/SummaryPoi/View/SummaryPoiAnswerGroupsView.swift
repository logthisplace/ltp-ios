//
//  Log This Place
//
//

import UIKit

final class SummaryPoiAnswerGroupsView: BaseView, Fillable {
    
    struct ViewModel {
        let groups: [SummaryPoiGroupItemView.ViewModel]
    }
    
    var onGroupTapped: ((Int) -> Void)?
    
    private let label = with(LTPUILabel.titleH3SemiBold) {
        $0.numberOfLines = 0
        $0.text = L10n.Summary.Summary.text
    }
    private let rootStackView = with(UIStackView.vertical) {
        $0.background(color: .white)
        $0.edgeInsets(topSpacing: 20, bottomSpacing: 20, leadingSpacing: 16, trailingSpacing: 16)
    }
    
    override func setupUI() {
        super.setupUI()
        addSubview(rootStackView)
        
        rootStackView.addArrangedSubview(label)
    }
    
    override func setupConstraints() {
        super.setupConstraints()
        rootStackView.positionToTheEdges()
    }
    
    @objc private func tappedGroup(sender: SummaryPoiGroupItemView) {
        onGroupTapped?(sender.index)
    }
}

extension SummaryPoiAnswerGroupsView {
    func fill(with model: ViewModel) {
        clearGroupStackView()
        
        for group in model.groups {
            let groupView = SummaryPoiGroupItemView()
            groupView.fill(with: group)
            groupView.addTarget(self, action: #selector(tappedGroup), for: .valueChanged)
            rootStackView.addArrangedSubview(groupView)
        }
    }
    
    private func clearGroupStackView() {
        rootStackView.subviews.forEach { (view) in
            if view != label {
                view.removeFromSuperview()
            }
        }
    }
}
