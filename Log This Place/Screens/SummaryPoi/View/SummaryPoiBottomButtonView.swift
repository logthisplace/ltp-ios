//
//  Log This Place
//
//

import UIKit

final class SummaryPoiBottomButtonView: BaseView {
    
    private let rootStackView = with(UIStackView.vertical) {
        $0.spacing = 12
        $0.edgeInsets(topSpacing: 16, bottomSpacing: 16, leadingSpacing: 16, trailingSpacing: 16)
    }

    let button = LTPButton.standard(style: .black)
    
    override func setupUI() {
        super.setupUI()
        addSubview(rootStackView)
        
        rootStackView.addArrangedSubviews([
            button
        ])
    }
    
    override func setupConstraints() {
        super.setupConstraints()
        
        rootStackView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
    }
    
    override func setupVisuals() {
        super.setupVisuals()
        backgroundColor = .white
    }
    
}
