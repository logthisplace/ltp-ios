//
//  Log This Place
//
//

import UIKit

final class SummaryPoiImagesView: BaseView, Fillable {
    
    struct ViewModel {
        let images: [ImageSource]
    }
    private let rootStackView = with(UIStackView.vertical) {
        $0.backgroundColor = .white
        $0.edgeInsets(topSpacing: 20, bottomSpacing: 20, leadingSpacing: 16, trailingSpacing: 0)
    }
    private let label = with(LTPUILabel.titleH3SemiBold) {
        $0.numberOfLines = 0
        $0.text = L10n.Summary.UploadedImages.text
    }
    private let pickerView = with(ImagePickerView()) {
        $0.setupRootStackViewArea(top: 20, left: 0, bottom: 0, right: 0)
    }
    var onImageTapped: (([ImageSource], Int) -> Void)?
    
    override func setupUI() {
        super.setupUI()
        addSubview(rootStackView)
        
        rootStackView.addArrangedSubviews([
            label,
            pickerView
        ])
    }
    
    override func setupConstraints() {
        super.setupConstraints()
        rootStackView.positionToTheEdges()
    }
    
}

extension SummaryPoiImagesView {
    func fill(with model: ViewModel) {
        pickerView.clearImages()
        pickerView.onImageTapped = onImageTapped
        pickerView.fill(with: .init(title: nil, images: model.images))
    }
}
