//
//  Log This Place
//
//

import UIKit

enum StatusType {
    case completed
    case incompleted
}

final class SummaryPoiStatusView: BaseView, Fillable {
    
    struct ViewModel {
        let type: StatusType
    }
    
    private let label = with(UILabel.standartMediumBold) {
        $0.textColor = UIColor.LogThisPlace.gray
        $0.textAlignment = .center
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        clipsToBounds = true
        layer.cornerRadius = bounds.height / 2
    }
    
    override func setupUI() {
        super.setupUI()
        addSubview(label)
    }
    
    override func setupConstraints() {
        super.setupConstraints()
        
        label.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview().inset(8)
            make.top.bottom.equalToSuperview()
        }
    }
}

extension SummaryPoiStatusView {
    func fill(with model: ViewModel) {
        switch model.type {
        case .completed:
            // TODO: Make it dynamicaly
            snp.makeConstraints { make in
                make.width.equalTo(120)
            }
            
            backgroundColor = UIColor.LogThisPlace.green
            label.textColor = .white
            label.text = L10n.Summary.Completed.text
        case .incompleted:
            // TODO: Make it dynamicaly
            snp.makeConstraints { make in
                make.width.equalTo(150)
            }
            
            backgroundColor = UIColor.LogThisPlace.grayAlmostWhite
            label.textColor = .black
            label.text = L10n.Summary.NotCompleted.text
        }
    }
}
