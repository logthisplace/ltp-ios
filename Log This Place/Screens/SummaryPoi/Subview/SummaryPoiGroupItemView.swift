//
//  Log This Place
//
//

import UIKit

final class SummaryPoiGroupItemView: BaseControl, Fillable {
    
    struct ViewModel {
        let order: Int
        let heading: SummaryPoiHeadingView.ViewModel
        let items: [SummaryPoiQuestionItemView.ViewModel]
        let position: Position
        
        enum Position {
            case first
            case intermediate
            case last
        }
    }
    
    var index: Int { order - 1 }
    
    private var order: Int = 0
    private let tapGR = UITapGestureRecognizer()
    private let orderContainer = with(UIView()) {
        $0.backgroundColor = UIColor.LogThisPlace.darkGray
        $0.layer.cornerRadius = 16
        $0.clipsToBounds = true
    }
    private let orderLabel = with(UILabel.calloutRegular) {
        $0.numberOfLines = 0
        $0.textColor = .white
    }
    private let substainStackView = with(UIStackView.vertical) {
        $0.spacing = 24
    }
    private let titleStatusView = SummaryPoiHeadingView()
    private let label = with(UILabel.standartMediumBold) {
        $0.attributedText = NSAttributedString(string: L10n.Summary.EditGroup.text,
                                               attributes: [.underlineStyle: NSUnderlineStyle.single.rawValue])
    }
    private let verticalTopLineView = with(UIView()) {
        $0.backgroundColor = UIColor.LogThisPlace.darkGray
        $0.snp.makeConstraints { make in
            make.width.equalTo(2)
        }
    }
    private let verticalBottomLineView = with(UIView()) {
        $0.backgroundColor = UIColor.LogThisPlace.darkGray
        $0.snp.makeConstraints { make in
            make.width.equalTo(2)
        }
    }
    private let separatorView = UIView.horizontalSeparator()
    
    override func setupUI() {
        super.setupUI()
        addGestureRecognizer(tapGR)
        tapGR.addTarget(self, action: #selector(controlTapped))
        
        orderContainer.addSubview(orderLabel)
        substainStackView.addArrangedSubviews([
            titleStatusView
        ])
        
        addSubviews([
            orderContainer,
            substainStackView,
            separatorView,
            verticalTopLineView,
            verticalBottomLineView
        ])
    }
    
    override func setupConstraints() {
        super.setupConstraints()
        
        verticalTopLineView.snp.makeConstraints { make in
            make.top.equalToSuperview()
            make.bottom.equalTo(orderContainer.snp.top)
            make.leading.equalToSuperview().offset(16)
        }
        orderContainer.snp.makeConstraints { make in
            make.size.equalTo(32)
            make.top.equalToSuperview().inset(18)
            make.leading.equalToSuperview()
            make.bottom.lessThanOrEqualToSuperview()
        }
        orderLabel.snp.makeConstraints { make in
            make.center.equalToSuperview()
        }
        verticalBottomLineView.snp.makeConstraints { make in
            make.top.equalTo(orderContainer.snp.bottom)
            make.bottom.greaterThanOrEqualToSuperview()
            make.leading.equalToSuperview().offset(16)
        }
        substainStackView.snp.makeConstraints { make in
            make.top.equalToSuperview().inset(18)
            make.trailing.equalToSuperview()
            make.leading.equalTo(orderContainer.snp.trailing).offset(16)
            make.bottom.equalTo(separatorView.snp.top).offset(-20)
        }
        separatorView.snp.makeConstraints { make in
            make.leading.trailing.bottom.equalToSuperview()
        }
    }
    
    @objc private func controlTapped() {
        sendActions(for: .valueChanged)
    }
}

extension SummaryPoiGroupItemView {
    func fill(with model: ViewModel) {
        order = model.order
        titleStatusView.fill(with: model.heading)
        orderLabel.text = String(order)
        
        switch model.position {
        case .first:
            verticalTopLineView.isHidden = true
            separatorView.isHidden = false
            verticalBottomLineView.isHidden = false
        case .intermediate:
            verticalTopLineView.isHidden = false
            verticalBottomLineView.isHidden = false
            separatorView.isHidden = false
        case .last:
            verticalTopLineView.isHidden = false
            verticalBottomLineView.isHidden = true
            separatorView.isHidden = true
        }
        
        for item in model.items {
            let questionItem = SummaryPoiQuestionItemView()
            questionItem.fill(with: item)
            substainStackView.addArrangedSubview(questionItem)
        }
        
        substainStackView.addArrangedSubview(label)
    }
}
