//
//  Log This Place
//
//

import UIKit


final class SummaryPoiQuestionItemView: BaseView, Fillable {
    
    struct ViewModel {
        let question: String
        let answer: String
    }
    private let contentStackView = with(UIStackView.vertical) {
        $0.spacing = 6
    }
    private let titleLabel = with(LTPUILabel.standartMediumBold) {
        $0.numberOfLines = 0
    }
    private let subtitleLabel = with(LTPUILabel.standartMediumRegular) {
        $0.numberOfLines = 0
    }
    
    override func setupUI() {
        super.setupUI()
        addSubview(contentStackView)
        
        contentStackView.addArrangedSubviews([
            titleLabel,
            subtitleLabel
        ])
    }
    
    override func setupConstraints() {
        super.setupConstraints()
        contentStackView.positionToTheEdges()
    }
}

extension SummaryPoiQuestionItemView {
    
    func fill(with model: ViewModel) {
        titleLabel.text = model.question
        subtitleLabel.text = model.answer
    }
}
