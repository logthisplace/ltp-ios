//
//  Log This Place
//
//

import UIKit

final class SummaryPoiHeadingView: BaseView, Fillable {
    struct ViewModel {
        let name: String
        let status: StatusType
    }
    
    private let placeLabel = with(UILabel.calloutBold) {
        $0.numberOfLines = 0
    }
    private let statusView = SummaryPoiStatusView()
    
    override func setupUI() {
        super.setupUI()
        addSubviews([
            statusView,
            placeLabel
        ])
    }
    
    override func setupConstraints() {
        super.setupConstraints()
        
        statusView.snp.makeConstraints { make in
            make.trailing.equalToSuperview()
            make.height.equalTo(36)
            make.centerY.equalToSuperview()
        }
        placeLabel.snp.makeConstraints { make in
            make.height.greaterThanOrEqualTo(36)
            make.leading.top.bottom.equalToSuperview()
            make.trailing.equalTo(statusView.snp.leading).offset(-8)
        }
    }
    
}

extension SummaryPoiHeadingView {
    func fill(with model: ViewModel) {
        placeLabel.text = model.name
        statusView.fill(with: .init(type: model.status))
    }
}
