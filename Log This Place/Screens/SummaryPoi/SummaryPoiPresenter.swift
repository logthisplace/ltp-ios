//
//  Log This Place
//
//

import Foundation
import DKPhotoGallery

protocol SummaryPoiPresenterInformable: AnyObject {
    func didUpdateAnswerSheet()
}

protocol SummaryPoiPresenterProtocol: Presenter {
    func groupTapped(groupId: Int)
    func didBackNavigationTapped()
    func didBackTapped()
    func imageTapped(items: [ImageSource], presentationIndex: Int)
}

final class SummaryPoiPresenter: BasePresenter, SummaryPoiPresenterProtocol, SummaryPoiPresenterInformable {
    
    private let coordinator: SummaryPoiCoordinatorProtocol
    private var viewModel: SummaryPoiViewModel
    
    private let conditionManager: ConditionManagerProtocol = AppContainer.resolve()
    private let firebaseImageManager: FirebaseImageManagerProtocol = AppContainer.resolve()
    private let questionnaireMapperManager: QuestionnaireMapperManagerProtocol = QuestionnaireMapperManager()
    private let answerSheetDataManager: AnswerSheetDataManagerProtocol = AppContainer.resolve()
    private let firebaseAnalyticManager: FirebaseAnalyticManagerProtocol = AppContainer.resolve()

    private var currentlyDisplayedImageNamesForQuestions = [String: [String]]()
    private var gcImages = [String: [(ImageSource, String)]]()
    
    weak var view: SummaryPoiViewInput?
    
    private var answerSheetWasUpdated = false
    private var answerSheet: AnswerSheet?
    
    init(coordinator: SummaryPoiCoordinatorProtocol,
         viewModel: SummaryPoiViewModel) {
        self.coordinator = coordinator
        self.viewModel = viewModel
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        firebaseAnalyticManager.logEvent(screen: .summary)
        
        bottomButtonFillSettings()
        fetchData()
    }
    
    private func bottomButtonFillSettings() {
        switch viewModel.processType {
        case let .add(input):
            switch input {
            case .poi:
                view?.fillButtonTitle(L10n.Summary.BackToMap.text)
            case .answerSheet:
                view?.fillButtonTitle(L10n.Summary.BackToPoi.text)
            }
        case .edit:
            view?.fillButtonTitle(L10n.Summary.BackToPoi.text)
        }
    }
        
    private func fetchData() {
        view?.showLoading(fullscreen: true)
        answerSheetDataManager.getAnswerSheetUserByDocumentId(poiId: viewModel.input.placeId) { [weak self] result in
            self?.view?.hideLoading()
            if case let .success(answerSheet) = result {
                self?.answerSheet = answerSheet
            }
            self?.handlePlaceDetail()
            self?.handleGroupAnswers()
            self?.handleImages()
        }
    }
    
    private func handlePlaceDetail() {
        let category = viewModel.input.questionnaire?.categoryBySheet(sheet: answerSheet?.answers)?.labels.localized ?? L10n.Common.NotFound.text
        view?.fillPlaceDetail(name: viewModel.input.placeName,
                              address: viewModel.input.placeAddress,
                              category: category)
    }
    
    private func handleGroupAnswers() {
        guard let questionGroups = viewModel.input.questionnaire?.questionGroups else { return }
        var answerGroups = [SummaryPoiGroupItemView.ViewModel]()
        
        for (index, questionGroup) in questionGroups.enumerated() {
            let order = index + 1
            let maxIndex = questionGroups.count - 1
            let groupName = questionGroup.groupName.localized
            var notAnswered = 0
            
            var answers = [SummaryPoiQuestionItemView.ViewModel]()
            for question in questionGroup.questions {
                /// skip if question is advanced or question is image
                if question.advanced ?? false || question.type == .pictures { continue }
                
                /// check and skip if condition is false
                if let condition = question.condition,
                   let answerSheet = answerSheet {
                    if !conditionManager.isConditionBasedOnAnswers(condition: condition,
                                                                   answeSheet: answerSheet.answers ?? [:],
                                                                   generateAnswersForNotCondition: false) {
                        continue
                    }
                }
                
                let answer = getAnswer(question: question)
                if answer == nil {
                    notAnswered += 1
                }
                
                answers.append(.init(question: question.labels.localized,
                                     answer: answer ?? L10n.Summary.NotAnswered.text))
            }
            answerGroups.append(.init(order: order,
                                      heading: .init(name: groupName,
                                                     status: notAnswered > 0 ? .incompleted : .completed),
                                      items: answers,
                                      position: getPositionType(index: index,
                                                                maxIndex: maxIndex)))
        }
        view?.fillGroupAnswers(groups: answerGroups)
    }
    
    private func getPositionType(index: Int,
                                 maxIndex: Int) -> SummaryPoiGroupItemView.ViewModel.Position {
        if index == 0 {
            return .first
        } else if index == maxIndex {
            return .last
        } else {
            return .intermediate
        }
    }
    
    private func getAnswer(question: QuestionItem) -> String? {
        switch question.type {
        case .selectSingle:
            if case let .string(answerId) = answerSheet?.answers(for: question.uuid) {
                guard let answer = question.values?.filter({ $0.uuid == answerId }).first else { return nil }
                return answer.labels.localized
            }
        case .pictures:
            return nil /// skip picture answers
        case .boolean:
            if case let .bool(bool) = answerSheet?.answers(for: question.uuid) {
                return bool ? L10n.Common.Yes.text : L10n.Common.No.text
            }
        case .selectMulti:
        if case let .array(answerIds) = answerSheet?.answers(for: question.uuid) {
            let answers = question.values?.filter({answerIds.contains($0.uuid)}).map({ $0.labels.localized })
            if let answers = answers, !answers.isEmpty {
                return answers.joined(separator: ", ")
            }
        }
        case .number:
            if case let .int(int) = answerSheet?.answers(for: question.uuid) {
                return String(int)
            }
        }
        return nil
    }
    
    private func handleImages() {
        guard let answerSheetId = answerSheet?.id,
              let questionnaire = viewModel.input.questionnaire else { return }
        
        view?.showLoading(fullscreen: true)
        firebaseImageManager.handleImages(viewModel: .init(questionnaire: questionnaire,
                                                           placeId: viewModel.input.placeId,
                                                           answerSheetId: answerSheetId)) { [weak self] result in
            guard let self = self else { return }
            
            switch result {
            case let .success(gcImages):
                self.gcImages = gcImages
                self.currentlyDisplayedImageNamesForQuestions = gcImages.mapValues { $0.map { $0.name }}
                
                var images = [ImageSource]()
                for ims in gcImages.map({ $0.value.map({ $0.image }) }) {
                    images.append(contentsOf: ims)
                }
                self.view?.fillImages(images: images)
                
            case let .failure(error):
                print(error)
            }
            
            self.view?.hideLoading()
        }
    }
}

extension SummaryPoiPresenter {
    func groupTapped(groupId: Int) {
        guard let answerSheet = answerSheet,
              let questionnaire = viewModel.input.questionnaire else { return }
        
        let viewModelGroups = questionnaireMapperManager.convertToQuestionnaireViewModel(questionnaire: questionnaire,
                                                                                         answerSheet: answerSheet.answers ?? [:],
                                                                                         images: gcImages,
                                                                                         skipCategoryQuestion: false)
        guard viewModelGroups.indices.contains(groupId) else { return }
        
        coordinator.showQuestionnaireGroup(mode: .edit(input: .answerSheet(input: .init(groups: viewModelGroups,
                                                                                        tappedGroupId: groupId,
                                                                                        answerSheet: answerSheet,
                                                                                        placeId: viewModel.input.placeId,
                                                                                        currentlyDisplayedImageNamesForQuestions: currentlyDisplayedImageNamesForQuestions))), summaryPOIPresenterInforable: self)
    }
    
    func didBackNavigationTapped() {
        coordinator.didBackNavigationTapped(answerSheetWasUpdated: answerSheetWasUpdated)
    }
    
    func didBackTapped() {
        coordinator.didBackTapped(answerSheetWasUpdated: answerSheetWasUpdated)
    }
    
    func imageTapped(items: [ImageSource], presentationIndex: Int) {
        var galleryItems = [DKPhotoGalleryItem]()
        items.forEach({
            switch $0 {
            case let .link(link):
                guard let imageURL = URL(string: link) else { return }
                galleryItems.append(.init(imageURL: imageURL))
            case let .media(media):
                galleryItems.append(.init(image: media))
            }
        })
        coordinator.showGallery(items: galleryItems, presentationIndex: presentationIndex)
    }
}

/// inforable
extension SummaryPoiPresenter {
    func didUpdateAnswerSheet() {
        answerSheetWasUpdated = true
        fetchData()
    }
}
