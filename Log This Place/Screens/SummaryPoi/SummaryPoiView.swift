//
//  Log This Place
//
//

import UIKit


final class SummaryPoiView: BaseScrollView {
    
    private let rootStackView = with(UIStackView.vertical) {
        $0.spacing = 16
    }
    private let headerStackView = with(UIStackView.vertical) {
        $0.spacing = 12
        $0.edgeInsets(topSpacing: 20, bottomSpacing: 20, leadingSpacing: 16, trailingSpacing: 16)
        $0.background(color: UIColor.LogThisPlace.purple)
    }
    private let headerIcon = with(UIImageView()) {
        $0.image = Asset.Summary.congratulationsSmile.image
        $0.contentMode = .left
    }
    private let headerLabel = with(UILabel.titleH1SemiBold) {
        $0.text = L10n.Summary.Description.text
        $0.numberOfLines = 0
        $0.textColor = .white
    }
    private let detailView = SummaryPoiDetailView()
    private let answerGroupsView = SummaryPoiAnswerGroupsView()
    let imagesView = with(SummaryPoiImagesView()) {
        $0.isHidden = true
    }
    let bottomButtonView = SummaryPoiBottomButtonView()
    
    var onGroupTapped: ((Int) -> Void)?
    var onImageTapped: (([ImageSource], Int) -> Void)?
    
    override func setupUI() {
        super.setupUI()
        backgroundColor = UIColor.LogThisPlace.backgroundGray
        scrollableContentView.addSubview(rootStackView)
        
        /// header
        headerStackView.addArrangedSubviews([
            headerIcon.wrapped(alignment: .horizontal(.leading)),
            headerLabel
        ])
        
        /// root view
        rootStackView.addArrangedSubviews([
            headerStackView,
            detailView,
            answerGroupsView,
            imagesView,
            bottomButtonView
        ])
    }
    
    override func setupConstraints() {
        super.setupConstraints()
        rootStackView.positionToTheEdges()
        
        headerIcon.snp.makeConstraints { make in
            make.size.equalTo(52)
        }
    }
}

extension SummaryPoiView {
    func fillPlaceDetail(name: String?, address: String?, category: String?) {
        detailView.fill(with: .init(name: name ?? L10n.Common.NotFound.text,
                                    address: address ?? L10n.Common.NotFound.text,
                                    category: category ?? L10n.Common.NotFound.text))
    }
    
    func fillAnswerGroups(groups: [SummaryPoiGroupItemView.ViewModel]) {
        answerGroupsView.fill(with: .init(groups: groups))
        answerGroupsView.onGroupTapped = { [weak self] groupId in self?.onGroupTapped?(groupId) }
    }
    
    func fillImages(images: [ImageSource]) {
        imagesView.isHidden = images.isEmpty ? true : false
        imagesView.fill(with: .init(images: images))
    }
}
