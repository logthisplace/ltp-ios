//
//  BottomSlideTransitioningDelegate.swift
//  Log This Place
//

import UIKit

final class BottomSlideTransitioningDelegate: NSObject, UIViewControllerTransitioningDelegate {
    
    var showDimView = false
    
    var percentDrivenTransitionController: BottomSlidePercentDrivenInteractiveTransitionController?
    
    func animationController(forPresented presented: UIViewController,
                             presenting: UIViewController,
                             source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        BottomSlideAnimationController(present: true)
    }

    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        BottomSlideAnimationController(present: false)
    }

    func presentationController(forPresented presented: UIViewController,
                                presenting: UIViewController?,
                                source: UIViewController) -> UIPresentationController? {
        let presentationController = BottomSlidePresentationController(presentedViewController: presented, presenting: presenting)
        presentationController.showDimView = showDimView
        return presentationController
    }
    
    func interactionControllerForDismissal(using animator: UIViewControllerAnimatedTransitioning) -> UIViewControllerInteractiveTransitioning? {
        (percentDrivenTransitionController?.hasStarted ?? false) ? percentDrivenTransitionController : nil
    }
}
