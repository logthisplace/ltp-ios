//
//  BottomSlideAnimationController.swift
//  Log This Place
//

import UIKit

final class BottomSlideAnimationController: NSObject, UIViewControllerAnimatedTransitioning {

    private let present: Bool

    private let animationDuration = 0.3

    init(present: Bool) {
        self.present = present
    }

    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        animationDuration
    }

    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        guard let fromVC = transitionContext.viewController(forKey: .from),
              let toVC = transitionContext.viewController(forKey: .to)
        else {
            return
        }

        let containerView = transitionContext.containerView

        let childVC = present ? toVC : fromVC
        let childPresentedFrame = present ? transitionContext.finalFrame(for: childVC) : transitionContext.initialFrame(for: childVC)
        let childDismissedFrame = CGRect(x: childPresentedFrame.origin.x,
                                         y: childPresentedFrame.origin.y + childPresentedFrame.height,
                                         width: childPresentedFrame.width,
                                         height: childPresentedFrame.height)

        let childInitialFrame = present ? childDismissedFrame : childPresentedFrame
        let childFinalFrame = present ? childPresentedFrame : childDismissedFrame

        childVC.view.frame = childInitialFrame

        if present {
            containerView.addSubview(childVC.view)
        }

        UIView.animate(withDuration: animationDuration) {
            childVC.view.frame = childFinalFrame
        } completion: { _ in
            transitionContext.completeTransition(!transitionContext.transitionWasCancelled)
        }
    }
}
