//
//  BottomSlideInteractiveTransitionController.swift
//  Log This Place
//

import UIKit

final class BottomSlidePercentDrivenInteractiveTransitionController: UIPercentDrivenInteractiveTransition {
    var hasStarted: Bool = false
    var shouldFinish = false
}
