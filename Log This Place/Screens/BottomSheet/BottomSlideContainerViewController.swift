//
//  BottomSlideContainerViewController.swift
//  Log This Place
//

import UIKit

final class BottomSlideContainerViewController<EmbededViewController: UIViewController>: BaseViewController {
    
    var userWantsToInteracitvelyDismiss: (() -> Void)?
    
    private let topHandler = with(UIView()) {
        $0.backgroundColor = UIColor.LogThisPlace.darkGray
        $0.layer.cornerRadius = 2
        $0.layer.masksToBounds = true
    }
    
    private let embededViewController: EmbededViewController
    private let percentDrivenTransitionController: BottomSlidePercentDrivenInteractiveTransitionController
    
    private let velocityRequiredForStateChange: CGFloat = 700
    private let percentRequiredForStateChange: CGFloat = 0.3
    private var didTouchStartInHandlerArea = false
    
    private var initialPanTouchPosition: CGPoint = .zero
    
    private lazy var panGestureRecognizer: UIPanGestureRecognizer = {
        let panGR = UIPanGestureRecognizer()
        panGR.addTarget(self, action: #selector(handlePan))
        return panGR
    }()
    
    init(embededViewController: EmbededViewController,
         percentDrivenTransitionController: BottomSlidePercentDrivenInteractiveTransitionController) {
        self.embededViewController = embededViewController
        self.percentDrivenTransitionController = percentDrivenTransitionController
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
      
        view.layer.shadowPath = UIBezierPath(
            roundedRect: view.bounds,
            cornerRadius: view.layer.cornerRadius
        ).cgPath
    }
    
    override func setupUI() {
        super.setupUI()
        view.backgroundColor = .white
        
        addChild(embededViewController)
        view.addSubview(topHandler)
        view.addSubview(embededViewController.view)
        
        view.addGestureRecognizer(panGestureRecognizer)
        
        view.layer.shadowRadius = 16
        view.layer.shadowOffset = CGSize(width: 0, height: 4)
        view.layer.shadowOpacity = 1
        view.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        view.layer.cornerRadius = 16
        view.layer.masksToBounds = true
    }
    
    override func setupConstraints() {
        super.setupConstraints()
        
        topHandler.snp.makeConstraints { make in
            make.top.equalToSuperview().inset(12)
            make.centerX.equalToSuperview()
            make.width.equalTo(36)
            make.height.equalTo(4)
        }
        
        embededViewController.view.snp.makeConstraints { make in
            make.top.equalTo(topHandler.snp.bottom).inset(-12)
            make.leading.trailing.equalToSuperview()
            make.bottom.equalToSuperview()
        }
        
        embededViewController.didMove(toParent: self)
    }
    
    @objc private func handlePan(gestureRecognizer: UIPanGestureRecognizer) {
        let translation = gestureRecognizer.translation(in: view).y
        switch gestureRecognizer.state {
        case .began:
            didTouchStartInHandlerArea = initialPanTouchPosition.y >= 0 && initialPanTouchPosition.y <= embededViewController.view.frame.minY
            if didTouchStartInHandlerArea {
                percentDrivenTransitionController.hasStarted = true
                userWantsToInteracitvelyDismiss?()
            }
        case .changed:
        if didTouchStartInHandlerArea {
            let movementRelativeToHeight = translation / view.bounds.height
            let downwardMovementPercent = max(movementRelativeToHeight, 0.0)
            percentDrivenTransitionController.shouldFinish = downwardMovementPercent >= percentRequiredForStateChange
            percentDrivenTransitionController.update(downwardMovementPercent)
        }
        case .ended:
            percentDrivenTransitionController.hasStarted = false
            percentDrivenTransitionController.shouldFinish
                ? percentDrivenTransitionController.finish()
                : percentDrivenTransitionController.cancel()
        case .failed, .possible, .cancelled:
            percentDrivenTransitionController.hasStarted = false
            percentDrivenTransitionController.cancel()
        @unknown default:
            percentDrivenTransitionController.hasStarted = false
            percentDrivenTransitionController.cancel()
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        initialPanTouchPosition = touches.first!.location(in: view)
    }
}
