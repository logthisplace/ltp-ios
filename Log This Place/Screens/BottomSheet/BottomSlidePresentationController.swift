//
//  BottomSlidePresentationController.swift
//  Log This Place
//

import UIKit

final class BottomSlidePresentationController: UIPresentationController {
    
    var showDimView = false

    private let dimView: UIView

    override init(presentedViewController: UIViewController, presenting presentingViewController: UIViewController?) {
        dimView = UIView()
        super.init(presentedViewController: presentedViewController, presenting: presentingViewController)
        setupDimView()
    }

    override var frameOfPresentedViewInContainerView: CGRect {
        guard let containerView = containerView else { return .zero }
        var frame = CGRect.zero
        frame.size = size(forChildContentContainer: presentedViewController, withParentContainerSize: containerView.bounds.size)
        let offset = containerView.frame.height - frame.height
        frame.origin.y += offset
        return frame
    }

    override func size(forChildContentContainer container: UIContentContainer, withParentContainerSize parentSize: CGSize) -> CGSize {
        //TODO: - Make dynamic calculation of height
        let height: CGFloat = 350
        return .init(width: parentSize.width, height: height)
    }

    override func presentationTransitionWillBegin() {
        super.presentationTransitionWillBegin()
        
        if showDimView {
            containerView?.addSubview(dimView)
            dimView.snp.makeConstraints { make in
                make.edges.equalToSuperview()
            }
            
            guard let transitionCoordinator = presentedViewController.transitionCoordinator else {
                dimView.alpha = 1.0
                return
            }
            
            transitionCoordinator.animate(alongsideTransition: { [weak self] _ in
                self?.dimView.alpha = 1.0
            }, completion: nil)
        } else {
            containerView?.frame = frameOfPresentedViewInContainerView
        }
    }

    override func dismissalTransitionWillBegin() {
        super.dismissalTransitionWillBegin()
        guard let transitionCoordinator = presentedViewController.transitionCoordinator else {
            dimView.alpha = 0.0
            return
        }

        transitionCoordinator.animate { [weak self] _ in
            self?.dimView.alpha = 0.0
        } completion: { [weak self] _ in
            self?.dimView.removeFromSuperview()
        }
    }

    private func setupDimView() {
        dimView.translatesAutoresizingMaskIntoConstraints = false
        dimView.alpha = 0.0
        let backgroundColor = UIColor.black.withAlphaComponent(0.5)
        dimView.backgroundColor = backgroundColor
    }
}
