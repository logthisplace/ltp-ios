//
//  OnboardingView.swift
//  Log This Place
//
//

import UIKit


final class OnboardingView: BaseView, UIScrollViewDelegate, Fillable {
    struct ViewModel {
        let views: [OnboardingItemView.ViewModel]
    }
    
    private let rootStackView = UIStackView.vertical
    let nextButton = with(LTPButton.standard(style: .black)) {
        $0.setImage(Asset.Onboarding.next.image, for: .normal)
        $0.imageView?.contentMode = .scaleAspectFit
        $0.contentVerticalAlignment = .center
        $0.tintColor = UIColor.white
        $0.contentHorizontalAlignment = .center
        /// shadow and radius
        $0.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.25).cgColor
        $0.layer.shadowOffset = CGSize(width: 0, height: 5.0)
        $0.layer.shadowOpacity = 0.5
        $0.layer.masksToBounds = false
        $0.layer.cornerRadius = 33
    }
    private let scrollView = with(UIScrollView()) {
        $0.isPagingEnabled = true
        $0.showsHorizontalScrollIndicator = false
    }
    private let pageIndicator = with(UIPageControl()) {
        $0.currentPageIndicatorTintColor = UIColor.LogThisPlace.purpure
        $0.pageIndicatorTintColor = UIColor.LogThisPlace.purpure.withAlphaComponent(0.4)
        $0.isUserInteractionEnabled = false
    }
    
    private var views = [OnboardingItemView.ViewModel]()
    private var presentationIndex: Int = 0
    private let screenHeight = UIScreen.main.bounds.height
    
    override init() {
        super.init()
        scrollView.delegate = self
    }
    
    override func setupUI() {
        super.setupUI()
        
        addSubviews([
            scrollView,
            pageIndicator,
            nextButton
        ])
    }
    
    override func setupConstraints() {
        super.setupConstraints()

        scrollView.snp.makeConstraints { make in
            make.leading.top.right.equalToSuperview()
            make.height.equalTo(screenHeight)
        }
        
        pageIndicator.snp.makeConstraints { make in
            make.centerX.equalToSuperview()
            make.top.equalToSuperview().inset(screenHeight/2 + 10)
            guard #available(iOS 14.0, *) else {
                make.height.equalTo(12)
                return
            }
        }
        
        nextButton.snp.makeConstraints { make in
            make.size.equalTo(67)
            make.centerX.equalToSuperview()
            make.bottom.equalToSuperview().inset(20)
        }
    }
    
    override func setupVisuals() {
        super.setupVisuals()
        if #available(iOS 14.0, *) {
            pageIndicator.backgroundStyle = .minimal
            pageIndicator.allowsContinuousInteraction = false
        }
    }
    
    func nextTapped() {
        let newIndex = presentationIndex + 1
        if newIndex > views.count {
            return
        }
        let width: CGFloat = scrollView.frame.size.width
        let height: CGFloat = scrollView.frame.size.height
        let newPosition: CGFloat = scrollView.contentOffset.x + width
        let toVisible: CGRect = CGRect(x: newPosition, y: 0, width: width,   height: height)
        scrollView.scrollRectToVisible(toVisible, animated: true)
        presentationIndex += 1
        pageIndicator.currentPage = presentationIndex
    }
    
    func isLastSlide() -> Bool {
        presentationIndex + 1 >= views.count
    }
}


extension OnboardingView {
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        let pageNumber = targetContentOffset.pointee.x / scrollView.bounds.width
        pageIndicator.currentPage = Int(pageNumber)
        presentationIndex = Int(pageNumber)
    }
}

// MARK: - Fillable

extension OnboardingView {
    
    func fill(with model: ViewModel) {
        views = model.views
        scrollView.layoutIfNeeded()
        
        pageIndicator.numberOfPages = model.views.count
        
        var frame = CGRect.zero
        for element in model.views.enumerated() {
            frame.size = scrollView.bounds.size
            frame.origin.x = scrollView.bounds.width * CGFloat(element.offset)
            let item = OnboardingItemView()
            item.frame = frame
            item.fill(with: .init(image: element.element.image,
                                  title: element.element.title,
                                  description: element.element.description))
            scrollView.addSubview(item)
        }
        scrollView.contentSize = CGSize(width: scrollView.bounds.width * CGFloat(model.views.count),
                                        height: scrollView.bounds.height)
    }
}
