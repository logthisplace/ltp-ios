//
//  OnboardingPresenter.swift
//  Log This Place
//
//

import Foundation


protocol OnboardingPresenterProtocol: Presenter {
    func showMap()
}

final class OnboardingPresenter: BasePresenter, OnboardingPresenterProtocol {
    
    private let items = [
        OnboardingItemView.ViewModel(image: Asset.Onboarding.onboardingFirst.image,
                                     title: L10n.Onboarding.FirstInfo.Title.text,
                                     description:  L10n.Onboarding.FirstInfo.Body.text),
        OnboardingItemView.ViewModel(image: Asset.Onboarding.onboardingSecond.image,
                                     title: L10n.Onboarding.SecondInfo.Title.text,
                                     description:  L10n.Onboarding.SecondInfo.Body.text),
        OnboardingItemView.ViewModel(image: Asset.Onboarding.onboardingThrees.image,
                                     title: L10n.Onboarding.ThirdInfo.Title.text,
                                     description:  L10n.Onboarding.ThirdInfo.Body.text),
    ]
    
    weak var view: OnboardingViewInput?
    private let userDefaultsManager: UserDefaultsManagerProtocol = AppContainer.resolve()
    private let coordinator: MapCoordinatorProtocol
    private let firebaseAnalyticManager: FirebaseAnalyticManagerProtocol = AppContainer.resolve()

    init(coordinator: MapCoordinatorProtocol) {
        self.coordinator = coordinator
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        firebaseAnalyticManager.logEvent(screen: .onboarding)
        
        view?.fillView(with: items)
    }
    
    func showMap() {
        userDefaultsManager.wasOnboardingShown = true
        coordinator.showMap()
    }
}
