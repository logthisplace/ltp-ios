//
//  OnboardingViewController.swift
//  Log This Place
//
//

import UIKit

protocol OnboardingViewInput: View {
    func fillView(with items: [OnboardingItemView.ViewModel])
}

final class OnboardingViewController: BaseViewController {
    
    override var contentView: BaseView {
        onboardingView
    }
    
    private let presenter: OnboardingPresenterProtocol
    private lazy var onboardingView = OnboardingView()
    
    init(presenter: OnboardingPresenterProtocol) {
        self.presenter = presenter
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("use init(presenter:)")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.viewDidLoad()
        onboardingView.nextButton.addTarget(self, action: #selector(nextTapped), for: .touchUpInside)
    }
    
    @objc func nextTapped() {
        if onboardingView.isLastSlide() {
            presenter.showMap()
        } else {
            onboardingView.nextTapped()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: false)
    }
}

extension OnboardingViewController: OnboardingViewInput {
    func fillView(with items: [OnboardingItemView.ViewModel]) {
        onboardingView.fill(with: .init(views: items))
    }
}
