//
//  OnboardingItemView.swift
//  Log This Place
//
//

import UIKit


final class OnboardingItemView: BaseView, Fillable {
    
    struct ViewModel {
        let image: UIImage
        let title: String
        let description: String
    }
    let headerImageView = with(UIImageView()) {
        $0.contentMode = .scaleAspectFill
    }
    let headerTextContainerStackView = with(UIStackView.vertical) {
        $0.edgeInsets(topSpacing: 20, bottomSpacing: 0, leadingSpacing: 20, trailingSpacing: 20)
    }
    let titleLabel = with(UILabel.titleLargeRegular) {
        $0.textAlignment = .center
        $0.numberOfLines = 0
    }
    let descriptionLabel = with(UILabel.headlineMediumRegular) {
        $0.textAlignment = .center
        $0.numberOfLines = 0
        $0.textColor = UIColor.LogThisPlace.gray
    }
    let whiteView = with(UIView()) {
        $0.backgroundColor = .white
    }
    private let screenHeight = UIScreen.main.bounds.height
    
    override func setupUI() {
        super.setupUI()
        
        headerTextContainerStackView.addArrangedSubviews([
            titleLabel,
            descriptionLabel
        ])
        addSubviews([headerImageView,
                     whiteView,
                     headerTextContainerStackView])
    }
    
    override func setupConstraints() {
        super.setupConstraints()
        
        headerTextContainerStackView.spacing = 16
        
        headerImageView.snp.makeConstraints{ make in
            make.leading.top.trailing.equalToSuperview()
            make.height.equalTo(screenHeight/2)
        }
        headerTextContainerStackView.snp.makeConstraints{ make in
            make.top.equalTo(headerImageView.snp.bottom)
            make.leading.trailing.equalToSuperview()
        }
        whiteView.snp.makeConstraints{ make in
            make.top.equalTo(headerImageView.snp.bottom)
            make.leading.bottom.trailing.equalToSuperview()
        }
    }
}

extension OnboardingItemView {
    func fill(with model: ViewModel) {
        headerImageView.image = model.image
        titleLabel.text = model.title
        descriptionLabel.text = model.description
    }
}
