//
//  QuestionnaireMapperManager.swift
//  Log This Place
//
//

import Foundation

protocol QuestionnaireMapperManagerProtocol {
    func convertToQuestionnaireViewModel(questionnaire: Questionnaire,
                                         answerSheet: [String: Any],
                                         images: [String: [(ImageSource, String)]]?,
                                         skipCategoryQuestion: Bool) -> [QuestionnaireGroupViewManagerInput]
    func getCategoryQuestion(questionnaire: Questionnaire) -> LTPCheckboxView.ViewModel?
}
extension QuestionnaireMapperManagerProtocol {
    func convertToQuestionnaireViewModel(questionnaire: Questionnaire,
                                         answerSheet: [String: Any],
                                         images: [String: [(ImageSource, String)]]? = nil,
                                         skipCategoryQuestion: Bool = true) -> [QuestionnaireGroupViewManagerInput] {
        return convertToQuestionnaireViewModel(questionnaire: questionnaire,
                                               answerSheet: answerSheet,
                                               images: images,
                                               skipCategoryQuestion: skipCategoryQuestion)
    }
}



final class QuestionnaireMapperManager: QuestionnaireMapperManagerProtocol {
    
    func getCategoryQuestion(questionnaire: Questionnaire) -> LTPCheckboxView.ViewModel? {
        for group in questionnaire.questionGroups {
            for question in group.questions {
                if case .selectSingle = (question.type) {
                    if question.uuid == FirestoreDatabase.CATEGORY_UUID {
                        if let answerItems = question.values?.map({
                            LTPAnswer.ViewModel(id: $0.uuid,
                                                text: $0.labels.localized,
                                                isChecked: false)
                        }) {
                            return .init(question: question.labels.localized,
                                         answerSheet: .init(mode: .singleChoice,
                                                            items: answerItems),
                                         questionId: question.uuid,
                                         condition: question.condition)
                        }
                    }
                }
            }
        }
        return nil
    }
    
    func convertToQuestionnaireViewModel(questionnaire: Questionnaire,
                                         answerSheet: [String: Any],
                                         images: [String: [(ImageSource, String)]]? = nil,
                                         skipCategoryQuestion: Bool) -> [QuestionnaireGroupViewManagerInput] {
                
        var viewModel = [QuestionnaireGroupViewManagerInput]()
        for group in questionnaire.questionGroups {
            var questions = [QuestionnaireViewManagerTypeOfField]()
            
            for question in group.questions {
                if question.advanced ?? false { continue }
                
                switch question.type {
                case .selectSingle:
                    let currentAnswerId = answerSheet[question.uuid] as? String
                    
                    if let answerItems = question.values?.map({
                        LTPAnswer.ViewModel(id: $0.uuid,
                                            text: $0.labels.localized,
                                            isChecked: ($0.uuid == currentAnswerId))
                    }) {
                        let selectSingleQuestion: LTPCheckboxView.ViewModel = .init(question: question.labels.localized,
                                                                                    answerSheet: .init(mode: .singleChoice,
                                                                                                       items: answerItems),
                                                                                    questionId: question.uuid,
                                                                                    condition: question.condition)
                        /// we handle category question separately
                        if skipCategoryQuestion {
                            if question.uuid == FirestoreDatabase.CATEGORY_UUID {
                                continue
                            }
                        }
                        
                        questions.append(.checkboxSet(selectSingleQuestion))
                    }
                case .selectMulti:
                    let currentAnswers = answerSheet[question.uuid] as? [String]
                    if let answerItems = question.values?.map({
                        LTPAnswer.ViewModel(id: $0.uuid,
                                            text: $0.labels.localized,
                                            isChecked: currentAnswers?.contains($0.uuid) ?? false)
                    }) {
                        questions.append(.checkboxSet(.init(question: question.labels.localized,
                                                            answerSheet: .init(mode: .multiChoice,
                                                                               items: answerItems),
                                                            questionId: question.uuid,
                                                            condition: question.condition)))
                    }
                    
                case .boolean:
                    let currentAnswer = answerSheet[question.uuid] as? Bool
                    let answerItems = [
                        LTPAnswer.ViewModel(id: QuestionnaireCheckboxSetView.trueAnswerKey, text: L10n.Common.Yes.text, isChecked: currentAnswer == true ? true : false),
                        LTPAnswer.ViewModel(id: QuestionnaireCheckboxSetView.falseAnswerKey, text: L10n.Common.No.text, isChecked: currentAnswer == false ? true : false)
                    ]
                    questions.append(.checkboxSet(.init(question: question.labels.localized,
                                                        answerSheet: .init(mode: .boolean,
                                                                           items: answerItems),
                                                        questionId: question.uuid,
                                                        condition: question.condition)))
                    
                case .pictures:
                    let imagesForQuestion = images?[question.uuid]
                    questions.append(.picture(.init(questionId: question.uuid,
                                                    imagePickerViewModel: .init(title: question.labels.localized,
                                                                                images: imagesForQuestion),
                                                    condition: question.condition)))
                case .number:
                    var currentAnswer: String?
                    if let userAnswer: Int = answerSheet[question.uuid] as? Int {
                        currentAnswer = "\(userAnswer)"
                    }
                    questions.append(.input(.init(questionId: question.uuid,
                                                  formTextFieldViewModel: .init(title: question.labels.localized,
                                                                                type: .number,
                                                                                value: currentAnswer),
                                                  condition: question.condition)))
                }
            }
            
            viewModel.append(.init(groupName: group.groupName.localized,
                                   questions: questions))
        }
        return viewModel
    }
    
}
