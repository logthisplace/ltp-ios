//
//  Log This Place
//
//

import UIKit

protocol QuestionnairePictureManagerProtocol {    
    func handlePictures(imageAnswers: [Answer<[(image: ImageSource, id: String)]>],
                        currentlyDisplayedImageNamesForQuestions: [String: [String]],
                        poiId: String,
                        sheetId: String,
                        completionHandler: (([Result<String, Error>]) -> Void)?)
}

final class QuestionnairePictureManager: QuestionnairePictureManagerProtocol {
    
    private let storageDataManager: StorageDataManagerProtocol = AppContainer.resolve()
    
    func handlePictures(imageAnswers: [Answer<[(image: ImageSource, id: String)]>],
                        currentlyDisplayedImageNamesForQuestions: [String: [String]],
                        poiId: String,
                        sheetId: String,
                        completionHandler: (([Result<String, Error>]) -> Void)?) {
        
        var processResults = [Result<String, Error>]()
        var outputUploadDictionary = [String: [UIImage]]()
        var outputRemovalDictionary = [String: [String]]()
        for imageAnswer in imageAnswers {
            guard !imageAnswer.answer.isEmpty else {
                if !(currentlyDisplayedImageNamesForQuestions[imageAnswer.questionId]?.isEmpty ?? false) {
                    outputRemovalDictionary[imageAnswer.questionId] = currentlyDisplayedImageNamesForQuestions[imageAnswer.questionId]
                }
                continue
            }
            
            guard let imagesForQuestionBeforeEditing = currentlyDisplayedImageNamesForQuestions[imageAnswer.questionId] else {
                let imagePickerSources = imageAnswer.answer.map { $0.image }
                
                // TODO: - Improve logic
                var imagesOnly = [UIImage]()
                for imageSource in imagePickerSources {
                    if case .media(let data) = imageSource {
                        imagesOnly.append(data)
                    }
                }
                
                if !imagesOnly.isEmpty {
                    outputUploadDictionary[imageAnswer.questionId] = imagesOnly
                }
                continue
            }
            
            let imagesForQuestionAfterEditingSet = Set(imageAnswer.answer.map { $0.id })
            let imagesForQuestionBeforeEditingSet = Set(imagesForQuestionBeforeEditing)
            
            let imageIdsToBeUploadedSet = imagesForQuestionAfterEditingSet.subtracting(imagesForQuestionBeforeEditingSet)
            let imageIdsToBeRemovedSet = imagesForQuestionBeforeEditingSet.subtracting(imagesForQuestionAfterEditingSet)
            
            let imagesPickerSourcesToBeUploaded = imageIdsToBeUploadedSet
                .compactMap { ids in imageAnswer.answer.first(where: { ids.contains($0.id) }) }
                .map { $0.image }
            
            // TODO: - Improve logic
            var imagesToBeUploaded = [UIImage]()
            for imageSource in imagesPickerSourcesToBeUploaded {
                if case .media(let data) = imageSource {
                    imagesToBeUploaded.append(data)
                }
            }
            
            if !imagesToBeUploaded.isEmpty {
                outputUploadDictionary[imageAnswer.questionId] = imagesToBeUploaded
            }
            if !imageIdsToBeRemovedSet.isEmpty {
                outputRemovalDictionary[imageAnswer.questionId] = Array(imageIdsToBeRemovedSet)
            }
        }
        
        guard !outputRemovalDictionary.isEmpty ||
                !outputUploadDictionary.isEmpty else {
            completionHandler?(processResults)
            return
        }
        
        let dispatchGroup = DispatchGroup()
        dispatchGroup.enter()
        uploadPictures(dictionary: outputUploadDictionary,
                       poiId: poiId,
                       sheetId: sheetId) { results in
            processResults.append(contentsOf: results)
            dispatchGroup.leave()
        }
        
        dispatchGroup.enter()
        removePictures(dictionary: outputRemovalDictionary,
                       poiId: poiId,
                       sheetId: sheetId) {
            dispatchGroup.leave()
        }
        
        dispatchGroup.notify(queue: .main) {
            completionHandler?(processResults)
        }
    }
    
    private func uploadPictures(dictionary: [String: [UIImage]],
                                poiId: String,
                                sheetId: String,
                                completionHandler: (([Result<String, Error>]) -> Void)?) {
        guard !dictionary.isEmpty else {
            completionHandler?([])
            return
        }
        
        var results = [Result<String, Error>]()
        
        let dispatchGroup = DispatchGroup()
        for questionImages in dictionary {
            dispatchGroup.enter()
            storageDataManager.uploadImages(images: questionImages.value,
                                            poiId: poiId,
                                            sheetId: sheetId,
                                            questionId: questionImages.key) { processResults in
                results.append(contentsOf: processResults)
                dispatchGroup.leave()
            }
        }
        
        dispatchGroup.notify(queue: .main) {
            completionHandler?(results)
        }
    }
    
    private func removePictures(dictionary: [String: [String]],
                                poiId: String,
                                sheetId: String,
                                completionHandler: (() -> Void)?) {
        guard !dictionary.isEmpty else {
            completionHandler?()
            return
        }
        
        let dispatchGroup = DispatchGroup()
        for questionIdImageNames in dictionary {
            dispatchGroup.enter()
            storageDataManager.removeImages(poiId: poiId,
                                            sheetId: sheetId,
                                            questionId: questionIdImageNames.key,
                                            imageNames: questionIdImageNames.value) { result in
                // TODO: - error handling
                dispatchGroup.leave()
            }
        }
        
        dispatchGroup.notify(queue: .main) {
            completionHandler?()
        }
    }
    
}
