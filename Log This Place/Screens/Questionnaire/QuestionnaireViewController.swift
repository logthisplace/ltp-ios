//
//  Log This Place
//

import Foundation
import UIKit

protocol QuestionnaireViewInput: View {
    func show(questionnaireTypeMode: QuestionnaireType, groups: [QuestionnaireGroupViewManagerInput])
    func showQuestionnaireGroup(groups: [QuestionnaireGroupViewManagerInput], groupIndex: Int)
    func addPoi(poi: QuestionsStartAddPoiView.ViewModel, groups: [QuestionnaireGroupViewManagerInput])
    func saveQuestionnaireTapped()
}

final class QuestionnaireViewController: BaseViewController, QuestionnaireViewInput, ImagePickerManagerContext {
    
    private let viewHelper: QuestionnaireViewHelperManagerProtocol = QuestionnaireViewHelper()
    
    private lazy var questionsView = QuestionnaireQuestionsView(imagePickerManager: ImagePickerManager(context: self),
                                                                viewHelper: viewHelper)
    
    override var contentView: QuestionnaireQuestionsView {
        questionsView
    }
    
    private let presenter: QuestionnairePresenterProtocol
    private let viewManager: QuestionnaireViewManagerProtocol
    
    init(presenter: QuestionnairePresenterProtocol,
         viewManager: QuestionnaireViewManagerProtocol) {
        self.presenter = presenter
        self.viewManager = viewManager
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        presenter.viewDidLoad()
        
        questionsView.containerContentView.nextButton.addTarget(self, action: #selector(nextTapped), for: .touchUpInside)
        questionsView.containerContentView.previousButton.addTarget(self, action: #selector(previousTapped), for: .touchUpInside)
        questionsView.containerContentView.saveButton.addTarget(self, action: #selector(saveTapped), for: .touchUpInside)
        questionsView.onGroupTapped = { [weak self] questionId in self?.groupTapped(id: questionId) }
        questionsView.onAnswerTapped = { [weak self] questionId, answerId in self?.answerTapped(questionId: questionId,
                                                                                                answerId: answerId) }
        questionsView.closeButton.addTarget(self, action: #selector(closeTapped), for: .touchUpInside)
        
        viewManager.view = questionsView
        viewManager.viewInput = self
        viewManager.viewHelper = viewHelper
    }
    
    override func setupBehaviour() {
        super.setupBehaviour()
        questionsView.onImageTapped = { [weak self] images, presentationIndex in
            self?.presenter.imageTapped(items: images, presentationIndex: presentationIndex)
        }
    }
    
    @objc private func closeTapped() {
        presenter.closeTapped()
    }
    
    @objc private func nextTapped() {
        viewManager.didTapNextFinish()
    }
    
    @objc private func previousTapped() {
        viewManager.didTapPrevious()
    }
    
    @objc private func saveTapped() {
        viewManager.didTapSave()
    }
    
    @objc private func groupTapped(id: Int) {
        viewManager.didTapGroup(with: id)
    }
    
    @objc private func answerTapped(questionId: String?, answerId: String) {
        viewManager.didTapAnswer(questionId: questionId, answerId: answerId)
    }
    
    @objc func saveQuestionnaireTapped() {
        presenter.saveQuestionnaireTapped(answers: viewManager.cleanedUpAnswers,
                                          imageAnswers: viewManager.imageAnswers)
    }
}

extension QuestionnaireViewController {
    func show(questionnaireTypeMode: QuestionnaireType, groups: [QuestionnaireGroupViewManagerInput]) {
        viewManager.start(questionnaireTypeMode: questionnaireTypeMode, groups: groups)
    }
    
    
    func showQuestionnaireGroup(groups: [QuestionnaireGroupViewManagerInput], groupIndex: Int) {
        viewManager.showQuestionnaireGroupByIndex(groups: groups, groupIndex: groupIndex)
    }

    func addPoi(poi: QuestionsStartAddPoiView.ViewModel, groups: [QuestionnaireGroupViewManagerInput]) {
        viewManager.addPoi(poi: poi, groups: groups)
    }
    

}
