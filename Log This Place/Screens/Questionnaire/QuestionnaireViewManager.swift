//
//  Log This Place
//

import UIKit

struct QuestionnaireGroupViewManagerInput {
    let groupName: String
    var questions: [QuestionnaireViewManagerTypeOfField]
}

enum QuestionnaireViewManagerTypeOfField {
    case input(QuestionnaireLTPFormTextField.ViewModel)
    case picture(QuestionnaireImagePickerView.ViewModel)
    case checkboxSet(LTPCheckboxView.ViewModel)
}

protocol QuestionnaireViewManagerProtocol: AnyObject {
    
    var view: QuestionnaireQuestionsViewManagable? { get set }
    var viewInput: QuestionnaireViewInput? { get set }
    var viewHelper: QuestionnaireViewHelperManagerProtocol? { get set }
    
    var cleanedUpAnswers: [String: Any] { get }
    var imageAnswers: [Answer<[(image: ImageSource, id: String)]>] { get }

    /// add new poi start from view then start questionnair flow
    func addPoi(poi: QuestionsStartAddPoiView.ViewModel, groups: [QuestionnaireGroupViewManagerInput])
    /// start questionnair flow
    func start(questionnaireTypeMode: QuestionnaireType, groups: [QuestionnaireGroupViewManagerInput])
    
    /// show separately group of questions
    func showQuestionnaireGroupByIndex(groups: [QuestionnaireGroupViewManagerInput], groupIndex: Int)
    
    func didTapNextFinish()
    func didTapPrevious()
    func didTapSave()
    func didTapGroup(with id: Int)
    func didTapAnswer(questionId: String?, answerId: String)
}

/// Class that manages displaying answers and takes care of view state.
final class QuestionnaireViewManager: QuestionnaireViewManagerProtocol {
    
    private struct Constants {
        static let maxImagesAnswer: Int = 20
    }
    
    private let userDefaultsManager: UserDefaultsManagerProtocol = AppContainer.resolve()
    
    weak var view: QuestionnaireQuestionsViewManagable?
    weak var viewInput: QuestionnaireViewInput?
    var viewHelper: QuestionnaireViewHelperManagerProtocol?
    
    private var groups = [QuestionnaireGroupViewManagerInput]()
    private var currentIndex = 0

    private var currentlyEditingQuestion: LTPCheckboxView.ViewModel?
    
    private var answers = [String: Any]()
    var cleanedUpAnswers: [String: Any] {
        answers.forEach({ item in
            if item.value is [String] {
                /// remove empty arrays
                guard let items = answers[item.key] as? [String] else { return }
                if items.isEmpty {
                    answers.removeValue(forKey: item.key)
                }
            }
        })
        return answers
    }
    
    var imageAnswers = [Answer<[(image: ImageSource, id: String)]>]()
    
    private var trueAnswerKey: String { return QuestionnaireCheckboxSetView.trueAnswerKey }
    private var falseAnswerKey: String { return QuestionnaireCheckboxSetView.falseAnswerKey }
    
    private var questionnaireTypeMode: QuestionnaireType?
    /// need when a user add a new poi
    private var poi: QuestionsStartAddPoiView.ViewModel?
    
    func addPoi(poi: QuestionsStartAddPoiView.ViewModel, groups: [QuestionnaireGroupViewManagerInput]) {
        self.questionnaireTypeMode = .poi
        self.poi = poi
        self.groups = groups
        /// set current index -1 for first page, need  only when we add new poi
        currentIndex = -1
        view?.showAddPoi(poi)
    }
    
    func start(questionnaireTypeMode: QuestionnaireType, groups: [QuestionnaireGroupViewManagerInput]) {
        self.questionnaireTypeMode = questionnaireTypeMode
        /// when a user start create profile we start show progress indicator
        if case .user = (questionnaireTypeMode) {
            view?.showProgressIndicator(currentQuestion: currentIndex, maxQuestions: groups.count)
        }
        
        self.groups = groups
        
        if let firstGroup = self.groups.first {
            view?.showQuestionnaireGroup(group: firstGroup,
                                         editing: false,
                                         previousNextButtonEnabled: true)
        }
    }
    
    func showQuestionnaireGroupByIndex(groups: [QuestionnaireGroupViewManagerInput], groupIndex: Int) {
        self.groups = groups
        currentIndex = groupIndex
        
        updateAnswersForInitialSummary()
        view?.showQuestionnaireGroup(group: groups[currentIndex],
                                     editing: true,
                                     previousNextButtonEnabled: false)
    }
}

//MARK: - Helper methods
extension QuestionnaireViewManager {
    
    private func updateAnswersForInitialSummary() {
        for group in groups {
            group.questions.forEach {
                switch $0 {
                case let .input(input):
                    switch input.formTextFieldViewModel.type {
                    case .normal:
                        self.answers[input.questionId] = input.formTextFieldViewModel.value
                    case .number:
                        if let userAnswer = (input.formTextFieldViewModel.value as NSString?)?.integerValue {
                            self.answers[input.questionId] = userAnswer
                        }
                    }
                case let .picture(input):
                    guard let images = input.imagePickerViewModel.images else { return }
                    imageAnswers.append(.init(questionId: input.questionId,
                                              answer: images))
                    
                case let .checkboxSet(checkboxSet):
                    switch checkboxSet.answerSheet.mode {
                    case .singleChoice:
                        let checkedId = checkboxSet.answerSheet.items.filter{ $0.isChecked }.first?.id
                        self.answers[checkboxSet.questionId] = checkedId
                    case .boolean:
                        guard let answer = checkboxSet.answerSheet.items.filter({ $0.isChecked }).first else { return }
                        self.answers[checkboxSet.questionId] = answer.id == trueAnswerKey ? true : false
                    case .multiChoice:
                        let checkedIds = checkboxSet.answerSheet.items
                            .filter { $0.isChecked }
                            .map { $0.id }
                        self.answers[checkboxSet.questionId] = checkedIds
                    }
                }
            }
        }
    }
}

// MARK: - Tap listeners
extension QuestionnaireViewManager {
    
    func didTapNextFinish() {
        guard !isPicturesExceedLimit() else { return }
        
        if currentIndex < 0 {
            addPoiUpdateAnswer()
        } else {
            updateAnswers()
        }
        
        if currentIndex + 1 < groups.count {
            currentIndex += 1
            view?.showProgressIndicator(currentQuestion: currentIndex, maxQuestions: groups.count)
            view?.showQuestionnaireGroup(group: groups[currentIndex],
                                         editing: false,
                                         previousNextButtonEnabled: true)
            
            if currentIndex + 1 == groups.count{
                view?.setNextButtonTitle(L10n.Common.Finish.text)
            } else {
                view?.setNextButtonTitle(L10n.Common.Next.text)
            }
            
        } else {
            didTapSave()
        }
    }
    
    func didTapPrevious() {
        guard !isPicturesExceedLimit() else { return }
        
        if currentIndex >= 0 {
            updateAnswers()
        }
        
        if currentIndex - 1 >= 0 {
            if case .poi = questionnaireTypeMode {
                if currentIndex + 1 == groups.count {
                    view?.setNextButtonTitle(L10n.Common.Next.text)
                }
            }
            
            currentIndex -= 1
            view?.showProgressIndicator(currentQuestion: currentIndex, maxQuestions: groups.count)
            view?.showQuestionnaireGroup(group: groups[currentIndex],
                                         editing: false,
                                         previousNextButtonEnabled: true)
        } else {
            /// show start screen for add a new poi
            switch questionnaireTypeMode {
            case .poi:
                guard let poi = poi else { return }
                currentIndex = -1
                view?.showAddPoi(.init(address: poi.address,
                                       placeName: poi.placeName,
                                       question: poi.question,
                                       answers: answers))
            case .user: break ///skip
            case .none: break
            }
        }
    }
    
    func didTapSave() {
        updateAnswers()
        viewInput?.saveQuestionnaireTapped()
    }
    
    func didTapGroup(with id: Int) {
        if groups.indices.contains(id) {
            currentIndex = id
            view?.showQuestionnaireGroup(group: groups[currentIndex],
                                         editing: true,
                                         previousNextButtonEnabled: true)
        }
    }
    
    func didTapAnswer(questionId: String?, answerId: String) {
        guard let questionId = questionId else { return }
        updateUserAnswersForExtraQuestions(questionId: questionId, answerId: answerId)
        showHideExtraQuestions()
    }
}

//MARK: - Extra question logic
extension QuestionnaireViewManager {
    
    /// Shows and hides additional questions
    private func showHideExtraQuestions() {
        guard let viewHelper = viewHelper else { return }
        /// get all questionIDs which has `condition`
        let allExtraQuestionIds = viewHelper.getExtraQuestionIds(groups[currentIndex].questions)
        let displayExtraQuestionIds = viewHelper.filterExtraQuestionsByAnswers(questions: groups[currentIndex].questions,
                                                                                          userAnswers: answers)
        
        displayExtraQuestionIds.forEach({
            view?.showExtraQuestion(conditionQuestionId: $0)
        })
        allExtraQuestionIds.forEach({
            if !displayExtraQuestionIds.contains($0) {
                view?.hideExtraQuestion(conditionQuestionId: $0)
                
                guard let questionId = viewHelper.getQuestionIdByAnswerId(answerId: $0, userAnswers: answers) else { return }
                answers.removeValue(forKey: questionId)
                showHideExtraQuestions()
            }
        })
    }
    
    /// Depending on the type of question, updating user responses
    /// - Parameters:
    ///   - questionId: the id of the question the user clicked on
    ///   - answerId: id of the answer the user clicked
    private func updateUserAnswersForExtraQuestions(questionId: String, answerId: String) {
        groups[currentIndex].questions.forEach({
            if case let .checkboxSet(checkboxSet) = $0 {
                if checkboxSet.questionId == questionId {
                    switch checkboxSet.answerSheet.mode {
                    case .boolean:
                        guard let answerId = Bool(answerId) else { return }
                        if answers[questionId] as? Bool == answerId {
                            answers.removeValue(forKey: questionId)
                        } else {
                            answers[questionId] = answerId
                        }
                    case .multiChoice:
                        guard var userAnswers = answers[questionId] as? [String] else {
                            answers[questionId] = [answerId]
                            return
                        }
                        
                        if userAnswers.contains(answerId) {
                            answers[questionId] = userAnswers.filter{ $0 != answerId }
                        } else {
                            userAnswers.append(answerId)
                            answers[questionId] = userAnswers
                        }
                        
                    case .singleChoice:
                        if answers[questionId] as? String == answerId {
                            answers.removeValue(forKey: questionId)
                        } else {
                            answers[questionId] = answerId
                        }
                    }
                }
            }
        })
    }
}

//MARK: - Update/Add/Convert user answers
extension QuestionnaireViewManager {
    
    private func showExceedLimitAlert() {
        viewInput?.showAlert(title: L10n.Questionnaire.Alert.LoadImage.Error.ExceedLimit.Title.text,
                             message: L10n.Questionnaire.Alert.LoadImage.Error.ExceedLimit.Body.text,
                             actions: [UIAlertAction.init(title: L10n.Common.Ok.text,
                                                          style: .default,
                                                          handler: {_ in })])
    }
    
    private func isPicturesExceedLimit() -> Bool {
        guard let groupAnswers = view?.lastGroupQuestionView?.getAnswers() else { return false }
        for answer in groupAnswers {
            if case .image(let answerSheet) = answer {
                if answerSheet.answer.count > Constants.maxImagesAnswer {
                    showExceedLimitAlert()
                    return true
                }
            }
        }
        
        return false
    }
    
    private func addPoiUpdateAnswer() {
        guard let answer = view?.addPoiView?.getAnswer() else { return }
        convertAndUpdateQuestionnaireAnswers(answers: [answer])
    }
    
    /// collect answers to `answers` list
    private func updateAnswers() {
        guard let groupAnswers = view?.lastGroupQuestionView?.getAnswers() else { return }
        convertAndUpdateQuestionnaireAnswers(answers: groupAnswers)
        
        for (index, question) in groups[currentIndex].questions.enumerated() {
            switch question {
            case let .checkboxSet(checkboxSet):
                
                switch checkboxSet.answerSheet.mode {
                case .boolean:
                    let currentAnswer = answers[checkboxSet.questionId] as? Bool
                    
                    let answerItems = [
                        LTPAnswer.ViewModel(id: trueAnswerKey, text: L10n.Common.Yes.text,
                                            isChecked: currentAnswer == true ? true : false),
                        LTPAnswer.ViewModel(id: falseAnswerKey, text: L10n.Common.No.text,
                                            isChecked: currentAnswer == false ? true : false)
                    ]
                    groups[currentIndex].questions[index] = .checkboxSet(.init(question: checkboxSet.question,
                                                                               answerSheet: .init(mode: .boolean,
                                                                                                  items: answerItems),
                                                                               questionId: checkboxSet.questionId,
                                                                               condition: checkboxSet.condition))
                case .multiChoice:
                    let currentAnswers = answers[checkboxSet.questionId] as? [String]
                    let answerItems = checkboxSet.answerSheet.items.map({
                        LTPAnswer.ViewModel(id: $0.id,
                                            text: $0.text,
                                            isChecked: currentAnswers?.contains($0.id) ?? false)
                    })
                    groups[currentIndex].questions[index] = .checkboxSet(.init(question: checkboxSet.question,
                                                                               answerSheet: .init(mode: .multiChoice,
                                                                                                  items: answerItems),
                                                                               questionId: checkboxSet.questionId,
                                                                               condition: checkboxSet.condition))
                    
                case .singleChoice:
                    let currentAnswerId = answers[checkboxSet.questionId] as? String
                    let answerItems = checkboxSet.answerSheet.items.map({
                        LTPAnswer.ViewModel(id: $0.id,
                                            text: $0.text,
                                            isChecked: ($0.id == currentAnswerId))
                    })
                    groups[currentIndex].questions[index] = .checkboxSet(.init(question: checkboxSet.question,
                                                                               answerSheet: .init(mode: .singleChoice,
                                                                                                  items: answerItems),
                                                                               questionId: checkboxSet.questionId,
                                                                               condition: checkboxSet.condition))
                }
            case let .input(input):
                var currentAnswer: String?
                if let userAnswer: Int = answers[input.questionId] as? Int {
                    currentAnswer = "\(userAnswer)"
                }
                groups[currentIndex].questions[index] = .input(.init(questionId: input.questionId,
                                                                     formTextFieldViewModel: .init(title: input.formTextFieldViewModel.title,
                                                                                                   placeholder: input.formTextFieldViewModel.placeholder,
                                                                                                   type: input.formTextFieldViewModel.type,
                                                                                                   value: currentAnswer), condition: input.condition))
            case let .picture(imageAnswerSheet):
                let imagesForQuestion = imageAnswers.filter({ $0.questionId == imageAnswerSheet.questionId }).first
                let images = imagesForQuestion.flatMap({ $0.answer.map({ $0.0 }) })
                groups[currentIndex].questions[index] = .picture(.init(questionId: imageAnswerSheet.questionId,
                                                                       imagePickerViewModel: .init(title: imageAnswerSheet.imagePickerViewModel.title,
                                                                                                   images: images ?? []), condition: imageAnswerSheet.condition))
                
            }
        }
    }
    
    private func convertAndUpdateQuestionnaireAnswers(answers: [TypeOfAnswer]) {
        
        for answerType in answers {
            switch answerType {
            case let .boolean(checkboxAnswer):
                let answerId = checkboxAnswer.answer?.id
                if answerId == self.answers[checkboxAnswer.questionId] as? String {
                    self.answers.removeValue(forKey: checkboxAnswer.questionId)
                } else if let isChecked = checkboxAnswer.answer?.isChecked {
                    self.answers[checkboxAnswer.questionId] = isChecked
                }
            case let .checkboxSet(checkboxSetAnswer):
                let checkedIds = checkboxSetAnswer.answer
                    .filter { $0.isChecked }
                    .map { $0.id }
                self.answers[checkboxSetAnswer.questionId] = checkedIds
            case let .dropdown(dropdownAnswer):
                self.answers[dropdownAnswer.questionId] = dropdownAnswer.answer?.id
            case let .string(stringAnswer):
                self.answers[stringAnswer.questionId] = stringAnswer.answer
            case let .int(intAnswer):
                self.answers[intAnswer.questionId] = intAnswer.answer
            case let .image(imageSetAnswer):
                if let row = imageAnswers.firstIndex(where: {$0.questionId == imageSetAnswer.questionId}) {
                    imageAnswers[row] = imageSetAnswer
                } else {
                    imageAnswers.append(imageSetAnswer)
                }
            }
        }
    }
}
