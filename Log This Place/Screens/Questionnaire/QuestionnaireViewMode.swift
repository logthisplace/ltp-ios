//
//  QuestionnaireViewModel.swift
//  Log This Place
//
//

import Foundation

enum QuestionnaireViewMode {
    
    /// create a new POI/new AnswerSheet
    enum Add {
        /// add a new POI, create new POI and new AnswerSheet
        struct PoiInput {
            let address: String?
            let placeName: String
            let googleId: String
            let location: PoiLocation?
        }
        
        /// add a new answer sheet to exist POI
        struct AnswerSheetInput {
            let placeId: String
            let category: PointOfInterestType?
            let placeName: String
            let address: String?
        }
        
        case poi(input: PoiInput)
        case answerSheet(input: AnswerSheetInput)
        case profile
    }
    
    /// edit Answer Sheet or Profile
    enum Edit {
        
        struct AnswerSheetInput {
            let groups: [QuestionnaireGroupViewManagerInput]
            let tappedGroupId: Int
            let answerSheet: AnswerSheet
            let placeId: String
            /// need for remove image and check
            let currentlyDisplayedImageNamesForQuestions: [String: [String]]?
        }
        
        struct UserProfileInput {
            let userPassport: UserPassport?
            let groups: [QuestionnaireGroupViewManagerInput]
            let tappedGroupId: Int
        }
        
        case answerSheet(input: AnswerSheetInput)
        case profile(input: UserProfileInput)
    }
    
    case add(input: Add)
    case edit(input: Edit)
}
