//
//  Log This Place
//
//

import Foundation

protocol QuestionnaireViewHelperManagerProtocol {
    func canShowConditionItemBasedOnAnswers(condition: Condition, answers: [String: Any]) -> Bool
    func getConditionByQuestion(question: QuestionnaireViewManagerTypeOfField) -> Condition?
    func mapQuestionsToDict(_ questions: [QuestionnaireViewManagerTypeOfField]) -> [String: Any]
    func getExtraQuestionIds(_ questions: [QuestionnaireViewManagerTypeOfField]) -> [String]
    func filterExtraQuestionsByAnswers(questions: [QuestionnaireViewManagerTypeOfField],
                                       userAnswers: [String: Any]) -> [String]
    func getQuestionIdByAnswerId(answerId: String, userAnswers: [String: Any]) -> String?
}

/// A class that helps to work with follow-up questions
class QuestionnaireViewHelper: QuestionnaireViewHelperManagerProtocol {
    
    private let conditionManager: ConditionManagerProtocol = AppContainer.resolve()
    
    /// generation dicrionary[String: Any] based on questions[QuestionnaireViewManagerTypeOfField]
    func mapQuestionsToDict(_ questions: [QuestionnaireViewManagerTypeOfField]) -> [String: Any] {
        var answers = [String: Any]()
        for question in questions {
            switch question {
            case let .checkboxSet(data):
                switch data.answerSheet.mode {
                case .boolean:
                    let currentAnswer = data.answerSheet.items.filter { $0.isChecked }.first
                    answers[data.questionId] = currentAnswer?.id
                case .multiChoice:
                    answers[data.questionId] = data.answerSheet.items
                        .filter { $0.isChecked }
                        .map{ $0.id }
                case .singleChoice:
                    answers[data.questionId] = data.answerSheet.items
                        .filter { $0.isChecked }
                        .map{ $0.id }
                        .first
                }
            case .picture: break
            case let .input(input):
                answers[input.questionId] = input.formTextFieldViewModel.value
            }
        }
        return answers
    }
    
    /// get condition field with contains question/answer id's
    func getConditionByQuestion(question: QuestionnaireViewManagerTypeOfField) -> Condition? {
        switch question {
        case let .checkboxSet(checkboxSet):
            if checkboxSet.condition != nil { return checkboxSet.condition }
        case let .input(input):
            if input.condition != nil  { return input.condition }
        case let .picture(picture):
            if picture.condition != nil { return picture.condition }
        }
        return nil
    }
    
    /// return questions(id) from questions
    func getExtraQuestionIds(_ questions: [QuestionnaireViewManagerTypeOfField]) -> [String] {
        mapQuestionsToIds(questions.filter({
            return getConditionByQuestion(question: $0) != nil
        }))
    }
    
    private func mapQuestionsToIds(_ questions: [QuestionnaireViewManagerTypeOfField]) -> [String] {
        questions.map({
            switch $0 {
            case let .checkboxSet(checkboxSet):
                return checkboxSet.questionId
            case let .input(input):
                return input.questionId
            case let .picture(picture):
                return picture.questionId
            }
        })
    }
    
    
    func filterExtraQuestionsByAnswers(questions: [QuestionnaireViewManagerTypeOfField],
                                       userAnswers: [String: Any]) -> [String] {
        mapQuestionsToIds(questions.filter({
            guard let condition = getConditionByQuestion(question: $0) else { return false }
            return canShowConditionItemBasedOnAnswers(condition: condition, answers: userAnswers)
        }))
    }
    
    func getQuestionIdByAnswerId(answerId: String, userAnswers: [String: Any]) -> String? {
        for (questionId, val) in userAnswers {
            if questionId == answerId { return questionId }
            
            if let answers = val as? [String] {
                let userAnswer = answers.filter({ $0 == answerId }).first
                if userAnswer != nil {
                    return questionId
                }
            }
            
            if let answer = val as? String {
                if answer == answerId {
                    return questionId
                }
            }
        }
        
        return nil
    }
    
    func canShowConditionItemBasedOnAnswers(condition: Condition,
                                            answers: [String: Any]) -> Bool {
        return conditionManager.isConditionBasedOnAnswers(condition: condition,
                                                          answeSheet: answers,
                                                          generateAnswersForNotCondition: false)
    }
}
