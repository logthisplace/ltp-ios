//
//  QQ.swift
//  Log This Place
//
//

import UIKit

enum TypeQuestionnaireView {
    case addNewPoi
    case questionnaireType(input: QuestionnaireViewType)
    
    struct QuestionnaireViewType {
        let editing: Bool
        let previousNextButtonEnabled: Bool
    }
}

final class QuestionnaireContainerContentView: BaseScrollView, Fillable {
    
    struct ViewModel {
        let view: UIView
        let typeQuestionnaireView: TypeQuestionnaireView
    }
    
    let previousButton = with(LTPButton.standard(style: .gray)) {
        $0.setTitle(L10n.Common.Back.text, for: .normal)
        $0.isHidden = true
    }
    let nextButton = with(LTPButton.standard(style: .black)) {
        $0.setTitle(L10n.Common.Next.text, for: .normal)
        $0.isHidden = true
    }
    let saveButton = with(LTPButton.standard(style: .black)) {
        $0.setTitle(L10n.Common.Save.text, for: .normal)
        $0.isHidden = true
    }
    let buttonsContainer = UIView()
    let buttonsStackView = with(UIStackView()) {
        $0.spacing = 8
        $0.distribution = .fillEqually
    }

    private let rootStackView = with(UIStackView.vertical) {
        $0.edgeInsets(topSpacing: 12, bottomSpacing: 0, leadingSpacing: 0, trailingSpacing: 0)
    }
    
    override func setupUI() {
        super.setupUI()
        backgroundColor = UIColor.LogThisPlace.backgroundGray
        scrollableContentView.addSubview(rootStackView)

        buttonsStackView.addArrangedSubviews([
            previousButton,
            nextButton,
            saveButton,
        ])
        
        buttonsContainer.addSubview(buttonsStackView)
    }
    
    override func setupConstraints() {
        super.setupConstraints()
        rootStackView.positionToTheEdges()

        buttonsStackView.positionUsingScreenMargins(leadingSpacing: 16, trailingSpacing: 16)
        buttonsContainer.positionToTheEdges()
    }

    private func setTypeOfView(type: TypeQuestionnaireView) {
        switch type {
        case .addNewPoi:
            previousButton.isHidden = false
            previousButton.set(enabled: false)
            nextButton.isHidden = false
            //confirmButton.isHidden = true
        case let .questionnaireType(input):
            previousButton.isHidden = input.editing
            previousButton.set(enabled: !input.editing)
            nextButton.set(enabled: input.previousNextButtonEnabled)
            nextButton.isHidden = input.editing
            saveButton.isHidden = !input.editing
        }
    }
    
    func setNextButtonTitle(_ title: String) {
        nextButton.setTitle(title, for: .normal)
    }
    
    func setNextButton(_ enabled: Bool) {
        nextButton.set(enabled: enabled)
    }
}

extension QuestionnaireContainerContentView {
    func fill(with model: ViewModel) {
        rootStackView.removeSubviews()
        
        let containerView = model.view
        rootStackView.addArrangedSubview(containerView)
        rootStackView.addArrangedSubview(buttonsContainer)
        
        setTypeOfView(type: model.typeQuestionnaireView)
    }
}
