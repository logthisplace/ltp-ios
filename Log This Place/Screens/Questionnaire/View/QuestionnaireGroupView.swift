//
//  Log This Place
//

import UIKit


final class QuestionnaireGroupView: BaseView {
    
    var onAnswerTapped: ((String?, String) -> Void)?

    private let rootStackView = UIStackView.vertical

    var inputs = [AnswerProvideable]()

    private let imagePickerManager: ImagePickerManagerProtocol
    var onImageTapped: (([ImageSource], Int) -> Void)?

    private var viewHelper: QuestionnaireViewHelperManagerProtocol
    
    init(imagePickerManager: ImagePickerManagerProtocol,
         viewHelper: QuestionnaireViewHelperManagerProtocol) {
        self.imagePickerManager = imagePickerManager
        self.viewHelper = viewHelper
        super.init()
    }
    
    override func setupUI() {
        super.setupUI()
        addSubview(rootStackView)
    }
    
    override func setupConstraints() {
        super.setupConstraints()
        rootStackView.positionToTheEdges()
        rootStackView.spacing = 12
    }
    
    func fill(group: QuestionnaireGroupViewManagerInput) {
        let userAnswers: [String: Any] = viewHelper.mapQuestionsToDict(group.questions)

        for question in group.questions {
            switch question {
            case let .checkboxSet(data):
                let questionView = QuestionnaireCheckboxSetView(questionId: data.questionId)
                questionView.fill(with: data)
                questionView.onAnswerTap = { [weak self] questionId, answerId in
                    self?.onAnswerTapped?(questionId, answerId)
                }
                questionView.positionToTheEdges()
                rootStackView.addArrangedSubview(questionView)
                inputs.append(questionView)
                extraQuestionChecker(question: question, userAnswers: userAnswers, view: questionView)
            case let .picture(data):
                let imagePicker = QuestionnaireImagePickerView(questionId: data.questionId)
                imagePicker.onImageTapped = onImageTapped
                imagePicker.imagePickerManager = imagePickerManager
                imagePicker.fill(with: .init(title: data.imagePickerViewModel.title,
                                             images: data.imagePickerViewModel.images))
                rootStackView.addArrangedSubview(imagePicker)
                inputs.append(imagePicker)
                extraQuestionChecker(question: question, userAnswers: userAnswers, view: imagePicker)
            case let .input(data):
                let input = QuestionnaireLTPFormTextField(questionId: data.questionId)
                input.fill(with: .init(title: data.formTextFieldViewModel.title,
                                       placeholder: data.formTextFieldViewModel.placeholder,
                                       type: data.formTextFieldViewModel.type,
                                       value: data.formTextFieldViewModel.value))
                rootStackView.addArrangedSubview(input)
                inputs.append(input)
                extraQuestionChecker(question: question, userAnswers: userAnswers, view: input)
            }
        }
    }
    
    func getAnswers() -> [TypeOfAnswer] {
        inputs.map { $0.answer() }
    }
}

//MARK: - Extra question logic
extension QuestionnaireGroupView {
    
    func extraQuestionChecker(question: QuestionnaireViewManagerTypeOfField,
                                 userAnswers: [String: Any],
                                 view: QuestionnaireViewProvideable) {
        guard let condition = viewHelper.getConditionByQuestion(question: question) else { return }
        if viewHelper.canShowConditionItemBasedOnAnswers(condition: condition, answers: userAnswers) {
            view.showQuestion()
        } else {
            view.hideQuestion()
        }
    }
}
