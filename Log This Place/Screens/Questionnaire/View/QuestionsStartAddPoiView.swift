//
//  QuestionsStartAddNewPoiView.swift
//  Log This Place
//
//

import UIKit


final class QuestionsStartAddPoiView: BaseView, Fillable {
    
    struct ViewModel {
        let address: String?
        let placeName: String
        let question: LTPCheckboxView.ViewModel?
        let answers: [String: Any]
    }
    
    private let logoImageContainerView = UIView()
    private let logoImage = with(UIImageView()) {
        $0.image = Asset.logo.image
    }
    private let titleLabel = with(UILabel.titleH2Bold) {
        $0.numberOfLines = 0
        $0.text = L10n.Questionnaire.AddNewPoi.Title.text
        $0.textAlignment = .center
    }
    private let subtitleLabel = with(UILabel.calloutRegular) {
        $0.numberOfLines = 0
        $0.text = L10n.Questionnaire.AddNewPoi.Subtitle.text
        $0.textAlignment = .center
    }
    private let rootStackView = with(UIStackView.vertical) {
        $0.spacing = 12
    }
    private let questionsStackView = with(UIStackView.vertical) {
        $0.spacing = 12
    }
    private let headerStackView = with(UIStackView.vertical) {
        $0.edgeInsets(topSpacing: 16, bottomSpacing: 16, leadingSpacing: 16, trailingSpacing: 16)
        $0.spacing = 6
        $0.backgroundColor = .white
    }
    var questionView: QuestionnaireCheckboxSetView?
    
    override func setupUI() {
        super.setupUI()
        addSubview(rootStackView)
        
        logoImageContainerView.addSubview(logoImage)
        
        headerStackView.addArrangedSubviews([
            logoImageContainerView,
            titleLabel,
            subtitleLabel
        ])
        
        rootStackView.addArrangedSubviews([
            headerStackView,
            questionsStackView
        ])
    }
    
    override func setupConstraints() {
        super.setupConstraints()
        
        rootStackView.positionToTheEdges()
        
        logoImageContainerView.snp.makeConstraints { make in
            make.height.equalTo(50)
        }
        logoImage.snp.makeConstraints { make in
            make.center.equalToSuperview()
            make.size.equalTo(45)
        }
    }
}

extension QuestionsStartAddPoiView {
    func fill(with model: ViewModel) {

        questionsStackView.addArrangedSubview(generationInformationStackView(title: L10n.Questionnaire.Name.text,
                                                                             information: model.placeName))
        
        questionsStackView.addArrangedSubview(generationInformationStackView(title: L10n.Questionnaire.Address.text,
                                                                             information: model.address ?? L10n.Common.NotFound.text))

        
        if var question = model.question {
            let currentAnswerId = model.answers[question.questionId] as? String
            let answerItems = question.answerSheet.items.map({
                LTPAnswer.ViewModel(id: $0.id,
                                    text: $0.text,
                                    isChecked: ($0.id == currentAnswerId))
            })
            question.answerSheet = .init(mode: .singleChoice,
                                    items: answerItems)
            
            let questionView = QuestionnaireCheckboxSetView(questionId: question.questionId)
            questionView.fill(with: question)
            
            self.questionView = questionView
            questionsStackView.addArrangedSubview(questionView)
        }
    }
    
    func generationInformationStackView(title: String, information: String) -> UIStackView {
        let containerStackView = with(UIStackView.vertical) {
            $0.spacing = 8
            $0.edgeInsets(topSpacing: 16, bottomSpacing: 16, leadingSpacing: 16, trailingSpacing: 16)
            $0.background(color: .white)
        }
        let titleLabel = with(UILabel.headlineMediumSemiBold) {
            $0.numberOfLines = 0
        }
        let informationLabel = with(UILabel.calloutRegular) {
            $0.numberOfLines = 0
        }
        
        containerStackView.addArrangedSubviews([
            titleLabel,
            informationLabel
        ])
        
        titleLabel.text = title
        informationLabel.text = information
        
        return containerStackView
    }
    
    func getAnswer() -> TypeOfAnswer? {
        return questionView?.answer()
    }
}

