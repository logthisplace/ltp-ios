//
//  Log This Place
//

import UIKit

protocol QuestionnaireQuestionsViewManagable: AnyObject {
    var lastGroupQuestionView: QuestionnaireGroupView? { get }
    var addPoiView: QuestionsStartAddPoiView? { get }
    
    func showQuestionnaireGroup(group: QuestionnaireGroupViewManagerInput, editing: Bool, previousNextButtonEnabled: Bool)
    func showAddPoi(_ poi: QuestionsStartAddPoiView.ViewModel)
    func showProgressIndicator(currentQuestion: Int, maxQuestions: Int)
    func setNextButtonTitle(_ title: String)
    func selectSingleItem(questionId: String, answerId: String)
    func deselectItem(questionId: String, answerId: String)
    func setNextButton(enabled: Bool)
    func hideExtraQuestion(conditionQuestionId: String)
    func showExtraQuestion(conditionQuestionId: String)
}

final class QuestionnaireQuestionsView: BaseView, QuestionnaireQuestionsViewManagable, KeyboardHandlable {
    
    let headerView = UIView()
    let titleLabel = with(UILabel.headlineMediumSemiBold) {
        $0.numberOfLines = 1
    }
    let closeButton = with(LTPButton.standard(style: .clear)) {
        $0.setTitle(L10n.Common.Exit.text, for: .normal)
    }
    let progressIndicatorStackView = with(UIStackView()) {
        $0.axis = .horizontal
        $0.distribution = .fillEqually
    }
    
    /// Called when user taps on group on summary screen. Passes question id as argument.
    var onGroupTapped: ((Int) -> Void)?
    
    /// Called when user taps on answer. Passes question id and answer id as argument.
    var onAnswerTapped: ((String?, String) -> Void)?
    
    private let rootContainerContentView = UIView()
    let containerContentView = QuestionnaireContainerContentView()
    
    private var checkboxQuestionViews: [QuestionnaireCheckboxSetView] {
        // TODO: - change getting QuestionnaireContainerContentView
        let questionGroup = (rootContainerContentView.subviews.first as? QuestionnaireContainerContentView)?.subviews.first?.subviews.first?.subviews.first?.subviews.first as? QuestionnaireGroupView
        let questions = questionGroup?.subviews.first?.subviews.filter({ $0 is QuestionnaireCheckboxSetView }) as? [QuestionnaireCheckboxSetView]
        return questions ?? []
    }
    
    private var questionViews: [QuestionnaireViewProvideable] {
        // TODO: - change getting QuestionnaireContainerContentView
        let questionGroup = (rootContainerContentView.subviews.first as? QuestionnaireContainerContentView)?.subviews.first?.subviews.first?.subviews.first?.subviews.first as? QuestionnaireGroupView
        let questions = questionGroup?.subviews.first?.subviews.filter({ $0 is QuestionnaireViewProvideable }) as? [QuestionnaireViewProvideable]
        return questions ?? []
    }
    
    var lastGroupQuestionView: QuestionnaireGroupView?
    var addPoiView: QuestionsStartAddPoiView?
    //private var summaryView: ProfileSummaryView?
    
    private let imagePickerManager: ImagePickerManagerProtocol
    private let viewHelper: QuestionnaireViewHelperManagerProtocol
    
    var onImageTapped: (([ImageSource], Int) -> Void)?
    
    init(imagePickerManager: ImagePickerManagerProtocol,
         viewHelper: QuestionnaireViewHelperManagerProtocol) {
        self.imagePickerManager = imagePickerManager
        self.viewHelper = viewHelper
        super.init()
    }
    
    override func setupUI() {
        super.setupUI()
        
        headerView.addSubview(titleLabel)
        headerView.addSubview(closeButton)
        
        addSubviews([
            headerView,
            progressIndicatorStackView,
            rootContainerContentView
        ])
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        containerContentView.scrollView.setBottomInsets(to: keyboardHeightConstraint.constant)
        containerContentView.scrollView.verticalScrollIndicatorInsets.bottom += safeAreaInsets.bottom
    }
    
    override func setupConstraints() {
        super.setupConstraints()
        
        closeButton.snp.makeConstraints { make in
            make.top.bottom.trailing.equalToSuperview()
            make.centerY.equalToSuperview()
        }
        
        titleLabel.snp.makeConstraints { make in
            make.centerX.centerY.equalToSuperview()
        }
        
        headerView.snp.makeConstraints { make in
            make.top.equalToSuperview().inset(UIView.topSpacingUnderNavigationBar)
            make.leading.trailing.equalToSuperview().inset(UIView.horizontalScreenMargin)
            make.height.equalTo(50)
        }
        progressIndicatorStackView.snp.makeConstraints { make in
            make.top.equalTo(headerView.snp.bottom)
            make.leading.trailing.equalToSuperview()
            make.height.equalTo(3)
        }
        rootContainerContentView.snp.makeConstraints { make in
            make.top.equalTo(progressIndicatorStackView.snp.bottom)
            make.leading.trailing.bottom.equalToSuperview()
        }
    }
    
    func showAddPoi(_ poi: QuestionsStartAddPoiView.ViewModel) {
        hideProgressIndicator()
        
        let addNewPoiView = QuestionsStartAddPoiView()
        addNewPoiView.fill(with: poi)
        
        titleLabel.text = ""
        
        addPoiView = addNewPoiView
        lastGroupQuestionView?.removeFromSuperviewAnimation()
        
        containerContentView.fill(with: .init(view: addNewPoiView, typeQuestionnaireView: .addNewPoi))
        rootContainerContentView.addSubviewAnimation(containerContentView)
        containerContentView.positionToTheEdges()
        containerContentView.layoutIfNeeded()
    }
    
    func showQuestionnaireGroup(group: QuestionnaireGroupViewManagerInput,
                                editing: Bool,
                                previousNextButtonEnabled: Bool) {
        let questionsView = QuestionnaireGroupView(imagePickerManager: imagePickerManager,
                                                   viewHelper: viewHelper)
        questionsView.onImageTapped = onImageTapped
        questionsView.fill(group: group)
        questionsView.onAnswerTapped = { [weak self] questionId, answerId in
            self?.onAnswerTapped?(questionId, answerId)
        }
        
        titleLabel.text = group.groupName
        
        lastGroupQuestionView = questionsView
        
        containerContentView.fill(with: .init(view: questionsView,
                                              typeQuestionnaireView: .questionnaireType(input: .init(editing: editing,
                                                                                                     previousNextButtonEnabled: previousNextButtonEnabled))))
        rootContainerContentView.addSubviewAnimation(containerContentView)
        containerContentView.scrollView.setContentOffset(.zero, animated: false)
        containerContentView.positionToTheEdges()
        containerContentView.layoutIfNeeded()
    }
    
    private func hideProgressIndicator() {
        progressIndicatorStackView.removeAllArrangedSubviews()
        progressIndicatorStackView.isHidden = true
    }
    
    private func showProgressIndicator() {
        progressIndicatorStackView.removeAllArrangedSubviews()
        progressIndicatorStackView.isHidden = false
    }
    
    func setNextButtonTitle(_ title: String) {
        containerContentView.setNextButtonTitle(title)
    }
    
    func showProgressIndicator(currentQuestion: Int, maxQuestions: Int) {
        showProgressIndicator()
        for index in(0...maxQuestions-1) {
            let itemIndicatorView = UIView()
            if currentQuestion >= index {
                itemIndicatorView.backgroundColor = UIColor.LogThisPlace.purpure
            }
            progressIndicatorStackView.addArrangedSubview(itemIndicatorView)
        }
    }
    
    func setNextButton(enabled: Bool) {
        containerContentView.setNextButton(enabled)
    }
    
    func selectSingleItem(questionId: String, answerId: String) {
        checkboxQuestionViews.first(where: { $0.questionId == questionId })?.selectSingleItem(withId: answerId)
    }
    
    func deselectItem(questionId: String, answerId: String) {
        checkboxQuestionViews.first(where: { $0.questionId == questionId })?.deselectItem(withId: answerId)
    }
    
    func hideExtraQuestion(conditionQuestionId: String) {
        questionViews.first(where: { $0.questionId == conditionQuestionId })?.hideQuestion(duration: 0.5)
    }
    
    func showExtraQuestion(conditionQuestionId: String) {
        questionViews.first(where: { $0.questionId == conditionQuestionId })?.showQuestion(duration: 0.5)
    }
}
