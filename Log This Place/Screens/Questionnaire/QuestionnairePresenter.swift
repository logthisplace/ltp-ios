//
//  Log This Place
//

import Foundation
import UIKit
import DKPhotoGallery
import FirebaseFirestore

protocol QuestionnairePresenterProtocol: Presenter {
    var mode: QuestionnaireViewMode { get }
    
    func imageTapped(items: [ImageSource], presentationIndex: Int)
    func closeTapped()
    func saveQuestionnaireTapped(answers: [String: Any],
                                 imageAnswers: [Answer<[(image: ImageSource, id: String)]>])
}

final class QuestionnairePresenter: BasePresenter, QuestionnairePresenterProtocol {
    
    weak var view: QuestionnaireViewInput?
    var mode: QuestionnaireViewMode
    
    private let userPassportDataManager: UserPassportDataManagerProtocol = AppContainer.resolve()
    private let questionnaireDataManager: QuestionnaireDataManagerProtocol = AppContainer.resolve()
    private let userDefaultsManager: UserDefaultsManagerProtocol = AppContainer.resolve()
    private let poiDataManager: PointOfInterestDataManagerProtocol = AppContainer.resolve()
    private let firebaseAnalyticManager: FirebaseAnalyticManagerProtocol = AppContainer.resolve()
    private let answerSheetDataManager: AnswerSheetDataManagerProtocol = AppContainer.resolve()
    /// helpers for presenter
    private let questionnaireMapperManager: QuestionnaireMapperManagerProtocol = QuestionnaireMapperManager()
    private var pictureManager: QuestionnairePictureManagerProtocol = QuestionnairePictureManager()
    
    private let coordinator: QuestionnaireCoordinatorProtocol
    
    /// need for summary screen then
    private var questionnaire: Questionnaire?
    /// for images
    private var currentlyDisplayedImageNamesForQuestions = [String: [String]]()
    /// defines the type `poi` or `user`
    private var questionnaireTypeMode: QuestionnaireType {
        switch mode {
        case let .add(input):
            switch input {
            case .poi:
                return .poi
            case .answerSheet:
                return .poi
            case .profile:
                return .user
            }
        case let .edit(input):
            switch input {
            case .answerSheet:
                return .poi
            case .profile:
                return .user
            }
        }
    }
    
    init(mode: QuestionnaireViewMode,
         coordinator: QuestionnaireCoordinatorProtocol) {
        self.mode = mode
        self.coordinator = coordinator
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        fetchData()
    }
}

//MARK: - TAP
extension QuestionnairePresenter {
    
    func closeTapped() {
        view?.showAlert(title: L10n.Common.Warning.text, message: L10n.Alert.Close.text, actions: [
            UIAlertAction.init(title: L10n.Common.Exit.text, style: .default, handler: { [weak self] (action) in
                self?.coordinator.closeQuestionnaire()
            }),
            UIAlertAction.init(title: L10n.Common.Cancel.text, style: .default, handler: { (action) in }),
        ])
    }
    
    func imageTapped(items: [ImageSource], presentationIndex: Int) {
        var galleryItems = [DKPhotoGalleryItem]()
        items.forEach({
            switch $0 {
            case let .link(link):
                guard let imageURL = URL(string: link) else { return }
                galleryItems.append(.init(imageURL: imageURL))
            case let .media(media):
                galleryItems.append(.init(image: media))
            }
        })
        coordinator.showGallery(items: galleryItems, presentationIndex: presentationIndex)
    }
    
    func saveQuestionnaireTapped(answers: [String: Any],
                                 imageAnswers: [Answer<[(image: ImageSource, id: String)]>]) {
        switch mode {
        case let .add(input):
            switch input {
            case let .poi(input):
                addPoi(input: input, answers: answers, imageAnswers: imageAnswers)
            case let .answerSheet(input):
                createAnswerSheetAndImages(input: input, answers: answers, imageAnswers: imageAnswers)
            case .profile:
                updateProfile(answers: answers)
            }
        case let .edit(input):
            switch input {
            case let .answerSheet(input):
                updateAnswerSheetAndImages(input: input, answers: answers, imageAnswers: imageAnswers)
            case .profile:
                updateProfile(answers: answers)
            }
        }
    }
}

//MARK: - Prepare And Show Data(POI or User)
extension QuestionnairePresenter {
    
    /// Receiving questions. You can get 2 types of question - for the user and for the POI
    private func fetchData() {
        view?.showLoading(fullscreen: true)
        questionnaireDataManager.getQuestionnaire(questionnaireType: questionnaireTypeMode) { [weak self] result in
            guard let self = self else { return }
            switch result {
            case let .success(questionnaire):
                self.questionnaire = questionnaire
                self.handle(questionnaire)
            case .failure:
                self.coordinator.failedToFetchData()
            }
        }
    }
    
    private func handle(_ questionnaire: Questionnaire) {
        let viewModelGroups = questionnaireMapperManager.convertToQuestionnaireViewModel(questionnaire: questionnaire,
                                                                                         answerSheet: [:])
        switch mode {
        case let .add(input):
            switch input {
            case let .poi(input):
                self.firebaseAnalyticManager.logEvent(screen: .addPoi)
                let categoryQuestion = questionnaireMapperManager.getCategoryQuestion(questionnaire: questionnaire)
                view?.addPoi(poi: .init(address: input.address,
                                        placeName: input.placeName,
                                        question: categoryQuestion,
                                        answers: [:]), groups: viewModelGroups)
            case let .answerSheet(input):
                self.firebaseAnalyticManager.logEvent(screen: .addAnswerSheet)
                let categoryQuestion = questionnaireMapperManager.getCategoryQuestion(questionnaire: questionnaire)
                view?.addPoi(poi: .init(address: input.address,
                                        placeName: input.placeName,
                                        question: categoryQuestion,
                                        answers: [:]), groups: viewModelGroups)
            case .profile:
                self.firebaseAnalyticManager.logEvent(screen: .addProfileSheet)
                view?.show(questionnaireTypeMode: questionnaireTypeMode,
                           groups: viewModelGroups)
            }
        case let .edit(input):
            switch input {
            case let .answerSheet(input):
                self.firebaseAnalyticManager.logEvent(screen: .editAnswerSheet)
                currentlyDisplayedImageNamesForQuestions = input.currentlyDisplayedImageNamesForQuestions ?? [:]
                view?.showQuestionnaireGroup(groups: input.groups,
                                             groupIndex: input.tappedGroupId)
            case let .profile(input):
                self.firebaseAnalyticManager.logEvent(screen: .editProfileSheet)
                view?.showQuestionnaireGroup(groups: input.groups,
                                             groupIndex: input.tappedGroupId)
            }
        }
        view?.hideLoading()
    }
}

//MARK: - POI/AnswerSheet: Update/Create
extension QuestionnairePresenter {
    
    private func userAnswersIsEmpty(answers: [String: Any],
                                    imageAnswers: [Answer<[(image: ImageSource, id: String)]>]?,
                                    errorMessage: String) -> Bool {
        let imageAnswersIsEmpty = imageAnswers?.filter({ !$0.answer.isEmpty }).count ?? 0 == 0
        if answers.isEmpty && imageAnswersIsEmpty {
            view?.showErrorAlert(message: errorMessage)
            return true
        }
        return false
    }
    
    private func addPoi(input: QuestionnaireViewMode.Add.PoiInput,
                        answers: [String: Any],
                        imageAnswers: [Answer<[(image: ImageSource, id: String)]>]? = nil) {
        if userAnswersIsEmpty(answers: answers, imageAnswers: imageAnswers, errorMessage: L10n.Questionnaire.AddNewPoi.Empty.Error.text) {
            return
        }
        
        view?.showLoading(fullscreen: true)
        
        /// create new Point Of Interested
        poiDataManager.createPointOfInterest(newPoi: .init(googleId: input.googleId)) { [weak self] result in
            guard let self = self else { return }
            switch result {
            case let .success(newPoi):
                /// add to analytic
                self.firebaseAnalyticManager.logEvent(action: .addedPoi)
                
                guard let newPoiId = newPoi?.id else {
                    self.view?.hideLoading()
                    self.view?.showErrorAlert(message: L10n.Common.SomethingWentWrong.text)
                    return
                }
                
                let answerSheetRerence = self.answerSheetDataManager.answerSheetReference(poiId: newPoiId)
                
                ///upload images
                if let imageAnswers = imageAnswers {
                    self.handlePictures(imageAnswers: imageAnswers,
                                        currentlyDisplayedImageNamesForQuestions: self.currentlyDisplayedImageNamesForQuestions,
                                        poiId: newPoiId,
                                        sheetId: answerSheetRerence.documentID) {
                        /// set data to answer sheet reference
                        self.processDataToNewPoiAnswerSheetReference(poi: newPoi,
                                                                     input: input,
                                                                     answerSheetRerence: answerSheetRerence,
                                                                     answers: answers)
                    }
                } else {
                    /// set data to answer sheet reference
                    self.processDataToNewPoiAnswerSheetReference(poi: newPoi,
                                                                 input: input,
                                                                 answerSheetRerence: answerSheetRerence,
                                                                 answers: answers)
                }
                
            case let .failure(error):
                self.view?.hideLoading()
                self.view?.showErrorAlert(message: error.localizedDescription)
            }
        }
    }
    
    
    private func updateAnswerSheet(poiId: String,
                                   answerSheet: AnswerSheet) {
        answerSheetDataManager.updateAnswerSheet(poiId: poiId,
                                                 answerSheet: answerSheet) { [weak self] result in
            guard let self = self else { return }
            self.view?.hideLoading()
            
            switch result {
            case .success:
                self.firebaseAnalyticManager.logEvent(action: .updatedPoi)
                self.coordinator.didUpdatePoiAnswerSheet()
                
            case let .failure(error):
                self.view?.showErrorAlert(message: error.localizedDescription)
            }
        }
    }
    
    private func updateAnswerSheetAndImages(input: QuestionnaireViewMode.Edit.AnswerSheetInput,
                                            answers: [String: Any],
                                            imageAnswers: [Answer<[(image: ImageSource, id: String)]>]? = nil) {
        view?.showLoading(fullscreen: true)
        
        var answerSheet = input.answerSheet
        answerSheet.answers = answers
        
        /// check images and if images is exist then upload images first then update answerSheet
        if let imageAnswers = imageAnswers,
           let answerSheetId = answerSheet.id {
            handlePictures(imageAnswers: imageAnswers,
                           currentlyDisplayedImageNamesForQuestions: currentlyDisplayedImageNamesForQuestions,
                           poiId: input.placeId,
                           sheetId: answerSheetId) { [weak self] in
                self?.updateAnswerSheet(poiId: input.placeId,
                                        answerSheet: answerSheet)
            }
        } else {
            updateAnswerSheet(poiId: input.placeId,
                              answerSheet: answerSheet)
        }
    }
    
    private func createAnswerSheetAndImages(input: QuestionnaireViewMode.Add.AnswerSheetInput,
                                            answers: [String: Any],
                                            imageAnswers: [Answer<[(image: ImageSource, id: String)]>]? = nil) {
        if userAnswersIsEmpty(answers: answers, imageAnswers: imageAnswers, errorMessage: L10n.Questionnaire.UpdatePoi.Empty.Error.text) {
            return
        }
        
        let poiId = input.placeId
        let answerSheetRerence = answerSheetDataManager.answerSheetReference(poiId: poiId)
        
        view?.showLoading(fullscreen: true)
        
        ///upload images
        if let imageAnswers = imageAnswers {
            handlePictures(imageAnswers: imageAnswers,
                           currentlyDisplayedImageNamesForQuestions: self.currentlyDisplayedImageNamesForQuestions,
                           poiId: poiId,
                           sheetId: answerSheetRerence.documentID) { [weak self] in
                self?.processDataToOldPoiAnswerSheetReference(input: input, answerSheetRerence: answerSheetRerence, answers: answers)
            }
        } else {
            processDataToOldPoiAnswerSheetReference(input: input, answerSheetRerence: answerSheetRerence, answers: answers)
        }
    }
}

//MARK: - Process answerSheet for old and new POI. Call coordinator when succes result
extension QuestionnairePresenter {
    private func processDataToNewPoiAnswerSheetReference(poi: PointOfInterest?,
                                                         input: QuestionnaireViewMode.Add.PoiInput,
                                                         answerSheetRerence: DocumentReference,
                                                         answers: [String: Any]) {
        guard let poi = poi,
              let poiId = poi.id else {
                  view?.hideLoading()
                  view?.showErrorAlert(message: L10n.Common.SomethingWentWrong.text)
                  return
              }
        
        answerSheetDataManager.setDataToAnswerSheetReference(answerSheetReference: answerSheetRerence,
                                                             answerSheet: AnswerSheet(answers: answers)) { [weak self] result in
            switch result {
                
            case .success:
                self?.view?.hideLoading()
                self?.coordinator.showPoiSummary(viewModel: .init(processType: .add(input: .poi(input: .init(newPoi: poi, location: input.location?.coreLocation))), input: .init(placeId: poiId, placeName: input.placeName, placeAddress: input.address, questionnaire: self?.questionnaire)))
                
            case let .failure(error):
                //TODO: - Handle error, remove all images from storage
                self?.view?.hideLoading()
                self?.view?.showErrorAlert(message: error.localizedDescription)
            }
        }
    }
    
    private func processDataToOldPoiAnswerSheetReference(input: QuestionnaireViewMode.Add.AnswerSheetInput,
                                                         answerSheetRerence: DocumentReference,
                                                         answers: [String: Any]) {
        answerSheetDataManager.setDataToAnswerSheetReference(answerSheetReference: answerSheetRerence,
                                                             answerSheet: AnswerSheet(answers: answers)) { [weak self] result in
            switch result {
            case .success:
                self?.view?.hideLoading()
                self?.coordinator.showPoiSummary(viewModel: .init(processType: .add(input: .answerSheet),
                                                                                         input: .init(placeId: input.placeId,
                                                                                                      placeName: input.placeName,
                                                                                                      placeAddress: input.address,
                                                                                                      questionnaire: self?.questionnaire)))
            case let .failure(error):
                self?.view?.hideLoading()
                self?.view?.showErrorAlert(message: error.localizedDescription)
            }
        }
    }
}

//MARK: - Profile
extension QuestionnairePresenter {
    
    private func updateProfile(answers: [String: Any]) {
        view?.showLoading(fullscreen: true)
        
        let userPassport = UserPassport(answers: answers)
        userPassportDataManager.setAnswers(userPassport: userPassport) { [weak self] result in
            guard let self = self else { return }
            self.view?.hideLoading()
            switch result {
            case let .failure(error):
                self.view?.showErrorAlert(message: error.localizedDescription)
            case .success:
                if !self.userDefaultsManager.wasWelcomeShown {
                    self.userDefaultsManager.wasWelcomeShown = true
                }
                
                if case let .add(add) = self.mode, case .profile = add {
                    self.coordinator.didCreateProfileAnswerSheet()
                } else if case let .edit(edit) = self.mode, case .profile = edit {
                    self.coordinator.didUpdateProfileAnswerSheet(userPassport)
                }
            }
        }
    }
}


//MARK: - Handle images
extension QuestionnairePresenter {
    
    private func calmingToastForHandlePictures() {
        view?.showToast(L10n.Questionnaire.Toast.LoadImage.Calm.Message.text)
    }
    
    private func handlePictures(imageAnswers: [Answer<[(image: ImageSource, id: String)]>],
                                currentlyDisplayedImageNamesForQuestions: [String: [String]],
                                poiId: String,
                                sheetId: String,
                                completionHandler: (() -> Void)?) {
        guard !imageAnswers.isEmpty else {
            completionHandler?()
            return
        }
        
        /// we timed the running time of the method to show calming toast message
        let start = DispatchTime.now()
        var calmingMessageWasShown = false
        let timer: Timer = Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true) { [weak self] timer in
            let seconds = Double(DispatchTime.now().uptimeNanoseconds - start.uptimeNanoseconds) / 1_000_000_000
            if seconds > 10 && !calmingMessageWasShown {
                calmingMessageWasShown = true
                timer.invalidate()
                self?.calmingToastForHandlePictures()
            }
        }
        
        pictureManager.handlePictures(imageAnswers: imageAnswers,
                                      currentlyDisplayedImageNamesForQuestions: currentlyDisplayedImageNamesForQuestions,
                                      poiId: poiId,
                                      sheetId: sheetId) { [weak self] processResults in
            timer.invalidate()
            self?.checkImageUploadResults(with: processResults) {
                completionHandler?()
            }
        }
    }
    
    private func checkImageUploadResults(with results: [Result<String, Error>],
                                         completionHandler: (() -> Void)?) {
        guard let view = view else {
            completionHandler?()
            return
        }
        
        let uploadImageFailures = results.filter({ if case .failure = $0 { return true } else { return false } })
        let uploadImageSuccesses = results.filter({ if case .success = $0 { return true } else { return false } })
        
        if uploadImageFailures.isEmpty && uploadImageSuccesses.isEmpty {
            completionHandler?()
            return
        }
        
        if uploadImageFailures.isEmpty && !uploadImageSuccesses.isEmpty {
            /// uploading images success
            view.showAlert(title: L10n.Questionnaire.Alert.LoadImage.Success.Title.text,
                           message: L10n.Questionnaire.Alert.LoadImage.Success.Message.text, actions: [
                            UIAlertAction.init(title: L10n.Common.Ok.text, style: .default, handler: { (action) in
                                completionHandler?()
                            })
                           ])
        } else if !uploadImageFailures.isEmpty && uploadImageSuccesses.isEmpty {
            /// all image was uploaded with error
            view.showAlert(title: L10n.Questionnaire.Alert.LoadImage.Error.Other.Title.text,
                           message: L10n.Questionnaire.Alert.LoadImage.Error.Other.Message.text, actions: [
                            UIAlertAction.init(title: L10n.Common.Ok.text, style: .default, handler: { (action) in
                                completionHandler?()
                            })
                           ])
        } else {
            /// Patially images uploaded
            view.showAlert(title: L10n.Questionnaire.Alert.LoadImage.Error.Partially.Title.text,
                           message: String(format: "%@ %@%d %@%d",
                                           L10n.Questionnaire.Alert.LoadImage.Error.Partially.Message.text,
                                           L10n.Questionnaire.Alert.LoadImage.Error.Partially.Message.NumberSuccessfullyUploaded.text,
                                           uploadImageSuccesses.count,
                                           L10n.Questionnaire.Alert.LoadImage.Error.Partially.Message.NumberUnsuccessfullyUploaded.text,
                                           uploadImageFailures.count),
                           actions: [
                            UIAlertAction.init(title: L10n.Common.Ok.text, style: .default, handler: { (action) in
                                completionHandler?()
                            })
                           ])
        }
    }
}
