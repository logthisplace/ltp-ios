//
//  UpdateAlert.swift
//  Log This Place
//
//

import UIKit

protocol MandatoryUpdateAlertViewInput: View {
    func fillAlert(alertViewModel: MandatoryUpdateAlertView.ViewModel)
}

final class MandatoryUpdateAlertViewController: BaseViewController {
    
    override var contentView: BaseView {
        alertView
    }
        
    private let presenter: MandatoryUpdateAlertPresenterProtocol
    private lazy var alertView = MandatoryUpdateAlertView()
    
    init(presenter: MandatoryUpdateAlertPresenterProtocol) {
        self.presenter = presenter
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("use init(presenter:)")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .clear
        view.isOpaque = false
        
        alertView.openAppstoreButton.addTarget(self, action: #selector(openAppstoreTapped), for: .touchUpInside)
        
        presenter.viewDidLoad()
    }
    
    @objc func openAppstoreTapped() {
        presenter.openAppstoreTapped()
    }
}

extension MandatoryUpdateAlertViewController: MandatoryUpdateAlertViewInput {
    func fillAlert(alertViewModel: MandatoryUpdateAlertView.ViewModel) {
        alertView.fill(with: alertViewModel)
    }
}
