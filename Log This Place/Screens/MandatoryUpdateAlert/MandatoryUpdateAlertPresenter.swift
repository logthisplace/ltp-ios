//
//  UpdateAlertPresenter.swift
//  Log This Place
//
//

import UIKit

protocol MandatoryUpdateAlertPresenterProtocol: Presenter {
    func openAppstoreTapped()
}


final class MandatoryUpdateAlertPresenter: BasePresenter, MandatoryUpdateAlertPresenterProtocol {
    
    // TODO: Move URL to config
    private struct Constants {
        static let appstoreUrl = "itms-apps://apple.com/app/log-this-place/id1585118918"
    }
    
    private let alertViewModel: MandatoryUpdateAlertView.ViewModel
    weak var view: MandatoryUpdateAlertViewInput?
    
    private let firebaseAnalyticManager: FirebaseAnalyticManagerProtocol = AppContainer.resolve()

    init(alertViewModel: MandatoryUpdateAlertView.ViewModel) {
        self.alertViewModel = alertViewModel
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        firebaseAnalyticManager.logEvent(screen: .mandatoryUpdate)
        view?.fillAlert(alertViewModel: alertViewModel)
    }
    
    func openAppstoreTapped() {
        if let url = URL(string: Constants.appstoreUrl) {
            if UIApplication.shared.canOpenURL(url) {
                firebaseAnalyticManager.logEvent(action: .openAppstore)
                UIApplication.shared.open(url)
            } else {
                view?.showErrorAlert(message: L10n.Common.Error.OpenUrl.text)
            }
        } else {
            view?.showErrorAlert(message: L10n.Common.Error.OpenUrl.text)
        }
    }
}

