//
//  UpdateAlertView.swift
//  Log This Place
//
//

import UIKit


final class MandatoryUpdateAlertView: BaseView, Fillable {
    struct ViewModel {
        let title: String
        let description: String
    }
    
    override var respectBottomSafeArea: Bool {
        return false
    }
    override var respectTopSafeArea: Bool {
        return false
    }
    
    private let rootStackView = with(UIStackView.vertical) {
        $0.edgeInsets(topSpacing: 20, bottomSpacing: 20, leadingSpacing: 20, trailingSpacing: 20)
        $0.spacing = 10
        $0.background(color: .white, radius: 8)
    }
    private let blurEffectView = UIVisualEffectView(effect: UIBlurEffect(style: UIBlurEffect.Style.regular))
    private let logoImageContainerView = UIView()
    private let logoImage = with(UIImageView()) {
        $0.image = Asset.logo.image
    }
    private let titleLabel = with(UILabel.titleH2SemiBold) {
        $0.text = L10n.Alert.UpdateApp.Title.text
        $0.numberOfLines = 0
        $0.textAlignment = .center
    }
    private let descriptionLabel = with(UILabel.calloutRegular) {
        $0.text = L10n.Alert.UpdateApp.Body.text
        $0.numberOfLines = 0
        $0.textAlignment = .center
    }
    let openAppstoreButton = with(LTPButton.standard(style: .gray)) {
        $0.setTitle(L10n.Alert.UpdateApp.Button.text, for: .normal)
    }
 
    override func setupUI() {
        super.setupUI()
        
        addSubview(blurEffectView)
        addSubview(rootStackView)

        logoImageContainerView.addSubview(logoImage)
        rootStackView.addArrangedSubviews([
            logoImageContainerView,
            titleLabel,
            descriptionLabel,
            openAppstoreButton
        ])
    }
    
    override func setupConstraints() {
        super.setupConstraints()
        
        logoImageContainerView.snp.makeConstraints { make in
            make.height.equalTo(50)
        }
        logoImage.snp.makeConstraints { make in
            make.center.equalToSuperview()
            make.size.equalTo(45)
        }
        rootStackView.snp.makeConstraints { make in
            make.center.equalToSuperview()
            make.trailing.leading.equalToSuperview().inset(20)
        }
    }
    
    override func setupVisuals() {
        super.setupVisuals()
        
        blurEffectView.frame = bounds
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
    }
}

extension MandatoryUpdateAlertView {
    func fill(with model: ViewModel) {
        titleLabel.text = model.title
        descriptionLabel.text = model.description
    }
}
