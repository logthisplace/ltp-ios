//
//  AutocompleteSearchViewController.swift
//  Log This Place
//
//

import Foundation
import UIKit

protocol AutocompleteSearchViewControllerViewInput: View {
    func fillFoundPlaces(_ foundPlaces: [AutocompleteSearchView.ViewModel.Poi])
}

final class AutocompleteSearchViewController: BaseViewController {
    
    override var contentView: BaseView {
        autocompleteSearchView
    }
    
    private let presenter: AutocompleteSearchPresenterProtocol
    private lazy var autocompleteSearchView = AutocompleteSearchView(viewInput: self)
    
    init(presenter: AutocompleteSearchPresenterProtocol) {
        self.presenter = presenter
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
}

extension AutocompleteSearchViewController: AutocompleteSearchViewProtocol {
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        if let poiName = searchBar.text?.nilIfEmpty {
            presenter.searchButtonTapped(poiName)
        }
    }

    func placeTapped(_ place: AutocompleteSearchView.ViewModel.Poi) {
        presenter.placeTapped(place)
    }
}

extension AutocompleteSearchViewController: AutocompleteSearchViewControllerViewInput {
    
    func fillFoundPlaces(_ foundPlaces: [AutocompleteSearchView.ViewModel.Poi]) {
        autocompleteSearchView.fill(with: .init(pois: foundPlaces))
    }
}
