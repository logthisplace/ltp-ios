//
//  AutocompleteSearchPresenter.swift
//  Log This Place
//
//

import Foundation
import GooglePlaces
import GoogleMaps

protocol AutocompleteSearchPresenterProtocol {
    func searchButtonTapped(_ poiName: String)
    func placeTapped(_ place: AutocompleteSearchView.ViewModel.Poi)
}


final class AutocompleteSearchPresenter: BasePresenter, AutocompleteSearchPresenterProtocol {
    private let poiDataManager: PointOfInterestDataManagerProtocol = AppContainer.resolve()
    private let cityLocationManager: CityLocationManagerProtocol = AppContainer.resolve()
    private let placesClient: GooglePlacesProtocol = AppContainer.resolve()
    private let firebaseAnalyticManager: FirebaseAnalyticManagerProtocol = AppContainer.resolve()
    private let cityManager: CityLocationManagerProtocol = AppContainer.resolve()

    private var namePoiFilter: String = ""
    private let sessionToken = GMSAutocompleteSessionToken.init()
    private var foundPlaces = [AutocompleteSearchView.ViewModel.Poi]()
    private var coordinator: MapCoordinatorProtocol
    
    weak var view: AutocompleteSearchViewControllerViewInput?
    private var informable: MapViewPresenterInformable?
    
    init(coordinator: MapCoordinatorProtocol,
         informable: MapViewPresenterInformable?) {
        self.coordinator = coordinator
        self.informable = informable
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        firebaseAnalyticManager.logEvent(screen: .search)
    }
    
    private func fetchData() {
        foundPlaces.removeAll()
        view?.showLoading(fullscreen: true)
        
        let dispatchGroup = DispatchGroup()
        
        dispatchGroup.enter()
        fetchGooglePlaceByQuery { [weak self] results, error in
            if error != nil {
                Logger.shared.error(String(describing: error))
                dispatchGroup.leave()
            }
            Logger.shared.info("Found places \(results?.count ?? 0)")
            
            self?.handleAutocompleteResults(results) { places in
                Logger.shared.info("Found places after filtering by zip \(places.count)")
                self?.foundPlaces.append(contentsOf: places)
                dispatchGroup.leave()
            }
        }
        
        dispatchGroup.notify(queue: .main) { [weak self] in
            guard let self = self else { return }
            self.view?.hideLoading()
            self.view?.fillFoundPlaces(self.foundPlaces)
        }
    }
    
    func searchButtonTapped(_ poiName: String) {
        namePoiFilter = poiName
        fetchData()
    }
    
    /// The method checks the place that was clicked, whether it is in a certain list of zip codes, if it is, it shows on the map
    /// - Parameter place: item from `foundPlaces` list
    func placeTapped(_ place: AutocompleteSearchView.ViewModel.Poi) {
        self.coordinator.dismissSearchPois()
        self.informable?.didSelectedFoundPoi(placeID: place.placeId,
                                             location: place.placeCoordinate)
    }
}

extension AutocompleteSearchPresenter {
    
    /// Searches for a place by name, limiting the search for a place by coordinates(city area)
    private func fetchGooglePlaceByQuery(completionHandler: @escaping GMSAutocompletePredictionsCallback) {
        let placesFilter: GMSAutocompleteFilter = GMSAutocompleteFilter()
        placesFilter.country = "SE"
        placesFilter.locationRestriction = GMSPlaceRectangularLocationOption(cityLocationManager.cityBounds.northEast,
                                                                             cityLocationManager.cityBounds.southWest)
        
        placesClient.findAutocompletePredictions(fromQuery: "\(namePoiFilter)",
                                                 filter: placesFilter,
                                                 sessionToken: sessionToken, callback: completionHandler)
    }
    
    private func handleAutocompleteResults(_ results: [GMSAutocompletePrediction]?,
                                           completionHandler: @escaping ([AutocompleteSearchView.ViewModel.Poi]) -> Void) {
        var places = [AutocompleteSearchView.ViewModel.Poi]()
        
        let dispatchGroup = DispatchGroup()
        for autocompletePlace in results ?? [] {
            dispatchGroup.enter()
            placesClient.fetchPlace(fromPlaceID: autocompletePlace.placeID,
                                    placeFields: [.coordinate, .addressComponents],
                                    sessionToken: sessionToken) { [weak self] place, error in
                
                if let error = error {
                    dispatchGroup.leave()
                    Logger.shared.error(String(describing: error))
                }
                
                guard let place = place,
                      let addressComponents = place.addressComponents,
                      let postalCodeComponent = addressComponents.first(where: { $0.types.contains(where: { $0 == "postal_code" })}),
                      let postalCode = Int(postalCodeComponent.name.replacingOccurrences(of: " ", with: "")) else {
                          Logger.shared.info("Skipped place: \(String(describing: place))")
                          dispatchGroup.leave()
                          return
                      }
                
               self?.cityLocationManager.isLocationInCity(postalCode: postalCode) { (result) in
                    if case let .success(val) = result, val {
                        if places.filter({ $0.placeId == autocompletePlace.placeID }).first == nil {
                            places.append(.init(placeId: autocompletePlace.placeID,
                                                placeAttributedPrimaryText: autocompletePlace.attributedFullText.string,
                                                placeCoordinate: place.coordinate))
                        }
                    } else {
                        Logger.shared.info("Skipped place by postal code: \(postalCode)")
                    }
                }
                
                dispatchGroup.leave()
            }
        }
        
        dispatchGroup.notify(queue: .main) {
            completionHandler(places)
        }
    }
}
