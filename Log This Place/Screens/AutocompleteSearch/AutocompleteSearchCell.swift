//
//  AutocompleteSearchCell.swift
//  Log This Place
//
//

import UIKit


final class AutocompleteSearchCell: UITableViewCell {
    
    let poiInfoLabel = with(UILabel.footnoteRegular) {
        $0.numberOfLines = 0
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        addSubview(poiInfoLabel)
        
        poiInfoLabel.snp.makeConstraints{ make in
            make.margins.equalToSuperview().inset(10)
            make.centerX.equalToSuperview()
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func fill(data: AutocompleteSearchView.ViewModel.Poi) {
        poiInfoLabel.text = data.placeAttributedPrimaryText
    }
    
}
