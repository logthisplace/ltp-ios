//
//  AutocompleteSearchView.swift
//  Log This Place
//
//

import UIKit
import GoogleMaps

protocol AutocompleteSearchViewProtocol {
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar)
    func placeTapped(_ place: AutocompleteSearchView.ViewModel.Poi)
}

final class AutocompleteSearchView: BaseView {
    
    struct ViewModel {
        let pois: [Poi]
        
        struct Poi {
            let placeId: String
            let placeAttributedPrimaryText: String
            let placeCoordinate: CLLocationCoordinate2D
        }
    }
    
    private let tableView = UITableView()
    private let searchBarView = SearchBarView()
    private let rootStackView = UIStackView.vertical
    private let noResultsLabel = with(UILabel.calloutRegular) {
        $0.text = L10n.AutocompleteSearch.ResultsNotFound.text
        $0.isHidden = true
        $0.numberOfLines = 0
        $0.textAlignment = .center
    }

    private var data = [ViewModel.Poi]()
    
    private let viewInput: AutocompleteSearchViewProtocol
    
    init(viewInput: AutocompleteSearchViewProtocol) {
        self.viewInput = viewInput
        super.init()
        self.searchBarView.searchBar.delegate = self
    }
    
    override func setupUI() {
        super.setupUI()
        addSubview(rootStackView)
        addSubview(noResultsLabel)

        rootStackView.addArrangedSubviews([
            searchBarView,
            tableView
        ])
        
        setupTableView()
        
        searchBarView.searchBar.becomeFirstResponder()
    }
    
    override func setupConstraints() {
        super.setupConstraints()
        rootStackView.positionUsingScreenMargins(topSpacing: 10)
        
        noResultsLabel.snp.makeConstraints { make in
            make.center.equalToSuperview()
            make.leading.trailing.equalToSuperview().inset(20)
            make.top.bottom.equalToSuperview()
        }
    }
    
    override func setupVisuals() {
        super.setupVisuals()
        rootStackView.spacing = 5
        tableView.register(item: AutocompleteSearchCell.self)
    }
    
    private func setupTableView() {
        tableView.dataSource = self
        tableView.delegate = self
        tableView.tableFooterView = getFooterView()
    }
    
    private func getFooterView() -> UIView {
        let footerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 50))
        let googleIcon = with(UIImageView()) {
            $0.image = Asset.poweredByGoogle.image
        }
        footerView.addSubview(googleIcon)
        googleIcon.snp.makeConstraints { make in
            make.width.equalTo(150)
            make.height.equalTo(20)
            make.center.equalToSuperview()
        }
        return footerView
    }
}

extension AutocompleteSearchView {
    
    func fill(with model: ViewModel) {
        data.removeAll()
        data.append(contentsOf: model.pois)
        tableView.reloadData()
        if data.isEmpty {
            noResultsLabel.isHidden = false
            tableView.alpha = 0
        } else {
            noResultsLabel.isHidden = true
            tableView.alpha = 1
        }
    }
}

extension AutocompleteSearchView: UISearchBarDelegate {
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        viewInput.searchBarSearchButtonClicked(searchBar)
    }
}

extension AutocompleteSearchView: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: AutocompleteSearchCell = tableView.deque(for: indexPath)
        cell.fill(data: data[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let place = data[indexPath.row]
        viewInput.placeTapped(place)
    }
}
