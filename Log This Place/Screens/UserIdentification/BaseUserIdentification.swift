//
//  Log This Place
//
//

import Foundation
import UIKit

protocol BaseUserIdentificationViewInput: View {
    func clearInputFields()
}

class BaseUserIdentificationPresenter: BasePresenter {
    
    private struct Constants {
        static let emailRegEx: String = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        static let minPasswordLength: Int = 6
    }
    
    func isValidEmail(_ email: String) -> Bool {
        let emailPred = NSPredicate(format:"SELF MATCHES %@", Constants.emailRegEx)
        return emailPred.evaluate(with: email)
    }
    
    func isValidPassword(_ password: String) -> Bool {
        return password.count >= Constants.minPasswordLength
    }
}
