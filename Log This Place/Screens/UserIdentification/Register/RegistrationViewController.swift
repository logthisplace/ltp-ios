//
//  RegistrationViewController.swift
//  Log This Place
//
//

import UIKit

protocol RegistrationViewInput: BaseUserIdentificationViewInput {
    func fillTitle(with title: String)
}

final class RegistrationViewController: BaseViewController, StoryboardedInitializable {
    
    @IBOutlet weak var emailField: LTPTextField!
    @IBOutlet weak var passwordField: LTPTextField!
    @IBOutlet weak var repeatedPasswordField: LTPTextField!
    @IBOutlet weak var registrationButton: LTPButton!
    @IBOutlet weak var titleLabel: LTPUILabel!
    @IBOutlet weak var scrollView: UIScrollView!
    
    var presenter: RegistrationPresenterProtocol?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter?.viewDidLoad()
        keyboadSettings()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    override func setupUI() {
        super.setupUI()
        UITextField.connectFields(fields: [emailField, passwordField, repeatedPasswordField])
        title = L10n.UserIdentification.CreateAccount.text
        emailField.placeholder = L10n.UserIdentification.Email.placeholder
        passwordField.placeholder = L10n.UserIdentification.Password.placeholder
        repeatedPasswordField.placeholder = L10n.UserIdentification.RepeatedPassword.placeholder
        registrationButton.setTitle(L10n.UserIdentification.Registration.text, for: .normal)
    }
    
    // MARK: - Actions
    @IBAction func registrationTapped(_ sender: Any) {
        presenter?.signUp(email: emailField.text.emptyIfNil,
                         password: passwordField.text.emptyIfNil,
                         repeatedPassword: repeatedPasswordField.text.emptyIfNil)
    }
    
    // MARK: - BAD CODE, NEED REFACTOR
    private func keyboadSettings() {
        NotificationCenter.default.addObserver(self, selector: #selector(RegistrationViewController.keyboardWillShowOnThisScreen), name:UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(RegistrationViewController.keyboardWillHideOnThisScreen), name:UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc private func keyboardWillShowOnThisScreen(notification: Notification) {
        
        guard let userInfo = notification.userInfo else { return }
        var keyboardFrame:CGRect = (userInfo[UIResponder.keyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
        keyboardFrame = self.view.convert(keyboardFrame, from: nil)
        
        var contentInset:UIEdgeInsets = scrollView.contentInset
        contentInset.bottom = keyboardFrame.size.height + 20
        scrollView.contentInset = contentInset
    }
    
    @objc private func keyboardWillHideOnThisScreen(notification: NSNotification) {
        let contentInset:UIEdgeInsets = UIEdgeInsets.zero
        scrollView.contentInset = contentInset
    }
}

extension RegistrationViewController: RegistrationViewInput {
    
    func clearInputFields() {
        emailField.text = ""
        passwordField.text = ""
        repeatedPasswordField.text = ""
    }
    
    func fillTitle(with title: String) {
        titleLabel.text = title
    }
}
