//
//  RegistrationPresenter.swift
//  Log This Place
//
//

import Foundation
import UIKit

enum RegistrationType {
    case general /// general registration, user will see this type when click register from `welcome` screen
    case permanent /// registration after the user has clicked `continue as guest`
}

protocol RegistrationPresenterProtocol: Presenter {
    func signUp(email: String, password: String, repeatedPassword: String)
}

final class RegistrationPresenter: BaseUserIdentificationPresenter, RegistrationPresenterProtocol {
    
    private let userIdentificationDataManager: UserIdentificationDataManagerProtocol = AppContainer.resolve()
    private let userDefaultsManager: UserDefaultsManagerProtocol = AppContainer.resolve()
    private let firebaseAnalyticManager: FirebaseAnalyticManagerProtocol = AppContainer.resolve()
    
    private let coordinator: UserIdentificationCoordinatorProtocol
    private let registrationType: RegistrationType
    
    weak var view: RegistrationViewInput?
    
    init(coordinator: UserIdentificationCoordinatorProtocol,
         registrationType: RegistrationType) {
        self.coordinator = coordinator
        self.registrationType = registrationType
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        firebaseAnalyticManager.logEvent(screen: .register)
        fillTitle()
    }
    
    private func fillTitle() {
        switch registrationType {
        case .general:
            view?.fillTitle(with: L10n.UserIdentification.CreateAccount.text)
        case .permanent:
            view?.fillTitle(with: L10n.UserIdentification.MakePermanent.text)
        }
    }
    
    func signUp(email: String,
                password: String,
                repeatedPassword: String) {
        if !isValidEmail(email) {
            view?.showErrorAlert(message: L10n.UserIdentification.EmailNotValid.text)
            return
        }
        
        if !isValidPassword(password) {
            view?.showErrorAlert(message:  L10n.UserIdentification.PasswordNotValid.text)
            return
        }
        
        if password != repeatedPassword {
            view?.showErrorAlert(message: L10n.UserIdentification.PasswordMismatch.text)
            return
        }
        
        self.view?.showLoading(fullscreen: true)
        userIdentificationDataManager.registration(email: email,
                                                   password: password) { [weak self] result in
            guard let self = self else { return }
            self.view?.clearInputFields()
            self.view?.hideLoading()
            
            switch result {
            case let .failure(error):
                self.view?.showErrorAlert(message: error.localizedDescription)
                break
            case .success:
                switch self.registrationType {
                case .general:
                    self.userDefaultsManager.wasWelcomeShown = true
                    self.view?.showAlert(title: L10n.UserIdentification.AccountCreated.Title.text,
                                         message: L10n.UserIdentification.AccountCreated.Body.text,
                                         actions: [
                                            UIAlertAction.init(title: L10n.Common.Ok.text, style: .default, handler: { [weak self] (action) in
                                                self?.coordinator.showUserPassport()
                                            })
                                         ])
                case .permanent:
                    self.coordinator.completePermanentAccount()
                }
            }
        }
    }
}
