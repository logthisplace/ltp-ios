//
//  WelcomeView.swift
//  Log This Place
//
//

import UIKit


final class WelcomeView: BaseView {

    let headerView = UIView()
    let closeButton = with(LTPButton.standard(style: .clear)) {
        $0.setTitle(L10n.Common.Close.text, for: .normal)
        $0.setTitleColor(UIColor.LogThisPlace.gray, for: .normal)
    }
    
    private let rootContainerContentView = UIView()
    let contentView = WelcomeContentView()
    
    override func setupUI() {
        super.setupUI()
        
        headerView.addSubview(closeButton)
        rootContainerContentView.addSubview(contentView)
        
        addSubviews([
            headerView,
            rootContainerContentView
        ])
    }
    
    override func setupConstraints() {
        super.setupConstraints()
        
        closeButton.snp.makeConstraints { make in
            make.top.bottom.trailing.equalToSuperview()
            make.centerY.equalToSuperview()
        }
        
        headerView.snp.makeConstraints { make in
            make.top.equalToSuperview().inset(UIView.topSpacingUnderNavigationBar)
            make.leading.trailing.equalToSuperview().inset(UIView.horizontalScreenMargin)
            make.height.equalTo(50)
        }
        
        rootContainerContentView.snp.makeConstraints { make in
            make.top.equalTo(headerView.snp.bottom)
            make.leading.trailing.bottom.equalToSuperview()
        }
        
        contentView.positionToTheEdges()
    }
}

final class WelcomeContentView: BaseScrollView {
    
    private let rootStackView = with(UIStackView.vertical) {
        $0.spacing = 12
        $0.edgeInsets(topSpacing: 12, bottomSpacing: 0, leadingSpacing: 20, trailingSpacing: 20)
    }
    
    private let logoImageContainerView = UIView()
    private let logoImage = with(UIImageView()) {
        $0.image = Asset.logo.image
    }
    private let titleLabel = with(UILabel.titleH2SemiBold) {
        $0.text = L10n.Welcome.Title.text
        $0.textAlignment = .center
    }
    private let infoBlockStackView = with(UIStackView.vertical) {
        $0.spacing = 20
    }
    private let actionsStackView = with(UIStackView.vertical) {
        $0.spacing = 12
        $0.edgeInsets(topSpacing: 20, bottomSpacing: 20, leadingSpacing: 12, trailingSpacing: 12)
        $0.layer.borderWidth = 1
        $0.layer.borderColor = UIColor.LogThisPlace.gray.cgColor
        $0.layer.cornerRadius = 10
    }
    private let titleActionsLabel = with(UILabel.headlineMediumSemiBold) {
        $0.text = L10n.Welcome.AreYouReady.text
        $0.textAlignment = .center
    }
    let createProfileButton = with(LTPButton.standard(style: .black)) {
        $0.setTitle(L10n.Welcome.CreateProfile.text, for: .normal)
    }
    let continueAsGuestButton = with(LTPButton.standard(style: .gray)) {
        $0.setTitle(L10n.Welcome.ContinueAsGuest.text, for: .normal)
    }
    let loginButton = with(LTPButton.standard(style: .clear)) {
        $0.setTitle(L10n.Welcome.AlreadyHaveAccount.text, for: .normal)
    }
    let deleteAccountButton = with(LTPButton.standard(style: .gray)) {
        $0.setTitle(L10n.Profile.Button.Delete.Contribution.text, for: .normal)
    }
    
        
    override func setupUI() {
        super.setupUI()
        scrollableContentView.addSubview(rootStackView)
        logoImageContainerView.addSubview(logoImage)

        let firstInformationBlock = WelcomeInformItem()
        firstInformationBlock.fill(with: WelcomeInformItem.ViewModel(title: L10n.Welcome.FirstTitleItem.text,
                                                                     subTitle: L10n.Welcome.FirstDescriptionItem.text,
                                                                     label: "1"))
        infoBlockStackView.addArrangedSubview(firstInformationBlock)
        let secondInformationBlock = WelcomeInformItem()
        secondInformationBlock.fill(with: WelcomeInformItem.ViewModel(title: L10n.Welcome.SecondTitleItem.text,
                                                                      subTitle: L10n.Welcome.SecondDescriptionItem.text,
                                                                      label: "2"))
        infoBlockStackView.addArrangedSubview(secondInformationBlock)
        let thirdInformationBlock = WelcomeInformItem()
        thirdInformationBlock.fill(with: WelcomeInformItem.ViewModel(title: L10n.Welcome.ThirdTitleItem.text,
                                                                     subTitle: L10n.Welcome.ThirdDescriptionItem.text,
                                                                     label: "3"))
        infoBlockStackView.addArrangedSubview(thirdInformationBlock)
        let fourInformationBlock = WelcomeInformItem()
        fourInformationBlock.fill(with: WelcomeInformItem.ViewModel(title: L10n.Welcome.FourTitleItem.text,
                                                                    subTitle: L10n.Welcome.FourDescriptionItem.text,
                                                                    label: "4"))
        infoBlockStackView.addArrangedSubview(fourInformationBlock)
        
        actionsStackView.addArrangedSubviews([
            titleActionsLabel,
            createProfileButton,
            continueAsGuestButton
        ])
        
        rootStackView.addArrangedSubviews([
            logoImageContainerView,
            titleLabel,
            infoBlockStackView,
            actionsStackView,
            loginButton,
            deleteAccountButton
        ])
    }
    
    override func setupConstraints() {
        super.setupConstraints()
        rootStackView.positionToTheEdges()
        
        logoImageContainerView.snp.makeConstraints { make in
            make.height.equalTo(70)
        }
        
        logoImage.snp.makeConstraints { make in
            make.center.equalToSuperview()
            make.size.equalTo(70)
        }
    }
}
