//
//  Log This Place
//
//

import UIKit
import SnapKit


final class WelcomeInformItem: UIStackView, Fillable {
    
    struct ViewModel {
        let title: String
        let subTitle: String
        let label: String
    }
    
    private var itemImage = with(UIImageView()) {
        $0.image = #imageLiteral(resourceName: "PinkCircle")
    }
    private var itemLabel = with(UILabel.standartMediumRegular) {
        $0.numberOfLines = 0
        $0.textColor = UIColor.LogThisPlace.white
    }
    
    private var itemImageContainerView = UIView()
    private var textItemStackView = WelcomeInformTextItem()
    
    init() {
        super.init(frame: .zero)
        
        setupStackView()
        setupViewHierarchy()
        setupConstraints()
    }
    
    required init(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupStackView() {
        axis = .horizontal
        spacing = 10
        alignment = .fill
        distribution = .fill
    }
    
    private func setupConstraints() {
        itemImageContainerView.snp.makeConstraints { make in
            make.width.equalTo(24)
        }
        
        itemImage.snp.makeConstraints { make in
            make.size.equalTo(24)
            make.center.equalTo(itemImageContainerView)
        }
        
        itemLabel.snp.makeConstraints { make in
            make.center.equalTo(itemImage)
        }
    }
    
    func setupViewHierarchy() {
        addArrangedSubview(itemImageContainerView)
        addArrangedSubview(textItemStackView)
        itemImageContainerView.addSubview(itemImage)
        itemImage.addSubview(itemLabel)
    }
}

extension WelcomeInformItem {
    func fill(with model: ViewModel) {
        textItemStackView.fill(with: .init(title: model.title, subTitle: model.subTitle))
        itemLabel.text = model.label
    }
}


private final class WelcomeInformTextItem: UIStackView, Fillable {
    
    struct ViewModel {
        let title: String
        let subTitle: String
    }
    
    private var itemTitleLbl = with(UILabel.headlineMediumSemiBold) {
        $0.numberOfLines = 0
    }
    
    private var itemSubTitleLbl = with(UILabel.calloutRegular) {
        $0.numberOfLines = 0
        $0.textColor = UIColor.LogThisPlace.gray
    }
    
    init() {
        super.init(frame: .zero)

        setupStackView()
        
        addArrangedSubview(itemTitleLbl)
        addArrangedSubview(itemSubTitleLbl)
    }
    
    required init(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupStackView() {
        axis = .vertical
        spacing = 2
        alignment = .fill
        distribution = .fill
    }
}

extension WelcomeInformTextItem {
    func fill(with model: ViewModel) {
        itemTitleLbl.text = model.title
        itemSubTitleLbl.text = model.subTitle
    }
}
