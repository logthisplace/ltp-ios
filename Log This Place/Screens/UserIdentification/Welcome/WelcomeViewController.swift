//
//  AuthorizationViewController.swift
//  Log This Place
//
//

import UIKit

protocol WelcomeViewInput: View {}

final class WelcomeViewController: BaseViewController, WelcomeViewInput {
    
    private lazy var welcomeView = WelcomeView()
    override var contentView: WelcomeView {
        welcomeView
    }
    
    private let presenter: WelcomePresenterProtocol
    
    init(presenter: WelcomePresenterProtocol) {
        self.presenter = presenter
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.viewDidLoad()
        
        welcomeView.closeButton.addTarget(self, action: #selector(closeTapped), for: .touchUpInside)
        welcomeView.contentView.createProfileButton.addTarget(self, action: #selector(createProfileTapped), for: .touchUpInside)
        welcomeView.contentView.continueAsGuestButton.addTarget(self, action: #selector(continueAsGuestTapped), for: .touchUpInside)
        welcomeView.contentView.loginButton.addTarget(self, action: #selector(loginTapped), for: .touchUpInside)
        welcomeView.contentView.deleteAccountButton.addTarget(self, action: #selector(deleteAccountTapped), for: .touchUpInside)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: true)
    }

    @objc func deleteAccountTapped() {
        presenter.deleteAccountTapped()
    }
    
    @objc func createProfileTapped(_ sender: Any) {
        presenter.createProfileTapped()
    }

    @objc func closeTapped(_ sender: Any) {
        presenter.closeTapped()
    }

    @objc func loginTapped(_ sender: Any) {
        presenter.loginTapped()
    }

    @objc func continueAsGuestTapped(_ sender: Any) {
        presenter.continueAsGuestTapped()
    }
}
