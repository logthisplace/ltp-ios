//
//  WelcomePresenter.swift
//  Log This Place
//
//

import Foundation


protocol WelcomePresenterProtocol: Presenter {
    func createProfileTapped()
    func continueAsGuestTapped()
    func closeTapped()
    func loginTapped()
    func deleteAccountTapped()
}

final class WelcomePresenter: BasePresenter, WelcomePresenterProtocol, DeleteAccountManagerDelegate {
    
    weak var view: WelcomeViewInput?
    
    private let coordinator: UserIdentificationCoordinatorProtocol
    private let firebaseAnalyticManager: FirebaseAnalyticManagerProtocol = AppContainer.resolve()
    
    private var deleteAccountManager: DeleteAccountManagerProtocol?

    init(coordinator: UserIdentificationCoordinatorProtocol) {
        self.coordinator = coordinator
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        deleteAccountManager = DeleteAccountManager(view: view)
        deleteAccountManager?.delegate = self
        
        firebaseAnalyticManager.logEvent(screen: .welcome)
    }
    
    func createProfileTapped() {
        coordinator.showRegistration(registrationType: .general)
    }
    
    func continueAsGuestTapped() {
        coordinator.showUserPassport()
    }
    
    func closeTapped() {
        coordinator.closeWelcome()
    }
    
    func loginTapped() {
        coordinator.showLogin()
    }
    
    func deleteAccountTapped() {
        deleteAccountManager?.deleteAccountTapped()
    }
    
    func accountDeleted() {
        coordinator.deletedAccount()
    }
}
