//
//  Log This Place
//
//

import UIKit

protocol LoginViewInput: BaseUserIdentificationViewInput {
    
}

final class LoginViewController: BaseViewController, StoryboardedInitializable {

    @IBOutlet weak var emailField: LTPTextField!
    @IBOutlet weak var passwordField: LTPTextField!
    @IBOutlet weak var loginButton: LTPButton!
    @IBOutlet weak var createAccountButton: LTPButton!
    @IBOutlet weak var titleLabel: LTPUILabel!
    @IBOutlet weak var scrollView: UIScrollView!
    
    var presenter: LoginPresenterProtocol?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter?.viewDidLoad()
        keyboadSettings()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    override func setupUI() {
        super.setupUI()
        titleLabel.text = L10n.UserIdentification.LogIn.text
        UITextField.connectFields(fields: [emailField, passwordField])
        title = L10n.UserIdentification.LogIn.text
        emailField.placeholder = L10n.UserIdentification.Email.placeholder
        passwordField.placeholder = L10n.UserIdentification.Password.placeholder
        loginButton.setTitle(L10n.UserIdentification.LogIn.text, for: .normal)
        createAccountButton.setTitle(L10n.UserIdentification.CreateAccount.text, for: .normal)
    }
    
    // MARK: - Actions
    @IBAction func loginTapped(_ sender: Any) {
        presenter?.signIn(email: emailField.text.emptyIfNil,
                         password: passwordField.text.emptyIfNil)
    }
    
    @IBAction func createAccountTapped(_ sender: Any) {
        presenter?.createAccountTapped()
    }
    
    // MARK: - BAD CODE, NEED REFACTOR
    private func keyboadSettings() {
        NotificationCenter.default.addObserver(self, selector: #selector(LoginViewController.keyboardWillShowOnThisScreen), name:UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(LoginViewController.keyboardWillHideOnThisScreen), name:UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc private func keyboardWillShowOnThisScreen(notification: Notification) {
        
        guard let userInfo = notification.userInfo else { return }
        var keyboardFrame:CGRect = (userInfo[UIResponder.keyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
        keyboardFrame = self.view.convert(keyboardFrame, from: nil)
        
        var contentInset:UIEdgeInsets = scrollView.contentInset
        contentInset.bottom = keyboardFrame.size.height + 20
        scrollView.contentInset = contentInset
    }
    
    @objc private func keyboardWillHideOnThisScreen(notification: NSNotification) {
        let contentInset:UIEdgeInsets = UIEdgeInsets.zero
        scrollView.contentInset = contentInset
    }
}

extension LoginViewController: LoginViewInput {
    
    func clearInputFields() {
        emailField.text = ""
        passwordField.text = ""
    }
}
