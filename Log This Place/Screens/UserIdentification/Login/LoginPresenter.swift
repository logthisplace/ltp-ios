//
//  LoginPresenter.swift
//  Log This Place
//
//

import Foundation
import UIKit

protocol LoginPresenterProtocol: Presenter {
    func createAccountTapped()
    func signIn(email: String, password: String)
}

final class LoginPresenter: BaseUserIdentificationPresenter, LoginPresenterProtocol {
    
    private let userIdentificationDataManager: UserIdentificationDataManagerProtocol = AppContainer.resolve()
    private let userDefaultsManager: UserDefaultsManagerProtocol = AppContainer.resolve()
    private let userPassportDataManager: UserPassportDataManagerProtocol = AppContainer.resolve()
    private let firebaseAnalyticManager: FirebaseAnalyticManagerProtocol = AppContainer.resolve()
    
    private let coordinator: UserIdentificationCoordinatorProtocol
    
    weak var view: LoginViewInput?
    
    init(coordinator: UserIdentificationCoordinatorProtocol) {
        self.coordinator = coordinator
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        firebaseAnalyticManager.logEvent(screen: .login)
    }
    
    func signIn(email: String, password: String) {
        
        if !isValidEmail(email) {
            view?.showErrorAlert(message: L10n.UserIdentification.EmailNotValid.text)
            return
        }
        
        if !isValidPassword(password) {
            view?.showErrorAlert(message: L10n.UserIdentification.PasswordNotValid.text)
            return
        }
        
        view?.showLoading(fullscreen: false)
        userIdentificationDataManager.authorization(email: email,
                                                    password: password) { [weak self] result in
            guard let self = self else { return }
            self.view?.clearInputFields()
            self.view?.hideLoading()
            
            switch result {
            case let .failure(error):
                self.view?.showErrorAlert(message: error.localizedDescription)
                break
            case .success:
                self.userDefaultsManager.wasWelcomeShown = true
                self.getUserPassport()
                break
            }
        }
    }
    
    private func getUserPassport() {
        userPassportDataManager.getUserPassport() { [weak self] (result) in
            guard let self = self else { return }

            switch result {
            case let .success(data):
                self.coordinator.showUserPassport(userPassport: data)
                break
            case .failure:
                self.coordinator.showUserPassport()
                break
            }
        }
    }
    
    func createAccountTapped() {
        coordinator.showRegistration(registrationType: .general)
    }
    
}
