//
//  XCTestCase+Async.swift
//  Log This PlaceTests
//

import XCTest

/// See: https://medium.com/macoclock/clean-async-xctests-2d6ee62d0881
extension XCTestCase {

    // MARK: - Async versions without return value.

    /// This method helps flattening calls in unit tests by avoiding nesting code in closures. Useful when testing e.g. services.
    /// This variant accepts method without parameters having completion handler.
    /// - Parameters:
    ///   - timeout: Time limit.
    ///   - callbackMethod: Async call of which result should be tested.
    ///   - description: Description for XCTestExpectation.
    ///   - handler: Completion handler for waiter (XCWait).
    /// - Returns: Result of block passed.
    func async<A>(timeout: TimeInterval = 4.0,
                  _ callbackMethod: (@escaping (A) -> Void) -> Void,
                  description: String = "",
                  handler: XCWaitCompletionHandler? = nil) -> A {
        let expectaction = expectation(description: description)

        var result: A!
        callbackMethod {
            result = $0
            expectaction.fulfill()
        }

        waitForExpectations(timeout: timeout, handler: handler)
        return result
    }

    // swiftlint:disable identifier_name
    /// Same as `async<A>` but this variant accepts also parameter additionaly to completion handler.
    func async<A, B>(_ a: A,
                     timeout: TimeInterval = 4.0,
                     _ callbackMethod: @escaping (A, @escaping (B) -> Void) -> Void,
                     description: String = "Async Execution",
                     handler: XCWaitCompletionHandler? = nil) -> B {
        let curriedCallback = curry(f: callbackMethod)(a)
        return async(timeout: timeout,
                     curriedCallback,
                     description: description,
                     handler: handler)
    }

    /// Same as `async<A>` but this variant accepts also two parameters additionaly to completion handler.
    func async<A, B, C>(_ a: A,
                        _ b: B,
                        timeout: TimeInterval = 4.0,
                        _ callbackMethod: @escaping (A, B, @escaping (C) -> Void) -> Void,
                        description: String = "Async Execution",
                        handler: XCWaitCompletionHandler? = nil) -> C {
        let curriedCallback = curry(f: callbackMethod)(a)(b)
        return async(timeout: timeout,
                     curriedCallback,
                     description: description,
                     handler: handler)
    }

    /// Same as `async<A>` but this variant accepts also three parameters additionaly to completion handler.
    func async<A, B, C, D>(_ a: A,
                           _ b: B,
                           _ c: C,
                           timeout: TimeInterval = 4.0,
                           _ callbackMethod: @escaping (A, B, C, @escaping (D) -> Void) -> Void,
                           description: String = "Async Execution",
                           handler: XCWaitCompletionHandler? = nil) -> D {
        let curriedCallback = curry(f: callbackMethod)(a)(b)(c)
        return async(timeout: timeout,
                     curriedCallback,
                     description: description,
                     handler: handler)
    }

    // MARK: - Async versions with return value.

    /// This method helps flattening calls in unit tests by avoiding nesting code in closures. Useful when testing e.g. services.
    /// This variant accepts method without parameters having completion handler.
    /// - Parameters:
    ///   - timeout: Time limit.
    ///   - callbackMethod: Async call (returning some value synchroniously) of which result should be tested.
    ///   - description: Description for XCTestExpectation.
    ///   - handler: Completion handler for waiter (XCWait).
    /// - Returns: Result of block passed.
    func async<A, ReturnType>(timeout: TimeInterval = 4.0,
                              _ callbackMethod: (@escaping (A) -> Void) -> ReturnType,
                              description: String = "",
                              handler: XCWaitCompletionHandler? = nil) -> A {
        let expectaction = expectation(description: description)

        var result: A!
        _ = callbackMethod {
            result = $0
            expectaction.fulfill()
        }

        waitForExpectations(timeout: timeout, handler: handler)
        return result
    }

    /// Same as `async<A, ReturnType>` but this variant accepts also parameter additionaly to completion handler.
    func async<A, B, ReturnType>(_ a: A,
                                 timeout: TimeInterval = 4.0,
                                 _ callbackMethod: @escaping (A, @escaping (B) -> Void) -> ReturnType,
                                 description: String = "Async Execution",
                                 handler: XCWaitCompletionHandler? = nil) -> B {
        let curriedCallback = curry(f: callbackMethod)(a)
        return async(timeout: timeout,
                     curriedCallback,
                     description: description,
                     handler: handler)
    }

    /// Same as `async<A, ReturnType>` but this variant accepts also two parameters additionaly to completion handler.
    func async<A, B, C, ReturnType>(_ a: A,
                                    _ b: B,
                                    timeout: TimeInterval = 4.0,
                                    _ callbackMethod: @escaping (A, B, @escaping (C) -> Void) -> ReturnType,
                                    description: String = "Async Execution",
                                    handler: XCWaitCompletionHandler? = nil) -> C {
        let curriedCallback = curry(f: callbackMethod)(a)(b)
        return async(timeout: timeout,
                     curriedCallback,
                     description: description,
                     handler: handler)
    }

    /// Same as `async<A, ReturnType>` but this variant accepts also three parameters additionaly to completion handler.
    func async<A, B, C, D, ReturnType>(_ a: A,
                                       _ b: B,
                                       _ c: C,
                                       timeout: TimeInterval = 4.0,
                                       _ callbackMethod: @escaping (A, B, C, @escaping (D) -> Void) -> ReturnType,
                                       description: String = "Async Execution",
                                       handler: XCWaitCompletionHandler? = nil) -> D {
        let curriedCallback = curry(f: callbackMethod)(a)(b)(c)
        return async(timeout: timeout,
                     curriedCallback,
                     description: description,
                     handler: handler)
    }

    // MARK: - Curry versions without return value.

    private func curry<A, B>(f: @escaping (A, B) -> Void) -> (A) -> ((B) -> Void) {
        return { a in { b in f(a, b) } }
    }

    private func curry<A, B, C>(f: @escaping (A, B, C) -> Void) -> (A) -> ((B) -> ((C) -> Void)) {
        return { a in { b in { c in f(a, b, c) } } }
    }

    private func curry<A, B, C, D>(f: @escaping (A, B, C, D) -> Void) -> (A) -> ((B) -> ((C) -> ((D) -> Void))) {
        return { a in { b in { c in { d in f(a, b, c, d) } } } }
    }

    // MARK: - Curry versions with return value

    private func curry<A, B, ReturnType>(f: @escaping (A, B) -> ReturnType) -> (A) -> ((B) -> Void) {
        return { a in { b in _ = f(a, b) } }
    }

    private func curry<A, B, C, ReturnType>(f: @escaping (A, B, C) -> ReturnType) -> (A) -> ((B) -> ((C) -> Void)) {
        return { a in { b in { c in _ = f(a, b, c) } } }
    }

    private func curry<A, B, C, D, ReturnType>(f: @escaping (A, B, C, D) -> ReturnType) -> (A) -> ((B) -> ((C) -> ((D) -> Void))) {
        return { a in { b in { c in { d in _ = f(a, b, c, d) } } } }
    }

    // swiftlint:enable identifier_name
}
