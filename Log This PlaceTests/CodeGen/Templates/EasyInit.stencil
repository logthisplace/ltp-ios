import Foundation
import UIKit
@testable import Log_This_Place
{% for type in types.implementing.EasyInit %}

// MARK: - {{type.name}}

{% macro easyInitInsert var %}{% if var.type.based.EasyInit %}{{var.unwrappedTypeName}}.fixture(){% else %}{% if var.unwrappedTypeName == "String" %}"fixture{{ var.name|upperFirstLetter }}"{% else %}{% if var.typeName.isClosure %}nil{% else %}{{ var.unwrappedTypeName }}(){% endif %}{% endif %}{% endif %}{% endmacro %}

extension {{type.name}} {

{% if type.kind == "enum" %}
    static func fixture() -> {{ type.name }} {
        {% if type.cases.first.hasAssociatedValue %} return {{ type.cases.first.name }}({% for associated in type.cases.first.associatedValues %}{% if associated.localName %}{{ associated.localName }}: .fixture() {% else %}{% if associated.typeName == "String" %}"fixture{{ associated.typeName.localName|upperFirstLetter }}"{% else %}{{ associated.typeName }}(){% endif %}{% endif %} {{', ' if not forloop.last}}{% endfor %})
        {% else %} return {{ type.cases.first.name }}
        {% endif %}
    }
{% elif type.name == "URL" %}
    static func fixture() -> {{ type.name }} {
        URL(string: "http://example.com")!
    }
{% elif type.name == "UIColor" %}
    static func fixture() -> {{ type.name }} {
        UIColor.clear
    }
{% else %}
    static func fixture(
        shouldProvideDefaultValuesForOptionals: Bool = false,
{% for var in type.storedVariables %}
        {{ var.name }}: {{'(' if var.typeName.isClosure}}{{ var.unwrappedTypeName }}{{')' if var.typeName.isClosure}}? = nil{{ ',' if not forloop.last }}
{% endfor %}
    ) -> {{ type.name }} {
        {{ type.name }}(
{% for var in type.storedVariables %}
            {{ var.name }}: {{ var.name }}{% if not var.isOptional %} ?? {% call easyInitInsert var %}{% else %} ?? (shouldProvideDefaultValuesForOptionals ? {% call easyInitInsert var %} : {{ var.name }}){% endif %}{{ ',' if not forloop.last }}
{% endfor %}
        )
    }
{% endif %}
}
{% endfor %}


